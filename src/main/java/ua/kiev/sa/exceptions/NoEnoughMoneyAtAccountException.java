package ua.kiev.sa.exceptions;


public class NoEnoughMoneyAtAccountException extends Exception {


    public NoEnoughMoneyAtAccountException(String s) {
        super(s);
    }
}
