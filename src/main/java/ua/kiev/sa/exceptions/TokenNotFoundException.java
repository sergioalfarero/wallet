package ua.kiev.sa.exceptions;


public class TokenNotFoundException extends Exception {


    public TokenNotFoundException(String s) {
        super(s);
    }
}
