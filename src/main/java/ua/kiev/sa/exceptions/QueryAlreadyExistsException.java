package ua.kiev.sa.exceptions;

public class QueryAlreadyExistsException extends Throwable {
    public QueryAlreadyExistsException(String s) {
        super("The account with the name " + s + " already exists");
    }

}
