package ua.kiev.sa.exceptions;

public class AccountAlreadyExistsException extends Throwable {
    public AccountAlreadyExistsException(String s) {
        super("The account with the name " + s + " already exists");
    }

}
