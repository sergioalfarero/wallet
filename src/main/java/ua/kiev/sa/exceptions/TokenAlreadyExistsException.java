package ua.kiev.sa.exceptions;


public class TokenAlreadyExistsException extends Exception {


    public TokenAlreadyExistsException(String s) {
        super(s);
    }
}
