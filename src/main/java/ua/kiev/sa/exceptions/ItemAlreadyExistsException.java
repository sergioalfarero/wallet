package ua.kiev.sa.exceptions;

public class ItemAlreadyExistsException extends Throwable {
    public ItemAlreadyExistsException(String s) {
        super("The account with the name " + s + " already exists");
    }

}
