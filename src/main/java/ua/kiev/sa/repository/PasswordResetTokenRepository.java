package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ua.kiev.sa.model.PasswordResetToken;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {

    @Query("select  t from PasswordResetToken t where t.token=:token")
    PasswordResetToken getPasswordResetToken(@Param("token") String token);

}
