package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.UserInfo;
import ua.kiev.sa.model.VerificationToken;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select  u from User u where u.login=:login")
    User getByUserName(@Param("login") String login);

    @Query("select  u from User u where u.email=:email")
    User findByEmail(@Param("email") String email);

    @Query("select  u from User u where u.id=:id")
    User findById(@Param("id") Integer userId);

    @Query("select  ui from UserInfo ui where ui.user=:user")
    UserInfo getUserInformation(@Param("user") User user);

    @Query("select  v from VerificationToken v where v.token=:token")
    VerificationToken getVerificationToken(@Param("token") String token);

    @Query("select  v.user from VerificationToken v where v.token=:token")
    User getByToken(@Param("token") String token);

    @Query("select  u from User u where u.login=:login and u.password=:password")
    boolean isValidUser(@Param("login") String login, @Param("password") String password);

}
