package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ua.kiev.sa.model.Permission;

public interface PermissionRepository extends JpaRepository<Permission, Long> {
}
