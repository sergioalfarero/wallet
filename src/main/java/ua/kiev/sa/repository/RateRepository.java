package ua.kiev.sa.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.CurrencyRates;
import ua.kiev.sa.model.User;

import java.util.List;

@Repository("rateRepository")
public interface RateRepository extends JpaRepository<CurrencyRates, Long> {
    @Query("select r from CurrencyRates r where r.user=:user")
    CurrencyRates findRatesForUser(@Param("user") User user);

    @Query("select r from CurrencyRates r where r.user.login=:login")
    CurrencyRates findForUserLogin(@Param("login") String login);

    @Query("select r from CurrencyRates r where r.bank!=null order by r.date desc")
    List<CurrencyRates> findTop1OrderByDate(Pageable pageable);

    @Query("update CurrencyRates r set r.rates=:rates where r.user.login=:login")
    void save(@Param ("rates") CurrencyRates userRates, @Param("login") String login);
}
