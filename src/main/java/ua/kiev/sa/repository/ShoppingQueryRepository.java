package ua.kiev.sa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.ShoppingListQuery;
import ua.kiev.sa.model.User;

import java.util.List;

@Repository("shoppingQueryRepository")
public interface ShoppingQueryRepository extends JpaRepository<ShoppingListQuery, Long> {

    @Query("select count(i) from ShoppingItem i where i.shoppingListQuery.id=:id")
    Integer getCountOfItemsInList(@Param("id") Long id);

    @Query("select max(i.priority) from ShoppingItem i where i.shoppingListQuery.id=:id")
    Integer getMaxValueOfItemsInList(@Param("id") Long id);

    @Query("select q from ShoppingListQuery q where q.user=:user")
    List<ShoppingListQuery> findAllQueriesForUser(@Param("user") User u);

    @Query("select q from ShoppingListQuery q where q.user=:user and q.name=:name")
    ShoppingListQuery findQueryByName(@Param("name") String name, @Param("user") User user);

    @Query("select i from ShoppingListQuery i where i.name=:name and i.user=:user")
    ShoppingListQuery getByNameAndUser(@Param("name") String name, @Param("user") User u);
}
