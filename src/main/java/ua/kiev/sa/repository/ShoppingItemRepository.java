package ua.kiev.sa.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.ShoppingItem;
import ua.kiev.sa.model.User;

import java.util.List;

@Repository("shoppingItemRepository")
public interface ShoppingItemRepository extends JpaRepository<ShoppingItem, Long> {
    @Query("select i from ShoppingItem i where i.user=:user and i.name=:name and i.shoppingListQuery.name=:query")
    ShoppingItem findItemByName(@Param("name") String name, @Param("query") String queryListName, @Param("user") User user);

    @Query("select i from ShoppingItem i where i.user=:user")
    List<ShoppingItem> findAllForUser(@Param("user") User u);

    @Query("select i from ShoppingItem i where i.id=:id and i.user=:user")
    ShoppingItem findItemByIdAndUser(@Param("id") Long id, @Param("user") User u);

    @Query("select i from ShoppingItem i WHERE i.shoppingListQuery.id=:id and i.user=:user and i.priority<:priority ORDER BY i.priority DESC")
    List<ShoppingItem> findWithClosestHigherPriority(@Param("id") Long id, @Param("priority") Integer priority, @Param("user") User u, Pageable pageable);

    @Query("select i from ShoppingItem i WHERE i.shoppingListQuery.id=:id and i.user=:user and i.priority>:priority ORDER BY i.priority ASC")
    List<ShoppingItem> findWithClosestLowerPriority(@Param("id") Long id, @Param("priority") Integer priority, @Param("user") User u, Pageable pageable);

    @Modifying
    @Query("UPDATE ShoppingItem item SET item.priority=:higherItemPriority  WHERE item.id=:currentItemId")
    int changePriority(@Param("currentItemId") Long id, @Param("higherItemPriority") Integer higherItemPriority);
}
