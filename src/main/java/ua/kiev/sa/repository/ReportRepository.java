package ua.kiev.sa.repository;

import com.querydsl.core.Tuple;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.AccountStat;
import ua.kiev.sa.model.Operation;
import ua.kiev.sa.model.OperationCriteria;
import ua.kiev.sa.model.dto.OperationCriteriaDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


public interface ReportRepository {

     List<AccountStat> getCategoriesCostsForPeriod(OperationCriteria operationCriteria);
     List<AccountStat> getSubcategoriesCostsForPeriod(OperationCriteria operationCriteria);
     List<Operation> findAllByCriteria(OperationCriteria criteria);
}
