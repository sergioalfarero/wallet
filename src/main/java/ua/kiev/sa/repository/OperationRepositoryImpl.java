package ua.kiev.sa.repository;


import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

public abstract class OperationRepositoryImpl implements OperationRepository{
    @PersistenceContext
    private EntityManager em;


    public  List<Operation> findAllByCriteria(OperationCriteria operationCriteria){
        final JPAQuery<Operation> query = new JPAQuery(em);
        final QOperation operation = QOperation.operation;
        final QSubcategory subcategory = QSubcategory.subcategory;
        List<Operation> operations = null;
        try {
            operations = query.select(operation).from(operation).
                    where(operation.account.id.in(operationCriteria.getAccounts()).
                            and(operation.date.between(operationCriteria.getStart(), operationCriteria.getEnd())).
                            and(operation.account.id.in(operationCriteria.getAccounts())).
                            and(operation.subcategory.category.id.in(operationCriteria.getCategories()))).
                    fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return operations;
    };
}
