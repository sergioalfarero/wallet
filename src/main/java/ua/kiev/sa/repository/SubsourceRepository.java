package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.model.IncomeSource;
import ua.kiev.sa.model.Subsource;
import ua.kiev.sa.model.User;

import java.util.List;

@Repository("subsourceRepository")
public interface SubsourceRepository extends JpaRepository<Subsource, Long> {

    @Query("select s from Subsource s where s.user=:user and s.name=:subsource")
    Subsource findSubsourceByName(@Param("subsource") String subsource, @Param("user") User user);

    @Query("select s from Subsource s where s.user=:user and s.source=:source")
    List<Subsource> findAllSubcategoriesForUserAndIncomeSource(@Param("source") IncomeSource incomeSource, @Param("user") User u);

    @Query("select s from Subsource s where s.name=:name and s.source.name=:parentName and s.user.id=:id")
    Subsource findSubsourceByNameAndUserId(@Param("name") String name, @Param("parentName") String parentName, @Param("id") Long userId);

    @Query("select s from Subsource s where s.name=:name and s.source.id=:parentId and s.user.id=:id")
    Subsource findSubsourceByParentIdAndUserId(@Param("name") String text, @Param("parentId") Long parentName, @Param("id") Long userId);

    @Query("select s from Subsource s where s.user=:user and s.source.name=:incomeSource and s.name=:subsource")
    Subsource findSubsourceByIncomeSourceAndName(@Param("subsource") String subsource, @Param("incomeSource") String incomeSource, @Param("user") User login);


    @Query("delete from Subsource s where s.source=:incomeSource")
    boolean deleteAllSubcategoriesForIncomeSource(@Param("incomeSource") IncomeSource incomeSource);

    @Transactional
    @Query("delete from Subsource s where s.id=:id")
    @Modifying
    void deleteById(@Param("id") Long id);

    @Query("select s from Subsource s where s.user=:user and s.id=:subsource and s.source.id=:id")
    Subsource findSubsourceByNameAndIncomeSourceId(@Param("subsource") Long subsource, @Param("id") Long id, @Param("user") User user);

}
