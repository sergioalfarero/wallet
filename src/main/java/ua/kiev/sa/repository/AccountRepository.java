package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.User;

import java.util.List;

@Repository("accountRepository")
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("select a from Account a where a.user=:user")
    List<Account> findAllByUserId(@Param("user") User user);

    @Query("select a from Account a where a.user=:user and a.name=:account")
    Account findAccountByName(@Param("account") String account, @Param("user") User u);

    @Query("select a from Account a where a.user=:user and a.hidden=false")
    List<Account> findAllNotHiddenByUserId(@Param ("user") User user);

    @Query("select a from Account a where a.user.login in :users")
    List<Account> findAllForSomeUsers(@Param("users") List<String> users);
}
