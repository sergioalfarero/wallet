package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.Category;
import ua.kiev.sa.model.User;

import java.util.List;

@Repository("categoryRepository")
public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query("select c from Category c where c.user=:user ")
//    @Query("select c from Category c join Subcategory s on (c=s.category)")
    List<Category> findAll(@Param("user") User user);

    @Query("select c from Category c where c.user=:user")
    List<Category> findAllCategoriesForUser(@Param("user") User user);


//    @Query("select s from Subcategory s where s.user=:user and s.category=:c")
//    Subcategory findSubcategoryByCategoryAndName(String subcategory, Category c, User login);

    @Query("select c from Category c where c.user=:user and c.name=:name")
    Category findCategoryByName(@Param("name") String category, @Param("user") User user);

    @Query("select c from Category c where c.name=:name and c.user.id=:id")
    Category findByNameAndUserId(@Param("name") String name, @Param("id") Long id);

}
