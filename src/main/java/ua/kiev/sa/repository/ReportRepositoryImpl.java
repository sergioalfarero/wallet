package ua.kiev.sa.repository;

import com.querydsl.core.Tuple;
import com.querydsl.core.types.CollectionExpression;
import com.querydsl.core.types.Expression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.*;

import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.querydsl.core.group.GroupBy.sum;

@Repository("reportRepository")
public class ReportRepositoryImpl implements ReportRepository {

    @PersistenceContext
    private EntityManager em;


    public  List<Operation> findAllByCriteria(OperationCriteria operationCriteria){
        final JPAQuery<Operation> query = new JPAQuery(em);
        // where entityManager is a JPA EntityManager
//        HQLQuery query = new JPAQuery (entityManager);
//        JPAQueryFactory queryFactory = new JPAQueryFactory(em);
        final QOperation operation = QOperation.operation;
        final QSubcategory subcategory = QSubcategory.subcategory;
//        JPAQuery<Operation> query = queryFactory.selectFrom(operation);
        List<Operation> operations = null;
//        List<Operation> list = new ArrayList<Operation>();
        try {
            operations = query.select(operation).from(operation).
                    where(operation.account.id.in(operationCriteria.getAccounts()).
                            and(operation.date.between(operationCriteria.getStart(), operationCriteria.getEnd())).
                            and(operation.account.id.in(operationCriteria.getAccounts())).
                            and(operation.subcategory.category.id.in(operationCriteria.getCategories()))).
                    fetch();



        } catch (Exception e) {
            e.printStackTrace();
        }


        return operations;
    };


    public List<AccountStat> getCategoriesCostsForPeriod(OperationCriteria operationCriteria) {
//        System.out.println(operationCriteria);
        final JPAQuery<Operation> query = new JPAQuery(em);
        // where entityManager is a JPA EntityManager
//        HQLQuery query = new JPAQuery (entityManager);
//        JPAQueryFactory queryFactory = new JPAQueryFactory(em);
        final QOperation operation = QOperation.operation;
        final QSubcategory subcategory = QSubcategory.subcategory;
//        JPAQuery<Operation> query = queryFactory.selectFrom(operation);
        List<Tuple> operations = null;
        List<AccountStat> list = new ArrayList<AccountStat>();
        try {
            operations = query.select(operation.subcategory.category.name, operation.sum.sum().as("sum"), operation.currency).from(operation).
                    where(operation.account.id.in(operationCriteria.getAccounts()).
                            and(operation.date.between(operationCriteria.getStart(), operationCriteria.getEnd())).
                            and(operation.account.id.in(operationCriteria.getAccounts())).
                            and(operation.subcategory.category.id.in(operationCriteria.getCategories()))).
                    groupBy(operation.subcategory.category, operation.currency).
                    fetch();
//            having();

            System.out.println(operations);
            for (Tuple t : operations) {
                AccountStat as = new AccountStat(t.get(operation.subcategory.category.name), t.get(operation.sum.sum().as("sum")), t.get(operation.currency));
                list.add(as);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return list;
    }

    public List<AccountStat> getSubcategoriesCostsForPeriod(OperationCriteria operationCriteria) {
//        System.out.println(operationCriteria);
        final JPAQuery<Operation> query = new JPAQuery(em);
        // where entityManager is a JPA EntityManager
//        HQLQuery query = new JPAQuery (entityManager);
//        JPAQueryFactory queryFactory = new JPAQueryFactory(em);
        final QOperation operation = QOperation.operation;
        final QSubcategory subcategory = QSubcategory.subcategory;
//        JPAQuery<Operation> query = queryFactory.selectFrom(operation);
        List<Tuple> operations = null;
        List<AccountStat> list = new ArrayList<AccountStat>();
        try {
            operations = query.select(operation.subcategory.name, operation.sum.sum().as("sum"), operation.currency).from(operation).
                    where(operation.account.id.in(operationCriteria.getAccounts()).
                            and(operation.date.between(operationCriteria.getStart(), operationCriteria.getEnd())).
                            and(operation.account.id.in(operationCriteria.getAccounts())).
                            and(operation.subcategory.id.in(operationCriteria.getCategories()))).
                    groupBy(operation.subcategory, operation.currency).
                    fetch();
//            having();

            System.out.println(operations);
            for (Tuple t : operations) {
                AccountStat as = new AccountStat(t.get(operation.subcategory.name), t.get(operation.sum.sum().as("sum")), t.get(operation.currency));
                list.add(as);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return list;
    }
}
