package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.IncomeSource;
import ua.kiev.sa.model.User;

import java.util.List;

@Repository("sourceRepository")
public interface SourceRepository extends JpaRepository<IncomeSource, Long> {
    @Query("select c from IncomeSource c where c.user=:user ")
    List<IncomeSource> findAll(@Param("user") User user);

    @Query("select c from IncomeSource c where c.user=:user")
    List<IncomeSource> findAllSourcesForUser(@Param("user") User user);


    @Query("select c from IncomeSource c where c.user=:user and c.name=:name")
    IncomeSource findIncomeSourceByName(@Param("name") String source, @Param("user") User user);

    @Query("select c from IncomeSource c where c.name=:name and c.user.id=:id")
    IncomeSource findByNameAndUserId(@Param("name") String name, @Param("id") Long id);

}
