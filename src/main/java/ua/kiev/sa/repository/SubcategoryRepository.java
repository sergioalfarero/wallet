package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.model.Category;
import ua.kiev.sa.model.Subcategory;
import ua.kiev.sa.model.User;

import java.util.List;

@Repository("subcategoryRepository")
public interface SubcategoryRepository extends JpaRepository<Subcategory, Long> {

    @Query("select s from Subcategory s where s.user=:user and s.name=:subcategory")
    Subcategory findSubcategoryByName(@Param("subcategory") String subcategory, @Param("user") User user);

    @Query("select s from Subcategory s where s.user=:user and s.category=:category")
    List<Subcategory> findAllSubcategoriesForUserAndCategory(@Param("category") Category category, @Param("user") User u);

    @Query("select s from Subcategory s where s.name=:name and s.category.name=:parentName and s.user.id=:id")
    Subcategory findSubcategoryByNameAndUserId(@Param("name") String name, @Param("parentName") String parentName, @Param("id") Long userId);

    @Query("select s from Subcategory s where s.name=:name and s.category.id=:parentId and s.user.id=:id")
    Subcategory findSubcategoryByParentIdAndUserId(@Param("name") String text, @Param("parentId") Long parentName, @Param("id") Long userId);

    @Query("select s from Subcategory s where s.user=:user and s.category.name=:category and s.name=:subcategory")
    Subcategory findSubcategoryByCategoryAndName(@Param("subcategory") String subcategory, @Param("category") String category, @Param("user") User login);


    @Query("delete from Subcategory s where s.category=:category")
    boolean deleteAllSubcategoriesForCategory(@Param("category") Category category);

    @Transactional
    @Query("delete from Subcategory s where s.id=:id")
    @Modifying
    void deleteById(@Param("id") Long id);

    @Query("select s from Subcategory s where s.user=:user and s.id=:subcategory and s.category.id=:id")
    Subcategory findSubcategoryByNameAndCategoryId(@Param("subcategory") Long subcategory, @Param("id") Long id, @Param("user") User user);

}
