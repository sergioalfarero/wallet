package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.OperationCriteriaDto;

import java.util.List;

@Repository("operationRepository")
public interface OperationRepository extends JpaRepository<Operation, Long>, JpaSpecificationExecutor<Operation> {
    @Query("select o from Operation o where o.user.id=:user")
    List<Operation> findAllForUser(@Param("user")  Long id);

    @Query("select distinct o.subcategory.category from Operation o where o.user.id=:user_id")
    List<Category> findAllCategoriesOfOperationsForUser(@Param("user_id") Long id);

    @Query("select distinct o.subcategory.category from Operation o where o.user.id=:user_id")
    List<IncomeSource> findAllSourcesOfOperationsForUser(@Param("user_id") Long id);



}
