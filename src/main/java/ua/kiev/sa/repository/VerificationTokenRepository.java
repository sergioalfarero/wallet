package ua.kiev.sa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.VerificationToken;

@Repository("verificationTokenRepository")
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
    @Query("select t from VerificationToken t where t.token=:token")
    VerificationToken findByToken(@Param("token") String token);

    @Query("select t from VerificationToken t where t.user=:user")
    VerificationToken findByUser(@Param("user") User user);

}
