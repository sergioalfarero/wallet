package ua.kiev.sa.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.exceptions.CategoryExistsException;
import ua.kiev.sa.exceptions.SubcategoryExistsException;
import ua.kiev.sa.model.Category;
import ua.kiev.sa.model.Node;
import ua.kiev.sa.model.Subcategory;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.CategoryDto;
import ua.kiev.sa.model.dto.SubcategoryDto;
import ua.kiev.sa.service.CategoryService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.ConverterJSON;
import ua.kiev.sa.utils.GenericResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class CategoriesController {
    @Autowired
    CategoryService categoryService;
    @Autowired
    UserService userService;

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public ModelAndView categories() {
        ModelAndView model = new ModelAndView("category/categories");
        model.addObject("categoriesMap", getCategoriesMap());
        return model;
    }

    @RequestMapping(value = "/add_category", method = RequestMethod.GET)
    public ModelAndView addCategory() {
        ModelAndView model = new ModelAndView("category/add_category");
        CategoryDto category = new CategoryDto();
        model.addObject("categoryDto", category);
        return model;
    }

    @RequestMapping(value = "/add_category", method = RequestMethod.POST)
    public ModelAndView addCategoryPage(@ModelAttribute("categoryDto") CategoryDto categoryDto,
                                        BindingResult result,
                                        WebRequest request,
                                        Errors errors) {
        ModelAndView model = new ModelAndView("category/categories");
        categoryService.createCategory(categoryDto);
        model.addObject("categoryDto", categoryDto);
        model.addObject("categoriesMap", getCategoriesMap());
        return model;
    }

    @RequestMapping(value = "/add_new_category", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse getSearchResultViaAjax(@RequestBody Node node) {
        GenericResponse result = new GenericResponse();

        return result;

    }


    @RequestMapping(value = "/change_category_name", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> changeCategoryName(@RequestParam String id, @RequestParam String text, @RequestParam String parent) {
        Node oldNode = null;
        boolean b = false;

        Long catId = 0L;

        if (parent.equals("#")) {

            catId = Long.valueOf(id.subSequence(1, id.length()).toString());
            Category oldCat = categoryService.getCategoryById(catId);
            if (oldCat.getName().equals(text) && oldCat.getId().equals(catId)) {
                return new ResponseEntity<Node>(new Node(null, null, null), HttpStatus.OK);
            }

            oldNode = new Node("c" + oldCat.getId().toString(), "#", oldCat.getName());

            try {
                b = categoryService.updateCategory(catId, text);
            } catch (CategoryExistsException e) {
                e.printStackTrace();
                b = false;
            }
            Node n = null;
            if (b) {
                n = new Node("c" + catId.toString(), parent, text);
                return new ResponseEntity<Node>(n, HttpStatus.OK);
            } else {
                return new ResponseEntity<Node>(oldNode, HttpStatus.BAD_REQUEST);
            }


        } else {
            Long parentId = Long.valueOf(parent.subSequence(1, parent.length()).toString());
            catId = Long.valueOf(id.subSequence(1, id.length()).toString());
            Subcategory oldSubcat = categoryService.getSubcategoryById(catId);
            if (oldSubcat.getName().equals(text) && oldSubcat.getId().equals(catId)) {
                return new ResponseEntity<Node>(new Node(null, null, null), HttpStatus.OK);
            }

            oldNode = new Node("s" + oldSubcat.getId().toString(), oldSubcat.getCategory().getName(), oldSubcat.getName());

            try {
                b = categoryService.updateSubcategory(catId, text, parentId);

            } catch (SubcategoryExistsException e) {
                b = false;
                e.printStackTrace();
            }

            Node n = null;
            if (b) {
                n = new Node("s" + catId.toString(), parent, text);
                return new ResponseEntity<Node>(n, HttpStatus.OK);
            } else {
                return new ResponseEntity<Node>(oldNode, HttpStatus.BAD_REQUEST);
            }
        }


    }


    @RequestMapping(value = "/create_category", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> createCategoryName(@RequestParam String id, @RequestParam String text, @RequestParam String parent) {
        String parentId = null;
        Node n = null;
        if (parent.equals("#")) {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.setName(text);
            Category category = categoryService.createCategory(categoryDto);
            n = new Node("c" + category.getId().toString(), "#", category.getName());
            return new ResponseEntity<Node>(n, HttpStatus.OK);
        }

        return new ResponseEntity<Node>(new Node(null, null, null), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/new_category", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> createCategory(@RequestParam String text) {
        String parentId = null;
        Node n = null;
        if (text != null && !text.equals("")) {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.setName(text);
            Category category = categoryService.createCategory(categoryDto);
            n = new Node("c" + category.getId().toString(), "#", category.getName());
            return new ResponseEntity<Node>(n, HttpStatus.OK);
        }

        return new ResponseEntity<Node>(new Node(null, null, null), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/delete_category", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> deleteCategory(@RequestParam String id) {
        char s = id.charAt(0);

        Long catId = Long.valueOf(id.subSequence(1, id.length()).toString());
        boolean b = false;

        if (s == 'c') {
            b = categoryService.deleteCategory(catId);
        } else {

            b = categoryService.deleteSubcategory(catId);
        }

        if (b) return new ResponseEntity(new Node(null, null, null), HttpStatus.OK);
        else return new ResponseEntity(new Node(null, null, null), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/create_subcategory", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> createSubcategoryName(@RequestParam String id, @RequestParam String text, @RequestParam String parent) {
        Long catId = Long.valueOf(parent.subSequence(1, parent.length()).toString());
        Node n = null;
        Subcategory subcategory1 = categoryService.createSubcategory(text, catId);

        n = new Node("s" + subcategory1.getId().toString(), parent, subcategory1.getName());
        return new ResponseEntity<Node>(n, HttpStatus.OK);
    }

    @RequestMapping(value = "/create_new_subcategory", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> createNewSubcategory(@RequestParam String text, @RequestParam String parent) {
        Long catId = Long.valueOf(parent.subSequence(1, parent.length()).toString());
        Node n = null;
        Subcategory subcategory1 = categoryService.createSubcategory(text, catId);

        n = new Node("s" + subcategory1.getId().toString(), parent, subcategory1.getName());
        return new ResponseEntity<Node>(n, HttpStatus.OK);
    }

    @RequestMapping(value = "/add_subcategory", method = RequestMethod.GET)
    public ModelAndView addSubcategoryToCategory(@RequestParam("cat") String category) {
        ModelAndView model = new ModelAndView("category/add_subcategory");
        SubcategoryDto subcategory = new SubcategoryDto();
        subcategory.setCategoryName(category);

        model.addObject("subcategoryDto", subcategory);
        model.addObject("categories", getCategoriesList());

        return model;
    }


    @RequestMapping(value = "/addSubcategory", method = RequestMethod.GET)
    public ModelAndView addSubcategory() {
        ModelAndView model = new ModelAndView("category/add_subcategory");
        SubcategoryDto subcategory = new SubcategoryDto();
        model.addObject("subcategoryDto", subcategory);
        model.addObject("categories", getCategoriesList());
        return model;
    }


    @RequestMapping(value = "/add_subcategory", method = RequestMethod.POST)
    public ModelAndView addSubcategoryPage(@ModelAttribute("subcategoryDto") SubcategoryDto subcategoryDto,

                                           BindingResult result,
                                           WebRequest request,
                                           Errors errors) {
        categoryService.createSubcategory(subcategoryDto);
        ModelAndView model = new ModelAndView("category/categories");
        model.addObject("categoryDto", subcategoryDto);
        model.addObject("categoriesMap", getCategoriesMap());
        return model;
    }

    @RequestMapping(value = "/addSubcategory", method = RequestMethod.POST)
    public ModelAndView addSubcategoryPagePost(@ModelAttribute("subcategoryDto") SubcategoryDto subcategoryDto,

                                               BindingResult result,
                                               WebRequest request,
                                               Errors errors) {
        categoryService.createSubcategory(subcategoryDto);
        ModelAndView model = new ModelAndView("category/categories");
        model.addObject("categoryDto", subcategoryDto);
        model.addObject("categoriesMap", getCategoriesMap());
        return model;
    }


    @ResponseBody
    @RequestMapping(value = "/getAllCategoriesJSON", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public String getAllCategoriesMapJson(HttpServletResponse response) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        Map<Category, List<Subcategory>> map = categoryService.getCategories(login);
        StringBuffer s = new StringBuffer();
        Map<String, Node> nodes = new HashMap<String, Node>();
        nodes.putAll(categoryService.getCategoriesNodes(map.keySet()));
        for (Map.Entry e : map.entrySet()) {
            List<Subcategory> subcategories = ((List<Subcategory>) e.getValue());
            nodes.putAll(categoryService.getSubcategoriesForJSON(subcategories));
        }
        try {
            s.append(ConverterJSON.toJSON_String(nodes));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        response.setContentType("application/json; charset=utf-8");
        return s.toString();
    }

    @ResponseBody
    @RequestMapping(value = "/getAllCategoriesWithRootJSON", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public String getAllCategoriesMapWithRootJson(HttpServletResponse response) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        Map<Category, List<Subcategory>> map = categoryService.getCategories(login);
        StringBuffer s = new StringBuffer();
        Map<String, Node> nodes = new HashMap<String, Node>();
        nodes.putAll(categoryService.getCategoriesNodesWithRoot(map.keySet()));
        for (Map.Entry e : map.entrySet()) {
            List<Subcategory> subcategories = ((List<Subcategory>) e.getValue());
            nodes.putAll(categoryService.getSubcategoriesForJSON(subcategories));
        }
        try {
            s.append(ConverterJSON.toJSON_String(nodes));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        response.setContentType("application/json; charset=utf-8");
        return s.toString();
    }

    private Map<Category, List<Subcategory>> getCategoriesMap() {
        Map<Category, List<Subcategory>> categories = new HashMap<Category, List<Subcategory>>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userService.getByUserName(login);
        List<Category> cats = categoryService.getCategories(user);
        for (Category c : cats) {
            categories.put(c, c.getSubcategories());
        }
        return categories;
    }

    private List<Category> getCategories() {
        List<Category> categories = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userService.getByUserName(login);
        categories = categoryService.getCategoriesList(user);
        return categories;
    }

    @RequestMapping(value = "/findcategories", method = RequestMethod.GET)
    @ResponseBody
    public List<Category> findAllCategories() {

        return getCategories();
    }

    @RequestMapping(value = "/findsubcategories", method = RequestMethod.GET)
    @ResponseBody
    public List<Subcategory> findAllSubcategories(@RequestParam(value = "category", required = true) Long categoryId) {
        return categoryService.getSubcategories(categoryId);

    }


    private List<Category> getCategoriesList() {
        List<Category> categories = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userService.getByUserName(login);
        categories = categoryService.getCategoriesList(user);
        return categories;
    }


}
