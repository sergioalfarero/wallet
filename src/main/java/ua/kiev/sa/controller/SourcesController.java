package ua.kiev.sa.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.exceptions.SourceExistsException;
import ua.kiev.sa.exceptions.SubsourceExistsException;
import ua.kiev.sa.model.IncomeSource;
import ua.kiev.sa.model.dto.SourceDto;
import ua.kiev.sa.model.Node;
import ua.kiev.sa.model.Subsource;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.SubsourceDto;
import ua.kiev.sa.service.SourcesService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.ConverterJSON;
import ua.kiev.sa.utils.GenericResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class SourcesController {
    @Autowired
    SourcesService sourceService;
    @Autowired
    UserService userService;

    @RequestMapping(value = "/sources", method = RequestMethod.GET)
    public ModelAndView sources() {
        ModelAndView model = new ModelAndView("source/sources");
        model.addObject("sourcesMap", getSourcesMap());
        return model;
    }

    @RequestMapping(value = "/add_source", method = RequestMethod.GET)
    public ModelAndView addsource() {
        ModelAndView model = new ModelAndView("source/add_source");
        SourceDto source = new SourceDto();
        model.addObject("sourceDto", source);
        return model;
    }

    @RequestMapping(value = "/add_source", method = RequestMethod.POST)
    public ModelAndView addsourcePage(@ModelAttribute("sourceDto") SourceDto sourceDto,
                                        BindingResult result,
                                        WebRequest request,
                                        Errors errors) {
        ModelAndView model = new ModelAndView("source/sources");
        sourceService.createSource(sourceDto);
        model.addObject("sourceDto", sourceDto);
        model.addObject("sourcesMap", getSourcesMap());
        return model;
    }

    @RequestMapping(value = "/add_new_source", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse getSearchResultViaAjax(@RequestBody Node node) {
        GenericResponse result = new GenericResponse();

        return result;

    }


    @RequestMapping(value = "/change_source_name", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> changesourceName(@RequestParam String id, @RequestParam String text, @RequestParam String parent) {
        Node oldNode = null;
        boolean b = false;


        Long catId = 0L;


        if (parent.equals("#")) {

            catId = Long.valueOf(id.subSequence(1, id.length()).toString());
            IncomeSource oldCat = sourceService.getIncomeSourceById(catId);
            if (oldCat.getName().equals(text) && oldCat.getId().equals(catId)) {
                return new ResponseEntity<Node>(new Node(null, null, null), HttpStatus.OK);
            }

            oldNode = new Node("c" + oldCat.getId().toString(), "#", oldCat.getName());

            try {
                b = sourceService.updateIncomeSource(catId, text);
            } catch (SourceExistsException e) {
                e.printStackTrace();
                b = false;
            }

            Node n = null;
            if (b) {
                n = new Node("c" + catId.toString(), parent, text);
                return new ResponseEntity<Node>(n, HttpStatus.OK);
            } else {
                return new ResponseEntity<Node>(oldNode, HttpStatus.BAD_REQUEST);
            }


        } else {
            Long parentId = Long.valueOf(parent.subSequence(1, parent.length()).toString());
            catId = Long.valueOf(id.subSequence(1, id.length()).toString());
            Subsource oldSubcat = sourceService.getSubsourceById(catId);
            if (oldSubcat.getName().equals(text) && oldSubcat.getId().equals(catId)) {
                return new ResponseEntity<Node>(new Node(null, null, null), HttpStatus.OK);
            }

            oldNode = new Node("s" + oldSubcat.getId().toString(), oldSubcat.getSource().getName(), oldSubcat.getName());

            try {
                b = sourceService.updateSubsource(catId, text, parentId);

            } catch (SubsourceExistsException e) {
                b = false;
                e.printStackTrace();
            }

            Node n = null;
            if (b) {
                n = new Node("s" + catId.toString(), parent, text);
                return new ResponseEntity<Node>(n, HttpStatus.OK);
            } else {
                return new ResponseEntity<Node>(oldNode, HttpStatus.BAD_REQUEST);
            }
        }


    }


    @RequestMapping(value = "/create_source", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> createsourceName(@RequestParam String id, @RequestParam String text, @RequestParam String parent) {
        String parentId = null;
        Node n = null;
        if (parent.equals("#")) {
            SourceDto sourceDto = new SourceDto();
            sourceDto.setName(text);
            IncomeSource source = sourceService.createSource(sourceDto);
            n = new Node("c" + source.getId().toString(), "#", source.getName());
            return new ResponseEntity<Node>(n, HttpStatus.OK);
        }

        return new ResponseEntity<Node>(new Node(null, null, null), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/new_source", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> createsource(@RequestParam String text) {
        String parentId = null;
        Node n = null;
        if (text != null && !text.equals("")) {
            SourceDto sourceDto = new SourceDto();
            sourceDto.setName(text);
            IncomeSource source = sourceService.createSource(sourceDto);
            n = new Node("c" + source.getId().toString(), "#", source.getName());
            return new ResponseEntity<Node>(n, HttpStatus.OK);
        }

        return new ResponseEntity<Node>(new Node(null, null, null), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/delete_source", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> deletesource(@RequestParam String id) {
        char s = id.charAt(0);

        Long catId = Long.valueOf(id.subSequence(1, id.length()).toString());
        boolean b = false;

        if (s == 'c') {
            b = sourceService.deleteIncomeSource(catId);
        } else {

            b = sourceService.deleteSubsource(catId);
        }

        if (b) return new ResponseEntity(new Node(null, null, null), HttpStatus.OK);
        else return new ResponseEntity(new Node(null, null, null), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/create_subsource", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> createSubsourceName(@RequestParam String id, @RequestParam String text, @RequestParam String parent) {
        Long catId = Long.valueOf(parent.subSequence(1, parent.length()).toString());
        Node n = null;
        Subsource subsource1 = sourceService.createSubsource(text, catId);

        n = new Node("s" + subsource1.getId().toString(), parent, subsource1.getName());
        return new ResponseEntity<Node>(n, HttpStatus.OK);
    }

    @RequestMapping(value = "/create_new_subsource", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Node> createNewSubsource(@RequestParam String text, @RequestParam String parent) {
        Long catId = Long.valueOf(parent.subSequence(1, parent.length()).toString());
        Node n = null;
        Subsource subsource1 = sourceService.createSubsource(text, catId);

        n = new Node("s" + subsource1.getId().toString(), parent, subsource1.getName());
        return new ResponseEntity<Node>(n, HttpStatus.OK);
    }

    @RequestMapping(value = "/add_subsource", method = RequestMethod.GET)
    public ModelAndView addSubsourceTosource(@RequestParam("cat") String source) {
        ModelAndView model = new ModelAndView("source/add_subsource");
        SubsourceDto subsource = new SubsourceDto();
        subsource.setSourceName(source);

        model.addObject("subsourceDto", subsource);
        model.addObject("sources", getSourcesList());

        return model;
    }


    @RequestMapping(value = "/addSubsource", method = RequestMethod.GET)
    public ModelAndView addSubsource() {
        ModelAndView model = new ModelAndView("source/add_subsource");
        SubsourceDto subsource = new SubsourceDto();
        model.addObject("subsourceDto", subsource);
        model.addObject("sources", getSourcesList());
        return model;
    }


    @RequestMapping(value = "/add_subsource", method = RequestMethod.POST)
    public ModelAndView addSubsourcePage(@ModelAttribute("subsourceDto") SubsourceDto subsourceDto,

                                           BindingResult result,
                                           WebRequest request,
                                           Errors errors) {
        sourceService.createSubsource(subsourceDto);
        ModelAndView model = new ModelAndView("source/sources");
        model.addObject("sourceDto", subsourceDto);
        model.addObject("sourcesMap", getSourcesMap());
        return model;
    }

    @RequestMapping(value = "/addSubsource", method = RequestMethod.POST)
    public ModelAndView addSubsourcePagePost(@ModelAttribute("subsourceDto") SubsourceDto subsourceDto,

                                               BindingResult result,
                                               WebRequest request,
                                               Errors errors) {
        sourceService.createSubsource(subsourceDto);
        ModelAndView model = new ModelAndView("source/sources");
        model.addObject("sourceDto", subsourceDto);
        model.addObject("sourcesMap", getSourcesMap());
        return model;
    }


    @ResponseBody
    @RequestMapping(value = "/getAllSourcesJSON", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public String getAllSourcesMapJson(HttpServletResponse response) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        Map<IncomeSource, List<Subsource>> map = sourceService.getSources(login);
        StringBuffer s = new StringBuffer();
        Map<String, Node> nodes = new HashMap<String, Node>();
        nodes.putAll(sourceService.getSourcesNodes(map.keySet()));
        for (Map.Entry e : map.entrySet()) {
            List<Subsource> subsources= ((List<Subsource>) e.getValue());
            nodes.putAll(sourceService.getSubsourcesForJSON(subsources));
        }
        try {
            s.append(ConverterJSON.toJSON_String(nodes));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        response.setContentType("application/json; charset=utf-8");
        return s.toString();
    }


    private Map<IncomeSource, List<Subsource>> getSourcesMap() {
        Map<IncomeSource, List<Subsource>> sources = new HashMap<IncomeSource, List<Subsource>>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userService.getByUserName(login);
        List<IncomeSource> cats = sourceService.getSources(user);
        for (IncomeSource c : cats) {
            sources.put(c, c.getSubsources());
        }
        return sources;
    }

    private List<IncomeSource> getIncomeSource() {
        List<IncomeSource> sources = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userService.getByUserName(login);
        sources = sourceService.getSourcesList(user);
        return sources;
    }

    @RequestMapping(value = "/findsources", method = RequestMethod.GET)
    @ResponseBody
    public List<IncomeSource> findAllSources() {

        return getIncomeSource();
    }

    @RequestMapping(value = "/findsubsources", method = RequestMethod.GET)
    @ResponseBody
    public List<Subsource> findAllSubsources(@RequestParam(value = "source", required = true) Long sourceId) {
        return sourceService.getSubsources(sourceId);

    }


    private List<IncomeSource> getSourcesList() {
        List<IncomeSource> sources = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userService.getByUserName(login);
        sources = sourceService.getSourcesList(user);
        return sources;
    }


}
