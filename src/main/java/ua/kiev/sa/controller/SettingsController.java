package ua.kiev.sa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.model.BankType;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.UserSettings;
import ua.kiev.sa.model.dto.SettingsDto;
import ua.kiev.sa.service.AccountService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.ConverterJSON;
import ua.kiev.sa.utils.GenericResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
public class SettingsController {

    @Autowired
    AccountService accountService;
    @Autowired
    UserService userService;

    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public ModelAndView accountsPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("settings/settings");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User u = userService.getByUserName(login);
        SettingsDto settingsDto = new SettingsDto();
        if (u.getSettings() != null) {
            settingsDto.setBank(u.getSettings().getBank());
            settingsDto.setDefaultRates(u.getSettings().isDefaultRates());
            settingsDto.setDefaultCurrency(u.getSettings().getDefaultCurrency());
            settingsDto.setFavouriteCurrenciesSet(u.getSettings().getFavouriteCurrencySet());
        } else {
            settingsDto = new SettingsDto();
        }
        List<BankType> bankTypes = Arrays.asList(BankType.values());
        List<CurrencyType> currencyTypes = Arrays.asList(CurrencyType.values());
        Set<CurrencyType> favouriteCurrencies = u.getSettings().getFavouriteCurrencySet();
        model.addObject("settingsDto", settingsDto);
        model.addObject("banks", bankTypes);
        model.addObject("currencies", currencyTypes);
        model.addObject("favouriteCurrencies", favouriteCurrencies);
        return model;
    }


    @RequestMapping(value = "/settings", method = RequestMethod.POST)
    public ModelAndView settingPage(@ModelAttribute("settings") SettingsDto settingsDto,
                                    BindingResult result,
                                    WebRequest request,
                                    Errors errors, HttpServletRequest httpServletRequest) {
        User u = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        if (!result.hasErrors()) {
            UserSettings settings = new UserSettings();
            settings.setBank(settingsDto.getBank());
            settings.setDefaultRates(settingsDto.isDefaultRates());
            settings.setDefaultCurrency(settingsDto.getDefaultCurrency());
            settings.setFavouriteCurrencySet(settingsDto.getFavouriteCurrenciesSet());
            u = userService.changeUserSettings(settings, login);
        }

        if (u == null) {
            result.rejectValue("me", "message.settingsError");
        }
        if (result.hasErrors()) {
            ModelAndView model = new ModelAndView("settings/settings");
            model.addObject("settings", settingsDto);
            return model;
        } else {
            ModelAndView model = new ModelAndView("/index");
            return model;
        }

    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "settings/change/", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    GenericResponse changeOperation(@RequestBody SettingsDto settingsDto, HttpServletResponse httpServletResponse) {
        GenericResponse response = new GenericResponse();

        UserSettings updatedSettings = null;
        try {
            updatedSettings = userService.updateSettings(settingsDto);
        } catch (Exception e) {
            response.setError("400");
            response.setMessage(e.getMessage());
//            return response;
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            try {

                httpServletResponse.getWriter().write(ConverterJSON.toJSON_String(response));
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            return response;
        }

        if (updatedSettings != null) {
            response.setError("200");

        } else {
            response.setError("204");
            response.setMessage("Error!");
        }

        return response;
    }

}

