package ua.kiev.sa.controller;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.exceptions.ItemAlreadyExistsException;
import ua.kiev.sa.exceptions.ItemNotFoundException;
import ua.kiev.sa.exceptions.ListNotFoundException;
import ua.kiev.sa.exceptions.QueryAlreadyExistsException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.ShoppingItemDto;
import ua.kiev.sa.model.dto.ShoppingListDto;
import ua.kiev.sa.service.RateService;
import ua.kiev.sa.service.ShoppingItemService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.ConverterJSON;
import ua.kiev.sa.utils.GenericResponse;
import ua.kiev.sa.utils.SummarizeValues;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

@RestController
public class ShoppingListController {

    @Autowired
    ShoppingItemService shoppingItemService;

    @Autowired
    UserService userService;

    @Autowired
    RateService rateService;

    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/shoppingList", method = RequestMethod.GET)
    public ModelAndView accountsPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("/shoppingList/shoppingList");

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);

//        List<ShoppingItem> shoppingListItems = shoppingItemService.getShoppingItems(u);
        List<ShoppingListQuery> shoppingListQueries = shoppingItemService.getShoppingListQuery(u);
        Map<ShoppingListQuery, List<ShoppingItem>> map = new HashMap<ShoppingListQuery, List<ShoppingItem>>();
        for (ShoppingListQuery query : shoppingListQueries) {
            map.put(query, query.getItems());
        }


//        model.addObject("shoppingList", shoppingListItems);

        model.addObject("shoppingListQueries", map);
        model.addObject("user", u);
        model.addObject("path", request.getServletPath());
        model.addObject("def_curr", u.getSettings().getDefaultCurrency());
        model.addObject("fav_curr", u.getSettings().getFavouriteCurrencySet());

        return model;
    }

    @RequestMapping(value = "/addNewShoppingItem", method = RequestMethod.GET)
    public ModelAndView addShoppingItemPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("shoppingList/addItem");
        ShoppingItemDto shoppingItemDto = new ShoppingItemDto();
        List<CurrencyType> currencyTypes = Arrays.asList(CurrencyType.values());
        model.addObject("currencies", currencyTypes);
        model.addObject("shoppingItem", shoppingItemDto);
        model.addObject("prevPage", request.getHeader("referer"));
//        model.addObject("defaultIcons", getDefaultIconsUrl(request));
//        model.addObject("userIcons", getUserIconsUrl(request));
        return model;
    }

    @RequestMapping(value = "/addShoppingItem", method = RequestMethod.GET)
    public ModelAndView addShoppingItemForQueryPage(@RequestParam("query") String s, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("shoppingList/addItem");

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);
        ShoppingItemDto shoppingItemDto = new ShoppingItemDto();
        List<CurrencyType> currencyTypes = Arrays.asList(CurrencyType.values());
        model.addObject("queries", getAllQueries(u));
        model.addObject("currencies", currencyTypes);
        model.addObject("shoppingItem", shoppingItemDto);
        model.addObject("prevPage", request.getHeader("referer"));
//        model.addObject("defaultIcons", getDefaultIconsUrl(request));
//        model.addObject("userIcons", getUserIconsUrl(request));
        return model;
    }

    private List<ShoppingListQuery> getAllQueries(User u) {
        return shoppingItemService.getShoppingListQuery(u);
    }


    @RequestMapping(value = "/addShoppingList", method = RequestMethod.GET)
    public ModelAndView addShoppingListPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("shoppingList/addList");
        response.setCharacterEncoding("utf-8");
        ShoppingListDto shoppingListDto = new ShoppingListDto();

        model.addObject("shoppingList", shoppingListDto);
        model.addObject("prevPage", request.getHeader("referer"));
//        model.addObject("defaultIcons", getDefaultIconsUrl(request));
//        model.addObject("userIcons", getUserIconsUrl(request));
        return model;
    }


    @RequestMapping(value = "/addShoppingList", method = RequestMethod.POST)
    public ModelAndView addAccountPage(@Valid @ModelAttribute("shoppingList") ShoppingListDto shoppingListDto,
                                       BindingResult result,
                                       WebRequest request,
                                       Errors errors, HttpServletRequest httpServletRequest, HttpServletResponse response) {
        response.setCharacterEncoding("utf-8");

        ShoppingListQuery query = new ShoppingListQuery();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);

        if (!result.hasErrors()) {
            query = createNewQuery(shoppingListDto);
        }
        if (query == null) {
            result.rejectValue("name", "message.accError");
        }


        if (result.hasErrors()) {
            ModelAndView model = new ModelAndView("shoppingList/addList");
            model.addObject("shoppingList", shoppingListDto);
            return model;
        } else {
            ModelAndView model = new ModelAndView("shoppingList/shoppingList");

            List<ShoppingListQuery> shoppingListQueries = shoppingItemService.getShoppingListQuery(u);
            Map<ShoppingListQuery, List<ShoppingItem>> map = new HashMap<ShoppingListQuery, List<ShoppingItem>>();
            for (ShoppingListQuery query1 : shoppingListQueries) {
                map.put(query1, query1.getItems());
            }

            model.addObject("shoppingListQueries", map);


            List<ShoppingItem> items = shoppingItemService.getShoppingItems(u);
//            List<ShoppingListQuery> queries = shoppingItemService.getShoppingListQuery(u);
            model.addObject("shoppingList", items);
//            model.addObject("shoppingQuery", queries);
            model.addObject("users", login);
            model.addObject("path", httpServletRequest.getServletPath());
            model.addObject("def_curr", u.getSettings().getDefaultCurrency());

            return model;
        }


    }


    @RequestMapping(value = "/addNewShoppingList", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ShoppingListQuery> addNewShoppingList(@RequestParam String listName,
                                                                @RequestParam String listDescription) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);
        ShoppingListQuery listQuery = new ShoppingListQuery();
        ShoppingListDto query = new ShoppingListDto();
        query.setDesc(listDescription);
        query.setName(listName);
        query.setUser(login);
        listQuery = createNewQuery(query);
        if (listQuery != null) {
            return new ResponseEntity<ShoppingListQuery>(listQuery, HttpStatus.OK);
        } else {
            return new ResponseEntity<ShoppingListQuery>(new ShoppingListQuery(), HttpStatus.BAD_REQUEST);
        }


    }

    @RequestMapping(value = "/editList", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    @ResponseBody
    public ResponseEntity<ShoppingListQuery> editList(@RequestBody ShoppingListDto listDto, HttpServletResponse response) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);
        ShoppingListQuery list = null;
        try {

            list = shoppingItemService.editList(listDto, u);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (list != null) {

            return new ResponseEntity<ShoppingListQuery>(list, HttpStatus.OK);
        } else {
            response.setStatus(400);
            return new ResponseEntity<ShoppingListQuery>(new ShoppingListQuery(), HttpStatus.BAD_REQUEST);
        }


    }

    @RequestMapping(value = "/editItem", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    @ResponseBody
    public ResponseEntity<ShoppingItem> editItem(@RequestBody ShoppingItemDto itemDto, HttpServletResponse response) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);
        ShoppingItem item = null;

        try {

            shoppingItemService.editItem(itemDto, u);
            item=shoppingItemService.findById(itemDto.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (item != null) {

            return new ResponseEntity<ShoppingItem>(item, HttpStatus.OK);
        } else {
            response.setStatus(400);
            return new ResponseEntity<ShoppingItem>(new ShoppingItem(), HttpStatus.BAD_REQUEST);
        }


    }
    @RequestMapping(value = "/editItemWithMoving", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    @ResponseBody
    public ResponseEntity<List<ShoppingItem>> editItem1(@RequestBody ShoppingItemDto itemDto, HttpServletResponse response) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);
        List<ShoppingItem> list=new ArrayList<ShoppingItem>();

        ShoppingItem oldItem = shoppingItemService.findById(itemDto.getId());
        ShoppingListQuery outcomingQuery=shoppingItemService.findQueryById(oldItem.getShoppingListQuery().getId());

        Price p=shoppingItemService.getListInfoWithoutItem(outcomingQuery, oldItem, u); //ol1  it2
        oldItem.getShoppingListQuery().setSum(p.getPrice());
        oldItem.getShoppingListQuery().setType(p.getCurrencyType());
        ShoppingItem item = null;
        try {
           item= shoppingItemService.editItemWithMoving(itemDto, u);
           item=shoppingItemService.findById(itemDto.getId());

        } catch (Exception e) {
            item=null;
        }
        if (item != null) {
            list.add(item);
            list.add(oldItem);
            return new ResponseEntity<List<ShoppingItem>>(list, HttpStatus.OK);
        } else {
            response.setStatus(400);
            return new ResponseEntity<List<ShoppingItem>>(new ArrayList<ShoppingItem>(), HttpStatus.BAD_REQUEST);
        }


    }
    @RequestMapping(value = "/addNewShoppingItem1", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    @ResponseBody
    public ResponseEntity<ShoppingItem> addNewShoppingItem1(@RequestBody ShoppingItemDto itemDto, HttpServletResponse response) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);

        ShoppingItem item = new ShoppingItem();
        item = createNewItem(itemDto);
        if (item != null) {

            return new ResponseEntity<ShoppingItem>(item, HttpStatus.OK);
        } else {
            response.setStatus(400);
            return new ResponseEntity<ShoppingItem>(new ShoppingItem(), HttpStatus.BAD_REQUEST);
        }


    }

    private ShoppingItem createNewItem(ShoppingItemDto itemDto) {
        ShoppingItem created = null;
        try {
            created = shoppingItemService.createItem(itemDto);
        } catch (ItemAlreadyExistsException e) {
            e.printStackTrace();
            return null;
        }
        return created;
    }

    private ShoppingListQuery createNewQuery(ShoppingListDto shoppingListDto) {
        ShoppingListQuery registered = null;
        try {
            registered = shoppingItemService.createQuery(shoppingListDto);
        } catch (QueryAlreadyExistsException e) {

            e.printStackTrace();
            return null;
        }
        return registered;
    }


    @RequestMapping(value = "/addShoppingItem", method = RequestMethod.POST)
    public ModelAndView addAccountPage(@RequestParam("query") String query, @Valid @ModelAttribute("shoppingItem") ShoppingItemDto shoppingItemDto,
                                       BindingResult result,
                                       WebRequest request,
                                       Errors errors, HttpServletRequest httpServletRequest) {
        ShoppingItem item = new ShoppingItem();

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);

        if (!result.hasErrors()) {
            item = createNewItem(shoppingItemDto, query);
        }
        if (item == null) {
            result.rejectValue("name", "message.accError");
        }


        if (result.hasErrors()) {
            ModelAndView model = new ModelAndView("shoppingList/addItem");

            model.addObject("shoppingItem", shoppingItemDto);
            return model;
        } else {
            ModelAndView model = new ModelAndView("shoppingList/shoppingList");
            List<ShoppingItem> items = shoppingItemService.getShoppingItems(u);


            model.addObject("shoppingList", items);
            model.addObject("users", login);
            model.addObject("path", httpServletRequest.getServletPath());
            List<ShoppingItem> shoppingListItems = shoppingItemService.getShoppingItems(u);
            List<ShoppingListQuery> shoppingListQueries = shoppingItemService.getShoppingListQuery(u);
            Map<ShoppingListQuery, List<ShoppingItem>> map = new HashMap<ShoppingListQuery, List<ShoppingItem>>();
            for (ShoppingListQuery listQuery : shoppingListQueries) {
                map.put(listQuery, listQuery.getItems());
            }

            model.addObject("shoppingListQueries", map);

//        model.addObject("shoppingList", shoppingListItems);

            model.addObject("user", login);
            model.addObject("path", httpServletRequest.getServletPath());
            model.addObject("def_curr", u.getSettings().getDefaultCurrency());
//            model.addObject("def_curr", u.getSettings().getDefaultCurrency());
            return model;
        }


    }


    @RequestMapping(value = "/shoppingList/{id}/delete", method = RequestMethod.GET)
    public ModelAndView deleteShoppingItemPage(@PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("shoppingList/shoppingList");
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);
        try {
            shoppingItemService.delete(id, u);
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
        }
        List<ShoppingItem> shoppingListItems = shoppingItemService.getShoppingItems(u);
        model.addObject("shoppingList", shoppingListItems);
        return model;
    }

    @RequestMapping(value = "/shoppingList/item/{id}/delete", method = RequestMethod.GET)
    public GenericResponse deleteShoppingItem(@PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
        GenericResponse genericResponse = new GenericResponse();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);

        ShoppingListQuery query = shoppingItemService.getShoppingListQueryByItemId(id);
        ShoppingItem item = shoppingItemService.findById(id);
        List<ShoppingItem> queryItems = shoppingItemService.getQueryByItemId(id);
        ShoppingItem shoppingItem = null;
        try {
            shoppingItemService.delete(id, u);
        } catch (ItemNotFoundException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }

        shoppingItem = shoppingItemService.findById(id);

        if (shoppingItem == null) {

            genericResponse.setError("200");
            String s = null;
            List<ShoppingItem> list = new ArrayList<ShoppingItem>();

            list = queryItems;
            list.remove(item);
            String sum = SummarizeValues.summarize(list, u);
            try {
                Map<Object, Object> map = new HashMap<Object, Object>(2);
                map.put("id", id);
                map.put("sum", sum);
                s = ConverterJSON.toJSON_Map(map);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            genericResponse.setMessage(s);
        } else {
            genericResponse.setError("204");
            genericResponse.setMessage("Error!");
        }
        return genericResponse;
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "shoppingList/delete/{id}", method = RequestMethod.GET, produces = "application/json"/*, headers = {"Accept=text/html, application/json"}, produces = "application/json"*/)
    public
    @ResponseBody
    GenericResponse deleteShoppingList(@PathVariable("id") final Long id) {
        GenericResponse response = new GenericResponse();
        try {
            shoppingItemService.deleteList(id);
        } catch (ListNotFoundException e) {
            e.printStackTrace();
        }
        ShoppingListQuery query = shoppingItemService.findQueryById(id);
        if (query == null) {
            response.setError("200");
            String s = null;
            try {
                s = ConverterJSON.toJSON_String(id);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            response.setMessage(s);
        } else {
            response.setError("204");
            response.setMessage("Error!");
        }
        return response;

    }

    private ShoppingItem createNewItem(ShoppingItemDto shoppingItemDto, String query) {
        ShoppingItem registered = null;
        try {
            registered = shoppingItemService.createItem(shoppingItemDto, query);
        } catch (ItemAlreadyExistsException e) {
            e.printStackTrace();
            return null;
        }
        return registered;
    }

    @RequestMapping(value = "/shoppingList/{id}/update", method = RequestMethod.GET)
    public ModelAndView changeShoppingItemPage(@PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("shoppingList/changeItem");
        ShoppingItem items = shoppingItemService.findById(id);
        ShoppingItemDto shoppingItemDto = new ShoppingItemDto();
        shoppingItemDto.setName(items.getName());
        shoppingItemDto.setPrice(items.getPrice());
        shoppingItemDto.setDesc(items.getDesc());
//        shoppingItemDto.setIcon(items.getIcon());
        shoppingItemDto.setCurrencyType(items.getCurrencyType());
        shoppingItemDto.setUser(items.getUser().getLogin());
//        shoppingItemDto.setCredit(items.isCredit());
//        shoppingItemDto.setCash(items.isCash());
//        shoppingItemDto.setHidden(items.isHidden());

        List<CurrencyType> currencyTypes = Arrays.asList(CurrencyType.values());
        model.addObject("currencies", currencyTypes);

        model.addObject("shoppingItem", shoppingItemDto);

//        model.addObject("defaultIcons", getDefaultIconsUrl(request));
//        model.addObject("userIcons", getUserIconsUrl(request));
        model.addObject("prevPage", request.getHeader("referer"));
        return model;
    }

    @RequestMapping(value = "/shoppingList/{id}/update", method = RequestMethod.POST)
    public ModelAndView changeAccountPagePost(@PathVariable final Long id, @ModelAttribute("shoppingItem") ShoppingItemDto shoppingItemDto,
                                              BindingResult result,
                                              WebRequest request,
                                              Errors errors) {
        ModelAndView model = new ModelAndView("shoppingList/shoppingList");
        shoppingItemService.merge(shoppingItemDto, id);
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);

        model.addObject("shoppingList", shoppingItemService.getShoppingItems(u));
        return model;
    }


    @RequestMapping(value = "/shoppingList/save", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    public
    @ResponseBody
    GenericResponse newItem1(@RequestBody ShoppingItemDto item) {
        GenericResponse result = new GenericResponse();


        ShoppingItem shoppingItem = null;

        if (isValidShoppingItem(item)) {
            try {
                shoppingItem = shoppingItemService.createItem(item);
            } catch (ItemAlreadyExistsException e) {
                e.printStackTrace();
            }

            if (shoppingItem != null) {
                result.setError("200");
                try {
                    result.setMessage(ConverterJSON.toJSON_String(shoppingItem));
                } catch (JsonProcessingException e) {
                    result.setMessage("JsonProcessingException ");
                    result.setError("500");

                }


            } else {
                result.setError("204");
                result.setMessage("Error!");
            }

        } else {
            result.setError("400");
            result.setMessage("Search criteria is empty!");
        }
        return result;

    }

    private boolean isValidShoppingItem(ShoppingItemDto item) {
        boolean valid = true;
        if (item == null) {
            valid = false;
        }

        if (StringUtils.isEmpty(item.getName())) {
            valid = false;
        }

        return valid;
    }

    @RequestMapping(value = "/item_up", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    @ResponseBody
    public ResponseEntity<ShoppingItem> upItem(@RequestBody ShoppingItemDto itemDto, HttpServletResponse response) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);
        ShoppingItem item=null;
            try {

            item=shoppingItemService.upItem(itemDto.getId(), u);
//            item=shoppingItemService.findById(itemDto.getId());
            } catch (Exception e) {
            e.printStackTrace();
        }

        if (item != null) {
            return new ResponseEntity<ShoppingItem>(item, HttpStatus.OK);
        } else {
            response.setStatus(400);
            return new ResponseEntity<ShoppingItem>(new ShoppingItem(), HttpStatus.BAD_REQUEST);
        }


    }


    @RequestMapping(value = "/item_down", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    @ResponseBody
    public ResponseEntity<ShoppingItem> downItem(@RequestBody ShoppingItemDto itemDto, HttpServletResponse response) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userService.getByUserName(login);
        ShoppingItem item=null;
        try {

            item=shoppingItemService.downItem(itemDto.getId(), u);
//            item=shoppingItemService.findById(itemDto.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (item != null) {
            return new ResponseEntity<ShoppingItem>(item, HttpStatus.OK);
        } else {
            response.setStatus(400);
            return new ResponseEntity<ShoppingItem>(new ShoppingItem(), HttpStatus.BAD_REQUEST);
        }


    }

}

