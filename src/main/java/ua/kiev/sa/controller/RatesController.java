package ua.kiev.sa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.RatesListDto;
import ua.kiev.sa.service.AccountService;
import ua.kiev.sa.service.RateService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.CurrencyRatesDownloader;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

@Controller
public class RatesController {

    @Autowired
    AccountService accountService;
    @Autowired
    UserService userService;
    @Autowired
    RateService rateService;


    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/currencies/user", method = RequestMethod.GET)
    public ModelAndView allUserRates(HttpServletRequest request) {
        ModelAndView model = new ModelAndView("currencies/userRates");

        //getting user from SecurityContext
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User u = userService.getByUserName(login);

        //get list of accounts for user
        List<Account> accounts = accountService.findAllForLogin(login);

        //get list of currencies of user
        Set<CurrencyType> currencyTypes = new HashSet<CurrencyType>();

        //selecting used currencies types
        for (Account a : accounts) {
            currencyTypes.add(a.getCurrencyType());
        }
        currencyTypes.addAll(u.getSettings().getFavouriteCurrencySet());

        //creating dto object for send rates of user
        RatesListDto rl = new RatesListDto();

        //get current user`s rates
        CurrencyRates currencyRates = rateService.getCustomRatesForLogin(login);
        Set<Rate> rates = currencyRates.getRates();
        //create map of currency type and its rate
        Map<CurrencyType, Rate> map;

        //if rates don`t initialized create new map
        if (rates == null) {
            // create map with default rates
            map = createEmptyListOfRatesOfUsedCurrencies(currencyTypes);
            try {
                //get default rates from map
                rates = new HashSet<Rate>(map.values());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //
        List<Rate> newRates = new ArrayList<Rate>();

        //if rates doesn`t contain some currency type, change it rate to default
        assert rates != null;
        for (Rate r : rates) {
            if (!currencyTypes.contains(r.getCurrencyType())) {
                Rate newRate = new Rate();
                newRate.setCurrencyType(r.getCurrencyType());
                newRate.setRate(BigDecimal.ONE);
                newRates.add(newRate);
            }
        }
        //adding this default currency rates to user`s rates
        rates.addAll(newRates);

        //setting rates in dto
        rl.setRates(rates);

        //send info about previous page
        model.addObject("prevPage", request.getHeader("referer"));

        //send info about rates of user
        model.addObject("userRates", rl);

        //send user info
        model.addObject("user1", u);
        //return model
        return model;
    }


    //page where user can change his default currencies rates
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/currencies/user/change", method = RequestMethod.GET)
    public ModelAndView showAllUserRates() {
        //creating view page for show form for editing user`s rates
        ModelAndView model = new ModelAndView("currencies/changeUserRates");
        //get user
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User u = userService.getByUserName(login);

        //creating dto for show user`s currency rates
        RatesListDto ratesListDto = new RatesListDto();

        //getting default currency type
        CurrencyType defaultCurrency = u.getSettings().getDefaultCurrency();

        //getting accounts for current user
        List<Account> accounts = accountService.findAllForLogin(login);

        // creating list for user`s rates
        Set<Rate> rateList = new HashSet<Rate>();

        //creating set for user`s currency types
        Set<CurrencyType> usedCurrencies = new HashSet<CurrencyType>();

        //looping through account`s list for determine used currencies
        for (Account a : accounts) {
            //if set contains currency type if account continue
            if (!usedCurrencies.contains(a.getCurrencyType())) {
                //else if currency of account doesn`t equal default currency
                if (!a.getCurrencyType().equals(defaultCurrency)) {
                    //create and populate new rate
                    Rate r = new Rate();
                    r.setCurrencyType(a.getCurrencyType());
                    r.setRate(BigDecimal.ONE);
                    //add rate to list
                    rateList.add(r);
                    //add currency to list of used currencies
                    usedCurrencies.add(a.getCurrencyType());
                }
            }

        }
        //setting list of rates to dto
        ratesListDto.setRates(rateList);
        //adding dto to model
        model.addObject("userRates", ratesListDto);
        //return model
        return model;
    }

    //saving changed user`s rates
    // and show them on page
//    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/currencies/user/change", method = RequestMethod.POST)
    public ModelAndView saveUserRates(@ModelAttribute("userRates") RatesListDto rates) {
        //creating model for displaying changed user`s rates
        ModelAndView model = new ModelAndView("currencies/userRates");
        //getting user`s login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();

        //if rates input and their size more then 0 save them
        if (null != rates.getRates() && rates.getRates().size() > 0) {
            rateService.save(rates.getRates(), login);
        }
        //create dto for contain rates
        RatesListDto rl = new RatesListDto();
        //getting all user`s rates and setting them for dto
        rl.setRates(rateService.getCustomRatesForLogin(login).getRates());
        //adding rates to model
        model.addObject("userRates", rl);
        return model;
    }

    //create map with default rates
    private Map<CurrencyType, Rate> createEmptyListOfRatesOfUsedCurrencies(Set<CurrencyType> types) {

        Map<CurrencyType, Rate> rateMap = new HashMap<CurrencyType, Rate>();

        for (CurrencyType type : types) {
            Rate r = new Rate();
            r.setCurrencyType(type);
            r.setRate(BigDecimal.ONE);
            rateMap.put(type, r);
        }

        return rateMap;
    }

    //show page with downloaded rates
    @RequestMapping(value = "/currenciesRates", method = RequestMethod.GET)
    public ModelAndView getCurrenciesRates() {


        //creating rates object
        CurrencyRates rates =new CurrencyRates();
        try {
         rates = rateService.getDefaultRates();
        }catch (Exception e){
              e.printStackTrace();
        }
        //creating model
        ModelAndView model = new ModelAndView("currenciesRates");

        //getting list of banks for downloading rates
        List<BankType> bankTypes = Arrays.asList(BankType.values());

        //adding list of banks to model
        model.addObject("banks", bankTypes);

        //adding rates to model
        model.addObject("rateList", rates);

        //return model
        return model;
    }

    public void updateCurrenciesRates(CurrencyRates currencyRates) {

        rateService.save(currencyRates);
    }

}

