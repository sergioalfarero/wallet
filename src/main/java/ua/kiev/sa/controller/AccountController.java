package ua.kiev.sa.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ua.kiev.sa.constants.Constants;
import ua.kiev.sa.exceptions.AccountAlreadyExistsException;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.AccountStat;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.AccountDto;
import ua.kiev.sa.model.dto.AccountWithRatesDto;
import ua.kiev.sa.service.AccountService;
import ua.kiev.sa.service.RateService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.ConverterJSON;
import ua.kiev.sa.utils.GenericResponse;
import ua.kiev.sa.validation.InputValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


@RestController
@EnableWebMvc
public class AccountController {

    @Autowired
    AccountService accountService;

    @Autowired
    UserService userService;

    @Autowired
    RateService rateService;

    /**
     * returns model with information of all account of autorized user
     *
     * @param   login
     *          user's login
     *
     * @param   request
     *          HTTP request object
     */
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public ModelAndView getAllAccountsOfCustomer(Principal login, HttpServletRequest request) {
        ModelAndView model = new ModelAndView("account/accounts");
        try {
            User user = userService.getByUserName(login.getName());
            model.addObject("currencies", user.getSettings().getFavouriteCurrencySet());
            model.addObject("defaultIcons", getDefaultIconsUrl(request));
            model.addObject("userIcons", getUserIconsUrl(login.getName(), request));
            model.addObject("def_curr", user.getSettings().getDefaultCurrency());
            model.addObject("path", request.getServletPath());
            model.addObject("users", login.getName());
            List<AccountWithRatesDto> accountWithRatesDtoList = accountService.getAccountsWithRates(user);
            model.addObject("accounts", accountWithRatesDtoList);
        } catch (Exception e) {
            e.printStackTrace();
            return model;
        }

        return model;
    }

    /**
     * Return model and add information of all account of all users
     * Only user with administrator's permissions can access to this functionality
     *
     * @param   request
     *          HTTP request object
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/administration/all_accounts", method = RequestMethod.GET)
    public ModelAndView getAccountsOfAllUsers(HttpServletRequest request) {
        ModelAndView model = new ModelAndView("account/accounts");
        model.addObject("accounts", accountService.findAll());
        model.addObject("users", userService.findAll());
        model.addObject("path", request.getServletPath());
        return model;
    }


    /**
     * Returns account with specified id
     *
     * @param   id
     *          id of account to return
     *
     */
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "accounts/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    GenericResponse getAccountInfo(@PathVariable("id") final Long id) {
        GenericResponse response = new GenericResponse();
        Account acc = accountService.findById(id);

        if (acc != null) {
            response.setError("200");
            try {
              //:todo next 2 lines???
                acc.setTransactions(null);
                acc.setOperation(null);
                String s = ConverterJSON.toJSON_String(acc);
                response.setMessage(s);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            response.setError("204");
            response.setMessage("Can't return account with this id or it doesn't exists!");
        }
        return response;

    }

    /**
     * Receives dto of account, verifies it and creates new account.
     * If new account is created retrieves GenericResponse object that contains account information
     * Sets 400 error to HttpResponse object if account isn't valid or already exists and adds appropriate message
     *
     * @param   accountDto
     *          data transafer objects from form in a page
     *
     * @param   httpServletResponse
     *          HTTP response object
     *
     */
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "account/save", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    public
    @ResponseBody
    GenericResponse createNewAccount(@RequestBody AccountDto accountDto,
                                     HttpServletResponse httpServletResponse) {
        GenericResponse response = new GenericResponse();

        Account acc;
        if (isValidAccount(accountDto)) {
            acc = createNewAccount(accountDto);

            if (acc != null) {
                response.setError("200");
                AccountWithRatesDto accountWithRatesDto = new AccountWithRatesDto(acc);
                accountWithRatesDto.setStartBalanceConverted(
                        accountService.getBalanceConverted(
                                accountWithRatesDto.getStartBalance(), accountWithRatesDto.getCurrencyType()));

                try {
                    response.setMessage(ConverterJSON.toJSON_String(accountWithRatesDto));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                    return response;
                }

            } else {
                httpServletResponse.setStatus(400);
                response.setError("400");
                response.setMessage("Account with this name already exists");
            }

        } else {
            httpServletResponse.setStatus(400);
            response.setError("400");
            response.setMessage("Some fields are empty or not valid");
        }

        return response;
    }

    /**
     * Updates account with specified id and information provided in AccountDto object
     *
     * @param   id
     *          id of account to return
     *
     * @param   accountDto
     *          dto object of account with information that must be updated
     *
     */
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/account/{id}/update", method = RequestMethod.PUT)
    public
    @ResponseBody
    GenericResponse changeAccountUpdatePost(@PathVariable final Long id,
                                            @RequestBody AccountDto accountDto,
                                            HttpServletResponse httpServletResponse) {
        GenericResponse response = new GenericResponse();
        AccountWithRatesDto accountWithRatesDto = null;
        if (isValidAccount(accountDto)) {
            Account updatedAccount = null;
            try {
                updatedAccount = accountService.merge(accountDto, id);

                accountWithRatesDto = new AccountWithRatesDto(updatedAccount);
                accountWithRatesDto.setStartBalanceConverted(
                        accountService.getBalanceConverted(
                                accountWithRatesDto.getStartBalance(), accountWithRatesDto.getCurrencyType()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (updatedAccount != null) {
                try {
                    response.setError("200");
                    response.setMessage(ConverterJSON.toJSON_String(accountWithRatesDto));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            } else {
                httpServletResponse.setStatus(400);
                response.setError("400");
                response.setMessage("Error while updating account");
            }
        }

        return response;
    }


    /**
     * Deletes account with specified id
     *
     * @param   id
     *          id of account to delete
     *
     * @return  response with id message and appropriate error code
     */
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "accounts/delete/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    GenericResponse deleteAccount(@PathVariable("id") final Long id) {
        GenericResponse response = new GenericResponse();
        //delete account by specified id
        accountService.delete(id);
        //trying to retrieve account with specified id to check if deletion was successful
        Account accountAfterDeletion = accountService.findById(id);

        if (accountAfterDeletion == null) {
            //if deletion was successful
            response.setError("200");
            String s = null;
            try {
                s = ConverterJSON.toJSON_String(id);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            response.setMessage(s);
        } else {
            //if deletion wasn't successful
            response.setError("204");
            response.setMessage("Can't find account with passed id");
        }
        //return response with id message and appropriate error code
        return response;
    }


    /**
     * Finds all accounts of user
     *
     * @param   login
     *          login of user
     *
     * @return list of all accounts of user
     */
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/findAccounts", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> findAllAccountsForUser(Principal login) {
        return accountService.findAllForLogin(login.getName());
    }

    /**
     * Finds current balance of account by its name
     *
     * @param   accountName
     *          name of account
     *
     * @param   login
     *          login of user
     *
     * @return AccountStat object with specified name, balance and currency
     */
    @RequestMapping(value = "/findAccountSum", method = RequestMethod.GET)
    @ResponseBody
    public AccountStat findBalanceOfAccountByName(@RequestParam(value = "accountName", required = true) String accountName, Principal login) {
        Account account =  accountService.findByName(accountName, login.getName());
        if(account==null) return null;
        else return new AccountStat(account.getName(), account.getBalance(), account.getCurrencyType());
    }

    /**
     * Finds current balance of account by its name
     *
     * @param   users
     *          string of user's id separated by comma
     *
     * @return list of all accounts of user
     */
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/findAccountsForUsers", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> findAccountsForUser(@RequestParam(value = "users", required = true) String users) {
        String[] chosenUsers = users.split(",");
        List<String> chosenUserList = Arrays.asList(chosenUsers);

        List<Account> accounts = accountService.findAllForSomeUsers(chosenUserList) ;
        if(!accounts.isEmpty()) return accounts;
        else return null;
    }


    /**
     * Creates and returns model view with attributes of user's accounts,
     *
     * @param   login
     *          user's login
     *
     * @param   request
     *          HTTP request object
     */
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/accountsStat", method = RequestMethod.GET)
    public ModelAndView showAccountsStats(Principal login, HttpServletRequest request) {
        ModelAndView model = new ModelAndView("account/accountsStat");
        model.addObject("accounts", accountService.findAllForLogin(login.getName()));
        model.addObject("users", userService.findAll());
        model.addObject("path", request.getServletPath());
        return model;
    }

    /**
     * Creates account from data transport object and returns it.
     * If account with this name for current user has already created throws AccountAlreadyExistsException
     * and returns null in catch block
     *
     * @param   accountDto
     *          data transfer object of account
     *
     * @throws  AccountAlreadyExistsException
     *          if account, that is trying to create, already exists
     *
     */
    private Account createNewAccount(AccountDto accountDto) {
        Account registered;
        try {
            registered = accountService.createAccount(accountDto);
        } catch (AccountAlreadyExistsException e) {
            e.printStackTrace();
            return null;
        }
        return registered;
    }

    /**
     * Returns list of paths to icons loaded by specified user
     *
     * @param   login
     *          user's login
     *
     * @param   request
     *          HttpRequest object
     *
     * @return  List of paths to icons loaded to server by specified user
     */
    private List<String> getUserIconsUrl(String login, HttpServletRequest request) {
        String str = "/resources/img/" + login + "/banks/";
        File folder = new File(request.getSession().getServletContext().
                getRealPath(str));
        File[] iconsFiles = folder.listFiles();
        if (iconsFiles != null && iconsFiles.length > 0) {
            List<String> fileServerList = new LinkedList<String>();
            for (File f : iconsFiles) {
                String path = f.getPath();
                int i = path.lastIndexOf(File.separator) + 1;
                String sq = str + f.getPath().substring(i);
                fileServerList.add(sq);
            }
            return fileServerList;
        } else return null;
    }

    /**
     * Returns list of paths to default icons
     *
     * @param   request
     *          HttpRequest object
     *
     * @return List of paths to default icons
     */

    private List<String> getDefaultIconsUrl(HttpServletRequest request) {

        File folder = new File(request.getSession().getServletContext().
                getRealPath(Constants.DEFAULT_ICON_PATH));
        File[] iconsFiles = folder.listFiles();

        if(iconsFiles!=null){
            List<String> fileServerList = new LinkedList<>();
            for (File f : iconsFiles) {
                String path = f.getPath();
                int i = path.lastIndexOf(File.separator);
                fileServerList.add(Constants.DEFAULT_ICON_PATH + f.getPath().substring(i));

            }
            return fileServerList;
        }else return null;
    }


    /**
     * returns if the parameter is valid for create account object
     * (object isn't null and name of account isn't blank or null)
     *
     * @param   accountDto
     *          dto object to check
     *
     * @return {@code true} if object is valid else returns false
     */
    private boolean isValidAccount(AccountDto accountDto) {
        boolean valid = true;

        if (accountDto == null) {
           return false;
        }

        if (StringUtils.isEmpty(accountDto.getName())) {
            return false;
        }

        return valid;
    }

}

