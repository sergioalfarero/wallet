package ua.kiev.sa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.exceptions.EmailExistsException;
import ua.kiev.sa.exceptions.TokenNotFoundException;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.VerificationToken;
import ua.kiev.sa.model.dto.UserDto;
import ua.kiev.sa.registration.OnRegistrationCompleteEvent;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.service.VerificationTokenService;
import ua.kiev.sa.utils.GenericResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Locale;

@Controller
public class RegistrationController {

    @Autowired
    ApplicationEventPublisher eventPublisher;
    @Autowired
    private MessageSource messages;
    @Autowired
    private UserService userService;
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private VerificationTokenService verificationTokenService;


    @RequestMapping(value = "/user/registration", method = RequestMethod.GET)
    public String showRegistrationForm(WebRequest request, Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "registration/registration";
    }


    @RequestMapping(value = "/badUser", method = RequestMethod.GET)
    public String showBadUserPage(HttpServletRequest request, HttpServletResponse response,
                                  @RequestParam(value = "error", required = false) String error,
                                  @RequestParam(value = "lang", required = false) String lang,
                                  @RequestParam(value = "message", required = false) String message,
                                  @RequestParam(value = "logout", required = false) String logout) {
        return "error/badUser";
    }

    //todo:doubling code with userDetailsServiceImpl
    @RequestMapping(value = "/user/registration", method = RequestMethod.POST)
    public ModelAndView registerUser(@Valid @ModelAttribute("user") UserDto user,
                                     BindingResult result,
                                     WebRequest request,
                                     Errors errors) {
        if (result.hasErrors()) {
            return new ModelAndView("registration/registration", "user", user);
        }

        User registered = createNewUser(user);

        if (registered == null) {
            result.rejectValue("email", "message.regError");
        }
        try {
            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent
                    (registered, request.getLocale(), appUrl));
        } catch (Exception me) {
            me.printStackTrace();
            return new ModelAndView("error/loginError", "user", user);
        }
        return new ModelAndView("registration/successRegister", "user", user);
    }


    @RequestMapping(value = "registrationConfirm", method = RequestMethod.GET)
    public String confirmRegistration
            (WebRequest request, Model model, @RequestParam("token") String token) {
        Locale locale = request.getLocale();

        VerificationToken verificationToken = null;
        try {
            verificationToken = verificationTokenService.getVerificationToken(token);
        } catch (TokenNotFoundException e) {
            e.printStackTrace();
        }
        if (verificationToken == null) {
            String message = messages.getMessage("auth.message.invalidToken", null, locale);
            model.addAttribute("message", message);
            return "redirect:/badUser.html?lang=" + locale.getLanguage();
        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            model.addAttribute("message", messages.getMessage("auth.message.expired", null, locale));
            model.addAttribute("expired", true);
            model.addAttribute("token", token);
            return "redirect:/badUser.html?lang=" + locale.getLanguage();
        }

        user.setEnabled(true);

        userService.saveOrUpdate(user);
        return "redirect:/login.html?lang=" + locale.getLanguage();
    }


    @RequestMapping(value = "resendRegistrationToken", method = RequestMethod.GET)
    @ResponseBody
    public GenericResponse resendRegistrationToken(
            HttpServletRequest request, @RequestParam("token") String existingToken) {
        VerificationToken newToken = null;
        try {
            newToken = verificationTokenService.generateNewVerificationToken(existingToken);
        } catch (TokenNotFoundException e) {
            e.printStackTrace();
        }

        User user = userService.getUser(newToken.getToken());
        String appUrl =
                "http://" + request.getServerName() +
                        ":" + request.getServerPort() +
                        request.getContextPath();
        SimpleMailMessage email =
                constructResendVerificationTokenEmail(appUrl, request.getLocale(), newToken, user);
        mailSender.send(email);

        return new GenericResponse(
                messages.getMessage("message.resendToken", null, request.getLocale()));
    }


    private SimpleMailMessage constructResendVerificationTokenEmail
            (String contextPath, Locale locale, VerificationToken newToken, User user) {
        String confirmationUrl =
                contextPath + "/registrationConfirm.html?token=" + newToken.getToken();
        String message = messages.getMessage("message.resendToken", null, locale);
        SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject("Resend Registration Token");
        email.setText(message + " rn" + confirmationUrl);
        email.setFrom("123");
        email.setTo(user.getEmail());
        return email;
    }


    private User createNewUser(UserDto user) {
        User registered = null;
        try {
            registered = userService.registerNewUser(user);
        } catch (EmailExistsException e) {
            e.printStackTrace();
            return null;
        }
        return registered;
    }
}
