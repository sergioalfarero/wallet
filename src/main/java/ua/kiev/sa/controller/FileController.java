package ua.kiev.sa.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import ua.kiev.sa.model.FileMeta;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

@RestController
public class FileController {


    LinkedList<FileMeta> files = new LinkedList<FileMeta>();
    FileMeta fileMeta = null;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public
    @ResponseBody
    String upload(MultipartHttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        String str = "/resources/img/" + login + "/banks";
        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf = null;
        String s = "";
        while (itr.hasNext()) {
            mpf = request.getFile(itr.next());
            System.out.println(mpf.getOriginalFilename() + " uploaded! " + files.size());

            if (files.size() >= 10)
                files.pop();

            File folder = new File(request.getSession().getServletContext().
                    getRealPath(str));
            try {
                folder.mkdirs();
                s = folder.getPath() + File.separatorChar + mpf.getOriginalFilename();
                System.out.println(s);
                FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(s));


            } catch (IOException e) {
                e.printStackTrace();
            }
            files.add(fileMeta);
        }
        String str1 = "<img align=\"absmiddle\"\n" + "width=\"24\"\n" + "height=\"24\" src='" + str + "" + File.separatorChar + "" + mpf.getOriginalFilename() + "'/>";

        return str1;
    }

    @ResponseBody
    @RequestMapping(value = "/deleteIcon", method = RequestMethod.POST)
    public String deleteIcon(@RequestBody String url, HttpServletRequest request, HttpServletResponse response) {
        String realPath = request.getSession().getServletContext().getRealPath("");
        String imgUrl = realPath + url.substring(1, url.length() - 1);
        File folder = new File(imgUrl);
        System.out.println(imgUrl);
        folder.delete();
        return "Ok";

    }

   @RequestMapping(value = "/get/{value}", method = RequestMethod.GET)
    public void get(HttpServletResponse response, @PathVariable String value) {

        System.out.println(2);
        FileMeta getFile = files.get(Integer.parseInt(value));
        try {
            response.setContentType(getFile.getFileType());
            response.setHeader("Content-disposition", "attachment; filename=\"" + getFile.getFileName() + "\"");
            FileCopyUtils.copy(getFile.getBytes(), response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
