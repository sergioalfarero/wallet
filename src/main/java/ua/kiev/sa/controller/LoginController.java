package ua.kiev.sa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.Operation;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.AccountSummary;
import ua.kiev.sa.model.dto.AccountWithRatesDto;
import ua.kiev.sa.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private OperationService operationService;
    @Autowired
    private CategoryService categoryService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView homePage() {
        ModelAndView model = new ModelAndView("index");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User u = userService.getByUserName(login);
        List<CurrencyType> currencyTypes = Arrays.asList(CurrencyType.values());
        model.addObject("currencies", currencyTypes);


        if (u != null) {
            long l1=System.currentTimeMillis();
            List<AccountWithRatesDto> accountWithRatesDtoList = accountService.getActiveAccountsWithRates(u);
            long l2=System.currentTimeMillis();

            l1=System.currentTimeMillis();
            List<Operation> operations=operationService.findAll();
            l2=System.currentTimeMillis();

            l1=System.currentTimeMillis();
            AccountSummary summary = accountService.getSummary();
            l2=System.currentTimeMillis();

            l1=System.currentTimeMillis();
            model.addObject("accounts", accountWithRatesDtoList);
            l2=System.currentTimeMillis();

            model.addObject("summary", summary);
            model.addObject("operations", operations);
        }

        long l1=System.currentTimeMillis();
        model.addObject("categories", categoryService.getCategories(login));
        long l2=System.currentTimeMillis();

        return model;

    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginPageWithMessage(HttpServletRequest request, HttpServletResponse response,
                                             @RequestParam(value = "error", required = false) String error,
                                             @RequestParam(value = "message", required = false) String message,
                                             @RequestParam(value = "logout", required = false) String logout) {
        ModelAndView model = new ModelAndView("login/login");
        return model;
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response,
                                             @RequestParam(value = "error", required = false) String error,
                                             @RequestParam(value = "message", required = false) String message,
                                             @RequestParam(value = "logout", required = false) String logout) {
        ModelAndView model = new ModelAndView("/index");
        return model;
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accessDenied() {
        ModelAndView model = new ModelAndView();

        //check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            model.addObject("username", userDetail.getUsername());

        }

        model.setViewName("error/403");
        return model;
    }


    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public ModelAndView userInfo(HttpServletRequest request, HttpServletResponse response,
                                 @RequestParam(value = "error", required = false) String error,
                                 @RequestParam(value = "logout", required = false) String logout) {
        ModelAndView model = new ModelAndView("user/userInfo");
        return model;
    }


    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";//You can redirect wherever you want, but generally it's a good idea to show login screen again.
    }


}



