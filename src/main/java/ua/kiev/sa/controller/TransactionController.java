package ua.kiev.sa.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.exceptions.NoEnoughMoneyAtAccountException;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.Transaction;
import ua.kiev.sa.model.Transaction;
import ua.kiev.sa.model.dto.OperationDto;
import ua.kiev.sa.model.dto.TransactionDto;
import ua.kiev.sa.service.AccountService;
import ua.kiev.sa.service.CategoryService;
import ua.kiev.sa.service.TransactionService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.ConverterJSON;
import ua.kiev.sa.utils.GenericResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@Controller
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @Autowired
    AccountService accountService;

    @Autowired
    UserService userService;

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/transactions", method = RequestMethod.GET)
    public ModelAndView transactionsPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("transaction/transactions");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();

        List<Account> accounts= accountService.findAllNotHiddenForLogin(login);
        model.addObject("accounts", accounts);

        model.addObject("transactions", getTransactions());
        return model;
    }

    private List<Transaction> getTransactions() {
        return transactionService.findAll();

    }

    @RequestMapping(value = "transaction/save", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    public
    @ResponseBody
    GenericResponse addTransaction(@RequestBody TransactionDto transactionDto) {
        GenericResponse response = new GenericResponse();


        Transaction transaction;
        if (isValidTransaction(transactionDto)) {
            transaction= createNewTransaction(transactionDto);


            if (transaction!= null) {
                response.setError("200");
                TransactionDto transactionDto1 = new TransactionDto(transaction);
                try {
                    response.setMessage(ConverterJSON.toJSON_String(transactionDto1));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

            } else {
                response.setError("204");
                response.setMessage("Error!");
            }

        } else {
            response.setError("400");
            response.setMessage("Search criteria is empty!");
        }
        return response;


    }


    private Transaction createNewTransaction(TransactionDto transactionDto) {
        Transaction registered = null;
        try {
            registered = transactionService.createTransaction(transactionDto);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return registered;
    }

    public boolean isValidTransaction(TransactionDto transactionDto) {
        boolean valid = true;
        if (transactionDto == null) {
            valid = false;
        }

        if (StringUtils.isEmpty(transactionDto.getSum())) {
            valid = false;
        }
        if(!transactionService.verifyTransaction(transactionDto.getOutcomingAccount(), transactionDto.getSum())) {
            valid = false;
        }

        return valid;

    }



    @RequestMapping(value = "/addTransaction", method = RequestMethod.GET)
    public ModelAndView addTransactionPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("transaction/addTransaction");

        TransactionDto transactionDto=new TransactionDto();
        model.addObject("transaction", transactionDto);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        List<Account> accounts= (List<Account>) accountService.findAllNotHiddenForLogin(login);
        model.addObject("accounts", accounts);

        return model;
    }

    @RequestMapping(value = "/addTransaction", method = RequestMethod.POST)
    public ModelAndView addTransactionPage(@ModelAttribute ("transaction") TransactionDto transactionDto,
                                           BindingResult result,
                                           WebRequest request,
                                           Errors errors) {
        if(result.hasErrors()){

            return new ModelAndView("transaction/addTransaction", "transaction", transactionDto);
        }

        try {
            Transaction transaction=transactionService.createTransaction(transactionDto);
        } catch (NoEnoughMoneyAtAccountException e) {

            result.rejectValue("account", "no enough money");
            return new ModelAndView("transaction/addTransaction", "transaction", transactionDto);
        }

        ModelAndView model = new ModelAndView("transaction/transactions");

        model.addObject("transaction", transactionDto);
        model.addObject("transactions", getTransactions());
        return model;
    }


    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "transactions/delete/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    GenericResponse deleteTransaction(@PathVariable("id") final Long id) {
        GenericResponse response = new GenericResponse();
        transactionService.delete(id);
        Transaction transaction = transactionService.findById(id);
        if (transaction == null) {
            response.setError("200");
            String s = null;
            try {
                s = ConverterJSON.toJSON_String(id);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            response.setMessage(s);
        } else {
            response.setError("204");
            response.setMessage("Error!");
        }

        return response;
    }

//    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.DELETE)
//    public ModelAndView deleteTransactionPage(@PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
//        ModelAndView model = new ModelAndView("transaction/transactions");
//        transactionService.delete(id);
//        model.addObject("transactions", getTransactions());
//        return model;
//    }
//
//    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.PUT)
//    public ModelAndView changeTransactionPage(@PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
//        ModelAndView model = new ModelAndView("transaction/changeTransaction");
//        Transaction transaction =transactionService.findById(id);
//        TransactionDto transactionDto=new TransactionDto();
//        transactionDto.setAccount(transaction.getAccount().getName());
//        transactionDto.setDate(transaction.getDate().toString());
//        transactionDto.setSubcategory(transaction.getSubcategory().getName());
//        transactionDto.setSum(transaction.getSum().toString());
//        model.addObject("transaction", transactionDto);
//        return model;
//    }

//    @RequestMapping(value = "/transaction/{id}", method = RequestMethod.POST)
//    public ModelAndView changeTransactionPageAfter(@ModelAttribute ("transaction") TransactionDto transactionDto,
//                                               BindingResult result,
//                                               @PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
//        ModelAndView model = new ModelAndView("transaction/transactions");
//        transactionService.merge(transactionDto);
//        model.addObject("transactions", getTransactions());
//        return model;
//    }
}
