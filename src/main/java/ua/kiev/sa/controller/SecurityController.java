package ua.kiev.sa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.exceptions.InvalidOldPasswordException;
import ua.kiev.sa.exceptions.UserNotFoundException;
import ua.kiev.sa.model.PasswordResetToken;
import ua.kiev.sa.model.User;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.GenericResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

@Controller
public class SecurityController {


    @Autowired
    UserService userService;
    @Autowired
    MailSender mailSender;
    @Autowired
    private MessageSource messages;
    @Autowired
    UserDetailsService userDetailsService;


    @RequestMapping(value = "/forgot", method = RequestMethod.GET)
    public ModelAndView resetPasswordPage(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("/registration/forgot");

    }

    @RequestMapping(value = "/forgot1", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    GenericResponse resetPassword(HttpServletRequest request, @RequestParam("email") String userEmail) {

        User user = userService.findUserByEmail(userEmail);
        if (user == null) {
            throw new UserNotFoundException();
        }
        String token = UUID.randomUUID().toString();
        userService.createPasswordResetTokenForUser(user, token);
        String appUrl =
                "http://" + request.getServerName() +
                        ":" + request.getServerPort() +
                        request.getContextPath();
        SimpleMailMessage email =
                constructResetTokenEmail(appUrl, request.getLocale(), token, user);
        try {
            mailSender.send(email);
        } catch (Exception e) {
            e.printStackTrace();
        }

        GenericResponse response = new GenericResponse(messages.getMessage("You should receive an Password Reset Email shortly", null, request.getLocale()));
        return response;
    }

    private SimpleMailMessage constructResetTokenEmail(String appUrl, Locale locale, String token, User user) {
        String url = appUrl + "/changePassword?id=" + user.getId() + "&token=" + token;
        String message = messages.getMessage("message.resetPassword", null, locale);

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(user.getEmail());
        email.setSubject("Reset Password");
        email.setText(message + " rn" + url);
        email.setFrom("support.email");
        return email;
    }

    @RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public String showChangePasswordPage(
            Locale locale, Model model, @RequestParam("id") long id,
            @RequestParam("token") String token) {
        User user = null;
        PasswordResetToken passToken = userService.getPasswordResetToken(token);

        if (passToken != null) {
            user = passToken.getUser();

        }
        if (passToken == null || user.getId() != id) {
            String message = messages.getMessage("auth.message.invalidToken", null, locale);
            model.addAttribute("message", message);
            return "redirect:/login.html?lang=" + locale.getLanguage();
        }

        Calendar cal = Calendar.getInstance();
        if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            model.addAttribute("message", messages.getMessage("auth.message.expired", null, locale));


            return "redirect:/login.html?lang=" + locale.getLanguage();
        }

        Authentication auth = new UsernamePasswordAuthenticationToken(
                user, null, userDetailsService.loadUserByUsername(user.getLogin()).getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

        return "login/updatePassword";
    }


    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
    @ResponseBody
    public GenericResponse changeUserPassword(final Locale locale, @RequestParam("password") final String password, @RequestParam("oldpassword") final String oldPassword) {
        final User user = userService.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        if (!userService.checkIfValidOldPassword(user, oldPassword)) {
            throw new InvalidOldPasswordException();
        }
        userService.changeUserPassword(user, password);
        return new GenericResponse(messages.getMessage("message.updatePasswordSuc", null, locale));
    }


    @RequestMapping(value = "/updatePassword", method = RequestMethod.GET)
    public String changeUserPassword(final Locale locale) {

        return "/updatePassword?lang=" + locale.getLanguage();
    }


    @RequestMapping(value = "/user/savePassword", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    GenericResponse savePassword(Locale locale, @RequestParam("password") String password) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
            userService.changeUserPassword(user, password);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new GenericResponse("message.resetPasswordSuc");
    }

}
