package ua.kiev.sa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.exceptions.EmailExistsException;
import ua.kiev.sa.model.RoleType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.UserInfo;
import ua.kiev.sa.model.dto.AddUserDto;
import ua.kiev.sa.registration.OnRegistrationCompleteEvent;
import ua.kiev.sa.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    ApplicationEventPublisher eventPublisher;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/administration/users", method = RequestMethod.GET)
    public ModelAndView usersPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("user/users");
        model.addObject("users", getUsers());
        return model;
    }

    private List<User> getUsers() {
        List<User> users;
        users = userService.findAll();
        return users;
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addUserPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("user/addUser");
        AddUserDto addUserDto = new AddUserDto();
        List<RoleType> roleTypes = Arrays.asList(RoleType.values());
        model.addObject("roleTypes", roleTypes);
        model.addObject("user", addUserDto);
        return model;
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addUserPage(@ModelAttribute("user") AddUserDto addUserDto,
                                    BindingResult result,
                                    WebRequest request,
                                    Errors errors) throws EmailExistsException {
        ModelAndView model = new ModelAndView("user/users");
        User u = userService.createUser(addUserDto);

        if (u == null) {
            result.rejectValue("email", "message.regError");
        }

        try {
            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent
                    (u, request.getLocale(), appUrl));
        } catch (Exception me) {
            me.printStackTrace();
            return new ModelAndView("user/addUser");
        }
        model.addObject("userDto", addUserDto);
        model.addObject("users", getUsers());
        return model;
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ModelAndView addUserInfo(@PathVariable("id") long id, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("/user/user_information");
        UserInfo info = userService.getUserInformation(id);
        model.addObject("userinfo", info);
        return model;
    }


//
//    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
//    public ModelAndView deleteUserPage(@PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
//        ModelAndView model = new ModelAndView("user/users");
//        userService.delete(id);
//        model.addObject("users", getUsers());
//        return model;
//    }
//
//    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
//    public ModelAndView changeUserPage(@PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
//        ModelAndView model = new ModelAndView("user/changeUser");
//        User user =userService.findById(id);
//        UserDto userDto=new UserDto();
//        userDto.setName(user.getName());
//        userDto.setBalance(user.getBalance());
//        userDto.setCurrencyType(user.getCurrencyType());
//        userDto.setUsername(user.getUser().getLogin());
//        model.addObject("user", userDto);
//        return model;
//    }
//
//    @RequestMapping(value = "/user/{id}", method = RequestMethod.POST)
//    public ModelAndView changeUserPageAfter(@ModelAttribute ("user") UserDto userDto,
//                                               BindingResult result,
//                                               @PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
//        ModelAndView model = new ModelAndView("user/users");
//        userService.merge(userDto);
//        model.addObject("users", getUsers());
//        return model;
//    }
}

