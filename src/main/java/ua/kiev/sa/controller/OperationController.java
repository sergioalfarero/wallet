package ua.kiev.sa.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.poi.hssf.record.crypto.Biff8Cipher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.constants.Constants;
import ua.kiev.sa.exceptions.NoEnoughMoneyAtAccountException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.OperationCriteriaDto;
import ua.kiev.sa.model.dto.OperationDto;
import ua.kiev.sa.service.AccountService;
import ua.kiev.sa.service.CategoryService;
import ua.kiev.sa.service.OperationService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.ConverterJSON;
import ua.kiev.sa.utils.GenericResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

@Controller
public class OperationController {

    @Autowired
    OperationService operationService;
    @Autowired
    AccountService accountService;
    @Autowired
    UserService userService;
    @Autowired
    CategoryService categoryService;

    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/operations", method = RequestMethod.GET)
    public ModelAndView operationsPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("operation/operations");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userService.getByUserName(login);
        List<Account> accounts = accountService.findAllNotHiddenForLogin(login);
        model.addObject("accounts", accounts);
        model.addObject("currencies", user.getSettings().getFavouriteCurrencySet());


        List<Operation> operations = getOperations();
        List<Operation> pageOfOperations=getDefaultPageOfOperations();

        try {
            model.addObject("operations", pageOfOperations);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int size=operations.size();
        int pageCount;
//        if(size%Constants.NUMBER_OF_OPERATIONS_TO_SHOW==0){
            pageCount = size/ Constants.NUMBER_OF_OPERATIONS_TO_SHOW+1;
        if(size%Constants.NUMBER_OF_OPERATIONS_TO_SHOW==0) model.addObject("fullPage", 1);
        else model.addObject("fullPage", 0);
//             pageCount = size/ Constants.NUMBER_OF_OPERATIONS_TO_SHOW+1;
//        }
//        if(operations.size()%Constants.NUMBER_OF_OPERATIONS_TO_SHOW>0) pageCount++;
        model.addObject("numberToShowOnPage", Constants.NUMBER_OF_OPERATIONS_TO_SHOW);
        model.addObject("numberOfItems", size);
        model.addObject("numberOfPages", pageCount);
        model.addObject("currentPage", 1);

        return model;
    }
    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/operations", params = "page", method = RequestMethod.GET)
    public @ResponseBody GenericResponse getOperationsPage(@RequestParam(value="page", required = true) Long page, HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userService.getByUserName(login);
        GenericResponse result=new GenericResponse();
        List<Operation> pageOfOperations=getPageOfOperations(page);
        if (pageOfOperations!= null) {
            result.setError("200");
            try {

                String s = ConverterJSON.toJSON_String(pageOfOperations);
                result.setMessage(s);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            result.setError("204");
            result.setMessage("Error!");
        }
        return result;
    }

    private List<Operation> getDefaultPageOfOperations() {
        List<Operation> operations;
        operations = operationService.findPageOfOperation(0, Constants.NUMBER_OF_OPERATIONS_TO_SHOW);
        return operations;

    }
    private List<Operation> getPageOfOperations(Long page) {
        List<Operation> operations;
        operations = operationService.findPageOfOperation(page.intValue()-1, Constants.NUMBER_OF_OPERATIONS_TO_SHOW);
        return operations;

    }
    private List<Operation> getOperations() {
        List<Operation> operations;
        operations = operationService.findAll();
        return operations;
    }

    @RequestMapping(value = "/addOperation", method = RequestMethod.GET)
    public ModelAndView addOperationPage(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("operation/addOperation");

        OperationDto operationDto = new OperationDto();
        model.addObject("operation", operationDto);

        List<CurrencyType> currencyTypes = Arrays.asList(CurrencyType.values());
        model.addObject("currencies", currencyTypes);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        List<Account> accounts = (List<Account>) accountService.findAllForLogin(login);
        model.addObject("accounts", accounts);
        model.addObject("categories", categoryService.getCategories(login));

        return model;
    }

    @RequestMapping(value = "/addOperation", method = RequestMethod.POST)
    public ModelAndView addOperationPage(@ModelAttribute("operation") OperationDto operationDto,
                                         BindingResult result,
                                         WebRequest request,
                                         Errors errors) {
        if (result.hasErrors()) {

            return new ModelAndView("operation/addOperation", "operation", operationDto);
        }

        try {
            Operation operation = operationService.createOperation(operationDto);
        } catch (NoEnoughMoneyAtAccountException e) {

            result.rejectValue("account", "no enough money");
            return new ModelAndView("operation/addOperation", "operation", operationDto);
        }

        ModelAndView model = new ModelAndView("operation/operations");

        model.addObject("operation", operationDto);
        model.addObject("operations", getOperations());
        return model;
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "operations/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    GenericResponse getOperationInfo(@PathVariable("id") final Long id) {
        GenericResponse response = new GenericResponse();
        Operation operation = operationService.findById(id);
        if (operation != null) {
            response.setError("200");
            try {

                String s = ConverterJSON.toJSON_String(operation);
                response.setMessage(s);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            response.setError("204");
            response.setMessage("Error!");
        }

        return response;
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "operations/change/{id}", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    GenericResponse changeOperation(@PathVariable("id") final Long id, @RequestBody OperationDto operationDto, HttpServletResponse httpServletResponse) {
        GenericResponse response = new GenericResponse();

        Operation updatedOperation = null;
        try {
            updatedOperation = operationService.updateOperation(id, operationDto);
        } catch (NoEnoughMoneyAtAccountException e) {
            response.setError("400");
            response.setMessage(e.getMessage());
//            return response;
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            try {

                httpServletResponse.getWriter().write(ConverterJSON.toJSON_String(response));
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            return response;
        }

        if (updatedOperation != null) {
            response.setError("200");

        } else {
            response.setError("204");
            response.setMessage("Error!");
        }

        return response;
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "operations/delete/{id}", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    GenericResponse deleteOperation(@PathVariable("id") final Long id) {
        GenericResponse response = new GenericResponse();
        operationService.delete(id);
        Operation oper = operationService.findById(id);
        if (oper == null) {
            response.setError("200");
            String s = null;
            try {
                s = ConverterJSON.toJSON_String(id);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            response.setMessage(s);
        } else {
            response.setError("204");
            response.setMessage("Error!");
        }

        return response;
    }

    @RequestMapping(value = "operation/save", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    public
    @ResponseBody
    GenericResponse addAccountPost(@RequestBody OperationDto operationDto) {
        GenericResponse response = new GenericResponse();


        Operation operation;
        if (isValidOperation(operationDto)) {
            operation = createNewOperation(operationDto);


            if (operation != null) {
                response.setError("200");
                OperationDto operationDto1 = new OperationDto(operation);
                try {
                    response.setMessage(ConverterJSON.toJSON_String(operationDto1));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

            } else {
                response.setError("204");
                response.setMessage("Error!");
            }

        } else {
            response.setError("400");
            response.setMessage("Search criteria is empty!");
        }
        return response;


    }

    private Operation createNewOperation(OperationDto operationDto) {
        Operation registered = null;
        try {
            registered = operationService.createOperation(operationDto);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return registered;
    }

    public boolean isValidOperation(OperationDto operationDto) {
        boolean valid = true;
        if (operationDto == null) {
            valid = false;
        }

        if (StringUtils.isEmpty(operationDto.getSum())) {
            valid = false;
        }

        return valid;
    }

    @RequestMapping(value = "/operation/{id}", method = RequestMethod.DELETE)
    public ModelAndView deleteOperationPage(@PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("operation/operations");
        operationService.delete(id);
        model.addObject("operations", getOperations());
        return model;
    }

    @RequestMapping(value = "/operation/{id}", method = RequestMethod.PUT)
    public ModelAndView changeOperationPage(@PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("operation/changeOperation");
        Operation operation = operationService.findById(id);
        OperationDto operationDto = new OperationDto();
        operationDto.setAccount(operation.getAccount().getName());
        operationDto.setDate(operation.getDate().toString());
        operationDto.setSubcategory(operation.getSubcategory().getName());
        operationDto.setSum(operation.getSum().toString());
        model.addObject("operation", operationDto);
        return model;
    }

    @RequestMapping(value = "/operation/{id}", method = RequestMethod.POST)
    public ModelAndView changeOperationPageAfter(@ModelAttribute("operation") OperationDto operationDto,
                                                 BindingResult result,
                                                 @PathVariable final Long id, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("operation/operations");
        operationService.save(operationDto);
        model.addObject("operations", getOperations());
        return model;
    }

    @PreAuthorize("hasRole('ROLE_CUSTOMER')")
    @RequestMapping(value = "/operationsStat", method = RequestMethod.GET)
    public ModelAndView showAccountsStats(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView model = new ModelAndView("operation/operationsStat");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user = userService.getByUserName(login);

        model.addObject("categories", getUniqueCategories(user));
        model.addObject("sources", null);
        model.addObject("accounts", accountService.findAllForLogin(user.getLogin()));
        model.addObject("users", userService.findAll());
        model.addObject("currencies", user.getSettings().getFavouriteCurrencySet());
        model.addObject("path", request.getServletPath());
        return model;
    }

    @RequestMapping(value = "/balanceOfOperations", method = RequestMethod.POST, produces = "application/json", headers = {"content-type=application/json"})
    @ResponseBody
    public GenericResponse findAllSubcategories(@RequestBody OperationCriteriaDto operationCriteriaDto) {
        GenericResponse response = new GenericResponse();
        if (operationCriteriaDto != null) {
            Map<String, BigDecimal> stats = operationService.getReport(operationCriteriaDto);
            List<Operation> operations = operationService.getOperations(operationCriteriaDto);
            response.setError("200");
            OperationResult result = new OperationResult();
            result.setOperations(operations);
            result.setStats(stats);
            try {
//                System.out.println(ConverterJSON.toJSON_Map(stats));
                String res = ConverterJSON.toJSON_String(result);
//                System.out.println(ConverterJSON.toJSON_String(operations));

                response.setMessage(res);

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        } else {
            response.setError("400");
            response.setMessage("Search criteria is empty!");
        }
        return response;

    }


    @RequestMapping(value = "/operations", params = {"page", "count"}, method = RequestMethod.GET)
    @ResponseBody
    public GenericResponse operationsPage(@RequestParam("page") int page,
                                          @RequestParam("count") int count) {
        GenericResponse response = new GenericResponse();
        List<Operation> operations = operationService.findPageOfOperation(page, count);
        try {
            String s = ConverterJSON.toJSON_String(operations);
            response.setMessage(s);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            response.setError("400");
            response.setMessage("Error when extracting pageble data from DB");
        }

        return response;
    }


    private List<Category> getUniqueCategories(User user) {
        return categoryService.getCategoriesOfOperations(user);
    }
}

