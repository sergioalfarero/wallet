package ua.kiev.sa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.AccountStat;
import ua.kiev.sa.model.User;
import ua.kiev.sa.service.AccountService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.utils.CurrenciesConverter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class AccountStatController {

    @Autowired
    AccountService accountService;
    @Autowired
    UserService userService;

    @RequestMapping(value = "/balanceOfAccounts", method = RequestMethod.GET)
    public
    @ResponseBody
    List<AccountStat> getBalanceOfAccounts(@RequestParam(value = "accounts", required = true) String accounts) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User u = userService.getByUserName(login);
        List<Account> allAccounts = accountService.findAllForLogin(login);
        String[] chosenAccounts = accounts.split(",");
        List<String> result = Arrays.asList(chosenAccounts);
        List<AccountStat> stats = new ArrayList<AccountStat>();
        CurrenciesConverter converter = new CurrenciesConverter(accountService.getRateMap(u), u);
        for (Account acc : allAccounts) {

            if (result.contains(acc.getName())) {
                BigDecimal balance = acc.getBalance();
                if (balance.compareTo(BigDecimal.ZERO) >= 0) {


                    stats.add(new AccountStat(acc.getName(),
                            converter.convert(balance, acc.getCurrencyType(),
                                    u.getSettings().getDefaultCurrency()).toString()));

                }

            }
        }

        System.out.println(stats);
        return stats;
    }

    @RequestMapping(value = "/balanceOfAccountsForSelectedUsers", method = RequestMethod.GET)
    public
    @ResponseBody
    List<AccountStat> getBalanceOfAccountsForSelectedUsers(@RequestParam(value = "accounts", required = true) String accounts, @RequestParam(value = "users", required = true) String users) {

        List<Account> allAccounts = accountService.findAll();

        String[] chosenAccounts = accounts.split(",");
        String[] chosenUsers = users.split(",");

//        Map<CurrencyType, Rate> map = accountService.getRateMap(u);
//        CurrenciesConverter converter = new CurrenciesConverter(map, u);


        List<String> resultAcc = Arrays.asList(chosenAccounts);
        List<String> resultUsers = Arrays.asList(chosenUsers);
        List<AccountStat> stats = new ArrayList<AccountStat>();

        for (Account acc : allAccounts) {
            if (resultAcc.contains(acc.getName())) {
                if (resultUsers.contains(acc.getUser().getLogin())) {
                    if (acc.getBalance().compareTo(BigDecimal.ZERO) >= 0) {
                        System.out.println(acc.getName());
                        stats.add(new AccountStat(acc.getName(), acc.getBalance().toString()));

                    }
                }


            }
        }


        return stats;
    }
}

