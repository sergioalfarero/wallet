package ua.kiev.sa.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class BalanceValidator implements ConstraintValidator<ValidBalance, BigDecimal> {


    public void initialize(ValidBalance constraintAnnotation) {

    }

    public boolean isValid(BigDecimal value, ConstraintValidatorContext context) {
        return validateBalance(value);
    }

    private boolean validateBalance(BigDecimal value) {

        return true;
    }
}
