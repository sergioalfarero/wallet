package ua.kiev.sa.validation;


public class InputValidator {



    public boolean isValid(Object dtoObject, String  ... fields ){
        boolean result =true;
        if (isNull(dtoObject)) return false;

        for(String s:fields){
            if (!isFieldValid(dtoObject, s)) return false;
        }
        return result;
    }

    /**
     * if object isn't null returns true else returns false
     * */
    public boolean isNull(Object dtoObject){
        return dtoObject==null;
    }

    /**
     * returns true if field of object isn't null. else returns false
     * */
    public boolean isFieldValid(Object dtoObject, String field){

        Object result = null;
        try {
            result=dtoObject.getClass().getDeclaredMethod("get"+field);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return !isNull(result);

    };

}
