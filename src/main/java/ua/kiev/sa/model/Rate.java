package ua.kiev.sa.model;


import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.math.BigDecimal;

@Embeddable
@Table(name = "RATE",schema = Constants.SCHEMA)
public class Rate {




    @Column(name = "RATE_CURRENCY_TYPE", length = 3, nullable = false)
    @Enumerated(value = EnumType.STRING)
    private CurrencyType currencyType;

    @Column(name = "RATE_COUNT", precision = 10, scale = 4, nullable = false)
    private BigDecimal rate;


    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "currencyType=" + currencyType +
                ", rate=" + rate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rate rate1 = (Rate) o;

        if (currencyType != rate1.currencyType) return false;
        return !(rate != null ? !rate.equals(rate1.rate) : rate1.rate != null);

    }

    @Override
    public int hashCode() {
        int result = currencyType != null ? currencyType.hashCode() : 0;
        result = 31 * result + (rate != null ? rate.hashCode() : 0);
        return result;
    }
}
