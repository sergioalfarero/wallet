package ua.kiev.sa.model;


import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.NotEmpty;
import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PERMISSION", schema = Constants.SCHEMA)
public class Permission {

    @Id
    @Column(name = "PERMISSION_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotEmpty
    @Column(name = "PERMISSION_NAME", unique = true)
    private String permissionName;


    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REFRESH})
    @JoinTable(name = "PERMISSION_GROUP", schema = Constants.SCHEMA, joinColumns = {
            @JoinColumn(name = "PERMISSION_ID", nullable = false, updatable = false)}
            ,
            inverseJoinColumns = {@JoinColumn(name = "GROUP_ID",
                    nullable = false, updatable = false)}
    )
    @Cascade({
            org.hibernate.annotations.CascadeType.DETACH,
            org.hibernate.annotations.CascadeType.LOCK,
            org.hibernate.annotations.CascadeType.REFRESH,
            org.hibernate.annotations.CascadeType.REPLICATE

    })
    private List<Role> roles = new ArrayList<Role>();


    @Embedded
    private AuditSection auditSection = new AuditSection();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public AuditSection getAuditSection() {
        return auditSection;
    }

    public void setAuditSection(AuditSection auditSection) {
        this.auditSection = auditSection;
    }


}
