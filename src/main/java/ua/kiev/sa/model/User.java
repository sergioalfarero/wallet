package ua.kiev.sa.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;
import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "USER", schema = Constants.SCHEMA)
public class User implements Serializable {

    @Id
    @Column(name = "USER_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    @Column(name = "USER_LOGIN", length = 100, unique = true)
    private String login;


    @NotEmpty
    @Column(name = "USER_EMAIL")
    private String email;


    @NotEmpty
    @Column(name = "USER_PASSWORD", length = 200)
    private String password;

    @JsonIgnore
    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Role role;

    @Column(name = "ENABLED")
    private boolean enabled;


    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<Account> accounts;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<Operation> operations;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<Transaction> transactions;


    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<Category> categories;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<IncomeSource> sources;

    @OneToOne(cascade = CascadeType.ALL)
    private UserInfo userInfo;

    @OneToOne(cascade = {CascadeType.ALL})
    private VerificationToken verificationToken;


    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL)
    private List<ShoppingListQuery> shoppingListQueries;


    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    private CurrencyRates currencyRates;


    @OneToOne(cascade = CascadeType.ALL)
    private UserSettings settings;

    public UserSettings getSettings() {
        return settings;
    }

    public void setSettings(UserSettings settings) {
        this.settings = settings;
    }

    public CurrencyRates getCurrencyRates() {

        return currencyRates;
    }

    public void setCurrencyRates(CurrencyRates currencyRates) {
        this.currencyRates = currencyRates;
    }

    public VerificationToken getVerificationToken() {
        return verificationToken;
    }

    public void setVerificationToken(VerificationToken verificationToken) {
        this.verificationToken = verificationToken;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<IncomeSource> getSources() {
        return sources;
    }

    public void setSources(List<IncomeSource> sources) {
        this.sources = sources;
    }

    public User() {
        super();
        this.enabled = false;
    }


    @Override
    public String toString() {
        return "User{" +
                "enabled=" + enabled +
                ", role=" + role +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", login='" + login + '\'' +
                ", id=" + id +
                ", currencyRates=" + currencyRates +
                ", settings=" + settings +
                '}';
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<ShoppingListQuery> getShoppingListQueries() {
        return shoppingListQueries;
    }

    public void setShoppingListQueries(List<ShoppingListQuery> shoppingListQueries) {
        this.shoppingListQueries = shoppingListQueries;


    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
