package ua.kiev.sa.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "SHOPPING_QUERY", schema = Constants.SCHEMA,
        uniqueConstraints = @UniqueConstraint(columnNames = {"QUERY_NAME", "user_user_id"}))
public class ShoppingListQuery {


    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "QUERY_NAME")
    private String name;

    @Column(name = "QUERY_DESC")
    private String desc;

    @Column(name = "QUERY_SUM")
    private BigDecimal sum;

    @Column(name = "QUERY_CT")
    private CurrencyType type;

    @JsonIgnore
    @OneToMany(mappedBy = "shoppingListQuery", cascade = CascadeType.ALL, fetch = FetchType.EAGER /*,orphanRemoval = true*/)
    private List<ShoppingItem> items;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.MERGE})
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ShoppingItem> getItems() {
        return items;
    }

    public void setItems(List<ShoppingItem> items) {
        this.items = items;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public CurrencyType getType() {
        return type;
    }

    public void setType(CurrencyType type) {
        this.type = type;
    }

    public void addItem(ShoppingItem item) {
        items.add(item);
        item.setShoppingListQuery(this);
    }

    public void removeItem(ShoppingItem item) {
        item.setShoppingListQuery(null);
        this.items.remove(item);
    }

    @Override
    public String toString() {
        return "ShoppingListQuery{" +
                "user=" + user.getLogin() +
                ", items=" + items +
                ", type=" + type +
                ", sum=" + sum +
                ", desc='" + desc + '\'' +
                ", name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}

