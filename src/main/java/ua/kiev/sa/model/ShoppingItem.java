package ua.kiev.sa.model;


import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "SHOPPING_ITEM", schema = Constants.SCHEMA,
        uniqueConstraints = @UniqueConstraint(columnNames = {"ITEM_NAME", "query_id", "user_user_id"}))
public class ShoppingItem {

    @Id
    @Column(name = "ITEM_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ITEM_NAME")
    private String name;

    @Column(name = "ITEM_DESC")
    private String desc;

    @Column(name = "ITEM_PRICE")
    private BigDecimal price;

    @Column(name = "ITEM_PRICE_CURRENCY")
    private CurrencyType currencyType;

    @Column(name = "ITEM_PRIORITY")
    private Integer priority;

    @ManyToOne(targetEntity = ShoppingListQuery.class, fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "query_id", nullable = false)
    private ShoppingListQuery shoppingListQuery;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    public ShoppingListQuery getShoppingListQuery() {
        return shoppingListQuery;
    }

    public void setShoppingListQuery(ShoppingListQuery shoppingListQuery) {
        this.shoppingListQuery = shoppingListQuery;
    }

    public Integer getPriority() {

        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "ShoppingItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", price=" + price +
                ", currencyType=" + currencyType +
                ", priority=" + priority +
                ", shoppingListQuery=" + shoppingListQuery.getId() +
                ", user=" + user.getId() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShoppingItem that = (ShoppingItem) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return !(name != null ? !name.equals(that.name) : that.name != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
