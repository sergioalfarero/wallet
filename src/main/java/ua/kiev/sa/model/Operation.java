package ua.kiev.sa.model;

import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "OPERATION", schema = Constants.SCHEMA)
public class Operation {

    @Id
    @Column(name = "OPERATION_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "OPERATION_DATE")
    private Date date;


    @Column(name = "OPERATION_SUMM")
    private BigDecimal sum;

    @Column(name = "OPERATION_CURRENCY")
    @Enumerated(value = EnumType.STRING)
    private CurrencyType currency;

    @ManyToOne
    private Subcategory subcategory;


    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
    private Account account;

    @ManyToOne
    private User user;

    @Override
    public String toString() {
        return "Operation{" +
                "id=" + id +
                ", date=" + date +
                ", sum=" + sum +
                ", currency=" + currency +
                ", subcategory=" + subcategory.getName() +
                ", account=" + account.getName() +
                ", user=" + user.getLogin() +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyType currency) {
        this.currency = currency;
    }
}



