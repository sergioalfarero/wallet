package ua.kiev.sa.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "INCOME_SOURCE", schema = Constants.SCHEMA, uniqueConstraints = {@UniqueConstraint(columnNames = {"SOURCE_NAME", "user_user_id"})})
public class IncomeSource implements Comparable {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "SOURCE_NAME")
    private String name;


    @JsonIgnore
    @OneToMany(mappedBy = "source", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Subsource> subsources = new ArrayList<Subsource>();

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.MERGE})
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "IncomeSource{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", subsources=" + subsources +
                ", user=" + user +
                '}';
    }

    public List<Subsource> getSubsources() {
        return subsources;
    }

    public void setSubsources(List<Subsource> subsources) {
        this.subsources = subsources;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }



    public int compareTo(Object o) {
        return this.name.compareTo(((IncomeSource) o).getName());
    }
}
