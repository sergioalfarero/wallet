package ua.kiev.sa.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by sergio on 10.04.17.
 */
public class OperationResult {
    Map<String, BigDecimal> stats;
    List<Operation> operations;

    public Map<String, BigDecimal> getStats() {
        return stats;
    }

    public void setStats(Map<String, BigDecimal> stats) {
        this.stats = stats;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    @Override
    public String toString() {
        return "OperationResult{" +
                "stats=" + stats +
                ", operations=" + operations +
                '}';
    }
}
