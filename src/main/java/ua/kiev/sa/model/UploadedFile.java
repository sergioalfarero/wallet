package ua.kiev.sa.model;

import org.springframework.web.multipart.MultipartFile;

public class UploadedFile {

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        System.out.println("setFile");
        this.file = file;
    }
}