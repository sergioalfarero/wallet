package ua.kiev.sa.model;

import java.math.BigDecimal;

public class AccountStat {

    private String name;

    private String balance;

    public CurrencyType getType() {
        return type;
    }

    public void setType(CurrencyType type) {
        this.type = type;
    }

    private CurrencyType type;

    public AccountStat() {
    }

    public AccountStat(String name, String balance) {
        this.name = name;
        this.balance = balance;
    }



    public AccountStat(String name, BigDecimal bigDecimal, CurrencyType currencyType) {
        this.name=name;
        this.balance=bigDecimal.toPlainString();
        this.type=currencyType;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "AccountStat{" +
                "name='" + name + '\'' +
                ", balance='" + balance + '\'' +
                '}';
    }
}
