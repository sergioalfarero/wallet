package ua.kiev.sa.model;


import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "GROUP", schema = Constants.SCHEMA)
public class Role implements Serializable {

    @Id
    @Column(name = "GROUP_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "GROUP_TYPE")
    @Enumerated(value = EnumType.STRING)
    private RoleType roleType;

//    @NotEmpty
//    @Column(name="GROUP_NAME", unique=true)
//    private String groupName;

//    @ManyToMany(mappedBy = "groups")
//    private Set<Permission> permissions = new HashSet<Permission>();

    @Embedded
    private AuditSection auditSection = new AuditSection();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return "Role{" +
                "roleType=" + roleType +
                '}';
    }
//    public String getGroupName() {
//        return groupName;
//    }
//
//    public void setGroupName(String groupName) {
//        this.groupName = groupName;
//    }

//    public Set<Permission> getPermissions() {
//        return permissions;
//    }

//    public void setPermissions(Set<Permission> permissions) {
//        this.permissions = permissions;
//    }

    public AuditSection getAuditSection() {
        return auditSection;
    }

    public void setAuditSection(AuditSection auditSection) {
        this.auditSection = auditSection;
    }

    public Role(RoleType role, User user) {
        this.roleType = role;
        this.user = user;
    }

    public Role() {
    }


}
