package ua.kiev.sa.model.builders;

import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.AccountDto;

import java.math.BigDecimal;


public class AccountBuilder {

    private Account model;

    public AccountBuilder(){
        model=new Account();
    }

    public AccountBuilder(AccountDto accountDto){
        model=new Account();
        model.setStartBalance(accountDto.getStartBalance());
        model.setName(accountDto.getName());
        model.setStartBalance(accountDto.getStartBalance());
        model.setCurrencyType(accountDto.getCurrencyType());
        model.setDescription(accountDto.getDescription());
        model.setCredit(accountDto.isCredit());
        model.setCash(accountDto.isCash());
        model.setHidden(accountDto.isHidden());
    }

    public AccountBuilder setId(Long id){
        model.setId(id);
        return this;
    }

    public AccountBuilder setName(String name){
        model.setName(name);
        return this;
    }

    public AccountBuilder setSum(BigDecimal sum, CurrencyType currencyType){
        model.setStartBalance(sum);
        model.setCurrencyType(currencyType);
        model.setBalance(sum);
        return this;
    }

    public AccountBuilder setDesc(String desc){
        model.setDescription(desc);
        return this;
    }

    public AccountBuilder setOptions(boolean isCredit, boolean isCash, boolean isHidden){
        model.setCredit(isCredit);
        model.setCash(isCash);
        model.setHidden(isHidden);
        return this;
    }

    public AccountBuilder setUser(User user){
        model.setUser(user);
        return this;
    }



    public Account build(){
        return model;
    }


}
