package ua.kiev.sa.model.builders;


import ua.kiev.sa.model.*;

import java.util.List;

public class UserBuilder {

    private User model;

    public UserBuilder(){
        model=new User();
    }

    public UserBuilder setId(Long id){
        model.setId(id);
        return this;
    }

    public UserBuilder setCredentials(String login, String password, Role role){
        model.setLogin(login);
        model.setPassword(password);
        model.setRole(role);
        return this;
    }

    public UserBuilder setSettings(UserSettings settings){
        model.setSettings(settings);
        return this;
    }

    public UserBuilder setEnabled(boolean isEnabled){
        model.setEnabled(isEnabled);
        return this;
    }

   public UserBuilder setAccounts(List<Account> accounts){
        model.setAccounts(accounts);
        return this;
   }


    public User build(){
        return model;
    }

}
