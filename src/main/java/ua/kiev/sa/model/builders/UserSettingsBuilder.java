package ua.kiev.sa.model.builders;

import ua.kiev.sa.model.*;

import java.util.Set;


public class UserSettingsBuilder {

    private UserSettings model;

    public UserSettingsBuilder(){
        model=new UserSettings();
    }

    public UserSettingsBuilder setId(Long id){
        model.setId(id);
        return this;
    }

    public UserSettingsBuilder setBank(BankType bank){
        model.setBank(bank);
        return this;
    }

    public UserSettingsBuilder setDefaultCurrency(CurrencyType currencyType){
        model.setDefaultCurrency(currencyType);
        return this;
    }

    public UserSettingsBuilder setFavouriteCurrencySet(Set currencySet){
        model.setFavouriteCurrencySet(currencySet);
        return this;
    }

    public UserSettings build(){
        return model;
    }


}
