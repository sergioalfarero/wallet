package ua.kiev.sa.model.builders;

import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.AccountDto;

import java.math.BigDecimal;


public class AccountDtoBuilder {

    private AccountDto model;

    public AccountDtoBuilder(){
        model=new AccountDto();
    }

    public AccountDtoBuilder(Account account){
        model=new AccountDto(account);
    }

//    public AccountDtoBuilder setId(Long id){
//        model.setId(id);
//        return this;
//    }
//
//    public AccountDtoBuilder setName(String name){
//        model.setName(name);
//        return this;
//    }
//
//    public AccountDtoBuilder setSum(BigDecimal sum, CurrencyType currencyType){
//        model.setStartBalance(sum);
//        model.setCurrencyType(currencyType);
//        model.setBalance(sum);
//        return this;
//    }
//
//    public AccountDtoBuilder setDesc(String desc){
//        model.setDescription(desc);
//        return this;
//    }
//
//    public AccountDtoBuilder setOptions(boolean isCredit, boolean isCash, boolean isHidden){
//        model.setCredit(isCredit);
//        model.setCash(isCash);
//        model.setHidden(isHidden);
//        return this;
//    }
//
//    public AccountDtoBuilder setUser(User user){
//        model.setUser(user);
//        return this;
//    }
//
//

    public AccountDto build(){
        return model;
    }


}
