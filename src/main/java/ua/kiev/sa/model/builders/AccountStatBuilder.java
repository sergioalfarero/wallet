package ua.kiev.sa.model.builders;


import ua.kiev.sa.model.AccountStat;
import ua.kiev.sa.model.CurrencyType;

import java.math.BigDecimal;

public class AccountStatBuilder {

    private AccountStat model;

    public AccountStatBuilder setName(String name){
        model.setName(name);
        return this;
    }
    public AccountStatBuilder setBalance(String balance){
        model.setBalance(balance);
        return this;
    }

    public AccountStatBuilder setCurrency(CurrencyType currency){
        model.setType(currency);
        return this;
    }

    public AccountStat build(){
        return model;
    }
}
