package ua.kiev.sa.model;


public class AccountName {

    private String name;

    @Override
    public String toString() {
        return "AccountName{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
