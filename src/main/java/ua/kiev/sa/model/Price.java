package ua.kiev.sa.model;

import java.math.BigDecimal;

/**
 * Created by sergio on 14.01.17.
 */
public class Price{

    public Price() {
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    private BigDecimal price;

        private CurrencyType currencyType;


    @Override
    public String toString() {
        return "Price{" +
                "price=" + price +
                ", currencyType=" + currencyType +
                '}';
    }
}
