package ua.kiev.sa.model;


import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "USER_INFO", schema = Constants.SCHEMA)
public class UserInfo {

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Id
    @Column(name = "USER_INFO_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "USER_FIRSTNAME")
    private String firstname;

    @Column(name = "USER_SURNAME")
    private String surname;

    @Column(name = "USER_BIRTHDAY")
    private Date birthday;

    @Column(name = "USER_ORGANIZATION")
    private String organization;

    @Column(name = "USER_SEX")
    private String gender;

    @Column(name = "USER_COUNTRY")
    private String country;

    @Column(name = "USER_CITY")
    private String city;

    @OneToOne
    private User user;


    public UserInfo() {
    }


}
