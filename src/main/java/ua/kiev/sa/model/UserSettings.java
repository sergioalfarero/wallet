package ua.kiev.sa.model;

import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "USER_SETTINGS", schema = Constants.SCHEMA)
public class UserSettings {
    @Id
    @Column(name = "USER_SETTINGS_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "bank")
    private BankType bank;

    @Column(name = "defaultRates")
    private boolean defaultRates;

    @Column(name = "defaultCurrency")
    private CurrencyType defaultCurrency;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "USER_CURRENCIES_LIST", joinColumns = @JoinColumn(name = "USER_SETTINGS_ID"))
    private Set<CurrencyType> favouriteCurrencySet;

    public CurrencyType getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(CurrencyType defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BankType getBank() {
        return bank;
    }

    public void setBank(BankType bank) {
        this.bank = bank;
    }

    public boolean isDefaultRates() {
        return defaultRates;
    }

    public void setDefaultRates(boolean defaultRates) {
        this.defaultRates = defaultRates;
    }

    public Set<CurrencyType> getFavouriteCurrencySet() {
        return favouriteCurrencySet;
    }

    public void setFavouriteCurrencySet(Set<CurrencyType> favouriteCurrencySet) {
        this.favouriteCurrencySet = favouriteCurrencySet;
    }

    @Override
    public String toString() {
        return "UserSettings{" +
                "id=" + id +
                ", bank=" + bank +
                ", defaultRates=" + defaultRates +
                ", defaultCurrency=" + defaultCurrency +
                ", favouriteCurrencySet=" + favouriteCurrencySet +
//                ", user=" + user +
                '}';
    }
}
