package ua.kiev.sa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;
import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


@Entity
@Table(name = "ACCOUNT", schema = Constants.SCHEMA,
        uniqueConstraints = @UniqueConstraint(columnNames = {"ACCOUNT_NAME", "user_user_id"}))
public class Account implements Serializable {

    @Id
    @Column(name = "ACCOUNT_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ACCOUNT_NAME", length = 64)
    private String name;

    @Column(name = "ACCOUNT_BALANCE")
    private BigDecimal balance;


    @Column(name = "ACCOUNT_START_BALANCE")
    private BigDecimal startBalance;

    @Column(name = "ACCOUNT_CURRENCY")
    @Enumerated(value = EnumType.STRING)
    private CurrencyType currencyType;


    @Column(name = "ACCOUNT_ICON")
    private String icon;

    @Column(name = "ACCOUNT_DESCRIPTION")
    private String description;

    @Column(name = "ACCOUNT_CREDIT")
    private boolean credit;

    @Column(name = "ACCOUNT_CASH")
    private boolean cash;

    @Column(name = "ACCOUNT_HIDDEN")
    private boolean hidden;

    @JsonIgnore
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, mappedBy = "account")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE,
            org.hibernate.annotations.CascadeType.DELETE,
            org.hibernate.annotations.CascadeType.MERGE,
            org.hibernate.annotations.CascadeType.PERSIST,
            org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    private List<Operation> operation;

    @JsonIgnore
    @OneToMany
    private List<Transaction> transactions;

    @ManyToOne
    private User user;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public BigDecimal getStartBalance() {
        return startBalance;
    }

    public void setStartBalance(BigDecimal startBalance) {
        this.startBalance = startBalance;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCredit() {
        return credit;
    }

    public void setCredit(boolean credit) {
        this.credit = credit;
    }

    public boolean isCash() {
        return cash;
    }

    public void setCash(boolean cash) {
        this.cash = cash;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public List<Operation> getOperation() {
        return operation;
    }

    public void setOperation(List<Operation> operation) {
        this.operation = operation;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (!id.equals(account.id)) return false;
        return name.equals(account.name);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Account{" +
                "hidden=" + hidden +
                ", cash=" + cash +
                ", credit=" + credit +
                ", description='" + description + '\'' +
                ", icon='" + icon + '\'' +
                ", currencyType=" + currencyType +
                ", startBalance=" + startBalance.setScale(2)+
                ", balance=" + balance.setScale(2) +
                ", name='" + name + '\'' +
                ", id=" + id +
                 ", user=" + user.getId() +
                '}';
    }
}
