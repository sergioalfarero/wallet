package ua.kiev.sa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Node {

    @JsonProperty("id")
    private String id;

    @JsonProperty("parent")
    private String parent;

    @JsonProperty("text")
    private String text;

//    @JsonProperty("children")
//    private boolean children;

    public Node(String id, String parent, String text) {
        this.id = id;
        this.parent = parent;
        this.text = text;
//        this.children = true;
    }

    //    public Node(String id, String parent, String text, boolean children) {
//        this.id = id;
//        this.parent = parent;
//        this.text = text;
//        this.children = children;
//    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

//    public boolean isChildren() {
//        return children;
//    }
//
//    public void setChildren(boolean children) {
//        this.children = children;
//    }

    @Override
    public String toString() {
        return "Node{" +
                "id='" + id + '\'' +
                ", parent='" + parent + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
