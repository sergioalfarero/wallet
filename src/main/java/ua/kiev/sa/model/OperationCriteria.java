package ua.kiev.sa.model;

import java.util.Date;
import java.util.Set;


public class OperationCriteria {

    Set<Long> users;
    Set<Long> categories;
    CurrencyType currencies;
    Date start;
    Date end;
    Set<Long> accounts;


    @Override
    public String toString() {
        return "OperationCriteria{" +
                "users=" + users +
                ", categories=" + categories +
                ", currencies=" + currencies +
                ", start=" + start +
                ", end=" + end +
                ", accounts=" + accounts +
                '}';
    }

    public Set<Long> getUsers() {
        return users;
    }

    public void setUsers(Set<Long> users) {
        this.users = users;
    }

    public Set<Long> getCategories() {
        return categories;
    }

    public void setCategories(Set<Long> categories) {
        this.categories = categories;
    }

    public CurrencyType getCurrencies() {
        return currencies;
    }

    public void setCurrencies(CurrencyType currencies) {
        this.currencies = currencies;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Set<Long> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Long> accounts) {
        this.accounts = accounts;
    }
}
