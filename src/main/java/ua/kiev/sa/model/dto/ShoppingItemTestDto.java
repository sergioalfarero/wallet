package ua.kiev.sa.model.dto;


import java.io.Serializable;

public class ShoppingItemTestDto implements Serializable {


    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
