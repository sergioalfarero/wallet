package ua.kiev.sa.model.dto;

import ua.kiev.sa.model.Rate;

import java.util.Set;

public class RatesListDto {

    private Set<Rate> rates;

    public Set<Rate> getRates() {
        return rates;
    }

    public void setRates(Set<Rate> rates) {
        this.rates = rates;
    }

}
