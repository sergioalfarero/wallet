package ua.kiev.sa.model.dto;


import org.hibernate.validator.constraints.NotEmpty;
import ua.kiev.sa.model.Transaction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TransactionDto {

    public static final SimpleDateFormat dateFormat =
            new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    public TransactionDto() {
    }

    public TransactionDto(Transaction transaction) {
        this.id=transaction.getId();
        this.incomingAccount = transaction.getIncomingAccount().getName();
        this.outcomingAccount = transaction.getOutcomingAccount().getName();
        this.date = dateFormat.format(transaction.getDate());
        this.sum = transaction.getSum().toString();
//        this.subcategory = operation.getSubcategory().getName();
//        this.category = operation.getSubcategory().getCategory().getName();
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getOutcomingAccount() {
        return outcomingAccount;
    }

    public void setOutcomingAccount(String outcomingAccount) {
        this.outcomingAccount = outcomingAccount;
    }

    public String getIncomingAccount() {
        return incomingAccount;
    }

    public void setIncomingAccount(String incomingAccount) {
        this.incomingAccount = incomingAccount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @NotEmpty
    private String date;

    @NotEmpty
    private String incomingAccount;
    @NotEmpty
    private String outcomingAccount;

private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotEmpty
    private String sum;


    public Date getFormatDate() throws ParseException {
        return OperationDto.dateFormat.parse(this.getDate());
    }
}