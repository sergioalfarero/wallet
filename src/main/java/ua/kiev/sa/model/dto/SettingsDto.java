package ua.kiev.sa.model.dto;

import ua.kiev.sa.model.BankType;
import ua.kiev.sa.model.CurrencyType;

import java.util.Set;

public class SettingsDto {

    private Long id;

    private BankType bank;

    private CurrencyType defaultCurrency;

    private Set<CurrencyType> favouriteCurrenciesSet;

    private boolean defaultRates;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BankType getBank() {
        return bank;
    }

    public void setBank(BankType bank) {
        this.bank = bank;
    }

    public CurrencyType getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(CurrencyType defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public Set<CurrencyType> getFavouriteCurrenciesSet() {
        return favouriteCurrenciesSet;
    }

    public void setFavouriteCurrenciesSet(Set<CurrencyType> favouriteCurrenciesSet) {
        this.favouriteCurrenciesSet = favouriteCurrenciesSet;
    }

    public boolean isDefaultRates() {
        return defaultRates;
    }

    public void setDefaultRates(boolean defaultRates) {
        this.defaultRates = defaultRates;
    }



    @Override
    public String toString() {
        return "SettingsDto{" +
                "id=" + id +
                ", bank=" + bank +
                ", defaultCurrency=" + defaultCurrency +
                ", favouriteCurrenciesSet=" + favouriteCurrenciesSet +
                ", defaultRates=" + defaultRates +
                '}';
    }
}
