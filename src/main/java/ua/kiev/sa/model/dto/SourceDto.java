package ua.kiev.sa.model.dto;

public class SourceDto {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
