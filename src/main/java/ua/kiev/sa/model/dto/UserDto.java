package ua.kiev.sa.model.dto;

import org.hibernate.validator.constraints.NotEmpty;
import ua.kiev.sa.validation.PasswordMatches;
import ua.kiev.sa.validation.ValidEmail;

@PasswordMatches
public class UserDto {

    @NotEmpty
    private String login;

    @ValidEmail
    @NotEmpty
    private String email;

    @NotEmpty
    private String password;

    @NotEmpty
    private String matchingPassword;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }


}
