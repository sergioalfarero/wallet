package ua.kiev.sa.model.dto;


import ua.kiev.sa.model.CurrencyType;

import java.math.BigDecimal;
import java.util.Map;

public class AccountSummary {

    private Map<CurrencyType, BigDecimal> map;
    private Map<CurrencyType, BigDecimal> total;

    public Map<CurrencyType, BigDecimal> getMap() {
        return map;
    }

    public void setMap(Map<CurrencyType, BigDecimal> map) {
        this.map = map;
    }

    public Map<CurrencyType, BigDecimal> getTotal() {
        return total;
    }

    public void setTotal(Map<CurrencyType, BigDecimal> total) {
        this.total = total;
    }
}
