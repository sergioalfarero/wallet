package ua.kiev.sa.model.dto;


import ua.kiev.sa.model.CurrencyType;

import java.io.Serializable;
import java.math.BigDecimal;

public class ShoppingItemDto implements Serializable {


    private Long id;
    private String name;
    private String desc;

    private Long listId;


    private BigDecimal price;
    private CurrencyType currencyType;
    private String user;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "ShoppingItemDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", listId=" + listId +
                ", price=" + price +
                ", currencyType=" + currencyType +
                ", user='" + user + '\'' +
                '}';
    }

    public ShoppingItemDto() {
    }

    public ShoppingItemDto(String name) {
        this.name = name;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

}

