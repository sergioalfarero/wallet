package ua.kiev.sa.model.dto;

import org.hibernate.validator.constraints.NotEmpty;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class AccountWithRatesDto {

    private Long id;

    public AccountWithRatesDto() {
    }

    public AccountWithRatesDto(Account a) {
        this.id = a.getId();
        this.name = a.getName();
        this.description = a.getDescription();
        this.startBalance = a.getStartBalance();
        this.startBalance.setScale(2);
        this.hidden=a.isHidden();

//        this.actualBalanceConverted=a.getBalance();
        BigDecimal bd1 = a.getStartBalance().setScale(2);
        this.setStartBalance(bd1);
        this.setBalance(bd1);
//        this.balance = a.getBalance();
//        balance.setScale(2);
        this.currencyType = a.getCurrencyType();
        this.icon = a.getIcon();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotEmpty
    private String name;

    private String description;

    private boolean hidden;

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    @NotEmpty
    private String icon;

    @NotNull
    private CurrencyType currencyType;

    @NotEmpty
    private String username;

    @NotNull
    private BigDecimal startBalance;

    private BigDecimal startBalanceConverted;

    @NotNull
    private BigDecimal balance;

    private BigDecimal actualBalanceConverted;

    private User u;


    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public User getU() {
        return u;
    }

    public void setU(User u) {
        this.u = u;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getStartBalance() {
        return startBalance;
    }

    public void setStartBalance(BigDecimal balance) {
        this.startBalance = balance;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getActualBalanceConverted() {
        return actualBalanceConverted;
    }

    public void setActualBalanceConverted(BigDecimal actualBalanceConverted) {
        this.actualBalanceConverted = actualBalanceConverted;
    }

    public BigDecimal getStartBalanceConverted() {
        return startBalanceConverted;
    }

    public void setStartBalanceConverted(BigDecimal startBalanceConverted) {
        this.startBalanceConverted = startBalanceConverted;
    }

    @Override
    public String toString() {
        return "AccountWithRatesDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startBalance=" + startBalance +
                ", icon='" + icon + '\'' +
                ", currencyType=" + currencyType +
                ", username='" + username + '\'' +
                ", startBalanceConverted=" + startBalanceConverted +
                ", actualBalanceConverted=" + actualBalanceConverted +
                ", u=" + u +
                '}';
    }
}
