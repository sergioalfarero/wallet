package ua.kiev.sa.model.dto;

import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyType;

import java.util.Set;

/**
 * Created by sergio on 28.01.17.
 */
public class OperationCriteriaDto {

    Set<Long> users;
    Set<Long> categories;
    CurrencyType currencies;
    String start;
    String end;
    Set<Long> accounts;
    int roundMod;
    boolean reportByCategory;

    public boolean isReportByCategory() {
        return reportByCategory;
    }

    public void setReportByCategory(boolean reportByCategory) {
        this.reportByCategory = reportByCategory;
    }

    public int getPeriodOption() {
        return periodOption;
    }

    public void setPeriodOption(int periodOption) {
        this.periodOption = periodOption;
    }

    int periodOption;


    @Override
    public String toString() {
        return "OperationCriteriaDto{" +
                "users=" + users +
                ", categories=" + categories +
                ", currencies=" + currencies +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", accounts=" + accounts +
                ", roundMod=" + roundMod +
                ", periodOption=" + periodOption +
                '}';
    }

    public int getRoundMod() {
        return roundMod;
    }

    public void setRoundMod(int roundMod) {
        this.roundMod = roundMod;
    }

    public Set<Long> getUsers() {
        return users;
    }

    public void setUsers(Set<Long> users) {
        this.users = users;
    }

    public Set<Long> getCategories() {
        return categories;
    }

    public void setCategories(Set<Long> categories) {
        this.categories = categories;
    }

    public CurrencyType getCurrencies() {
        return currencies;
    }

    public void setCurrencies(CurrencyType currencies) {
        this.currencies = currencies;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Set<Long> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Long> accounts) {
        this.accounts = accounts;
    }
}
