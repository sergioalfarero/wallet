package ua.kiev.sa.model.dto;

import org.hibernate.validator.constraints.NotEmpty;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.Operation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OperationDto {
    public static final SimpleDateFormat dateFormat =
            new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    private String id;

    @NotEmpty
    private String date;

    @NotEmpty
    private String account;

    @NotEmpty
    private String sum;

    @NotEmpty
    private CurrencyType currencyType;

    @NotEmpty
    private String category;
    @NotEmpty
    private String subcategory;

    public OperationDto() {
    }

    public OperationDto(Operation operation) {
        this.id = operation.getId().toString();
        this.account = operation.getAccount().getName();
        this.date = dateFormat.format(operation.getDate());
        this.sum = operation.getSum().toString();
        this.subcategory = operation.getSubcategory().getName();
        this.category = operation.getSubcategory().getCategory().getName();
        this.currencyType=operation.getCurrency();

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }


    public String getSum() {
        return sum;
    }

    @Override
    public String toString() {
        return "OperationDto{" +
                "date='" + date + '\'' +
                ", account='" + account + '\'' +
                ", sum='" + sum + '\'' +
                ", category='" + category + '\'' +
                ", subcategory='" + subcategory + '\'' +
                '}';
    }

    public void setSum(String sum) {

        System.out.println("setting sum" + sum);
        this.sum = sum;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public Date getFormatDate() throws ParseException {
        return OperationDto.dateFormat.parse(this.getDate());
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }
}
