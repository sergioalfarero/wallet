package ua.kiev.sa.model.dto;

import ua.kiev.sa.model.CurrencyType;

import java.math.BigDecimal;

public class ShoppingListDto {

    private Long id;
    private String name;
    private String desc;
    private BigDecimal sum;
    private CurrencyType currencyType;


    private String user;

    @Override
    public String toString() {
        return "ShoppingListDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", sum=" + sum +
                ", currencyType=" + currencyType +
                ", user='" + user + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }
}
