package ua.kiev.sa.model.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class UserForLoginDto {

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {

        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
