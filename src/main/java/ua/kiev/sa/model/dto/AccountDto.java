package ua.kiev.sa.model.dto;

import org.hibernate.validator.constraints.NotEmpty;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyType;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class AccountDto {

    @NotEmpty
    private String name;


    private String description;

    @NotNull
    private BigDecimal startBalance;

    private boolean credit;

    private boolean cash;

    private boolean hidden;

    @NotEmpty
    private String icon;

    @NotNull
    private CurrencyType currencyType;

    @NotEmpty
    private String username;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getStartBalance() {
        return startBalance;
    }

    public void setStartBalance(BigDecimal balance) {
        this.startBalance = balance;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isCredit() {
        return credit;
    }

    public void setCredit(boolean credit) {
        this.credit = credit;
    }

    public boolean isCash() {
        return cash;
    }

    public void setCash(boolean cash) {
        this.cash = cash;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startBalance=" + startBalance +
                ", credit=" + credit +
                ", cash=" + cash +
                ", hidden=" + hidden +
                ", icon='" + icon + '\'' +
                ", currencyType=" + currencyType +
                '}';
    }

    public AccountDto(Account account){
        this.setName(account.getName());
        this.setStartBalance(account.getStartBalance());
        this.setCurrencyType(account.getCurrencyType());
        this.setDescription(account.getDescription());
        this.setCredit(account.isCredit());
        this.setCash(account.isCash());
        this.setHidden(account.isHidden());



    }

    public AccountDto(){

    }
}
