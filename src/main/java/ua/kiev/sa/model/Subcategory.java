package ua.kiev.sa.model;

import ua.kiev.sa.constants.Constants;

import javax.persistence.*;

@Entity
@Table(name = "SUBCATEGORY", schema = Constants.SCHEMA,
        uniqueConstraints = @UniqueConstraint(columnNames = {"SUBCATEGORY_NAME", "CATEGORY_ID", "user_user_id"}))
public class Subcategory {

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "SUBCATEGORY_NAME")
    private String name;

    @ManyToOne(targetEntity = Category.class, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "CATEGORY_ID", nullable = false)
    private Category category;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Subcategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category=" + category.getName() +
                ", user=" + user.getLogin() +
                '}';
    }
}
