package ua.kiev.sa.model;


public enum BankType {
    NBU("http://www.bank.gov.ua/control/uk/curmetal/detail/currency?period=daily", CurrencyType.UAH);
    public String url;
    public CurrencyType currencyType;

    BankType(String s, CurrencyType curr) {
        currencyType = curr;
        url = s;

    }

    String getUrl() {
        return url;
    }

    CurrencyType getCurrencyType() {
        return currencyType;
    }
}
