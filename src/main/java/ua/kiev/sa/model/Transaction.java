package ua.kiev.sa.model;


import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "TRANSACTION", schema = Constants.SCHEMA)
public class Transaction {

    @Id
    @Column(name = "TRANSACTION_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "TRANSACTION_DATE")
    private Date date;


    @Column(name = "TRANSACTION_SUMM")
    private BigDecimal sum;

    @ManyToOne
    private Account incomingAccount;

    @ManyToOne
    private Account outcomingAccount;

    @ManyToOne
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getSum() {
        return sum;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", date=" + date +
                ", sum=" + sum +
                ", incomingAccount=" + incomingAccount.getName() +
                ", outcomingAccount=" + outcomingAccount.getName() +
                ", user=" + user.getLogin() +
                '}';
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Account getIncomingAccount() {
        return incomingAccount;
    }

    public void setIncomingAccount(Account incomingAccount) {
        this.incomingAccount = incomingAccount;
    }

    public Account getOutcomingAccount() {
        return outcomingAccount;
    }

    public void setOutcomingAccount(Account outcomingAccount) {
        this.outcomingAccount = outcomingAccount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
