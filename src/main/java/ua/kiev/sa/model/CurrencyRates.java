package ua.kiev.sa.model;


import ua.kiev.sa.constants.Constants;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
    @Table(name = "RATES", schema = Constants.SCHEMA)
public class CurrencyRates {

    @Id
    @Column(name = "RATES_ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DATE", unique = true, nullable = false)
    private Date date;

    @Column(name = "BANK")
    @Enumerated(value = EnumType.STRING)
    private BankType bank;

    @Column(name = "RATE_CURRENCY_TYPE", length = 3, nullable = false)
    @Enumerated(value = EnumType.STRING)
    private CurrencyType baseType;

    @Column(name = "RATE_COUNT", nullable = false, columnDefinition = "Decimal(10,4)"/*precision = 13, scale = 4*/)
    private BigDecimal baseRate;

    @NotNull
    @Column(name = "RATES_SET", nullable = false)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "RATE", joinColumns = @JoinColumn(name = "RATE_ID"))
    private Set<Rate> rates;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    private User user;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<Rate> getRates() {
        return rates;
    }

    public void setRates(Set<Rate> rates) {
        this.rates = rates;
    }

    public BankType getBank() {
        return bank;
    }

    public void setBank(BankType bank) {
        this.bank = bank;
    }

    public CurrencyType getBaseType() {
        return baseType;
    }

    public void setBaseType(CurrencyType baseType) {
        this.baseType = baseType;
    }

    public BigDecimal getBaseRate() {
        return baseRate;
    }

    public void setBaseRate(BigDecimal baseRate) {
        this.baseRate = baseRate;
    }

    @Override
    public String toString() {
        return "CurrencyRates{" +
                "id=" + id +
                ", date=" + date +
                ", bank=" + bank +
                ", baseType=" + baseType +
                ", baseRate=" + baseRate +
                ", rates=" + rates +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CurrencyRates that = (CurrencyRates) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (bank != that.bank) return false;
        if (baseType != that.baseType) return false;
        if (baseRate != null ? !baseRate.equals(that.baseRate) : that.baseRate != null) return false;
        if (rates != null ? !rates.equals(that.rates) : that.rates != null) return false;
        return !(user != null ? !user.equals(that.user) : that.user != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (bank != null ? bank.hashCode() : 0);
        result = 31 * result + (baseType != null ? baseType.hashCode() : 0);
        result = 31 * result + (baseRate != null ? baseRate.hashCode() : 0);
        result = 31 * result + (rates != null ? rates.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }
}
