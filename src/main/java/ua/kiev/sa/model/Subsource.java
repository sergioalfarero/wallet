package ua.kiev.sa.model;

import ua.kiev.sa.constants.Constants;

import javax.persistence.*;

@Entity
@Table(name = "SUBSOURCE", schema = Constants.SCHEMA,
        uniqueConstraints = @UniqueConstraint(columnNames = {"SUBSOURCE_NAME", "SOURCE_ID", "user_user_id"}))
public class Subsource {

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "SUBSOURCE_NAME")
    private String name;

    @ManyToOne(targetEntity = IncomeSource.class, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "SOURCE_ID", nullable = false)
    private IncomeSource source;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IncomeSource getSource() {
        return source;
    }

    public void setSource(IncomeSource source) {
        this.source = source;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "SUBSOURCE{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category=" + source.getName() +
                ", user=" + user +
                '}';
    }
}
