package ua.kiev.sa.model.wrappers;

import ua.kiev.sa.model.AccountName;

import java.util.List;

public class AccountNameWrapper {
    public List<AccountName> getAccountNameList() {
        return accountNameList;
    }

    public void setAccountNameList(List<AccountName> accountNameList) {
        this.accountNameList = accountNameList;
    }

    private List<AccountName> accountNameList;


}
