package ua.kiev.sa.model.wrappers;


import ua.kiev.sa.model.AccountStat;

import java.util.List;

public class AccountStatWrapper {


    private List<AccountStat> accountStats;

    /**
     * @return the account
     */
    public List<AccountStat> getAccountStats() {
        return accountStats;
    }

    /**
     * @param accountStats the accounts to set
     */
    public void setAccountStats(List<AccountStat> accountStats) {
        this.accountStats = accountStats;
    }

}
