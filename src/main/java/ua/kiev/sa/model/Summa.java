package ua.kiev.sa.model;

import java.math.BigDecimal;


public class Summa {

    private BigDecimal amount;
    private CurrencyType currencyType;


    public Summa() {
    }

    @Override
    public String toString() {
        return "Summa{" +
                "amount=" + amount +
                ", currencyType=" + currencyType +
                '}';
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CurrencyType currencyType) {
        this.currencyType = currencyType;
    }
}
