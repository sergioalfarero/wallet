package ua.kiev.sa.utils;

import java.util.List;
import java.util.Set;

public class ContainsCheck {
    public static boolean contains(Set set, Object o) {

        return set.contains(o);
    }

    public static boolean containsList(List set, Object o) {

        return set.contains(o);
    }
}
