package ua.kiev.sa.utils;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CompareCheck {

    public static boolean compare(BigDecimal number) {

        return number.compareTo(BigDecimal.ZERO) > 0 ? true : false;
    }


}
