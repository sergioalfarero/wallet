package ua.kiev.sa.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ua.kiev.sa.model.BankType;
import ua.kiev.sa.model.CurrencyRates;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.Rate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class CurrencyRatesDownloader {

    private BankType bankType;

    public CurrencyRatesDownloader(BankType bank) {
        this.bankType = bank;
    }


    public Set<Rate> getDaylyCurrencyRates() /*throws IOException*/{
        Document doc =null;
        try{
        doc= loadDocument();
          } catch (IOException e) {
        System.out.println("UnknownHostException: " + bankType.url);
         }
        return getRates(doc);

    }


    /**
     * Parses given document and creates
     *
     * @param doc loaded document for parsing
     * @return  currencyRates - currency rate CurrencyRate.class
     */


    private Set<Rate> getRates(Document doc) {

        if(doc!=null){
            Elements elementsByTag = doc.getElementsByTag("table");


        Set<Rate> rates = new HashSet<Rate>();
        Elements rows = elementsByTag.get(5).getElementsByTag("tr");
//        CurrencyRates currencyRates = new CurrencyRates();
//        currencyRates.setBank(bankType);
//        currencyRates.setBaseType(CurrencyType.UAH);
//        currencyRates.setBaseRate(BigDecimal.ONE);
//        currencyRates.setDate(new Date());
//        currencyRates.setRates(rates);
        for (int i = 0; i < rows.size(); i++) {

            if (i == 0) continue;
            Rate rate = new Rate();

            try {

                Element row = rows.get(i);

                String count = row.getElementsByTag("td").get(2).text();
                BigDecimal amount1 = new BigDecimal(count);

                rate.setCurrencyType(CurrencyType.valueOf(row.getElementsByTag("td").get(1).text()));
                rate.setRate((new BigDecimal(row.getElementsByTag("td").get(4).text())).divide(amount1, 4));
            } catch (Exception e) {
                e.printStackTrace();
            }
            rates.add(rate);
        }

        return rates;
        }else return null;
    }

    /**
     * Loads and returns document from URL in BankType enum
     *
     * @throws IOException if Internet connection error occurred
     */

    private Document loadDocument() throws IOException {
        Document doc = null;
        doc = Jsoup.connect(bankType.url).timeout(100 * 1000).get();
        assert doc != null;
        return doc;
    }


    public CurrencyRates getMonthlyCurrencyRates() {

        Document doc = null;
        try {
            doc = Jsoup.connect("http://www.bank.gov.ua/control/uk/curmetal/detail/currency?period=monthly").get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements elementsByTag = null;
        if (doc != null) {
            elementsByTag = doc.getElementsByTag("table");
        }
        CurrencyRates currencyRates = new CurrencyRates();
        currencyRates.setBaseType(CurrencyType.UAH);
        currencyRates.setBaseRate(BigDecimal.ONE);

        Set<Rate> cr = new HashSet<Rate>();
        Elements rows = elementsByTag.get(5).getElementsByTag("tr");
        for (int i = 0; i < rows.size(); i++) {
            if (i == 0) continue;

            Element row = rows.get(i);
            Rate currencyRate = new Rate();
            BigDecimal bd = new BigDecimal(row.getElementsByTag("td").get(2).text());
            currencyRate.setCurrencyType(CurrencyType.valueOf(row.getElementsByTag("td").get(1).text()));
            currencyRate.setRate((new BigDecimal(row.getElementsByTag("td").get(4).text())).divide(bd, 4));

            cr.add(currencyRate);

        }
        currencyRates.setRates(cr);
        return currencyRates;
    }

}
