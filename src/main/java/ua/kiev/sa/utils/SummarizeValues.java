package ua.kiev.sa.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.ShoppingItem;
import ua.kiev.sa.model.Summa;
import ua.kiev.sa.model.User;
import ua.kiev.sa.service.RateService;

import java.math.BigDecimal;
import java.util.List;


@Component
public class SummarizeValues {

    private static RateService rateService;

    @Autowired
    public SummarizeValues(RateService rateService) {
        SummarizeValues.rateService = rateService;
    }

    public static String summarize(java.util.List list, Object user) {
        Summa s = new Summa();
        User u = (User) user;
        CurrencyType type = ((User) user).getSettings().getDefaultCurrency();
        s.setCurrencyType(type);
        s.setAmount(BigDecimal.ZERO);
        CurrenciesConverter converter = new CurrenciesConverter(rateService.getRatesAccordingToSetting(u), u);
        for (ShoppingItem item : (List<ShoppingItem>) list) {
            if (item.getCurrencyType() != type) {
                BigDecimal sum = converter.convert(item.getPrice(), item.getCurrencyType(), type);
                BigDecimal newPrice = s.getAmount().add(sum);
                s.setAmount(newPrice);

            } else {
                BigDecimal price = item.getPrice();
                BigDecimal newPrice = s.getAmount().add(price);
                s.setAmount(newPrice);
            }

        }
        return s.getAmount().toString();
    }


}
