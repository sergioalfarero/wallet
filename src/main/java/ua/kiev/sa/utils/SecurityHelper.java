package ua.kiev.sa.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityHelper {

    public static String getUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        return login;
    }
}
