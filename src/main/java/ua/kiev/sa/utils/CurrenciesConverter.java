package ua.kiev.sa.utils;


import ua.kiev.sa.model.CurrencyRates;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.Rate;
import ua.kiev.sa.model.User;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class CurrenciesConverter {
    public Map<CurrencyType, BigDecimal> currencyRatesMap = new HashMap<CurrencyType, BigDecimal>();
    public CurrencyRates rate = new CurrencyRates();
    public User user;

    public CurrenciesConverter(CurrencyRates currencyRate, User u) {
        try {
            user = u;
            rate = currencyRate;
            for (Rate r : currencyRate.getRates()) {
                currencyRatesMap.put(r.getCurrencyType(), r.getRate());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public BigDecimal convert(BigDecimal amount, CurrencyType inputType, CurrencyType outputType) {
        if (user.getSettings().getDefaultCurrency().equals(inputType)) {
            return amount;
        }
            if (rate.getBank() == null) {
            return getUserConvertion(amount, inputType, outputType);
        } else {
            if (user.getSettings().isDefaultRates()) {
                //default currency of user equals to currency of account
                //leave value without changes


                // currency of account equals to currency of bank
                //
                if (inputType.equals(user.getSettings().getBank().currencyType)) {
                    BigDecimal r1 = currencyRatesMap.get(outputType);
                    return amount.divide(r1, 2, BigDecimal.ROUND_CEILING);
                } else if (outputType.equals(user.getSettings().getBank().currencyType)) {
                    BigDecimal r1 = currencyRatesMap.get(inputType);
                    return amount.multiply(r1).setScale(2, BigDecimal.ROUND_CEILING);
                } else {
                    BigDecimal input = currencyRatesMap.get(inputType);
                    BigDecimal output = currencyRatesMap.get(outputType);
                    return amount.multiply(input).divide(output, 2, BigDecimal.ROUND_HALF_UP);
                }

            } else {

                return getUserConvertion(amount, inputType, outputType);

            }
        }
    }


    private BigDecimal getUserConvertion(BigDecimal amount, CurrencyType inputType, CurrencyType outputType) {
        CurrencyType type = user.getSettings().getDefaultCurrency();
        BigDecimal base = BigDecimal.ONE;

        if (inputType.equals(type)) {
            BigDecimal r1 = currencyRatesMap.get(outputType);
            return amount.divide(r1, 2, BigDecimal.ROUND_CEILING);
        } else if (outputType.equals(type)) {
            BigDecimal r1 = currencyRatesMap.get(inputType);
            return amount.multiply(r1).setScale(2, BigDecimal.ROUND_CEILING);
        } else {
            BigDecimal input = currencyRatesMap.get(inputType);
            BigDecimal output = currencyRatesMap.get(outputType);
            return amount.multiply(input).divide(output, 2, BigDecimal.ROUND_HALF_UP);
        }
    }


}



