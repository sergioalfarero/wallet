package ua.kiev.sa.service;

import ua.kiev.sa.model.Role;
import ua.kiev.sa.model.RoleType;

import java.util.List;
import java.util.Set;

public interface GroupService {
    List<Role> listGroup(RoleType roleType);

    List<Role> listGroupByIds(Set<Integer> ids);
}
