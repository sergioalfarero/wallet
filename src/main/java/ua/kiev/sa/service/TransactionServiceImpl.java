package ua.kiev.sa.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.exceptions.NoEnoughMoneyAtAccountException;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.Transaction;
import ua.kiev.sa.model.Transaction;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.TransactionDto;
import ua.kiev.sa.repository.AccountRepository;
import ua.kiev.sa.repository.TransactionRepository;
import ua.kiev.sa.repository.UserRepository;
import ua.kiev.sa.utils.CurrenciesConverter;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

@Service("transactionService")
public class TransactionServiceImpl implements TransactionService {

    @Qualifier("transactionRepository")
    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    UserRepository userRepository;

    @Qualifier("accountRepository")
    @Autowired
    AccountRepository accountRepository;

    @Transactional
    public List<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    @Transactional
    public Transaction createTransaction(TransactionDto transactionDto) throws NoEnoughMoneyAtAccountException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User u = userRepository.getByUserName(login);

        Transaction transaction = new Transaction();

        Account outcomingAccount = accountRepository.findAccountByName(transactionDto.getOutcomingAccount(), u);
        Account incomingAccount = accountRepository.findAccountByName(transactionDto.getIncomingAccount(), u);
        BigDecimal sum = new BigDecimal(transactionDto.getSum());

        if (outcomingAccount.getBalance().compareTo(new BigDecimal(transactionDto.getSum())) >= 0) {
            try {
                transaction.setDate(transactionDto.getFormatDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            outcomingAccount.setBalance(outcomingAccount.getBalance().subtract(new BigDecimal(transactionDto.getSum())));
            incomingAccount.setBalance(incomingAccount.getBalance().add(new BigDecimal(transactionDto.getSum())));
            transaction.setIncomingAccount(incomingAccount);
            transaction.setOutcomingAccount(outcomingAccount);
            transaction.setSum(sum);
            transaction.setUser(u);
            transactionRepository.save(transaction);

        } else {
            throw new NoEnoughMoneyAtAccountException("no enough money at account" + outcomingAccount.getName());
        }
        return transaction;


    }

    public Transaction findById(Long id) {
        return transactionRepository.findOne(id);
    }


    public boolean verifyTransaction(String outcomingAccount, String sum) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User u = userRepository.getByUserName(login);

        Account account = accountRepository.findAccountByName(outcomingAccount, u);
        if (account.getBalance().compareTo(new BigDecimal(sum)) > 0) {
            return true;
        } else return false;
    }

    @Transactional
    public void delete(Long id) {
        Transaction transaction = transactionRepository.getOne(id);
        User user = userRepository.findOne(transaction.getUser().getId());
        Account inAccount = accountRepository.findAccountByName(transaction.getIncomingAccount().getName(), user);
        Account outAccount = accountRepository.findAccountByName(transaction.getOutcomingAccount().getName(), user);
        CurrenciesConverter converter = new CurrenciesConverter(user.getCurrencyRates(), user);
        BigDecimal sum = converter.convert(transaction.getSum(), inAccount.getCurrencyType(), outAccount.getCurrencyType());
        outAccount.setBalance(outAccount.getBalance().add(sum));
        inAccount.setBalance(inAccount.getBalance().subtract(sum));
        accountRepository.saveAndFlush(inAccount);
        accountRepository.saveAndFlush(outAccount);
        transactionRepository.delete(id);

    }
}
