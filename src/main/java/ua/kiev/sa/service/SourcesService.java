package ua.kiev.sa.service;


import ua.kiev.sa.exceptions.CategoryExistsException;
import ua.kiev.sa.exceptions.SourceExistsException;
import ua.kiev.sa.exceptions.SubsourceExistsException;
import ua.kiev.sa.model.Category;
import ua.kiev.sa.model.IncomeSource;
import ua.kiev.sa.model.Subsource;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.CategoryDto;
import ua.kiev.sa.model.dto.SourceDto;
import ua.kiev.sa.model.dto.SubsourceDto;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SourcesService {
    List<IncomeSource> getSources(User user);

    Map<IncomeSource, List<Subsource>> getSources(String login);

    IncomeSource createSource(SourceDto sourceDto);

    List<IncomeSource> getSourcesList(User user);

    Subsource createSubsource(SubsourceDto subsourceDto);

    List<Subsource> getSubsources(Long sourceId);

    Map getSourcesNodes(String login, List<IncomeSource> sourciesList);

    Map getSubsourcesForJSON(List<Subsource> subsourciesList);

    Map getSourcesNodes(Set<IncomeSource> sourcies);

    boolean updateIncomeSource(Long id, String text) throws SourceExistsException;

    boolean updateSubsource(Long id, String newName, Long parentId) throws SubsourceExistsException;

    IncomeSource getIncomeSourceById(Long catId);

    Subsource getSubsourceById(Long catId);

    Subsource createSubsource(String text, Long catId);

    boolean deleteIncomeSource(Long id);

    boolean deleteSubsource(Long id);

    Subsource getSubsourceByName(String subsource, String cat, String login);

    Subsource getSubsourceByName(String subsource);

    List<IncomeSource> getSourcesOfOperations(User user);
}
