package ua.kiev.sa.service;


import ua.kiev.sa.exceptions.CategoryExistsException;
import ua.kiev.sa.exceptions.SubcategoryExistsException;
import ua.kiev.sa.model.Category;
import ua.kiev.sa.model.Subcategory;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.CategoryDto;
import ua.kiev.sa.model.dto.SubcategoryDto;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface CategoryService {
    List<Category> getCategories(User user);

    Map<Category, List<Subcategory>> getCategories(String login);

    Category createCategory(CategoryDto categoryDto);

    List<Category> getCategoriesList(User user);

    Subcategory createSubcategory(SubcategoryDto subcategoryDto);

    List<Subcategory> getSubcategories(Long categoryId);

    Map getCategoriesNodes(String login, List<Category> categoriesList);

    Map getSubcategoriesForJSON(List<Subcategory> subcategoriesList);

    Map getCategoriesNodes(Set<Category> categories);
    Map getCategoriesNodesWithRoot(Set<Category> categories);

    boolean updateCategory(Long id, String text) throws CategoryExistsException;

    boolean updateSubcategory(Long id, String newName, Long parentId) throws SubcategoryExistsException;

    Category getCategoryById(Long catId);

    Subcategory getSubcategoryById(Long catId);

    Subcategory createSubcategory(String text, Long catId);

    boolean deleteCategory(Long id);

    boolean deleteSubcategory(Long id);

    Subcategory getSubcategoryByName(String subcategory, String cat, String login);

    Subcategory getSubcategoryByName(String subcategory);

    List<Category> getCategoriesOfOperations(User user);
}
