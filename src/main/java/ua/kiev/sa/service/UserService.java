package ua.kiev.sa.service;


import org.springframework.security.core.userdetails.UserDetailsService;
import ua.kiev.sa.exceptions.EmailExistsException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.AddUserDto;
import ua.kiev.sa.model.dto.SettingsDto;
import ua.kiev.sa.model.dto.UserDto;

import java.util.List;

public interface UserService extends UserDetailsService {
    User getByUserName(String userName) /*throws ServiceException*/;

    List<User> listUser() /*throws ServiceException*/;

    void saveOrUpdate(User user) /*throws ServiceException*/;


    User registerNewUser(UserDto user) throws EmailExistsException;

    List<User> findAll();

    User createUser(AddUserDto addUserDto) throws EmailExistsException;

    UserInfo getUserInformation(Long id);


    User getUser(String token);

    User findUserByEmail(String userEmail);

    void createPasswordResetTokenForUser(User user, String token);

    PasswordResetToken getPasswordResetToken(String token);

    void changeUserPassword(User user, String password);

    boolean checkIfValidOldPassword(User user, String oldPassword);

    User changeUserSettings(UserSettings settings, String login);

    List<Rate> getRates(String login);

    UserSettings updateSettings(SettingsDto settingsDto);
}
