package ua.kiev.sa.service;

import ua.kiev.sa.model.CurrencyRates;
import ua.kiev.sa.model.Rate;
import ua.kiev.sa.model.User;

import java.util.Set;

public interface RateService {
    void save(CurrencyRates dcr);

    CurrencyRates getCustomRatesForLogin(String login);

    void save(Set<Rate> rates, String login);

    void addRate(Rate rate, String login);

//    boolean isDefaultRatesUpdated(CurrencyRates currencyRates);

    CurrencyRates getDefaultRates();

    CurrencyRates getRatesAccordingToSetting(User user);

    CurrencyRates updateCurrenciesRates(Set<Rate> rates);
}
