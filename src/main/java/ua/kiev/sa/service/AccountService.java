package ua.kiev.sa.service;

import org.springframework.stereotype.Service;
import ua.kiev.sa.exceptions.AccountAlreadyExistsException;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyRates;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.AccountDto;
import ua.kiev.sa.model.dto.AccountSummary;
import ua.kiev.sa.model.dto.AccountWithRatesDto;

import java.math.BigDecimal;
import java.util.List;


@Service
public interface AccountService {
    Account findByName(String name, String login);


    /**
     * Returns all accounts of all users
     *
     * @return List of all accounts of all users
     * @author Sergio Alfarero
     */
    List<Account> findAll();


    /**
     * Method returns all accounts of one user whose login is transmitted as method's parameter
     *
     * @param login login of user
     * @return List of all accounts of one users
     * @author Sergio Alfarero
     */
    List<Account> findAllForLogin(String login);

    /**
     * Method returns all unhidden accounts of one user whose login is transmitted as method's parameter
     *
     * @param login login of user
     * @return List of all accounts of one users
     * @author Sergio Alfarero
     */
    List<Account> findAllNotHiddenForLogin(String login);

    Account createAccount(AccountDto accountDto) throws AccountAlreadyExistsException;

    Account findById(long id);

    void delete(Long id);

    void delete(Account account);

    Account merge(AccountDto accountDto, Long id);

    List<AccountWithRatesDto> getAccountsWithRates(User user);

    CurrencyRates getRateMap(User user);

    BigDecimal getBalanceConverted(BigDecimal startBalance, CurrencyType currencyType);

    AccountSummary getSummary();

    AccountSummary getActiveAccountsSummary();

    List<AccountWithRatesDto> getActiveAccountsWithRates(User u);

    List<Account> findAllForSomeUsers(List<String> users);


}
