package ua.kiev.sa.service;


import org.springframework.data.domain.Pageable;
import ua.kiev.sa.exceptions.NoEnoughMoneyAtAccountException;
import ua.kiev.sa.model.Operation;
import ua.kiev.sa.model.dto.OperationCriteriaDto;
import ua.kiev.sa.model.dto.OperationDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface OperationService {

    Operation createOperation(OperationDto operationDto) throws NoEnoughMoneyAtAccountException;

    List<Operation> findAll();

    void delete(Long id);


    Operation findById(Long id);

    void save(OperationDto operationDto);

    void save(Operation operation);

    Operation updateOperation(Long id, OperationDto operationDto) throws NoEnoughMoneyAtAccountException;

    Map<String, BigDecimal> getReport(OperationCriteriaDto operationCriteriaDto);

    List<Operation> getOperations(OperationCriteriaDto operationCriteriaDto);

    List<Operation> findPageOfOperation(Integer page, Integer size);
}
