package ua.kiev.sa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.exceptions.AccountAlreadyExistsException;
import ua.kiev.sa.exceptions.UserNotFoundException;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyRates;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.AccountDto;
import ua.kiev.sa.model.dto.AccountSummary;
import ua.kiev.sa.model.dto.AccountWithRatesDto;
import ua.kiev.sa.repository.AccountRepository;
import ua.kiev.sa.repository.UserRepository;
import ua.kiev.sa.utils.CurrenciesConverter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Qualifier("accountRepository")
    @Autowired
    private AccountRepository accountRepository;

    @Autowired (required = false)
    private UserRepository userRepository;

    @Autowired (required = false)
    private RateService rateService;


    /**
     * Returns all accounts of all users
     *
     * @return List of all accounts of all users
     * @author Sergio Alfarero
     */
    public List<Account> findAll() {
        return accountRepository.findAll();
    }


    /**
     * Method returns account with name for
     *
     * @param name name of account
     * @param login login of user
     *
     * @return List of all accounts of all users
     * @author Sergio Alfarero
     *
     */
    public Account findByName(String name, String login) {
        Account resultAccount=null;

        if(name!=null&&login!=null){

            User user = userRepository.getByUserName(login);
            if(user!=null){

                resultAccount=accountRepository.findAccountByName(name, user);
            }else throw new UserNotFoundException("No user with login "+login+" found");

        } else throw new IllegalArgumentException("some parameters are null");

        return resultAccount;
    }


    /**
     * Returns all accounts of one user whose login is transmitted as method's parameter
     *
     * @param login login of user
     * @return List of all accounts of one users
     * @author Sergio Alfarero
     */
    public List<Account> findAllForLogin(String login) {
        User user = userRepository.getByUserName(login);
        return accountRepository.findAllByUserId(user);
    }


    public List<Account> findAllNotHiddenForLogin(String login) {
        User user = userRepository.getByUserName(login);

        return accountRepository.findAllNotHiddenByUserId(user);
    }

    @Transactional
    public Account createAccount(AccountDto accountDto) throws AccountAlreadyExistsException {
        User user = userRepository.getByUserName(accountDto.getUsername());
        Account a = accountRepository.findAccountByName(accountDto.getName(), user);
        if (a == null) {
            a = new Account();
            a = populateAccount(a, accountDto, user);
            return accountRepository.save(a);
        } else throw new AccountAlreadyExistsException(a.getName());

    }

    private Account populateAccount(Account a, AccountDto accountDto, User user) {
        a.setName(accountDto.getName());

        BigDecimal bd1 = accountDto.getStartBalance().setScale(2);
        a.setStartBalance(bd1);
        a.setBalance(bd1);
        a.setCurrencyType(accountDto.getCurrencyType());
        if (user != null) {
            a.setUser(user);
        }
        a.setCash(accountDto.isCash());
        a.setCredit(accountDto.isCredit());
        a.setDescription(accountDto.getDescription());
        a.setHidden(accountDto.isHidden());
        a.setIcon(accountDto.getIcon());
        return a;
    }

    @Transactional
    public void delete(Long id) {

        Account account = accountRepository.findOne(id);
        accountRepository.save(account);
        accountRepository.delete(account);

    }

    @Transactional
    public void delete(Account account) {
        accountRepository.delete(account);
    }

    @Transactional
    public Account merge(AccountDto accountDto, Long id) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getByUserName(login);
        Account ac = accountRepository.findOne(id);
        ac = populateAccount(ac, accountDto, user);
        return accountRepository.save(ac);
    }

    public List<AccountWithRatesDto> getAccountsWithRates(User user) {
        List<AccountWithRatesDto> accountWithRatesDtos = new ArrayList<AccountWithRatesDto>();
        CurrencyRates rates = getRateMap(user);
        List<Account> accounts = findAllForLogin(user.getLogin());

        CurrenciesConverter converter = new CurrenciesConverter(rates, user);

        for (Account a : accounts) {
            AccountWithRatesDto accountWithRates = new AccountWithRatesDto();
            accountWithRates.setId(a.getId());
            accountWithRates.setStartBalance(a.getStartBalance());
            accountWithRates.setBalance(a.getBalance());
accountWithRates.setHidden(a.isHidden());
            accountWithRates.setU(a.getUser());
            accountWithRates.setIcon(a.getIcon());
            accountWithRates.setName(a.getName());
            accountWithRates.setCurrencyType(a.getCurrencyType());
            accountWithRates.setStartBalanceConverted(
                    converter.convert(a.getStartBalance(), a.getCurrencyType(), user.getSettings().getDefaultCurrency())
            );
            accountWithRates.setActualBalanceConverted(
                    converter.convert(a.getBalance(), a.getCurrencyType(), user.getSettings().getDefaultCurrency())
            );
            accountWithRatesDtos.add(accountWithRates);

        }

        return accountWithRatesDtos;
    }

    public CurrencyRates getRateMap(User u) {

        return rateService.getRatesAccordingToSetting(u);
    }

    public BigDecimal getBalanceConverted(BigDecimal startBalance, CurrencyType currencyType) {

        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getByUserName(login);

        CurrencyRates rates = getRateMap(user);
        CurrenciesConverter converter = new CurrenciesConverter(rates, user);
        return converter.convert(startBalance, currencyType, user.getSettings().getDefaultCurrency());

    }

    public AccountSummary getSummary() {
        AccountSummary summary = new AccountSummary();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getByUserName(login);
        List<AccountWithRatesDto> accountWithRatesDtos = getAccountsWithRates(user);
        Map<CurrencyType, BigDecimal> map = new TreeMap<CurrencyType, BigDecimal>();

        for (AccountWithRatesDto account : accountWithRatesDtos) {
            BigDecimal amount;
            if (map.get(account.getCurrencyType()) != null) {
                amount = map.get(account.getCurrencyType());
                amount = amount.add(account.getBalance());
                map.put(account.getCurrencyType(), amount);
            } else {
                map.put(account.getCurrencyType(), account.getBalance());
            }

        }
        Map<CurrencyType, BigDecimal> mapTotal = new TreeMap<CurrencyType, BigDecimal>();
        BigDecimal amount = new BigDecimal(0);
        for (AccountWithRatesDto account : accountWithRatesDtos) {
            amount = amount.add(account.getActualBalanceConverted());

        }
        mapTotal.put(user.getSettings().getDefaultCurrency(), amount);
        summary.setMap(map);
        summary.setTotal(mapTotal);
        return summary;
    }

    public AccountSummary getActiveAccountsSummary() {
        AccountSummary summary = new AccountSummary();
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getByUserName(login);
        List<AccountWithRatesDto> accountWithRatesDtos = getActiveAccountsWithRates(user);
        Map<CurrencyType, BigDecimal> map = new TreeMap<CurrencyType, BigDecimal>();

        for (AccountWithRatesDto account : accountWithRatesDtos) {
            BigDecimal amount;
            if (map.get(account.getCurrencyType()) != null) {
                amount = map.get(account.getCurrencyType());
                amount = amount.add(account.getBalance());
                map.put(account.getCurrencyType(), amount);
            } else {
                map.put(account.getCurrencyType(), account.getBalance());
            }

        }
        Map<CurrencyType, BigDecimal> mapTotal = new TreeMap<CurrencyType, BigDecimal>();
        BigDecimal amount = new BigDecimal(0);
        for (AccountWithRatesDto account : accountWithRatesDtos) {
            amount = amount.add(account.getActualBalanceConverted());

        }
        mapTotal.put(user.getSettings().getDefaultCurrency(), amount);
        summary.setMap(map);
        summary.setTotal(mapTotal);
        return summary;
    }

    public List<AccountWithRatesDto> getActiveAccountsWithRates(User user) {
        List<AccountWithRatesDto> accountWithRatesDtos = new ArrayList<AccountWithRatesDto>();
        CurrencyRates rates = getRateMap(user);
        List<Account> accounts = findAllNotHiddenForLogin(user.getLogin());

        CurrenciesConverter converter = new CurrenciesConverter(rates, user);

        for (Account a : accounts) {
            AccountWithRatesDto accountWithRates = new AccountWithRatesDto();
            accountWithRates.setId(a.getId());
            accountWithRates.setStartBalance(a.getStartBalance());
            accountWithRates.setBalance(a.getBalance());
            accountWithRates.setHidden(a.isHidden());
            accountWithRates.setU(a.getUser());
            accountWithRates.setIcon(a.getIcon());
            accountWithRates.setName(a.getName());
            accountWithRates.setCurrencyType(a.getCurrencyType());
            accountWithRates.setStartBalanceConverted(
                    converter.convert(a.getStartBalance(), a.getCurrencyType(), user.getSettings().getDefaultCurrency())
            );
            accountWithRates.setActualBalanceConverted(
                    converter.convert(a.getBalance(), a.getCurrencyType(), user.getSettings().getDefaultCurrency())
            );
            accountWithRatesDtos.add(accountWithRates);

        }

        return accountWithRatesDtos;
    }

    public List<Account> findAllForSomeUsers(List<String> users) {

        return accountRepository.findAllForSomeUsers(users);
    }


    @Transactional
    public Account findById(long id) {
        return accountRepository.findOne(id);
    }
}
