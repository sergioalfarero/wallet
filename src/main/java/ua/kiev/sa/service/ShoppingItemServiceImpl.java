package ua.kiev.sa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.exceptions.ItemAlreadyExistsException;
import ua.kiev.sa.exceptions.ItemNotFoundException;
import ua.kiev.sa.exceptions.ListNotFoundException;
import ua.kiev.sa.exceptions.QueryAlreadyExistsException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.ShoppingItemDto;
import ua.kiev.sa.model.dto.ShoppingListDto;
import ua.kiev.sa.repository.ShoppingItemRepository;
import ua.kiev.sa.repository.ShoppingQueryRepository;
import ua.kiev.sa.repository.UserRepository;
import ua.kiev.sa.utils.SummarizeValues;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service("shoppingItemService")
public class ShoppingItemServiceImpl implements ShoppingItemService {

    @Qualifier("shoppingItemRepository")
    @Autowired
    private ShoppingItemRepository shoppingItemRepository;

    @Qualifier("shoppingQueryRepository")
    @Autowired
    private ShoppingQueryRepository shoppingQueryRepository;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public List<ShoppingItem> getItems(User u) {
        return new LinkedList<ShoppingItem>();
    }


    @Transactional
    public ShoppingItem createItem(ShoppingItemDto shoppingItemDto) throws ItemAlreadyExistsException {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User u = userRepository.getByUserName(login);
        ShoppingListQuery query = shoppingQueryRepository.findOne(shoppingItemDto.getListId());
        shoppingItemDto.setUser(u.getId().toString());
        ShoppingItem a = shoppingItemRepository.findItemByName(shoppingItemDto.getName(), query.getName(), u);

        Integer maxCount = shoppingQueryRepository.getCountOfItemsInList(shoppingItemDto.getListId());
        Integer maxValue = shoppingQueryRepository.getMaxValueOfItemsInList(shoppingItemDto.getListId());
        if (maxValue == null) {
            maxValue = 0;
        }
        ;
        if (a == null) {
            a = new ShoppingItem();
            a = populateItem(a, shoppingItemDto, u, query);
            if (maxCount > maxValue) {
                a.setPriority(maxCount);

            } else {
                a.setPriority(maxValue + 1);

            }


            shoppingItemRepository.save(a);
            ShoppingListQuery shoppingListQuery = shoppingQueryRepository.findQueryByName(query.getName(), u);
            List<ShoppingItem> items = shoppingListQuery.getItems();
            items.add(a);
            String s = SummarizeValues.summarize(items, u);
            BigDecimal bd = new BigDecimal(s);
            shoppingListQuery.setSum(bd);
            shoppingListQuery.setType(u.getSettings().getDefaultCurrency());

            shoppingQueryRepository.save(shoppingListQuery);
            return shoppingItemRepository.findOne(a.getId());

        } else {
            throw new ItemAlreadyExistsException(a.getName());
        }

    }

    @Transactional
    public List<ShoppingItem> getShoppingItems(User u) {
        return shoppingItemRepository.findAllForUser(u);
    }

    @Transactional
    public void delete(Long id, User u) throws ItemNotFoundException {
        ShoppingItem item = shoppingItemRepository.findItemByIdAndUser(id, u);

        if (item != null) {
            item.getShoppingListQuery().getItems().remove(item);
            shoppingItemRepository.delete(item.getId());


        } else {
            throw new ItemNotFoundException();
        }
    }

    @Transactional
    public void deleteList(Long id) throws ListNotFoundException {
        ShoppingListQuery list = shoppingQueryRepository.findOne(id);
        if (list != null) shoppingQueryRepository.delete(list);
        else throw new ListNotFoundException();
    }

    @Transactional
    public ShoppingItem findById(Long id) {
        return shoppingItemRepository.findOne(id);

    }

    @Transactional
    public ShoppingListQuery findQueryById(Long id) {
        return shoppingQueryRepository.findOne(id);

    }

    @Transactional
    public ShoppingItem merge(ShoppingItemDto shoppingItemDto, Long id) {
        User user = userRepository.getByUserName(shoppingItemDto.getUser());
        ShoppingListQuery query = shoppingQueryRepository.findOne(shoppingItemDto.getListId());
        ShoppingItem item = shoppingItemRepository.findOne(id);
//        = shoppingItemRepository.findItemByName(shoppingItemDto.getName(), user);
        item = populateItem(item, shoppingItemDto, null, query);
        return shoppingItemRepository.save(item);
    }

    @Transactional
    public List<ShoppingListQuery> getShoppingListQuery(User u) {


        return shoppingQueryRepository.findAllQueriesForUser(u);
    }

    @Transactional
    public ShoppingListQuery createQuery(ShoppingListDto shoppingListDto) throws QueryAlreadyExistsException {
        User user = userRepository.getByUserName(shoppingListDto.getUser());
        ShoppingListQuery a = shoppingQueryRepository.findQueryByName(shoppingListDto.getName(), user);

        if (a == null) {
            a = new ShoppingListQuery();
            a = populateQuery(a, shoppingListDto, user);
            return shoppingQueryRepository.save(a);
        } else throw new QueryAlreadyExistsException(a.getName());

    }

    @Transactional
    public ShoppingItem createItem(ShoppingItemDto shoppingItemDto, String query) throws ItemAlreadyExistsException {
        User user = userRepository.getByUserName(shoppingItemDto.getUser());
        ShoppingItem a = shoppingItemRepository.findItemByName(shoppingItemDto.getName(), query, user);
        ShoppingListQuery shoppingListQuery = shoppingQueryRepository.findQueryByName(query, user);


        Integer maxCount = shoppingQueryRepository.getCountOfItemsInList(shoppingListQuery.getId());
        Integer maxValue = shoppingQueryRepository.getMaxValueOfItemsInList(shoppingListQuery.getId());
        if (a == null) {
            if (maxCount == null) maxCount = 0;
            if (maxValue == null) maxValue = 0;
            a = new ShoppingItem();
            a = populateItem(a, shoppingItemDto, user, shoppingListQuery);
            if (maxCount > maxValue) {
                a.setPriority(maxCount);

            } else {
                a.setPriority(maxValue + 1);

            }
            shoppingListQuery.getItems().add(a);
            String s = SummarizeValues.summarize(shoppingListQuery.getItems(), user);
            BigDecimal bd = new BigDecimal(s);
            shoppingListQuery.setSum(bd);
            shoppingListQuery.setType(user.getSettings().getDefaultCurrency());
            shoppingQueryRepository.save(shoppingListQuery);
            return shoppingItemRepository.save(a);

        } else throw new ItemAlreadyExistsException(a.getName());

    }

    public ShoppingListQuery findByNameAndUser(String name, User u) {
        return shoppingQueryRepository.getByNameAndUser(name, u);
    }

    @Transactional
    public ShoppingListQuery getShoppingListQueryByItemId(Long id) {
        ShoppingItem item = shoppingItemRepository.getOne(id);
        return item.getShoppingListQuery();
    }

    @Transactional
    public void saveOrUpdate(ShoppingListQuery query1) {
        shoppingQueryRepository.save(query1);
    }

    @Transactional
    public List<ShoppingItem> getQueryByItemId(Long id) {
        ShoppingItem item = shoppingItemRepository.findOne(id);

        return item.getShoppingListQuery().getItems();
    }

    //todo:
    private ShoppingListQuery getShoppingListQuery(String query) {
        return null;
    }

    private ShoppingListQuery populateQuery(ShoppingListQuery a, ShoppingListDto shoppingListDto, User user) {
        a.setName(shoppingListDto.getName());
        a.setDesc(shoppingListDto.getDesc());
        if (a.getUser() == null) {
            if (user != null) {
                a.setUser(user);
            }
        }
        return a;
    }


    private ShoppingItem populateItem(ShoppingItem a, ShoppingItemDto shoppingItemDto, User user, ShoppingListQuery shoppingListQuery) {
        a.setName(shoppingItemDto.getName());
        a.setPrice(shoppingItemDto.getPrice());
        a.setCurrencyType(shoppingItemDto.getCurrencyType());
        a.setDesc(shoppingItemDto.getDesc());
        if (a.getShoppingListQuery() != null) {
            if (shoppingListQuery != null) {
                a.setShoppingListQuery(shoppingListQuery);
            }
        }
        a.setShoppingListQuery(shoppingListQuery);
        if (a.getUser() == null) {
            if (user != null) {
                a.setUser(user);
            }
        }
        return a;

    }

    @Transactional
    public ShoppingListQuery editList(ShoppingListDto listDto, User u) {
        ShoppingListQuery list = new ShoppingListQuery();
        try {
            list = shoppingQueryRepository.findOne(listDto.getId());
            populateQuery(list, listDto, u);
            list = shoppingQueryRepository.save(list);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return list;
    }

    @Transactional
    public ShoppingItem editItem(ShoppingItemDto itemDto, User u) {
        ShoppingItem item = new ShoppingItem();
        ShoppingListQuery query = shoppingQueryRepository.findOne(itemDto.getListId());
        item = shoppingItemRepository.findOne(itemDto.getId());
        populateItem(item, itemDto, u, query);
        item = shoppingItemRepository.save(item);


        String s = SummarizeValues.summarize(query.getItems(), u);
        BigDecimal bd = new BigDecimal(s);
        query.setSum(bd);
        query.setType(u.getSettings().getDefaultCurrency());

        shoppingQueryRepository.save(query);
        return item;
    }

    @Transactional
    public ShoppingItem editItemWithMoving(ShoppingItemDto itemDto, User u) {
        ShoppingItem item = new ShoppingItem();
        ShoppingListQuery query = shoppingQueryRepository.findOne(itemDto.getListId());
        item = shoppingItemRepository.findOne(itemDto.getId());
        populateItem(item, itemDto, u, query);

        item = shoppingItemRepository.save(item);

        query.getItems().add(item);
        String s = SummarizeValues.summarize(query.getItems(), u);
        BigDecimal bd = new BigDecimal(s);
        query.setSum(bd);
        query.setType(u.getSettings().getDefaultCurrency());

        shoppingQueryRepository.save(query);

        return item;
    }

    @Transactional
    public Price getListInfoWithoutItem(ShoppingListQuery query, ShoppingItem item, User u) {

        Map<BigDecimal, CurrencyType> map = new HashMap<BigDecimal, CurrencyType>();

        query.getItems().remove(item);
        String s = SummarizeValues.summarize(query.getItems(), u);
        Price p = new Price();
        p.setPrice(new BigDecimal(s));
        p.setCurrencyType(u.getSettings().getDefaultCurrency());

        return p;
    }

    @Transactional
    public ShoppingItem upItem(Long id, User u) {
        ShoppingItem item=null;
        try {
            item = shoppingItemRepository.findOne(id);

            Integer currentItemPriority = item.getPriority();
            List<ShoppingItem> items=shoppingItemRepository.findWithClosestHigherPriority(item.getShoppingListQuery().getId(), currentItemPriority, u, new PageRequest(0, 1));
            ShoppingItem higherItem =items.get(0);

//            ShoppingItem higherItem = shoppingItemRepository.findWithClosestHigherPriority1(currentItemPriority);
            Integer higherItemPriority = higherItem.getPriority();
            item.setPriority(higherItemPriority);
            item=shoppingItemRepository.saveAndFlush(item);
//            int i = shoppingItemRepository.changePriority(item.getId(), higherItemPriority);
//            int j = shoppingItemRepository.changePriority(higherItem.getId(), currentItemPriority);
            higherItem.setPriority(currentItemPriority);
            shoppingItemRepository.saveAndFlush(higherItem);
        }
        catch (Exception e){
           item=null;
        }
        return item;
    }
    @Transactional
    public ShoppingItem downItem(Long id, User u) {
        ShoppingItem item=null;
        try {
            item = shoppingItemRepository.findOne(id);

            Integer currentItemPriority = item.getPriority();
            List<ShoppingItem> items=shoppingItemRepository.findWithClosestLowerPriority(item.getShoppingListQuery().getId(), currentItemPriority, u, new PageRequest(0, 1));
            ShoppingItem lowerItem =items.get(0);
            Integer lowerItemPriority = lowerItem.getPriority();
            item.setPriority(lowerItemPriority);
            item=shoppingItemRepository.save(item);
            lowerItem.setPriority(currentItemPriority);
            lowerItem=shoppingItemRepository.saveAndFlush(lowerItem);

        }
        catch (Exception e){
            e.printStackTrace();
            item=null;
        }
        return item;
    }

}
