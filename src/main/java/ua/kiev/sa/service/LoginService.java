package ua.kiev.sa.service;


public interface LoginService {

    boolean isValidUser(String login, String password);
}
