package ua.kiev.sa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.model.RoleType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service("userDetailService")
@Transactional
public class MyUserDetailsService implements UserDetailsService, Serializable {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LoginAttemptService loginAttemptService;
//@Autowired
//    private HttpServletRequest request;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        String ip = getClientIP();
//        if (loginAttemptService.isBlocked(ip)) {
//            throw new RuntimeException("blocked");
//        }

        User u = userRepository.getByUserName(username);
        if (u == null) {
            throw new UsernameNotFoundException("No user found with name" + username);
        }

        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        return new org.springframework.security.core.userdetails.User(u.getLogin(),
                u.getPassword(),
                u.isEnabled(),
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                getAuthorities(u.getRole().getRoleType()));


    }


    private Collection<? extends GrantedAuthority> getAuthorities(RoleType role) {
        List<RoleType> roles = new ArrayList();

        if (role == RoleType.ROLE_ADMIN) {
            roles.add(RoleType.ROLE_CUSTOMER);
            roles.add(RoleType.ROLE_ADMIN);

        }
        if (role == RoleType.ROLE_CUSTOMER) {
            roles.add(RoleType.ROLE_CUSTOMER);
        }


        List<GrantedAuthority> authList = getGrantedAuthorities(roles);
        return authList;
    }


    private static List<GrantedAuthority> getGrantedAuthorities(List<RoleType> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (RoleType role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.name()));
        }
        return authorities;
    }

//    private String getClientIP() {
//        String xfHeader = request.getHeader("X-Forwarded-For");
//        if (xfHeader == null) {
//            return request.getRemoteAddr();
//        }
//        return xfHeader.split(",")[0];
//    }
}
