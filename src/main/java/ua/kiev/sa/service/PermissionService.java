package ua.kiev.sa.service;


import ua.kiev.sa.model.Permission;
import ua.kiev.sa.model.Role;

import java.util.List;

public interface PermissionService {
    List<Permission> getByName();

    List<Permission> listPermission();

    Permission getById(Integer permissionId);

    void saveOrUpdate(Permission permission);

//	void deletePermission(Permission permission) throws ServiceException;

    List<Permission> getPermissions(List<Integer> groupIds);

    void deletePermission(Permission permission);

//    PermissionList listByCriteria(PermissionCriteria criteria) ;

    void removePermission(Permission permission, Role role);
}
