package ua.kiev.sa.service;


import ua.kiev.sa.exceptions.NoEnoughMoneyAtAccountException;
import ua.kiev.sa.model.Operation;
import ua.kiev.sa.model.Transaction;
import ua.kiev.sa.model.dto.TransactionDto;

import java.util.List;

public interface TransactionService {
    List<Transaction> findAll();

    Transaction createTransaction(TransactionDto transactionDto) throws NoEnoughMoneyAtAccountException;

    void delete(Long id);

    Transaction findById(Long id);

    boolean verifyTransaction(String outcomingAccount, String sum);
}
