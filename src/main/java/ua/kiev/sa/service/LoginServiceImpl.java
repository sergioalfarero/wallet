package ua.kiev.sa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.kiev.sa.repository.UserRepository;


@Service("loginService")
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserRepository userRepository;

    public boolean isValidUser(String login, String password) {

        return userRepository.isValidUser(login, password);
    }
}
