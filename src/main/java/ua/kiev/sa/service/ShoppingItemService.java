package ua.kiev.sa.service;

import org.springframework.stereotype.Service;
import ua.kiev.sa.exceptions.ItemAlreadyExistsException;
import ua.kiev.sa.exceptions.ItemNotFoundException;
import ua.kiev.sa.exceptions.ListNotFoundException;
import ua.kiev.sa.exceptions.QueryAlreadyExistsException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.ShoppingItemDto;
import ua.kiev.sa.model.dto.ShoppingListDto;

import java.util.List;

@Service
public interface ShoppingItemService {
    public ShoppingListQuery findQueryById(Long id);

    public List<ShoppingItem> getItems(User u);

    public void deleteList(Long id) throws ListNotFoundException;

    ShoppingItem createItem(ShoppingItemDto shoppingItemDto) throws ItemAlreadyExistsException;

    List<ShoppingItem> getShoppingItems(User u);

    void delete(Long id, User u) throws ItemNotFoundException;


    ShoppingItem findById(Long id);

    ShoppingItem merge(ShoppingItemDto shoppingItemDto, Long id);

    List<ShoppingListQuery> getShoppingListQuery(User u);

    ShoppingListQuery createQuery(ShoppingListDto shoppingListDto) throws QueryAlreadyExistsException;

    ShoppingItem createItem(ShoppingItemDto shoppingItemDto, String query) throws ItemAlreadyExistsException;

    ShoppingListQuery findByNameAndUser(String name, User u);

    ShoppingListQuery getShoppingListQueryByItemId(Long id);

    void saveOrUpdate(ShoppingListQuery query1);

    List<ShoppingItem> getQueryByItemId(Long id);

    ShoppingListQuery editList(ShoppingListDto listDto, User user);

    ShoppingItem editItem(ShoppingItemDto itemDto, User user);
    ShoppingItem editItemWithMoving(ShoppingItemDto itemDto, User user);

    Price getListInfoWithoutItem(ShoppingListQuery list, ShoppingItem item, User user);

    ShoppingItem upItem(Long id, User u);

    ShoppingItem downItem(Long id, User u);
}
