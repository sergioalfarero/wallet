package ua.kiev.sa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.exceptions.EmailExistsException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.AddUserDto;
import ua.kiev.sa.model.dto.SettingsDto;
import ua.kiev.sa.model.dto.UserDto;
import ua.kiev.sa.repository.PasswordResetTokenRepository;
import ua.kiev.sa.repository.UserRepository;
import ua.kiev.sa.repository.VerificationTokenRepository;

import java.math.BigDecimal;
import java.util.*;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Qualifier("verificationTokenRepository")
    @Autowired
    VerificationTokenRepository verificationTokenRepository;
    @Autowired
    PasswordResetTokenRepository passwordResetTokenRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    UserRepository userRepository;

    public User getByUserName(String userName) {
        return userRepository.getByUserName(userName);
    }

    public List<User> listUser() {
        try {
            return userRepository.findAll();
        } catch (Exception e) {

        }
        return null;
    }

    @Transactional
    public void saveOrUpdate(User user) {
        if (user.getId() == null || user.getId().longValue() == 0) {
            userRepository.save(user);
        } else {
            userRepository.save(user);
        }
    }

    @Transactional(readOnly = false)
    public User registerNewUser(UserDto userDto) throws EmailExistsException {
        if (emailExists(userDto.getEmail())) {
            throw new EmailExistsException("There is an account with that email address: " +
                    userDto.getEmail());
        }


        User u = new User();
        u.setLogin(userDto.getLogin());
        u.setEmail(userDto.getEmail());
        u.setPassword(passwordEncoder.encode(userDto.getPassword()));
        u.setSettings(prepareDefaultSettings());


        u.setCurrencyRates(prepareDefaultCurrenciesRates(u));
        //todo: change registration user with role admin
        u.setRole(new Role(RoleType.ROLE_ADMIN, u));

        u.setCategories(prepareDefaultCategories(u));
try {
    u.setSources(prepareDefaultSources(u));
}catch (Exception e){
    e.printStackTrace();
}
        try {
            userRepository.save(u);
        } catch (Exception e) {
            e.printStackTrace();

        }

        return u;
    }

    private CurrencyRates prepareDefaultCurrenciesRates(User u) {
        CurrencyType type = u.getSettings().getDefaultCurrency();
        Set<CurrencyType> types = u.getSettings().getFavouriteCurrencySet();
        CurrencyRates cr = new CurrencyRates();
        cr.setDate(new Date());
        cr.setBaseRate(BigDecimal.ONE);
        cr.setBaseType(type);
        Set<Rate> rates = new HashSet<Rate>();
        for (CurrencyType t : types) {
            if (!t.equals(type)) {
                Rate r = new Rate();
                r.setCurrencyType(t);
                r.setRate(BigDecimal.ONE);
                rates.add(r);
            }
        }
        cr.setRates(rates);
        cr.setUser(u);
        return cr;
    }

    List<Subcategory> createSubcategories(List<String> str, User u, Category c) {
        List<Subcategory> subcategories = new ArrayList<Subcategory>();
        for (String s : str) {
            Subcategory subcategory = new Subcategory();
            subcategory.setName(s);
            subcategory.setUser(u);
            subcategory.setCategory(c);
            subcategories.add(subcategory);

        }
        return subcategories;
    }
    List<Subsource> createSubsources(List<String> str, User u, IncomeSource ss) {
        List<Subsource> subsources= new ArrayList<Subsource>();
        for (String s : str) {
            Subsource subsource= new Subsource();
            subsource.setName(s);
            subsource.setUser(u);
            subsource.setSource(ss);
            subsources.add(subsource);

        }
        return subsources;
    }

    private List<Category> prepareDefaultCategories(User u) {

        List<Category> categories = new ArrayList<Category>();

        Category category = new Category();
        category.setName("Автомобиль");
        category.setUser(u);
        String gaz[] = {"Бензин",
                "мойка",
                "ремонт",
                "запчасти",
                "страховка",
                "стоянка",
                "техосмотр",
                "налоги",
                "штрафы",
                "парковка"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(gaz), u, category));
        categories.add(category);

        category = new Category();
        category.setName("Бизнес");
        category.setUser(u);
        String business[] = {"налоги",
                "зарплата",
                "реклама",
                "офис и канцелярия",
                "услуги"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(business), u, category));
        categories.add(category);


        category = new Category();
        category.setName("Благотворительность, помощь, подарки");
        category.setUser(u);
        categories.add(category);

        category = new Category();
        category.setName("Бытовая техника");
        category.setUser(u);
        String technic[] = {"компьютер",
                "расходные материалы"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(technic), u, category));
        ;
        categories.add(category);


        category = new Category();
        category.setName("Дети");
        category.setUser(u);
        String children[] = {"одежда",
                "питание",
                "игрушки",
                "книги",
                "няня",
                "мебель",
                "услуги",
                "развлечения"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(children), u, category));
        categories.add(category);


        category = new Category();
        category.setName("Домашние животные");
        category.setUser(u);
        String pets[] = {"питание",
                "товары для животных",
                "услуги ветеринара"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(pets), u, category));
        categories.add(category);

        category = new Category();
        category.setName("Здоровье и красота");
        category.setUser(u);
        String health[] = {"косметика",
                "парфюмерия",
                "салоны красоты",
                "спорт",
                "лекарства",
                "услуги"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(health), u, category));
        categories.add(category);


        category = new Category();
        category.setName("Ипотека, долги, кредиты");
        category.setUser(u);
        String credits[] = {"выплата по кредиту",
                "выплата по ипотеке",
                "досрочное гашение долга",
                "покрытие процентов"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(credits), u, category));
        categories.add(category);

        category = new Category();
        category.setName("Квартира и связь");
        category.setUser(u);
        String house[] = {"электричество",
                "вода",
                "тепло",
                "газ",
                "радио",
                "телефон",
                "интернет",
                "аренда",
                "вывоз мусора",
                "кабельное телевидение",
                "охрана",
                "консьерж"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(house), u, category));
        categories.add(category);


        category = new Category();
        category.setUser(u);
        category.setName("Налоги и страхование");
        categories.add(category);

        category = new Category();
        category.setName("Образование");
        category.setUser(u);
        String education[] = {"учебники",
                "канцтовары",
                "плата за обучение",
                "репетитор"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(education), u, category));
        categories.add(category);

        category = new Category();
        category.setName("Rest");
        category.setUser(u);
        String rest[] = {"игры",
                "фильмы",
                "книги",
                "диски",
                "журналы",
                "кафе и рестораны",
                "кино",
                "фото",
                "театр",
                "выставки",
                "боулинг"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(rest), u, category));
        categories.add(category);


        category = new Category();
        category.setName("Питание");
        category.setUser(u);
        String food[] = {"основные продукты",
                "деликатесы",
                "алкоголь",
                "еда на работе",
                "школьные завтраки"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(food), u, category));
        categories.add(category);

        category = new Category();
        category.setName("Разное");
        category.setUser(u);
        String other[] = {"служебные расходы",
                "карманные расходы",
                "чаевые",
                "взносы",
                "банковские комиссии",
                "нотариус",
                "утеря денег",
                "доставка товара"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(other), u, category));
        categories.add(category);

        category = new Category();
        category.setName("Ремонт и мебель");
        category.setUser(u);
        categories.add(category);

        category = new Category();
        category.setName("Товары для дома");
        category.setUser(u);
        String houseThings[] = {"белье",
                "мелкая техника",
                "инструменты",
                "посуда",
                "кухонная утварь",
                "товары для ванной",
                "предметы интерьера"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(houseThings), u, category));
        categories.add(category);


        category = new Category();
        category.setName("Транспорт");
        category.setUser(u);
        String transport[] = {"автобус",
                "проездные",
                "авиа",
                "метро",
                "такси",
                "электричка"
        };
        category.setSubcategories(createSubcategories(Arrays.asList(transport), u, category));
        categories.add(category);

        category = new Category();
        category.setName("Хобби");
        category.setUser(u);
        categories.add(category);

        return categories;
    }

    private List<IncomeSource> prepareDefaultSources(User u) {

        List<IncomeSource> sources = new ArrayList<IncomeSource>();

        IncomeSource source= new IncomeSource();
        source.setName("Работа");
        source.setUser(u);
        String work[] = {"Зарплата",
                "Премия",
                "Отпускные",
                "Командировочные",
                "Частный бизнес",
                "Бонусы"
        };
        source.setSubsources(createSubsources(Arrays.asList(work), u, source ));
        sources.add(source);


        source= new IncomeSource();
        source.setName("Азартные игры");
        source.setUser(u);
        String games[] = {"Лотерея",
                "Букмекерские конторы",

        };
        source.setSubsources(createSubsources(Arrays.asList(games), u, source ));
        sources.add(source);

        return sources;
    }

    private UserSettings prepareDefaultSettings() {
        UserSettings settings = new UserSettings();
        settings.setBank(BankType.NBU);
        settings.setDefaultRates(true);
        settings.setDefaultCurrency(CurrencyType.USD);
        Set<CurrencyType> currencyTypes = new HashSet<CurrencyType>();
        currencyTypes.add(CurrencyType.USD);
        currencyTypes.add(CurrencyType.UAH);
        currencyTypes.add(CurrencyType.EUR);
        settings.setFavouriteCurrencySet(currencyTypes);

        return settings;
    }

    public List<User> findAll() {

        return userRepository.findAll();
    }

    @Transactional
    public User createUser(AddUserDto addUserDto) throws EmailExistsException {
        if (emailExists(addUserDto.getEmail())) {
            throw new EmailExistsException("There is an account with that email address: " +
                    addUserDto.getEmail());
        }
        User u = new User();
        u.setLogin(addUserDto.getLogin());
        u.setEmail(addUserDto.getEmail());
        u.setPassword(passwordEncoder.encode(addUserDto.getPassword()));
        u.setRole(new Role(addUserDto.getRoleType(), u));
        userRepository.save(u);
        return u;
    }

    public UserInfo getUserInformation(Long id) {
        User user = userRepository.findById(id.intValue());

        UserInfo userInfo = userRepository.getUserInformation(user);

        return userInfo;
    }

    public VerificationToken getVerificationToken(String token) {
        return userRepository.getVerificationToken(token);

    }


    private boolean emailExists(String email) {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            return true;
        }
        return false;
    }

    public void delete(User user) /*throws ServiceException */ {
        userRepository.delete(user);

    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User u = userRepository.getByUserName(username);
        if (u == null) {
            throw new UsernameNotFoundException("No user found with name" + username);
        }
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        return new org.springframework.security.core.userdetails.User(u.getEmail(),
                u.getPassword(),
                u.isEnabled(),
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                getAuthorities(u.getRole().getRoleType()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(RoleType roleType) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(roleType));
        return authList;


    }

    private List<String> getRoles(RoleType role) {
        List<String> roles = new ArrayList<String>();
        return roles;
    }

    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }


    public User getUser(String token) {
        return userRepository.getByToken(token);
    }

    public User findUserByEmail(String userEmail) {

        return userRepository.findByEmail(userEmail);
    }

    @Transactional
    public void createPasswordResetTokenForUser(User user, String token) {
        PasswordResetToken passwordResetToken = new PasswordResetToken(token, user);
        passwordResetTokenRepository.save(passwordResetToken);
    }

    public PasswordResetToken getPasswordResetToken(String token) {
        PasswordResetToken passwordResetToken = passwordResetTokenRepository.getPasswordResetToken(token);

        return passwordResetToken;
    }

    @Transactional
    public void changeUserPassword(User user, String password) {
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    public boolean checkIfValidOldPassword(User user, String oldPassword) {
        User user1 = userRepository.findOne(user.getId());
        return user1.getPassword().equals(oldPassword);
    }

    @Transactional
    public User changeUserSettings(UserSettings settings, String login) {

        User user = new User();
        try {
            user = userRepository.getByUserName(login);
            user.setSettings(settings);
            user = userRepository.save(user);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return user;
    }

    public List<Rate> getRates(String login) {
        return null;
    }

    @Transactional
    public UserSettings updateSettings(SettingsDto settingsDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User user=userRepository.getByUserName(login);
        UserSettings settings=user.getSettings();

        settings.setBank(settingsDto.getBank());
        settings.setDefaultCurrency(settingsDto.getDefaultCurrency());
        settings.setFavouriteCurrencySet(settingsDto.getFavouriteCurrenciesSet());
        settings.setDefaultRates(settingsDto.isDefaultRates());
        user.setSettings(settings);
        User u=userRepository.saveAndFlush(user);
        return u.getSettings();
    }
}
