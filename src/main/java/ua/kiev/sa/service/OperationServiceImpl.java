package ua.kiev.sa.service;

import org.joda.time.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.exceptions.NoEnoughMoneyAtAccountException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.OperationCriteriaDto;
import ua.kiev.sa.model.dto.OperationDto;
import ua.kiev.sa.repository.*;
import ua.kiev.sa.utils.CurrenciesConverter;
import ua.kiev.sa.utils.SecurityHelper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("operationService")
public class OperationServiceImpl implements OperationService {
    @Autowired
    private UserRepository userRepository;
    @Qualifier("subcategoryRepository")
    @Autowired
    private SubcategoryRepository subcategoryRepository;
    @Qualifier("categoryRepository")
    @Autowired
    private CategoryRepository categoryRepository;
    @Qualifier("accountRepository")
    @Autowired
    private AccountRepository accountRepository;
    @Qualifier("operationRepository")
    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    @Qualifier("reportRepository")
    private ReportRepository reportRepository;
    @Autowired
    private RateService rateService;

    @Transactional
    public Operation createOperation(OperationDto operationDto) throws NoEnoughMoneyAtAccountException {
        User u = userRepository.getByUserName(SecurityHelper.getUser());

        Operation operation = new Operation();

        Category category = categoryRepository.findOne(new Long(operationDto.getCategory()));
        Subcategory subcategory=null;
        try {
            subcategory= subcategoryRepository.findSubcategoryByNameAndCategoryId(new Long(operationDto.getSubcategory()), category.getId(), u);
        }catch (Exception e){
            e.printStackTrace();
        }
        Account acc = accountRepository.findAccountByName(operationDto.getAccount(), u);

        BigDecimal sum = new BigDecimal(operationDto.getSum());
        CurrencyType currencyType=operationDto.getCurrencyType();
        CurrenciesConverter converter=new CurrenciesConverter(rateService.getRatesAccordingToSetting(u), u);
        BigDecimal convertedValueOfOperation=converter.convert(sum, currencyType, acc.getCurrencyType());

        if (acc.getBalance().compareTo(convertedValueOfOperation) >= 0) {
            try {
                operation.setDate(operationDto.getFormatDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            acc.setBalance(acc.getBalance().subtract(new BigDecimal(operationDto.getSum())));
            operation.setSubcategory(subcategory);
            operation.setAccount(acc);
            operation.setSum(sum);
            operation.setUser(u);
            operation.setCurrency(currencyType);
            operationRepository.save(operation);

        } else {
            throw new NoEnoughMoneyAtAccountException("no enough money at account" + acc.getName());
        }
        return operation;
    }


    public List<Operation> findAll() {
        User u = userRepository.getByUserName(SecurityHelper.getUser());
        return operationRepository.findAllForUser(u.getId());
    }

    public List<Operation> find10LatestOperations() {
        User u = userRepository.getByUserName(SecurityHelper.getUser());
        return operationRepository.findAllForUser(u.getId());
    }


    @Transactional
    public void delete(Long id) {
        Operation operation=operationRepository.getOne(id);
        User user =userRepository.findOne(operation.getUser().getId());

            Account account=accountRepository.findAccountByName(operation.getAccount().getName(), user);
            CurrenciesConverter converter=new CurrenciesConverter(user.getCurrencyRates(), user);
            BigDecimal sum=converter.convert(operation.getSum(), operation.getCurrency(), account.getCurrencyType());
            account.setBalance(account.getBalance().add(sum));
            accountRepository.saveAndFlush(account);
            operationRepository.delete(id);

//            throw new OperationDidntDeletedExeption();


    }

    public Operation findById(Long id) {
        return operationRepository.findOne(id);
    }

    public void save(OperationDto operationDto) {
        Operation operation = new Operation();
        User user = userRepository.getByUserName(SecurityHelper.getUser());

        Account account = accountRepository.findAccountByName(operationDto.getAccount(), user);
        Subcategory subcategory = subcategoryRepository.findSubcategoryByName(operationDto.getSubcategory(), user);

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        Date date = null;
        try {
            date = formatter.parse(operationDto.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        operation.setDate(date);
        operation.setSubcategory(subcategory);
        operation.setAccount(account);
        operation.setSum(new BigDecimal(operationDto.getSum()));
        operation.setUser(user);
        operationRepository.save(operation);
    }

    public void save(Operation operation) {

    }

    public Operation updateOperation(Long id, OperationDto operationDto) throws NoEnoughMoneyAtAccountException {
        User u = userRepository.getByUserName(SecurityHelper.getUser());

        Operation oldOperation = operationRepository.findOne(id);

        SimpleDateFormat sdf = new SimpleDateFormat();
        Date d = null;
        try {
            d = sdf.parse(operationDto.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date d1 = oldOperation.getDate();
        if (d1 != d) {
            oldOperation.setDate(d);
        }


        Account a = null;
        //if account hasnt changed
        if (!oldOperation.getAccount().getName().equals(operationDto.getAccount())) {
            a = accountRepository.findAccountByName(operationDto.getAccount(), u);

        } else a = oldOperation.getAccount();


        if ((oldOperation.getSum().compareTo(new BigDecimal(operationDto.getSum()))) != 0) {
            BigDecimal difference = oldOperation.getSum().subtract(new BigDecimal(operationDto.getSum()));

            if (a != null) {
                BigDecimal correctedBalance = a.getBalance().add(difference);
                if (correctedBalance.compareTo(BigDecimal.ZERO) >= 0) {
                    a.setBalance(correctedBalance);
                } else throw new NoEnoughMoneyAtAccountException("no enough money");

            }
            if (a != null) {
                oldOperation.setAccount(a);
            }
            oldOperation.setSum(new BigDecimal(operationDto.getSum()));
        }


        Subcategory s = null;

        try {
            if (!oldOperation.getSubcategory().getCategory().getName().equals(operationDto.getCategory())) {
                s = subcategoryRepository.findSubcategoryByCategoryAndName(operationDto.getSubcategory(), operationDto.getCategory(), u);
                oldOperation.setSubcategory(s);

            } else if (!oldOperation.getSubcategory().getName().equals(operationDto.getSubcategory())) {
                s = subcategoryRepository.findSubcategoryByCategoryAndName(operationDto.getSubcategory(), operationDto.getCategory(), u);
                oldOperation.setSubcategory(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Operation o = operationRepository.save(oldOperation);
        return o;
    }

    @Transactional
    public List<Operation> getOperations(OperationCriteriaDto operationCriteriaDto) {
        User u = userRepository.getByUserName(SecurityHelper.getUser());

        OperationCriteria operationCriteria=new OperationCriteria();
        operationCriteria.setAccounts(operationCriteriaDto.getAccounts());
        operationCriteria.setCategories(operationCriteriaDto.getCategories());
        operationCriteria.setCurrencies(operationCriteriaDto.getCurrencies());
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        Date start = null, end = null;
        try {
            start = format.parse(operationCriteriaDto.getStart());
            end = format.parse(operationCriteriaDto.getEnd());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long  averageMod=1;
        DateTime dateTime1 = new DateTime(start);
        DateTime dateTime2 = new DateTime(end);
        switch (operationCriteriaDto.getRoundMod()){
            case 1:averageMod=Days.daysBetween(dateTime1, dateTime2).getDays()+1; break;
            case 2:averageMod = Weeks.weeksBetween(dateTime1, dateTime2).getWeeks()+1;break;
            case 3:averageMod = Months.monthsBetween(dateTime1, dateTime2).getMonths()+1;break;
            case 4:averageMod = Years.yearsBetween(dateTime1, dateTime2).getYears()+1;break;
            case 5: averageMod=1;
        }

        int period=operationCriteriaDto.getPeriodOption();
        DateTime start1=null;
        DateTime end1=null;
        DateTime date=new DateTime();

        switch(period){
            //this day
            case 1:
                start=Calendar.getInstance().getTime();
                end=new Date();
                break;
            //this week
            case 2:
                start1=date.weekOfWeekyear().roundFloorCopy();
                end1=date.weekOfWeekyear().roundCeilingCopy();
                break;
            //this month
            case 3:
                start1=date.monthOfYear().roundFloorCopy();
                end1=date.monthOfYear().roundCeilingCopy();
                break;
            //this year
            case 4:
                start1=date.year().roundFloorCopy();
                end1=date.year().roundCeilingCopy();
                break;

            case 6:
                start1=date.minusDays(7);
                end1=date;
                break;

            //last 6 month
            case 7:
                start1=date.minusMonths(6);
                end1=date;
                break;

            //last month
            case 8:
                start1=date.minusMonths(1);
                end1=date;
                break;

            //last year
            case 9:
                start1=date.minusYears(1);
                end1=date;
                break;

            //all time
            case 10:
                start1=date.minusYears(100);
                end1=date;
                break;

            //Other period
            case 11:
                start1=new DateTime(start);
                end1=new DateTime(end);

        }


        operationCriteria.setEnd(new Date(end1.getMillis()));
        operationCriteria.setStart(new Date(start1.getMillis()));
        List<Operation> operations=reportRepository.findAllByCriteria(operationCriteria);
        return operations;
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public Map<String, BigDecimal> getReport(OperationCriteriaDto operationCriteriaDto) {
        User u = userRepository.getByUserName(SecurityHelper.getUser());

        OperationCriteria operationCriteria=new OperationCriteria();
        operationCriteria.setAccounts(operationCriteriaDto.getAccounts());
        operationCriteria.setCategories(operationCriteriaDto.getCategories());
        operationCriteria.setCurrencies(operationCriteriaDto.getCurrencies());
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        Date start = null, end = null;
        try {
            start = format.parse(operationCriteriaDto.getStart());
            end = format.parse(operationCriteriaDto.getEnd());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long  averageMod=1;
        DateTime dateTime1 = new DateTime(start);
        DateTime dateTime2 = new DateTime(end);
        switch (operationCriteriaDto.getRoundMod()){
            case 1:averageMod=Days.daysBetween(dateTime1, dateTime2).getDays()+1; break;
            case 2:averageMod = Weeks.weeksBetween(dateTime1, dateTime2).getWeeks()+1;break;
            case 3:averageMod = Months.monthsBetween(dateTime1, dateTime2).getMonths()+1;break;
            case 4:averageMod = Years.yearsBetween(dateTime1, dateTime2).getYears()+1;break;
            case 5: averageMod=1;
        }

        int period=operationCriteriaDto.getPeriodOption();
        DateTime start1=null;
        DateTime end1=null;
        DateTime date=new DateTime();

        switch(period){
            //this day
            case 1:
                start=Calendar.getInstance().getTime();
                end=new Date();
                break;
            //this week
            case 2:
                start1=date.weekOfWeekyear().roundFloorCopy();
                end1=date.weekOfWeekyear().roundCeilingCopy();
                break;
            //this month
            case 3:
                start1=date.monthOfYear().roundFloorCopy();
                end1=date.monthOfYear().roundCeilingCopy();
                break;
            //this year
            case 4:
                start1=date.year().roundFloorCopy();
                end1=date.year().roundCeilingCopy();
                break;

            case 6:
                start1=date.minusDays(7);
                end1=date;
                break;

            //last 6 month
            case 7:
                start1=date.minusMonths(6);
                end1=date;
                break;

            //last month
            case 8:
                start1=date.minusMonths(1);
                end1=date;
                break;

            //last year
            case 9:
                start1=date.minusYears(1);
                end1=date;
                break;

            //all time
            case 10:
                start1=date.minusYears(100);
                end1=date;
                break;

            //Other period
            case 11:
                start1=new DateTime(start);
                end1=new DateTime(end);

        }


        operationCriteria.setEnd(new Date(end1.getMillis()));
        operationCriteria.setStart(new Date(start1.getMillis()));
        CurrencyRates rates = rateService.getRatesAccordingToSetting(u);

        CurrenciesConverter converter = new CurrenciesConverter(rates, u);

        List<AccountStat> stats = null;
        if(operationCriteriaDto.isReportByCategory()){
           stats= reportRepository.getCategoriesCostsForPeriod(operationCriteria);
        }else{
            stats = reportRepository.getSubcategoriesCostsForPeriod(operationCriteria);
        }
        Map<String, BigDecimal> map=new HashMap();

        for(AccountStat accountStat:stats){
            if(map.get(accountStat.getName())!=null){
                BigDecimal previousValue=map.get(accountStat.getName());

                BigDecimal newValue;
                if(accountStat.getType()!=operationCriteriaDto.getCurrencies()){

                    BigDecimal convertedValue=converter.convert(new BigDecimal(accountStat.getBalance()),accountStat.getType(), operationCriteriaDto.getCurrencies());
                    newValue=previousValue.add(convertedValue);

                }else{
                    newValue=previousValue.add(new BigDecimal(accountStat.getBalance()));

                }
                map.put(accountStat.getName(), newValue.divide(new BigDecimal(averageMod), BigDecimal.ROUND_CEILING));
            }
            else{

                BigDecimal convertedValue = null;
                if(accountStat.getType()!=operationCriteriaDto.getCurrencies()){
                    convertedValue=converter.convert(new BigDecimal(accountStat.getBalance()),accountStat.getType(), operationCriteriaDto.getCurrencies());

                }
                else{
                        convertedValue=new BigDecimal(accountStat.getBalance());

                };
                map.put(accountStat.getName(), convertedValue.divide(new BigDecimal(averageMod), RoundingMode.CEILING));
            }


        }

        return map;
    }

    @Transactional
    public List<Operation> findPageOfOperation(Integer page, Integer size) {

        List<Operation> operations=operationRepository.findAll(new PageRequest(page, size)).getContent();
        return operations;
    }
}
