package ua.kiev.sa.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.exceptions.SourceExistsException;
import ua.kiev.sa.exceptions.SubsourceExistsException;
import ua.kiev.sa.model.IncomeSource;
import ua.kiev.sa.model.Node;
import ua.kiev.sa.model.Subsource;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.SourceDto;
import ua.kiev.sa.model.dto.SubsourceDto;
import ua.kiev.sa.repository.SourceRepository;
import ua.kiev.sa.repository.OperationRepository;
import ua.kiev.sa.repository.SubsourceRepository;
import ua.kiev.sa.repository.UserRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service("sourceService")
public class SourceServiceImpl implements SourcesService {

    @Qualifier("sourceRepository")
    @Autowired
    SourceRepository sourceRepository;
    @Qualifier("subsourceRepository")
    @Autowired
    SubsourceRepository subsourceRepository;
    @Autowired
    UserRepository userRepository;
    @Qualifier("operationRepository")
    @Autowired
    OperationRepository operationRepository;

    @Transactional
    public List<IncomeSource> getSources(User user) {
        return sourceRepository.findAll(user);
    }

    @Transactional
    public Map<IncomeSource, List<Subsource>> getSources(String login) {
        User u = userRepository.getByUserName(login);
        List<IncomeSource> cats = sourceRepository.findAll(u);
        Map<IncomeSource, List<Subsource>> map = new HashMap<IncomeSource, List<Subsource>>();
        for (IncomeSource c : cats) {
            map.put(c, c.getSubsources());
        }
        return map;
    }

    @Transactional
    public IncomeSource createSource(SourceDto sourceDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        IncomeSource source = new IncomeSource();
        source.setName(sourceDto.getName());
        source.setUser(userRepository.getByUserName(login));
        return sourceRepository.save(source);
    }

    @Transactional
    public List<IncomeSource> getSourcesList(User user) {
        return sourceRepository.findAllSourcesForUser(user);

    }

    @Transactional
    public Subsource createSubsource(SubsourceDto subsourceDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        Subsource subsource = new Subsource();
        subsource.setName(subsourceDto.getName());
        User u = userRepository.getByUserName(login);
        subsource.setUser(u);
        subsource.setSource(sourceRepository.findIncomeSourceByName(subsourceDto.getSourceName(), u));
        return subsourceRepository.save(subsource);
    }

    @Transactional
    public Subsource createSubsource(String subcatName, Long catId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        Subsource subsource = new Subsource();
        subsource.setName(subcatName);
        User u = userRepository.getByUserName(login);
        subsource.setUser(u);
        subsource.setSource(sourceRepository.findOne(catId));
        return subsourceRepository.save(subsource);
    }

    @Transactional
    public boolean deleteIncomeSource(Long id) {
        boolean b = false;
//        IncomeSource c=sourceRepository.findOne(id) ;


        sourceRepository.delete(id);
        b = sourceRepository.exists(id);
        return !b;
    }

    @Transactional
    public boolean deleteSubsource(Long id) {
        boolean b = false;
        try {
            subsourceRepository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        b = subsourceRepository.exists(id);
        return !b;
    }

    public Subsource getSubsourceByName(String subsource, String cat, String login) {
        User u = userRepository.getByUserName(login);
        IncomeSource c = sourceRepository.findIncomeSourceByName(cat, u);
        Subsource s = subsourceRepository.findSubsourceByIncomeSourceAndName(subsource, c.getName(), u);
        return s;
    }

    public Subsource getSubsourceByName(String subsource) {
        return null;
    }

    public List<IncomeSource> getSourcesOfOperations(User user) {

        return operationRepository.findAllSourcesOfOperationsForUser(user.getId());
    }

    @Transactional
    public List<Subsource> getSubsources(Long sourceId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User u = userRepository.getByUserName(login);
        IncomeSource source1 = sourceRepository.findOne(sourceId);

        return subsourceRepository.findAllSubcategoriesForUserAndIncomeSource(source1, u);

    }

    public Map getSourcesNodes(String login, List<IncomeSource> categories) {
        Map<String, Node> categoriesMap = new HashMap<String, Node>();
        for (IncomeSource c : categories) {
            Node n = new Node(c.getId().toString(), "#", c.getName());
            categoriesMap.put(n.getId(), n);
        }

        return categoriesMap;
    }

    public Map getSubsourcesForJSON(String login, List<Subsource> subcategoriesList) {
        Map<String, Node> subcategoriesMap = new HashMap<String, Node>();
        for (Subsource c : subcategoriesList) {
            Node n = new Node(c.getName(), c.getSource().getName(), c.getName());
            subcategoriesMap.put(n.getId(), n);
        }
        return subcategoriesMap;
    }

    public Map getSubsourcesForJSON(List<Subsource> subcategoriesList) {
        Map<String, Node> subcategoriesMap = new HashMap<String, Node>();
        for (Subsource c : subcategoriesList) {
            Node n = new Node("s" + c.getId(), "c" + c.getSource().getId(), c.getName());
            subcategoriesMap.put(n.getId(), n);
        }
        return subcategoriesMap;
    }

    public Map getSourcesNodes(Set<IncomeSource> categories) {
        Map<String, Node> categoriesMap = new HashMap<String, Node>();
        for (IncomeSource c : categories) {
            Node n = new Node("c" + c.getId(), "#", c.getName());
            categoriesMap.put(n.getId(), n);
        }
        return categoriesMap;
    }

    @Transactional
    public boolean updateIncomeSource(Long id, String text) throws SourceExistsException {
        IncomeSource oldIncomeSource = sourceRepository.findOne(id);

        if (sourceExists(text, oldIncomeSource.getUser().getId())) {
            throw new SourceExistsException("This source is already exists: " +
                    text);
        }

        IncomeSource source = sourceRepository.findOne(id);
        source.setName(text);
        source = sourceRepository.save(source);
        if (source == null) return false;
        else return true;

    }

    @Transactional
    public boolean updateSubsource(Long id, String newName, Long parentId) throws SubsourceExistsException {
        Subsource oldSubsource = subsourceRepository.findOne(id);


        if (subsourceExists(newName, parentId, oldSubsource.getUser().getId())) {
            throw new SubsourceExistsException("This subsource is already exists: " +
                    newName);
        }

        Subsource subsource = subsourceRepository.findOne(id);
        subsource.setName(newName);
        Subsource subsource1 = subsourceRepository.save(subsource);
        return subsource1 != null;
    }

    public IncomeSource getIncomeSourceById(Long catId) {
        return sourceRepository.findOne(catId);
    }

    public Subsource getSubsourceById(Long catId) {
        return subsourceRepository.findOne(catId);
    }

    private boolean subsourceExists(String text, Long parentId, Long userId) {
        Subsource subsource = subsourceRepository.findSubsourceByParentIdAndUserId(text, parentId, userId);
        if (subsource != null) {
            return true;
        }
        return false;
    }


    public boolean sourceExists(String name, Long id) {
        IncomeSource source = sourceRepository.findByNameAndUserId(name, id);
        if (source != null) {
            return true;
        }
        return false;
    }

    public Map getSubsourcesNodes(String login, List<Subsource> subcategories) {
        Map<String, Node> subcategoriesMap = new HashMap<String, Node>();
        for (Subsource s : subcategories) {
            Node n = new Node(s.getId().toString(), s.getSource().getName(), s.getName());
            subcategoriesMap.put(n.getId(), n);
        }

        return subcategoriesMap;
    }
}
