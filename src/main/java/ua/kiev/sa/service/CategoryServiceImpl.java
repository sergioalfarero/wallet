package ua.kiev.sa.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.exceptions.CategoryExistsException;
import ua.kiev.sa.exceptions.SubcategoryExistsException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.CategoryDto;
import ua.kiev.sa.model.dto.SubcategoryDto;
import ua.kiev.sa.repository.CategoryRepository;
import ua.kiev.sa.repository.OperationRepository;
import ua.kiev.sa.repository.SubcategoryRepository;
import ua.kiev.sa.repository.UserRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    @Qualifier("categoryRepository")
    @Autowired
    CategoryRepository categoryRepository;
    @Qualifier("subcategoryRepository")
    @Autowired
    SubcategoryRepository subcategoryRepository;
    @Autowired
    UserRepository userRepository;
    @Qualifier("operationRepository")
    @Autowired
    OperationRepository operationRepository;

    @Transactional
    public List<Category> getCategories(User user) {
        return categoryRepository.findAll(user);
    }

    @Transactional
    public Map<Category, List<Subcategory>> getCategories(String login) {
        User u = userRepository.getByUserName(login);
        List<Category> cats = categoryRepository.findAll(u);
        Map<Category, List<Subcategory>> map = new HashMap<Category, List<Subcategory>>();
        for (Category c : cats) {
            map.put(c, c.getSubcategories());
        }
        return map;
    }

    @Transactional
    public Category createCategory(CategoryDto categoryDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        Category category = new Category();
        category.setName(categoryDto.getName());
        category.setUser(userRepository.getByUserName(login));
        return categoryRepository.save(category);
    }

    @Transactional
    public List<Category> getCategoriesList(User user) {
        return categoryRepository.findAllCategoriesForUser(user);

    }

    @Transactional
    public Subcategory createSubcategory(SubcategoryDto subcategoryDto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        Subcategory subcategory = new Subcategory();
        subcategory.setName(subcategoryDto.getName());
        User u = userRepository.getByUserName(login);
        subcategory.setUser(u);
        subcategory.setCategory(categoryRepository.findCategoryByName(subcategoryDto.getCategoryName(), u));
        return subcategoryRepository.save(subcategory);
    }

    @Transactional
    public Subcategory createSubcategory(String subcatName, Long catId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        Subcategory subcategory = new Subcategory();
        subcategory.setName(subcatName);
        User u = userRepository.getByUserName(login);
        subcategory.setUser(u);
        subcategory.setCategory(categoryRepository.findOne(catId));
        return subcategoryRepository.save(subcategory);
    }

    @Transactional
    public boolean deleteCategory(Long id) {
        boolean b = false;
//        Category c=categoryRepository.findOne(id) ;


        categoryRepository.delete(id);
        b = categoryRepository.exists(id);
        return !b;
    }

    @Transactional
    public boolean deleteSubcategory(Long id) {
        boolean b = false;
        try {
            subcategoryRepository.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        b = subcategoryRepository.exists(id);
        return !b;
    }

    public Subcategory getSubcategoryByName(String subcategory, String cat, String login) {
        User u = userRepository.getByUserName(login);
        Category c = categoryRepository.findCategoryByName(cat, u);
        Subcategory s = subcategoryRepository.findSubcategoryByCategoryAndName(subcategory, c.getName(), u);
        return s;
    }

    public Subcategory getSubcategoryByName(String subcategory) {
        return null;
    }

    public List<Category> getCategoriesOfOperations(User user) {

        return operationRepository.findAllCategoriesOfOperationsForUser(user.getId());
    }

    @Transactional
    public List<Subcategory> getSubcategories(Long categoryId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        User u = userRepository.getByUserName(login);
        Category category1 = categoryRepository.findOne(categoryId);

        return subcategoryRepository.findAllSubcategoriesForUserAndCategory(category1, u);

    }

    public Map getCategoriesNodes(String login, List<Category> categories) {
        Map<String, Node> categoriesMap = new HashMap<String, Node>();
        for (Category c : categories) {
            Node n = new Node(c.getId().toString(), "#", c.getName());
            categoriesMap.put(n.getId(), n);
        }

        return categoriesMap;
    }

    public Map getSubcategoriesForJSON(String login, List<Subcategory> subcategoriesList) {
        Map<String, Node> subcategoriesMap = new HashMap<String, Node>();
        for (Subcategory c : subcategoriesList) {
            Node n = new Node(c.getName(), c.getCategory().getName(), c.getName());
            subcategoriesMap.put(n.getId(), n);
        }
        return subcategoriesMap;
    }

    public Map getSubcategoriesForJSON(List<Subcategory> subcategoriesList) {
        Map<String, Node> subcategoriesMap = new HashMap<String, Node>();
        for (Subcategory c : subcategoriesList) {
            Node n = new Node("s" + c.getId(), "c" + c.getCategory().getId(), c.getName());
            subcategoriesMap.put(n.getId(), n);
        }
        return subcategoriesMap;
    }

    public Map getCategoriesNodes(Set<Category> categories) {
        Map<String, Node> categoriesMap = new HashMap<String, Node>();
        for (Category c : categories) {
            Node n = new Node("c" + c.getId(), "#", c.getName());
            categoriesMap.put(n.getId(), n);
        }

        return categoriesMap;
    }
    public Map getCategoriesNodesWithRoot(Set<Category> categories) {
        Map<String, Node> categoriesMap = new HashMap<String, Node>();
        for (Category c : categories) {
            Node n = new Node("c" + c.getId(), "root", c.getName());
            categoriesMap.put(n.getId(), n);
        }
        Node root=new Node("root", "#", "All");
        categoriesMap.put(root.getId(), root);
        return categoriesMap;
    }

    @Transactional
    public boolean updateCategory(Long id, String text) throws CategoryExistsException {
        Category oldCategory = categoryRepository.findOne(id);

        if (categoryExists(text, oldCategory.getUser().getId())) {
            throw new CategoryExistsException("This category is already exists: " +
                    text);
        }

        Category category = categoryRepository.findOne(id);
        category.setName(text);
        category = categoryRepository.save(category);
        if (category == null) return false;
        else return true;

    }

    @Transactional
    public boolean updateSubcategory(Long id, String newName, Long parentId) throws SubcategoryExistsException {
        Subcategory oldSubcategory = subcategoryRepository.findOne(id);


        if (subcategoryExists(newName, parentId, oldSubcategory.getUser().getId())) {
            throw new SubcategoryExistsException("This subcategory is already exists: " +
                    newName);
        }

        Subcategory subcategory = subcategoryRepository.findOne(id);
        subcategory.setName(newName);
        Subcategory subcategory1 = subcategoryRepository.save(subcategory);
        return subcategory1 != null;
    }

    public Category getCategoryById(Long catId) {
        return categoryRepository.findOne(catId);
    }

    public Subcategory getSubcategoryById(Long catId) {
        return subcategoryRepository.findOne(catId);
    }

    private boolean subcategoryExists(String text, Long parentId, Long userId) {
        Subcategory subcategory = subcategoryRepository.findSubcategoryByParentIdAndUserId(text, parentId, userId);
        if (subcategory != null) {
            return true;
        }
        return false;
    }


    public boolean categoryExists(String name, Long id) {
        Category category = categoryRepository.findByNameAndUserId(name, id);
        if (category != null) {
            return true;
        }
        return false;
    }

    public Map getSubcategoriesNodes(String login, List<Subcategory> subcategories) {
        Map<String, Node> subcategoriesMap = new HashMap<String, Node>();
        for (Subcategory s : subcategories) {
            Node n = new Node(s.getId().toString(), s.getCategory().getName(), s.getName());
            subcategoriesMap.put(n.getId(), n);
        }

        return subcategoriesMap;
    }
}
