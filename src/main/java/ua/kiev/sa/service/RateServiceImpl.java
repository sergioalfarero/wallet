package ua.kiev.sa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.model.*;
import ua.kiev.sa.repository.RateRepository;
import ua.kiev.sa.repository.UserRepository;
import ua.kiev.sa.utils.CurrencyRatesDownloader;

import java.math.BigDecimal;
import java.util.*;

@Service("rateService")
public class RateServiceImpl implements RateService {

    @Qualifier("rateRepository")
    @Autowired
    RateRepository rateRepository;
    @Autowired
    UserRepository userRepository;


    /**
     * Transmits a given entity to repository layer for saving it in database.
     * Return nothing
     *
     * @param currencyRates - currency rate CurrencyRate.class
     */
    @Transactional
    public void save(CurrencyRates currencyRates) {
        rateRepository.save(currencyRates);
    }



    /**
     * Uses given login of user for obtain
     * currency rates for this user
     *
     * @param login - login for authentificated user
     * @retun currency rates for user
     */
    @Transactional
    public CurrencyRates getCustomRatesForLogin(String login) {
        return rateRepository.findForUserLogin(login);
    }

    /**
     * Saves custom user's rates for user with some login
     * If user with transmitted login doesn't have rates
     * than new CurrencyRate object is created.
     * Is used for editing rates
     *
     * @param rates - currency rate CurrencyRate.class
     * @param login - user's login
     */
    @Transactional
    public void save(Set<Rate> rates, String login) {
        CurrencyRates userRates =rateRepository.findForUserLogin(login);
        if (userRates == null) {
            userRates = new CurrencyRates();
        }
        userRates.setRates(rates);
        rateRepository.save(userRates, login);
    }


    /**
     * Adds new rate to the user's currency rates
     * Method retrieves user's rates and
     * if they exists adds new rate to the set with rates
     * otherwise it creates new set of rates and adds new rate to it.
     * Than method saves rates in database for this user
     *
     * @param rate - new rate
     * @param login - user's login
     */
    @Transactional
    public void addRate(Rate rate, String login) {
        CurrencyRates userRates = rateRepository.findForUserLogin(login);
        if (userRates != null) {
            Set<Rate> existingRates = userRates.getRates();
            existingRates.add(rate);
            userRates.setRates(existingRates);

        } else {
            userRates = new CurrencyRates();
            Set<Rate> newRates = new HashSet<Rate>();
            newRates.add(rate);
            userRates.setRates(newRates);
        }
        rateRepository.save(userRates, login);
    }



    /**
     * Returns the latest default rates from database
     */
    @Transactional
    public CurrencyRates getDefaultRates() {
        CurrencyRates currencies=rateRepository.findTop1OrderByDate(new PageRequest(0, 1)).get(0);
        if(!isDefaultRatesUpdated(currencies)){
            Set<Rate> setOfDownloadedRates = new CurrencyRatesDownloader(BankType.NBU).getDaylyCurrencyRates();
            if(setOfDownloadedRates!=null) {
                return updateCurrenciesRates(setOfDownloadedRates);
            }
            else return null;
        }
        return currencies;
    }
    /**
     * Returns Currencies Rates according to settings of user.
     * If default rates is false then user's rates are used.
     * If default rates is true then system rates are used.
     * System rates are downloaded from sites of several central banks
     * if they weren't updated earlier this day
     */
    public CurrencyRates getRatesAccordingToSetting(User user) {
        CurrencyRates rates = new CurrencyRates();
        if (user.getSettings().isDefaultRates()) {
            rates=getDefaultRates();
            if(rates!=null) return rates;
            else return getCustomRatesForLogin(user.getLogin());
        } else {
            rates = getCustomRatesForLogin(user.getLogin());
        }
        return rates;
    }

    /**
     * Creates CurrencyRates object,
     * sets downloaded rates and
     * saves them to database
     *
     * @param rates - downloaded rates
     * @return CurrencyRate object
     */
    @Transactional
    public CurrencyRates updateCurrenciesRates(Set<Rate> rates) {

        CurrencyRates dcr = new CurrencyRates();
        dcr.setBaseRate(BigDecimal.ONE);
        dcr.setBaseType(CurrencyType.UAH);
        dcr.setDate(new Date());
        dcr.setBank(BankType.NBU);
        dcr.setRates(rates);
        save(dcr);
        return dcr;
    }


     private boolean isDefaultRatesUpdated(CurrencyRates defaultCurrencyRates) {
        if (defaultCurrencyRates.getBank() == null) return false;

        Calendar lastDay = Calendar.getInstance();
        lastDay.setTime(defaultCurrencyRates.getDate());
        Calendar thisDay = Calendar.getInstance();
        thisDay.setTime(new Date());

        if (isSameDay(lastDay, thisDay)) {
            return true;
        } else return false;
    }



     private static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }


//    public Map<CurrencyType, Rate> getRateMap(User u) {
//        Map<CurrencyType, Rate> rateMap = new HashMap<CurrencyType, Rate>();
//        CurrencyRates rates = getCustomRatesForLogin(u.getLogin());
//        for (Rate r : rates.getRates()) {
//            rateMap.put(r.getCurrencyType(), r);
//        }
//        return rateMap;
//    }




}


