package ua.kiev.sa.service;

import ua.kiev.sa.exceptions.TokenAlreadyExistsException;
import ua.kiev.sa.exceptions.TokenNotFoundException;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.VerificationToken;

public interface VerificationTokenService {
    VerificationToken getVerificationToken(String token) throws TokenNotFoundException;

    void createVerificationToken(User user, String token) throws TokenAlreadyExistsException;

    VerificationToken generateNewVerificationToken(String existingToken) throws TokenNotFoundException;


}
