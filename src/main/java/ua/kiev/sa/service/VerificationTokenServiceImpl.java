package ua.kiev.sa.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.kiev.sa.constants.Constants;
import ua.kiev.sa.exceptions.TokenAlreadyExistsException;
import ua.kiev.sa.exceptions.TokenNotFoundException;
import ua.kiev.sa.exceptions.UserNotFoundException;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.VerificationToken;
import ua.kiev.sa.repository.UserRepository;
import ua.kiev.sa.repository.VerificationTokenRepository;

import java.util.UUID;

import static ua.kiev.sa.model.QUser.user;

@Service("verificationTokenService")
public class VerificationTokenServiceImpl implements VerificationTokenService {
    @Qualifier("verificationTokenRepository")
    @Autowired
    VerificationTokenRepository verificationTokenRepository;
    @Autowired
    UserRepository userRepository;

    @Transactional
    public VerificationToken generateNewVerificationToken(String existingToken) throws TokenNotFoundException {
        if (existingToken==null)throw new IllegalArgumentException("Token can't be null");
        if(!existingToken.matches(Constants.TOKEN_PATTERN))throw new IllegalArgumentException("Token doesn't match pattern");
        VerificationToken oldToken = verificationTokenRepository.findByToken(existingToken);

        VerificationToken newToken =null;
        if(oldToken!=null){
            newToken = verificationTokenRepository.save(oldToken.updateToken(UUID.randomUUID().toString()));

        }else throw new TokenNotFoundException("Old verification token doesn't exists");
        return newToken;
    }

    @Transactional
    public VerificationToken getVerificationToken(String token) throws TokenNotFoundException {
        if (token==null)throw new IllegalArgumentException("Token can't be null");
        if(!token.matches(Constants.TOKEN_PATTERN))throw new IllegalArgumentException("Token doesn't match pattern");

        VerificationToken verificationToken = verificationTokenRepository.findByToken(token);
        if (verificationToken == null) throw new TokenNotFoundException("Token " + token + " doesn't exists");
        else return verificationToken;
    }

    //creates VerificationToken for user. Check

    @Transactional
    public void createVerificationToken(User user, String token) throws TokenAlreadyExistsException {

        if(user!=null&&token!=null) {
            User u = userRepository.getByUserName(user.getLogin());
            if(u!=null) {
                VerificationToken existingToken=verificationTokenRepository.findByToken(token);
                if(existingToken!=null) {
                    throw new TokenAlreadyExistsException("Generated token " + token + "already exists");
                }
                VerificationToken myToken = new VerificationToken(token, u);
                verificationTokenRepository.save(myToken);

            }else throw new UserNotFoundException("User "+user.getLogin()+" not found");
        }else throw new IllegalArgumentException("Illegal parameter passed");

    }
}
