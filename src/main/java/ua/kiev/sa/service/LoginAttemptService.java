package ua.kiev.sa.service;


public interface LoginAttemptService {

    void loginSucceeded(String remoteAddress);

    boolean isBlocked(String ip);

    void loginFailed(String remoteAddress);
}
