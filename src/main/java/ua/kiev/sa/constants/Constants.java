package ua.kiev.sa.constants;

/**
 * The {@code Constants} class includes constants used in application
 *
*/
public class Constants {

    /**name of DB schema */
    public static final String SCHEMA = "WALLET";

    /**path to images of accounts */
    public static final String DEFAULT_ICON_PATH="/resources/img/default/banks";

    /**pattern used to verify that token is valid
     * Token should consists of 36 symbols (letters and numbers)*/
    public static final String TOKEN_PATTERN="^[a-f0-9-]{36}";

    /** defines number of operations to show by default on the page of operations*/
    public static final Integer NUMBER_OF_OPERATIONS_TO_SHOW=7;


}
