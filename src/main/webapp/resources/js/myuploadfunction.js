function myiconLoad2(files) {


    var x = ($('input[name=_csrf]').val());
    if (!files || files.length != 1) {
        alert('Файл не выбран. Что-то не так у вас в браузере.');
        return false;
    }
    if (files[0].size > 1024 * 1024) {
        alert('Файл слишком большой. Иконка не может быть такого размера.');
        return false;
    }


    var fd = new FormData();
    fd.append("myicon", files[0]);


    $.ajax({
        url: '/upload?' + '_csrf' + '=' + x,
        data: fd,
        dataType: 'text',
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            var firstIndex = data.indexOf("resources");
            var lastIndex = data.indexOf("'/>");
            var value = data.substring(firstIndex, lastIndex);

            var code = '<span style="float:left;padding-right:25px;">' +
                '<input type="radio" name="icon" id="icon" value="' + value + '">'
                + '<label for="' + value + '"><span class="ch"></span>'
                + data + '</label>'
                +'<span style="color: black; border-bottom:1px dashed gray; cursor:pointer;" class="tMini "'
            +'onclick="myiconDelete("'+value+'");">Удалить</span>';
            //var alreadyExists=$('#result').;
            //                       alert(alreadyExists);
            //var newCode =alreadyExists+''+code;
            //alert(newCode);
            $('#result').after(code);
        }
    });

}
function myiconDelete(id) {

    if (!confirm('Точно удалить?')) {
        return;
    }
    var x = ($('input[name=_csrf]').val());
    alert('image=' + id);
    $.ajax({
        contentType: "application/json",
        data: JSON.stringify(id),
        url: '/deleteIcon?' + '_csrf' + '=' + x,
        type: 'post',
        timeout: 100000,
        success: function () {

            //var forDelete = '<span style="float:left;padding-right:25px;">' +
            //    '<input type="radio" name="icon" value=' + id + '">' +
            //    '<label for="' + id + '"><span class="ch"></span><img src="' + id + '" align="absmiddle" width="24" height="24"></label>' +
            //    '<span style="color: black; border-bottom:1px dashed gray; cursor:pointer; " class="tMini " onclick="myiconDelete(\'' + id + '\');">Удалить</span>' +
            //    '</span>';
            alert(id);
            var el = document.getElementById(id);
            alert(el)
            el.parentNode.removeChild(el);
            //alert(forDelete);
        },
        error: function (e) {
            alert('error' + e);
        },
        done: function (e) {
            alert('done' + e);
        }
    });
    //var myAjax = new Ajax.Updater(
    //    {
    //        success: ''
    //    },
    //    '/deleteIcon',
    //    {
    //        method: 'post',
    //        parameters: 'icon' + id,
    //        onFailure: function () {
    //            window.alert('Нет соединения с сервером');
    //        },
    //        evalScripts: true
    //    }
    //);
}


function deleteItem(element) {
    // Удаляем элемент
    $(element).parent().remove()
    // Если кнопка добавления элемента скрыта, то показываем ее.
    if ($('#adder:hidden')) {
        $('#add').css({
            'display': 'inline-block'
        })
    }
}

