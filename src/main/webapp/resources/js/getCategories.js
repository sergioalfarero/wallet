jQuery(document).ready(

    function () {
        findCategories();
    });

$(document).ready(function () {
    $('#category').change(
        function () {
            var categoryName=$("#category :selected").text();
            if(categoryName!="Category"){

            $.getJSON('/findsubcategories', {
                category: $(this).val(),
                ajax: 'true'
            }, function (data) {
                var len = data.length;
                if(len!=0&&categoryName!="Category"){

                var html = '';
                for (var i = 0; i < len; i++) {
                    html += '<option value="' + data[i].id + '">'
                        + data[i].name + '</option>';
                }
                html += '</option>';

                $('#subcategory').html(html);
                jQuery("#subcatDiv").show();

                } else{
                    $('#subcategory').html('<option value="">Subcategory</option>');
                    jQuery("#subcatDiv").hide();

                }
                jQuery("#addNewOperationButton").prop("disabled", false
                );

            });

            }else{
                jQuery("#addNewOperationButton").prop("disabled", true);
                $('#subcategory').html('<option value="">Subcategory</option>');
                jQuery("#subcatDiv").hide();

            }
            checkFieldValues();
        });
});
function findCategories(){
    jQuery.getJSON('/findcategories', {
        ajax: 'true'
    }, function (data) {
        var html = '<option value="">Category</option>';
        var len = data.length;

        for (var i = 0; i < len; i++) {
            html += '<option value="' + data[i].id + '">'
                + data[i].name + '</option>';
        }
        html += '</option>';
        jQuery('#category').html(html);

    });
}