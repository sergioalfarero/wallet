/* $Id: allOne.js 3723 2016-06-08 09:34:10Z alexan $ */
//************************************************************
// ��?�����?���� js, �?�?���?�?��
//************************************************************
function setCookie(name, value, days, hours, minutes, domain) {	// �?�������?�? cookie
    var expires = "";
    var str = name + '=' + escape(value);
    if (days || hours || minutes) {
        var date = new Date();
        days = days ? days : 0;
        hours = hours ? hours : 0;
        minutes = minutes ? minutes : 0;
        date.setTime(date.getTime() + 60 * 1000 * (minutes + 60 * (hours + 24 * days)));
        var expires = date.toGMTString();
    }
    if (expires) str += '; expires=' + expires;
    str += '; path=/';
    if (typeof(domain) === "undefined") {
        domain = '.' + window.location.hostname;
    }
    str += '; domain=' + domain;
    document.cookie = str;
}
function getCookie(name) {								// �?�����?�?�������� cookie
    var c = document.cookie
    if (c.indexOf(name) == -1) return null
    else {
        var start = c.indexOf(name) + name.length + 1
        for (var end = start; end < c.length; end++) {
            if (c.charAt(end) == ";") break
        }
        return unescape(c.substring(start, end))
    }
}
function deleteCookie(name) {							// �������?�?������������ cookie
    setCookie(name, "", -1)
}

/*  Prototype JavaScript framework, version 1.7
 *  (c) 2005-2010 Sam Stephenson
 *
 *  Prototype is freely distributable under the terms of an MIT-style license.
 *  For details, see the Prototype web site: http://www.prototypejs.org/
 *
 *--------------------------------------------------------------------------*/

var Prototype = {

    Version: '1.7',

    Browser: (function () {
        var ua = navigator.userAgent;
        var isOpera = Object.prototype.toString.call(window.opera) == '[object Opera]';
        return {
            IE: !!window.attachEvent && !isOpera,
            Opera: isOpera,
            WebKit: ua.indexOf('AppleWebKit/') > -1,
            Gecko: ua.indexOf('Gecko') > -1 && ua.indexOf('KHTML') === -1,
            MobileSafari: /Apple.*Mobile/.test(ua)
        }
    })(),

    BrowserFeatures: {
        XPath: !!document.evaluate,

        SelectorsAPI: !!document.querySelector,

        ElementExtensions: (function () {
            var constructor = window.Element || window.HTMLElement;
            return !!(constructor && constructor.prototype);
        })(),
        SpecificElementExtensions: (function () {
            if (typeof window.HTMLDivElement !== 'undefined')
                return true;

            var div = document.createElement('div'),
                form = document.createElement('form'),
                isSupported = false;

            if (div['__proto__'] && (div['__proto__'] !== form['__proto__'])) {
                isSupported = true;
            }

            div = form = null;

            return isSupported;
        })()
    },

    ScriptFragment: '<script[^>]*>([\\S\\s]*?)<\/script>',
    JSONFilter: /^\/\*-secure-([\s\S]*)\*\/\s*$/,

    emptyFunction: function () {
    },

    K: function (x) {
        return x
    }
};

if (Prototype.Browser.MobileSafari)
    Prototype.BrowserFeatures.SpecificElementExtensions = false;


var Abstract = {};


var Try = {
    these: function () {
        var returnValue;

        for (var i = 0, length = arguments.length; i < length; i++) {
            var lambda = arguments[i];
            try {
                returnValue = lambda();
                break;
            } catch (e) {
            }
        }

        return returnValue;
    }
};

/* Based on Alex Arnell's inheritance implementation. */

var Class = (function () {

    var IS_DONTENUM_BUGGY = (function () {
        for (var p in {toString: 1}) {
            if (p === 'toString') return false;
        }
        return true;
    })();

    function subclass() {
    };
    function create() {
        var parent = null, properties = $A(arguments);
        if (Object.isFunction(properties[0]))
            parent = properties.shift();

        function klass() {
            this.initialize.apply(this, arguments);
        }

        Object.extend(klass, Class.Methods);
        klass.superclass = parent;
        klass.subclasses = [];

        if (parent) {
            subclass.prototype = parent.prototype;
            klass.prototype = new subclass;
            parent.subclasses.push(klass);
        }

        for (var i = 0, length = properties.length; i < length; i++)
            klass.addMethods(properties[i]);

        if (!klass.prototype.initialize)
            klass.prototype.initialize = Prototype.emptyFunction;

        klass.prototype.constructor = klass;
        return klass;
    }

    function addMethods(source) {
        var ancestor = this.superclass && this.superclass.prototype,
            properties = Object.keys(source);

        if (IS_DONTENUM_BUGGY) {
            if (source.toString != Object.prototype.toString)
                properties.push("toString");
            if (source.valueOf != Object.prototype.valueOf)
                properties.push("valueOf");
        }

        for (var i = 0, length = properties.length; i < length; i++) {
            var property = properties[i], value = source[property];
            if (ancestor && Object.isFunction(value) &&
                value.argumentNames()[0] == "$super") {
                var method = value;
                value = (function (m) {
                    return function () {
                        return ancestor[m].apply(this, arguments);
                    };
                })(property).wrap(method);

                value.valueOf = method.valueOf.bind(method);
                value.toString = method.toString.bind(method);
            }
            this.prototype[property] = value;
        }

        return this;
    }

    return {
        create: create,
        Methods: {
            addMethods: addMethods
        }
    };
})();
(function () {

    var _toString = Object.prototype.toString,
        NULL_TYPE = 'Null',
        UNDEFINED_TYPE = 'Undefined',
        BOOLEAN_TYPE = 'Boolean',
        NUMBER_TYPE = 'Number',
        STRING_TYPE = 'String',
        OBJECT_TYPE = 'Object',
        FUNCTION_CLASS = '[object Function]',
        BOOLEAN_CLASS = '[object Boolean]',
        NUMBER_CLASS = '[object Number]',
        STRING_CLASS = '[object String]',
        ARRAY_CLASS = '[object Array]',
        DATE_CLASS = '[object Date]',
        NATIVE_JSON_STRINGIFY_SUPPORT = window.JSON &&
            typeof JSON.stringify === 'function' &&
            JSON.stringify(0) === '0' &&
            typeof JSON.stringify(Prototype.K) === 'undefined';

    function Type(o) {
        switch (o) {
            case null:
                return NULL_TYPE;
            case (void 0):
                return UNDEFINED_TYPE;
        }
        var type = typeof o;
        switch (type) {
            case 'boolean':
                return BOOLEAN_TYPE;
            case 'number':
                return NUMBER_TYPE;
            case 'string':
                return STRING_TYPE;
        }
        return OBJECT_TYPE;
    }

    function extend(destination, source) {
        for (var property in source)
            destination[property] = source[property];
        return destination;
    }

    function inspect(object) {
        try {
            if (isUndefined(object)) return 'undefined';
            if (object === null) return 'null';
            return object.inspect ? object.inspect() : String(object);
        } catch (e) {
            if (e instanceof RangeError) return '...';
            throw e;
        }
    }

    function toJSON(value) {
        return Str('', {'': value}, []);
    }

    function Str(key, holder, stack) {
        var value = holder[key],
            type = typeof value;

        if (Type(value) === OBJECT_TYPE && typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

        var _class = _toString.call(value);

        switch (_class) {
            case NUMBER_CLASS:
            case BOOLEAN_CLASS:
            case STRING_CLASS:
                value = value.valueOf();
        }

        switch (value) {
            case null:
                return 'null';
            case true:
                return 'true';
            case false:
                return 'false';
        }

        type = typeof value;
        switch (type) {
            case 'string':
                return value.inspect(true);
            case 'number':
                return isFinite(value) ? String(value) : 'null';
            case 'object':

                for (var i = 0, length = stack.length; i < length; i++) {
                    if (stack[i] === value) {
                        throw new TypeError();
                    }
                }
                stack.push(value);

                var partial = [];
                if (_class === ARRAY_CLASS) {
                    for (var i = 0, length = value.length; i < length; i++) {
                        var str = Str(i, value, stack);
                        partial.push(typeof str === 'undefined' ? 'null' : str);
                    }
                    partial = '[' + partial.join(',') + ']';
                } else {
                    var keys = Object.keys(value);
                    for (var i = 0, length = keys.length; i < length; i++) {
                        var key = keys[i], str = Str(key, value, stack);
                        if (typeof str !== "undefined") {
                            partial.push(key.inspect(true) + ':' + str);
                        }
                    }
                    partial = '{' + partial.join(',') + '}';
                }
                stack.pop();
                return partial;
        }
    }

    function stringify(object) {
        return JSON.stringify(object);
    }

    function toQueryString(object) {
        return $H(object).toQueryString();
    }

    function toHTML(object) {
        return object && object.toHTML ? object.toHTML() : String.interpret(object);
    }

    function keys(object) {
        if (Type(object) !== OBJECT_TYPE) {
            throw new TypeError();
        }
        var results = [];
        for (var property in object) {
            if (object.hasOwnProperty(property)) {
                results.push(property);
            }
        }
        return results;
    }

    function values(object) {
        var results = [];
        for (var property in object)
            results.push(object[property]);
        return results;
    }

    function clone(object) {
        return extend({}, object);
    }

    function isElement(object) {
        return !!(object && object.nodeType == 1);
    }

    function isArray(object) {
        return _toString.call(object) === ARRAY_CLASS;
    }

    var hasNativeIsArray = (typeof Array.isArray == 'function')
        && Array.isArray([]) && !Array.isArray({});

    if (hasNativeIsArray) {
        isArray = Array.isArray;
    }

    function isHash(object) {
        return object instanceof Hash;
    }

    function isFunction(object) {
        return _toString.call(object) === FUNCTION_CLASS;
    }

    function isString(object) {
        return _toString.call(object) === STRING_CLASS;
    }

    function isNumber(object) {
        return _toString.call(object) === NUMBER_CLASS;
    }

    function isDate(object) {
        return _toString.call(object) === DATE_CLASS;
    }

    function isUndefined(object) {
        return typeof object === "undefined";
    }

    extend(Object, {
        extend: extend,
        inspect: inspect,
        toJSON: NATIVE_JSON_STRINGIFY_SUPPORT ? stringify : toJSON,
        toQueryString: toQueryString,
        toHTML: toHTML,
        keys: Object.keys || keys,
        values: values,
        clone: clone,
        isElement: isElement,
        isArray: isArray,
        isHash: isHash,
        isFunction: isFunction,
        isString: isString,
        isNumber: isNumber,
        isDate: isDate,
        isUndefined: isUndefined
    });
})();
Object.extend(Function.prototype, (function () {
    var slice = Array.prototype.slice;

    function update(array, args) {
        var arrayLength = array.length, length = args.length;
        while (length--) array[arrayLength + length] = args[length];
        return array;
    }

    function merge(array, args) {
        array = slice.call(array, 0);
        return update(array, args);
    }

    function argumentNames() {
        var names = this.toString().match(/^[\s\(]*function[^(]*\(([^)]*)\)/)[1]
            .replace(/\/\/.*?[\r\n]|\/\*(?:.|[\r\n])*?\*\//g, '')
            .replace(/\s+/g, '').split(',');
        return names.length == 1 && !names[0] ? [] : names;
    }

    function bind(context) {
        if (arguments.length < 2 && Object.isUndefined(arguments[0])) return this;
        var __method = this, args = slice.call(arguments, 1);
        return function () {
            var a = merge(args, arguments);
            return __method.apply(context, a);
        }
    }

    function bindAsEventListener(context) {
        var __method = this, args = slice.call(arguments, 1);
        return function (event) {
            var a = update([event || window.event], args);
            return __method.apply(context, a);
        }
    }

    function curry() {
        if (!arguments.length) return this;
        var __method = this, args = slice.call(arguments, 0);
        return function () {
            var a = merge(args, arguments);
            return __method.apply(this, a);
        }
    }

    function delay(timeout) {
        var __method = this, args = slice.call(arguments, 1);
        timeout = timeout * 1000;
        return window.setTimeout(function () {
            return __method.apply(__method, args);
        }, timeout);
    }

    function defer() {
        var args = update([0.01], arguments);
        return this.delay.apply(this, args);
    }

    function wrap(wrapper) {
        var __method = this;
        return function () {
            var a = update([__method.bind(this)], arguments);
            return wrapper.apply(this, a);
        }
    }

    function methodize() {
        if (this._methodized) return this._methodized;
        var __method = this;
        return this._methodized = function () {
            var a = update([this], arguments);
            return __method.apply(null, a);
        };
    }

    return {
        argumentNames: argumentNames,
        bind: bind,
        bindAsEventListener: bindAsEventListener,
        curry: curry,
        delay: delay,
        defer: defer,
        wrap: wrap,
        methodize: methodize
    }
})());


(function (proto) {


    function toISOString() {
        return this.getUTCFullYear() + '-' +
            (this.getUTCMonth() + 1).toPaddedString(2) + '-' +
            this.getUTCDate().toPaddedString(2) + 'T' +
            this.getUTCHours().toPaddedString(2) + ':' +
            this.getUTCMinutes().toPaddedString(2) + ':' +
            this.getUTCSeconds().toPaddedString(2) + 'Z';
    }


    function toJSON() {
        return this.toISOString();
    }

    if (!proto.toISOString) proto.toISOString = toISOString;
    if (!proto.toJSON) proto.toJSON = toJSON;

})(Date.prototype);


RegExp.prototype.match = RegExp.prototype.test;

RegExp.escape = function (str) {
    return String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
};
var PeriodicalExecuter = Class.create({
    initialize: function (callback, frequency) {
        this.callback = callback;
        this.frequency = frequency;
        this.currentlyExecuting = false;

        this.registerCallback();
    },

    registerCallback: function () {
        this.timer = setInterval(this.onTimerEvent.bind(this), this.frequency * 1000);
    },

    execute: function () {
        this.callback(this);
    },

    stop: function () {
        if (!this.timer) return;
        clearInterval(this.timer);
        this.timer = null;
    },

    onTimerEvent: function () {
        if (!this.currentlyExecuting) {
            try {
                this.currentlyExecuting = true;
                this.execute();
                this.currentlyExecuting = false;
            } catch (e) {
                this.currentlyExecuting = false;
                throw e;
            }
        }
    }
});
Object.extend(String, {
    interpret: function (value) {
        return value == null ? '' : String(value);
    },
    specialChar: {
        '\b': '\\b',
        '\t': '\\t',
        '\n': '\\n',
        '\f': '\\f',
        '\r': '\\r',
        '\\': '\\\\'
    }
});

Object.extend(String.prototype, (function () {
    var NATIVE_JSON_PARSE_SUPPORT = window.JSON &&
        typeof JSON.parse === 'function' &&
        JSON.parse('{"test": true}').test;

    function prepareReplacement(replacement) {
        if (Object.isFunction(replacement)) return replacement;
        var template = new Template(replacement);
        return function (match) {
            return template.evaluate(match)
        };
    }

    function gsub(pattern, replacement) {
        var result = '', source = this, match;
        replacement = prepareReplacement(replacement);

        if (Object.isString(pattern))
            pattern = RegExp.escape(pattern);

        if (!(pattern.length || pattern.source)) {
            replacement = replacement('');
            return replacement + source.split('').join(replacement) + replacement;
        }

        while (source.length > 0) {
            if (match = source.match(pattern)) {
                result += source.slice(0, match.index);
                result += String.interpret(replacement(match));
                source = source.slice(match.index + match[0].length);
            } else {
                result += source, source = '';
            }
        }
        return result;
    }

    function sub(pattern, replacement, count) {
        replacement = prepareReplacement(replacement);
        count = Object.isUndefined(count) ? 1 : count;

        return this.gsub(pattern, function (match) {
            if (--count < 0) return match[0];
            return replacement(match);
        });
    }

    function scan(pattern, iterator) {
        this.gsub(pattern, iterator);
        return String(this);
    }

    function truncate(length, truncation) {
        length = length || 30;
        truncation = Object.isUndefined(truncation) ? '...' : truncation;
        return this.length > length ?
            this.slice(0, length - truncation.length) + truncation : String(this);
    }

    function strip() {
        return this.replace(/^\s+/, '').replace(/\s+$/, '');
    }

    function stripTags() {
        return this.replace(/<\w+(\s+("[^"]*"|'[^']*'|[^>])+)?>|<\/\w+>/gi, '');
    }

    function stripScripts() {
        return this.replace(new RegExp(Prototype.ScriptFragment, 'img'), '');
    }

    function extractScripts() {
        var matchAll = new RegExp(Prototype.ScriptFragment, 'img'),
            matchOne = new RegExp(Prototype.ScriptFragment, 'im');
        return (this.match(matchAll) || []).map(function (scriptTag) {
            return (scriptTag.match(matchOne) || ['', ''])[1];
        });
    }

    function evalScripts() {
        return this.extractScripts().map(function (script) {
            return eval(script)
        });
    }

    function escapeHTML() {
        return this.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }

    function unescapeHTML() {
        return this.stripTags().replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
    }


    function toQueryParams(separator) {
        var match = this.strip().match(/([^?#]*)(#.*)?$/);
        if (!match) return {};

        return match[1].split(separator || '&').inject({}, function (hash, pair) {
            if ((pair = pair.split('='))[0]) {
                var key = decodeURIComponent(pair.shift()),
                    value = pair.length > 1 ? pair.join('=') : pair[0];

                if (value != undefined) value = decodeURIComponent(value);

                if (key in hash) {
                    if (!Object.isArray(hash[key])) hash[key] = [hash[key]];
                    hash[key].push(value);
                }
                else hash[key] = value;
            }
            return hash;
        });
    }

    function toArray() {
        return this.split('');
    }

    function succ() {
        return this.slice(0, this.length - 1) +
            String.fromCharCode(this.charCodeAt(this.length - 1) + 1);
    }

    function times(count) {
        return count < 1 ? '' : new Array(count + 1).join(this);
    }

    function camelize() {
        return this.replace(/-+(.)?/g, function (match, chr) {
            return chr ? chr.toUpperCase() : '';
        });
    }

    function capitalize() {
        return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
    }

    function underscore() {
        return this.replace(/::/g, '/')
            .replace(/([A-Z]+)([A-Z][a-z])/g, '$1_$2')
            .replace(/([a-z\d])([A-Z])/g, '$1_$2')
            .replace(/-/g, '_')
            .toLowerCase();
    }

    function dasherize() {
        return this.replace(/_/g, '-');
    }

    function inspect(useDoubleQuotes) {
        var escapedString = this.replace(/[\x00-\x1f\\]/g, function (character) {
            if (character in String.specialChar) {
                return String.specialChar[character];
            }
            return '\\u00' + character.charCodeAt().toPaddedString(2, 16);
        });
        if (useDoubleQuotes) return '"' + escapedString.replace(/"/g, '\\"') + '"';
        return "'" + escapedString.replace(/'/g, '\\\'') + "'";
    }

    function unfilterJSON(filter) {
        return this.replace(filter || Prototype.JSONFilter, '$1');
    }

    function isJSON() {
        var str = this;
        if (str.blank()) return false;
        str = str.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@');
        str = str.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
        str = str.replace(/(?:^|:|,)(?:\s*\[)+/g, '');
        return (/^[\],:{}\s]*$/).test(str);
    }

    function evalJSON(sanitize) {
        var json = this.unfilterJSON(),
            cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
        if (cx.test(json)) {
            json = json.replace(cx, function (a) {
                return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            });
        }
        try {
            if (!sanitize || json.isJSON()) return eval('(' + json + ')');
        } catch (e) {
        }
        throw new SyntaxError('Badly formed JSON string: ' + this.inspect());
    }

    function parseJSON() {
        var json = this.unfilterJSON();
        return JSON.parse(json);
    }

    function include(pattern) {
        return this.indexOf(pattern) > -1;
    }

    function startsWith(pattern) {
        return this.lastIndexOf(pattern, 0) === 0;
    }

    function endsWith(pattern) {
        var d = this.length - pattern.length;
        return d >= 0 && this.indexOf(pattern, d) === d;
    }

    function empty() {
        return this == '';
    }

    function blank() {
        return /^\s*$/.test(this);
    }

    function interpolate(object, pattern) {
        return new Template(this, pattern).evaluate(object);
    }

    return {
        gsub: gsub,
        sub: sub,
        scan: scan,
        truncate: truncate,
        strip: String.prototype.trim || strip,
        trim: strip,
        stripTags: stripTags,
        stripScripts: stripScripts,
        extractScripts: extractScripts,
        evalScripts: evalScripts,
        escapeHTML: escapeHTML,
        unescapeHTML: unescapeHTML,
        toQueryParams: toQueryParams,
        parseQuery: toQueryParams,
        toArray: toArray,
        succ: succ,
        times: times,
        camelize: camelize,
        capitalize: capitalize,
        underscore: underscore,
        dasherize: dasherize,
        inspect: inspect,
        unfilterJSON: unfilterJSON,
        isJSON: isJSON,
        evalJSON: NATIVE_JSON_PARSE_SUPPORT ? parseJSON : evalJSON,
        include: include,
        startsWith: startsWith,
        endsWith: endsWith,
        empty: empty,
        blank: blank,
        interpolate: interpolate
    };
})());

var Template = Class.create({
    initialize: function (template, pattern) {
        this.template = template.toString();
        this.pattern = pattern || Template.Pattern;
    },

    evaluate: function (object) {
        if (object && Object.isFunction(object.toTemplateReplacements))
            object = object.toTemplateReplacements();

        return this.template.gsub(this.pattern, function (match) {
            if (object == null) return (match[1] + '');

            var before = match[1] || '';
            if (before == '\\') return match[2];

            var ctx = object, expr = match[3],
                pattern = /^([^.[]+|\[((?:.*?[^\\])?)\])(\.|\[|$)/;

            match = pattern.exec(expr);
            if (match == null) return before;

            while (match != null) {
                var comp = match[1].startsWith('[') ? match[2].replace(/\\\\]/g, ']') : match[1];
                ctx = ctx[comp];
                if (null == ctx || '' == match[3]) break;
                expr = expr.substring('[' == match[3] ? match[1].length : match[0].length);
                match = pattern.exec(expr);
            }

            return before + String.interpret(ctx);
        });
    }
});
Template.Pattern = /(^|.|\r|\n)(#\{(.*?)\})/;

var $break = {};

var Enumerable = (function () {
    function each(iterator, context) {
        var index = 0;
        try {
            this._each(function (value) {
                iterator.call(context, value, index++);
            });
        } catch (e) {
            if (e != $break) throw e;
        }
        return this;
    }

    function eachSlice(number, iterator, context) {
        var index = -number, slices = [], array = this.toArray();
        if (number < 1) return array;
        while ((index += number) < array.length)
            slices.push(array.slice(index, index + number));
        return slices.collect(iterator, context);
    }

    function all(iterator, context) {
        iterator = iterator || Prototype.K;
        var result = true;
        this.each(function (value, index) {
            result = result && !!iterator.call(context, value, index);
            if (!result) throw $break;
        });
        return result;
    }

    function any(iterator, context) {
        iterator = iterator || Prototype.K;
        var result = false;
        this.each(function (value, index) {
            if (result = !!iterator.call(context, value, index))
                throw $break;
        });
        return result;
    }

    function collect(iterator, context) {
        iterator = iterator || Prototype.K;
        var results = [];
        this.each(function (value, index) {
            results.push(iterator.call(context, value, index));
        });
        return results;
    }

    function detect(iterator, context) {
        var result;
        this.each(function (value, index) {
            if (iterator.call(context, value, index)) {
                result = value;
                throw $break;
            }
        });
        return result;
    }

    function findAll(iterator, context) {
        var results = [];
        this.each(function (value, index) {
            if (iterator.call(context, value, index))
                results.push(value);
        });
        return results;
    }

    function grep(filter, iterator, context) {
        iterator = iterator || Prototype.K;
        var results = [];

        if (Object.isString(filter))
            filter = new RegExp(RegExp.escape(filter));

        this.each(function (value, index) {
            if (filter.match(value))
                results.push(iterator.call(context, value, index));
        });
        return results;
    }

    function include(object) {
        if (Object.isFunction(this.indexOf))
            if (this.indexOf(object) != -1) return true;

        var found = false;
        this.each(function (value) {
            if (value == object) {
                found = true;
                throw $break;
            }
        });
        return found;
    }

    function inGroupsOf(number, fillWith) {
        fillWith = Object.isUndefined(fillWith) ? null : fillWith;
        return this.eachSlice(number, function (slice) {
            while (slice.length < number) slice.push(fillWith);
            return slice;
        });
    }

    function inject(memo, iterator, context) {
        this.each(function (value, index) {
            memo = iterator.call(context, memo, value, index);
        });
        return memo;
    }

    function invoke(method) {
        var args = $A(arguments).slice(1);
        return this.map(function (value) {
            return value[method].apply(value, args);
        });
    }

    function max(iterator, context) {
        iterator = iterator || Prototype.K;
        var result;
        this.each(function (value, index) {
            value = iterator.call(context, value, index);
            if (result == null || value >= result)
                result = value;
        });
        return result;
    }

    function min(iterator, context) {
        iterator = iterator || Prototype.K;
        var result;
        this.each(function (value, index) {
            value = iterator.call(context, value, index);
            if (result == null || value < result)
                result = value;
        });
        return result;
    }

    function partition(iterator, context) {
        iterator = iterator || Prototype.K;
        var trues = [], falses = [];
        this.each(function (value, index) {
            (iterator.call(context, value, index) ?
                trues : falses).push(value);
        });
        return [trues, falses];
    }

    function pluck(property) {
        var results = [];
        this.each(function (value) {
            results.push(value[property]);
        });
        return results;
    }

    function reject(iterator, context) {
        var results = [];
        this.each(function (value, index) {
            if (!iterator.call(context, value, index))
                results.push(value);
        });
        return results;
    }

    function sortBy(iterator, context) {
        return this.map(function (value, index) {
            return {
                value: value,
                criteria: iterator.call(context, value, index)
            };
        }).sort(function (left, right) {
            var a = left.criteria, b = right.criteria;
            return a < b ? -1 : a > b ? 1 : 0;
        }).pluck('value');
    }

    function toArray() {
        return this.map();
    }

    function zip() {
        var iterator = Prototype.K, args = $A(arguments);
        if (Object.isFunction(args.last()))
            iterator = args.pop();

        var collections = [this].concat(args).map($A);
        return this.map(function (value, index) {
            return iterator(collections.pluck(index));
        });
    }

    function size() {
        return this.toArray().length;
    }

    function inspect() {
        return '#<Enumerable:' + this.toArray().inspect() + '>';
    }


    return {
        each: each,
        eachSlice: eachSlice,
        all: all,
        every: all,
        any: any,
        some: any,
        collect: collect,
        map: collect,
        detect: detect,
        findAll: findAll,
        select: findAll,
        filter: findAll,
        grep: grep,
        include: include,
        member: include,
        inGroupsOf: inGroupsOf,
        inject: inject,
        invoke: invoke,
        max: max,
        min: min,
        partition: partition,
        pluck: pluck,
        reject: reject,
        sortBy: sortBy,
        toArray: toArray,
        entries: toArray,
        zip: zip,
        size: size,
        inspect: inspect,
        find: detect
    };
})();

function $A(iterable) {
    if (!iterable) return [];
    if ('toArray' in Object(iterable)) return iterable.toArray();
    var length = iterable.length || 0, results = new Array(length);
    while (length--) results[length] = iterable[length];
    return results;
}


function $w(string) {
    if (!Object.isString(string)) return [];
    string = string.strip();
    return string ? string.split(/\s+/) : [];
}

Array.from = $A;


(function () {
    var arrayProto = Array.prototype,
        slice = arrayProto.slice,
        _each = arrayProto.forEach; // use native browser JS 1.6 implementation if available

    function each(iterator, context) {
        for (var i = 0, length = this.length >>> 0; i < length; i++) {
            if (i in this) iterator.call(context, this[i], i, this);
        }
    }

    if (!_each) _each = each;

    function clear() {
        this.length = 0;
        return this;
    }

    function first() {
        return this[0];
    }

    function last() {
        return this[this.length - 1];
    }

    function compact() {
        return this.select(function (value) {
            return value != null;
        });
    }

    function flatten() {
        return this.inject([], function (array, value) {
            if (Object.isArray(value))
                return array.concat(value.flatten());
            array.push(value);
            return array;
        });
    }

    function without() {
        var values = slice.call(arguments, 0);
        return this.select(function (value) {
            return !values.include(value);
        });
    }

    function reverse(inline) {
        return (inline === false ? this.toArray() : this)._reverse();
    }

    function uniq(sorted) {
        return this.inject([], function (array, value, index) {
            if (0 == index || (sorted ? array.last() != value : !array.include(value)))
                array.push(value);
            return array;
        });
    }

    function intersect(array) {
        return this.uniq().findAll(function (item) {
            return array.detect(function (value) {
                return item === value
            });
        });
    }


    function clone() {
        return slice.call(this, 0);
    }

    function size() {
        return this.length;
    }

    function inspect() {
        return '[' + this.map(Object.inspect).join(', ') + ']';
    }

    function indexOf(item, i) {
        i || (i = 0);
        var length = this.length;
        if (i < 0) i = length + i;
        for (; i < length; i++)
            if (this[i] === item) return i;
        return -1;
    }

    function lastIndexOf(item, i) {
        i = isNaN(i) ? this.length : (i < 0 ? this.length + i : i) + 1;
        var n = this.slice(0, i).reverse().indexOf(item);
        return (n < 0) ? n : i - n - 1;
    }

    function concat() {
        var array = slice.call(this, 0), item;
        for (var i = 0, length = arguments.length; i < length; i++) {
            item = arguments[i];
            if (Object.isArray(item) && !('callee' in item)) {
                for (var j = 0, arrayLength = item.length; j < arrayLength; j++)
                    array.push(item[j]);
            } else {
                array.push(item);
            }
        }
        return array;
    }

    Object.extend(arrayProto, Enumerable);

    if (!arrayProto._reverse)
        arrayProto._reverse = arrayProto.reverse;

    Object.extend(arrayProto, {
        _each: _each,
        clear: clear,
        first: first,
        last: last,
        compact: compact,
        flatten: flatten,
        without: without,
        reverse: reverse,
        uniq: uniq,
        intersect: intersect,
        clone: clone,
        toArray: clone,
        size: size,
        inspect: inspect
    });

    var CONCAT_ARGUMENTS_BUGGY = (function () {
        return [].concat(arguments)[0][0] !== 1;
    })(1, 2)

    if (CONCAT_ARGUMENTS_BUGGY) arrayProto.concat = concat;

    if (!arrayProto.indexOf) arrayProto.indexOf = indexOf;
    if (!arrayProto.lastIndexOf) arrayProto.lastIndexOf = lastIndexOf;
})();
function $H(object) {
    return new Hash(object);
};

var Hash = Class.create(Enumerable, (function () {
    function initialize(object) {
        this._object = Object.isHash(object) ? object.toObject() : Object.clone(object);
    }


    function _each(iterator) {
        for (var key in this._object) {
            var value = this._object[key], pair = [key, value];
            pair.key = key;
            pair.value = value;
            iterator(pair);
        }
    }

    function set(key, value) {
        return this._object[key] = value;
    }

    function get(key) {
        if (this._object[key] !== Object.prototype[key])
            return this._object[key];
    }

    function unset(key) {
        var value = this._object[key];
        delete this._object[key];
        return value;
    }

    function toObject() {
        return Object.clone(this._object);
    }


    function keys() {
        return this.pluck('key');
    }

    function values() {
        return this.pluck('value');
    }

    function index(value) {
        var match = this.detect(function (pair) {
            return pair.value === value;
        });
        return match && match.key;
    }

    function merge(object) {
        return this.clone().update(object);
    }

    function update(object) {
        return new Hash(object).inject(this, function (result, pair) {
            result.set(pair.key, pair.value);
            return result;
        });
    }

    function toQueryPair(key, value) {
        if (Object.isUndefined(value)) return key;
        return key + '=' + encodeURIComponent(String.interpret(value));
    }

    function toQueryString() {
        return this.inject([], function (results, pair) {
            var key = encodeURIComponent(pair.key), values = pair.value;

            if (values && typeof values == 'object') {
                if (Object.isArray(values)) {
                    var queryValues = [];
                    for (var i = 0, len = values.length, value; i < len; i++) {
                        value = values[i];
                        queryValues.push(toQueryPair(key, value));
                    }
                    return results.concat(queryValues);
                }
            } else results.push(toQueryPair(key, values));
            return results;
        }).join('&');
    }

    function inspect() {
        return '#<Hash:{' + this.map(function (pair) {
                return pair.map(Object.inspect).join(': ');
            }).join(', ') + '}>';
    }

    function clone() {
        return new Hash(this);
    }

    return {
        initialize: initialize,
        _each: _each,
        set: set,
        get: get,
        unset: unset,
        toObject: toObject,
        toTemplateReplacements: toObject,
        keys: keys,
        values: values,
        index: index,
        merge: merge,
        update: update,
        toQueryString: toQueryString,
        inspect: inspect,
        toJSON: toObject,
        clone: clone
    };
})());

Hash.from = $H;
Object.extend(Number.prototype, (function () {
    function toColorPart() {
        return this.toPaddedString(2, 16);
    }

    function succ() {
        return this + 1;
    }

    function times(iterator, context) {
        $R(0, this, true).each(iterator, context);
        return this;
    }

    function toPaddedString(length, radix) {
        var string = this.toString(radix || 10);
        return '0'.times(length - string.length) + string;
    }

    function abs() {
        return Math.abs(this);
    }

    function round() {
        return Math.round(this);
    }

    function ceil() {
        return Math.ceil(this);
    }

    function floor() {
        return Math.floor(this);
    }

    return {
        toColorPart: toColorPart,
        succ: succ,
        times: times,
        toPaddedString: toPaddedString,
        abs: abs,
        round: round,
        ceil: ceil,
        floor: floor
    };
})());

function $R(start, end, exclusive) {
    return new ObjectRange(start, end, exclusive);
}

var ObjectRange = Class.create(Enumerable, (function () {
    function initialize(start, end, exclusive) {
        this.start = start;
        this.end = end;
        this.exclusive = exclusive;
    }

    function _each(iterator) {
        var value = this.start;
        while (this.include(value)) {
            iterator(value);
            value = value.succ();
        }
    }

    function include(value) {
        if (value < this.start)
            return false;
        if (this.exclusive)
            return value < this.end;
        return value <= this.end;
    }

    return {
        initialize: initialize,
        _each: _each,
        include: include
    };
})());


var Ajax = {
    getTransport: function () {
        return Try.these(
                function () {
                    return new XMLHttpRequest()
                },
                function () {
                    return new ActiveXObject('Msxml2.XMLHTTP')
                },
                function () {
                    return new ActiveXObject('Microsoft.XMLHTTP')
                }
            ) || false;
    },

    activeRequestCount: 0
};

Ajax.Responders = {
    responders: [],

    _each: function (iterator) {
        this.responders._each(iterator);
    },

    register: function (responder) {
        if (!this.include(responder))
            this.responders.push(responder);
    },

    unregister: function (responder) {
        this.responders = this.responders.without(responder);
    },

    dispatch: function (callback, request, transport, json) {
        this.each(function (responder) {
            if (Object.isFunction(responder[callback])) {
                try {
                    responder[callback].apply(responder, [request, transport, json]);
                } catch (e) {
                }
            }
        });
    }
};

Object.extend(Ajax.Responders, Enumerable);

Ajax.Responders.register({
    onCreate: function () {
        Ajax.activeRequestCount++
    },
    onComplete: function () {
        Ajax.activeRequestCount--
    }
});
Ajax.Base = Class.create({
    initialize: function (options) {
        this.options = {
            method: 'post',
            asynchronous: true,
            contentType: 'application/x-www-form-urlencoded',
            encoding: 'UTF-8',
            parameters: '',
            evalJSON: true,
            evalJS: true
        };
        Object.extend(this.options, options || {});

        this.options.method = this.options.method.toLowerCase();

        if (Object.isHash(this.options.parameters))
            this.options.parameters = this.options.parameters.toObject();
    }
});
Ajax.Request = Class.create(Ajax.Base, {
    _complete: false,

    initialize: function ($super, url, options) {
        $super(options);
        this.transport = Ajax.getTransport();
        this.request(url);
    },

    request: function (url) {
        this.url = url;
        this.method = this.options.method;
        var params = Object.isString(this.options.parameters) ?
            this.options.parameters :
            Object.toQueryString(this.options.parameters);

        if (!['get', 'post'].include(this.method)) {
            params += (params ? '&' : '') + "_method=" + this.method;
            this.method = 'post';
        }

        if (params && this.method === 'get') {
            this.url += (this.url.include('?') ? '&' : '?') + params;
        }

        this.parameters = params.toQueryParams();

        try {
            var response = new Ajax.Response(this);
            if (this.options.onCreate) this.options.onCreate(response);
            Ajax.Responders.dispatch('onCreate', this, response);

            this.transport.open(this.method.toUpperCase(), this.url,
                this.options.asynchronous);

            if (this.options.asynchronous) this.respondToReadyState.bind(this).defer(1);

            this.transport.onreadystatechange = this.onStateChange.bind(this);
            this.setRequestHeaders();

            this.body = this.method == 'post' ? (this.options.postBody || params) : null;
            this.transport.send(this.body);

            /* Force Firefox to handle ready state 4 for synchronous requests */
            if (!this.options.asynchronous && this.transport.overrideMimeType)
                this.onStateChange();

        }
        catch (e) {
            this.dispatchException(e);
        }
    },

    onStateChange: function () {
        var readyState = this.transport.readyState;
        if (readyState > 1 && !((readyState == 4) && this._complete))
            this.respondToReadyState(this.transport.readyState);
    },

    setRequestHeaders: function () {
        var headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'X-Prototype-Version': Prototype.Version,
            'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
        };

        if (this.method == 'post') {
            headers['Content-type'] = this.options.contentType +
                (this.options.encoding ? '; charset=' + this.options.encoding : '');

            /* Force "Connection: close" for older Mozilla browsers to work
             * around a bug where XMLHttpRequest sends an incorrect
             * Content-length header. See Mozilla Bugzilla #246651.
             */
            if (this.transport.overrideMimeType &&
                (navigator.userAgent.match(/Gecko\/(\d{4})/) || [0, 2005])[1] < 2005)
                headers['Connection'] = 'close';
        }

        if (typeof this.options.requestHeaders == 'object') {
            var extras = this.options.requestHeaders;

            if (Object.isFunction(extras.push))
                for (var i = 0, length = extras.length; i < length; i += 2)
                    headers[extras[i]] = extras[i + 1];
            else
                $H(extras).each(function (pair) {
                    headers[pair.key] = pair.value
                });
        }

        for (var name in headers)
            this.transport.setRequestHeader(name, headers[name]);
    },

    success: function () {
        var status = this.getStatus();
        return !status || (status >= 200 && status < 300) || status == 304;
    },

    getStatus: function () {
        try {
            if (this.transport.status === 1223) return 204;
            return this.transport.status || 0;
        } catch (e) {
            return 0
        }
    },

    respondToReadyState: function (readyState) {
        var state = Ajax.Request.Events[readyState], response = new Ajax.Response(this);

        if (state == 'Complete') {
            try {
                this._complete = true;
                (this.options['on' + response.status]
                || this.options['on' + (this.success() ? 'Success' : 'Failure')]
                || Prototype.emptyFunction)(response, response.headerJSON);
            } catch (e) {
                this.dispatchException(e);
            }

            var contentType = response.getHeader('Content-type');
            if (this.options.evalJS == 'force'
                || (this.options.evalJS && this.isSameOrigin() && contentType
                && contentType.match(/^\s*(text|application)\/(x-)?(java|ecma)script(;.*)?\s*$/i)))
                this.evalResponse();
        }

        try {
            (this.options['on' + state] || Prototype.emptyFunction)(response, response.headerJSON);
            Ajax.Responders.dispatch('on' + state, this, response, response.headerJSON);
        } catch (e) {
            this.dispatchException(e);
        }

        if (state == 'Complete') {
            this.transport.onreadystatechange = Prototype.emptyFunction;
        }
    },

    isSameOrigin: function () {
        var m = this.url.match(/^\s*https?:\/\/[^\/]*/);
        return !m || (m[0] == '#{protocol}//#{domain}#{port}'.interpolate({
                protocol: location.protocol,
                domain: document.domain,
                port: location.port ? ':' + location.port : ''
            }));
    },

    getHeader: function (name) {
        try {
            return this.transport.getResponseHeader(name) || null;
        } catch (e) {
            return null;
        }
    },

    evalResponse: function () {
        try {
            return eval((this.transport.responseText || '').unfilterJSON());
        } catch (e) {
            this.dispatchException(e);
        }
    },

    dispatchException: function (exception) {
        (this.options.onException || Prototype.emptyFunction)(this, exception);
        Ajax.Responders.dispatch('onException', this, exception);
    }
});

Ajax.Request.Events =
    ['Uninitialized', 'Loading', 'Loaded', 'Interactive', 'Complete'];


Ajax.Response = Class.create({
    initialize: function (request) {
        this.request = request;
        var transport = this.transport = request.transport,
            readyState = this.readyState = transport.readyState;

        if ((readyState > 2 && !Prototype.Browser.IE) || readyState == 4) {
            this.status = this.getStatus();
            this.statusText = this.getStatusText();
            this.responseText = String.interpret(transport.responseText);
            this.headerJSON = this._getHeaderJSON();
        }

        if (readyState == 4) {
            var xml = transport.responseXML;
            this.responseXML = Object.isUndefined(xml) ? null : xml;
            this.responseJSON = this._getResponseJSON();
        }
    },

    status: 0,

    statusText: '',

    getStatus: Ajax.Request.prototype.getStatus,

    getStatusText: function () {
        try {
            return this.transport.statusText || '';
        } catch (e) {
            return ''
        }
    },

    getHeader: Ajax.Request.prototype.getHeader,

    getAllHeaders: function () {
        try {
            return this.getAllResponseHeaders();
        } catch (e) {
            return null
        }
    },

    getResponseHeader: function (name) {
        return this.transport.getResponseHeader(name);
    },

    getAllResponseHeaders: function () {
        return this.transport.getAllResponseHeaders();
    },

    _getHeaderJSON: function () {
        var json = this.getHeader('X-JSON');
        if (!json) return null;
        json = decodeURIComponent(escape(json));
        try {
            return json.evalJSON(this.request.options.sanitizeJSON || !this.request.isSameOrigin());
        } catch (e) {
            this.request.dispatchException(e);
        }
    },

    _getResponseJSON: function () {
        var options = this.request.options;
        if (!options.evalJSON || (options.evalJSON != 'force' && !(this.getHeader('Content-type') || '').include('application/json')) ||
            this.responseText.blank())
            return null;
        try {
            return this.responseText.evalJSON(options.sanitizeJSON || !this.request.isSameOrigin());
        } catch (e) {
            this.request.dispatchException(e);
        }
    }
});

Ajax.Updater = Class.create(Ajax.Request, {
    initialize: function ($super, container, url, options) {
        this.container = {
            success: (container.success || container),
            failure: (container.failure || (container.success ? null : container))
        };

        options = Object.clone(options);
        var onComplete = options.onComplete;
        options.onComplete = (function (response, json) {
            this.updateContent(response.responseText);
            if (Object.isFunction(onComplete)) onComplete(response, json);
        }).bind(this);

        $super(url, options);
    },

    updateContent: function (responseText) {
        var receiver = this.container[this.success() ? 'success' : 'failure'],
            options = this.options;

        if (!options.evalScripts) responseText = responseText.stripScripts();

        if (receiver = $(receiver)) {
            if (options.insertion) {
                if (Object.isString(options.insertion)) {
                    var insertion = {};
                    insertion[options.insertion] = responseText;
                    receiver.insert(insertion);
                }
                else options.insertion(receiver, responseText);
            }
            else receiver.update(responseText);
        }
    }
});

Ajax.PeriodicalUpdater = Class.create(Ajax.Base, {
    initialize: function ($super, container, url, options) {
        $super(options);
        this.onComplete = this.options.onComplete;

        this.frequency = (this.options.frequency || 2);
        this.decay = (this.options.decay || 1);

        this.updater = {};
        this.container = container;
        this.url = url;

        this.start();
    },

    start: function () {
        this.options.onComplete = this.updateComplete.bind(this);
        this.onTimerEvent();
    },

    stop: function () {
        this.updater.options.onComplete = undefined;
        clearTimeout(this.timer);
        (this.onComplete || Prototype.emptyFunction).apply(this, arguments);
    },

    updateComplete: function (response) {
        if (this.options.decay) {
            this.decay = (response.responseText == this.lastText ?
                this.decay * this.options.decay : 1);

            this.lastText = response.responseText;
        }
        this.timer = this.onTimerEvent.bind(this).delay(this.decay * this.frequency);
    },

    onTimerEvent: function () {
        this.updater = new Ajax.Updater(this.container, this.url, this.options);
    }
});


function $(element) {
    if (arguments.length > 1) {
        for (var i = 0, elements = [], length = arguments.length; i < length; i++)
            elements.push($(arguments[i]));
        return elements;
    }
    if (Object.isString(element))
        element = document.getElementById(element);
    return Element.extend(element);
}

if (Prototype.BrowserFeatures.XPath) {
    document._getElementsByXPath = function (expression, parentElement) {
        var results = [];
        var query = document.evaluate(expression, $(parentElement) || document,
            null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
        for (var i = 0, length = query.snapshotLength; i < length; i++)
            results.push(Element.extend(query.snapshotItem(i)));
        return results;
    };
}

/*--------------------------------------------------------------------------*/

if (!Node) var Node = {};

if (!Node.ELEMENT_NODE) {
    Object.extend(Node, {
        ELEMENT_NODE: 1,
        ATTRIBUTE_NODE: 2,
        TEXT_NODE: 3,
        CDATA_SECTION_NODE: 4,
        ENTITY_REFERENCE_NODE: 5,
        ENTITY_NODE: 6,
        PROCESSING_INSTRUCTION_NODE: 7,
        COMMENT_NODE: 8,
        DOCUMENT_NODE: 9,
        DOCUMENT_TYPE_NODE: 10,
        DOCUMENT_FRAGMENT_NODE: 11,
        NOTATION_NODE: 12
    });
}


(function (global) {
    function shouldUseCache(tagName, attributes) {
        if (tagName === 'select') return false;
        if ('type' in attributes) return false;
        return true;
    }

    var HAS_EXTENDED_CREATE_ELEMENT_SYNTAX = (function () {
        try {
            var el = document.createElement('<input name="x">');
            return el.tagName.toLowerCase() === 'input' && el.name === 'x';
        }
        catch (err) {
            return false;
        }
    })();

    var element = global.Element;

    global.Element = function (tagName, attributes) {
        attributes = attributes || {};
        tagName = tagName.toLowerCase();
        var cache = Element.cache;

        if (HAS_EXTENDED_CREATE_ELEMENT_SYNTAX && attributes.name) {
            tagName = '<' + tagName + ' name="' + attributes.name + '">';
            delete attributes.name;
            return Element.writeAttribute(document.createElement(tagName), attributes);
        }

        if (!cache[tagName]) cache[tagName] = Element.extend(document.createElement(tagName));

        var node = shouldUseCache(tagName, attributes) ?
            cache[tagName].cloneNode(false) : document.createElement(tagName);

        return Element.writeAttribute(node, attributes);
    };

    Object.extend(global.Element, element || {});
    if (element) global.Element.prototype = element.prototype;

})(this);

Element.idCounter = 1;
Element.cache = {};

Element._purgeElement = function (element) {
    var uid = element._prototypeUID;
    if (uid) {
        Element.stopObserving(element);
        element._prototypeUID = void 0;
        delete Element.Storage[uid];
    }
}

Element.Methods = {
    visible: function (element) {
        return $(element).style.display != 'none';
    },

    toggle: function (element) {
        element = $(element);
        Element[Element.visible(element) ? 'hide' : 'show'](element);
        return element;
    },

    hide: function (element) {
        element = $(element);
        element.style.display = 'none';
        return element;
    },

    show: function (element) {
        element = $(element);
        element.style.display = '';
        return element;
    },

    remove: function (element) {
        element = $(element);
        element.parentNode.removeChild(element);
        return element;
    },

    update: (function () {

        var SELECT_ELEMENT_INNERHTML_BUGGY = (function () {
            var el = document.createElement("select"),
                isBuggy = true;
            el.innerHTML = "<option value=\"test\">test</option>";
            if (el.options && el.options[0]) {
                isBuggy = el.options[0].nodeName.toUpperCase() !== "OPTION";
            }
            el = null;
            return isBuggy;
        })();

        var TABLE_ELEMENT_INNERHTML_BUGGY = (function () {
            try {
                var el = document.createElement("table");
                if (el && el.tBodies) {
                    el.innerHTML = "<tbody><tr><td>test</td></tr></tbody>";
                    var isBuggy = typeof el.tBodies[0] == "undefined";
                    el = null;
                    return isBuggy;
                }
            } catch (e) {
                return true;
            }
        })();

        var LINK_ELEMENT_INNERHTML_BUGGY = (function () {
            try {
                var el = document.createElement('div');
                el.innerHTML = "<link>";
                var isBuggy = (el.childNodes.length === 0);
                el = null;
                return isBuggy;
            } catch (e) {
                return true;
            }
        })();

        var ANY_INNERHTML_BUGGY = SELECT_ELEMENT_INNERHTML_BUGGY ||
            TABLE_ELEMENT_INNERHTML_BUGGY || LINK_ELEMENT_INNERHTML_BUGGY;

        var SCRIPT_ELEMENT_REJECTS_TEXTNODE_APPENDING = (function () {
            var s = document.createElement("script"),
                isBuggy = false;
            try {
                s.appendChild(document.createTextNode(""));
                isBuggy = !s.firstChild ||
                    s.firstChild && s.firstChild.nodeType !== 3;
            } catch (e) {
                isBuggy = true;
            }
            s = null;
            return isBuggy;
        })();


        function update(element, content) {
            element = $(element);
            var purgeElement = Element._purgeElement;

            var descendants = element.getElementsByTagName('*'),
                i = descendants.length;
            while (i--) purgeElement(descendants[i]);

            if (content && content.toElement)
                content = content.toElement();

            if (Object.isElement(content))
                return element.update().insert(content);

            content = Object.toHTML(content);

            var tagName = element.tagName.toUpperCase();

            if (tagName === 'SCRIPT' && SCRIPT_ELEMENT_REJECTS_TEXTNODE_APPENDING) {
                element.text = content;
                return element;
            }

            if (ANY_INNERHTML_BUGGY) {
                if (tagName in Element._insertionTranslations.tags) {
                    while (element.firstChild) {
                        element.removeChild(element.firstChild);
                    }
                    Element._getContentFromAnonymousElement(tagName, content.stripScripts())
                        .each(function (node) {
                            element.appendChild(node)
                        });
                } else if (LINK_ELEMENT_INNERHTML_BUGGY && Object.isString(content) && content.indexOf('<link') > -1) {
                    while (element.firstChild) {
                        element.removeChild(element.firstChild);
                    }
                    var nodes = Element._getContentFromAnonymousElement(tagName, content.stripScripts(), true);
                    nodes.each(function (node) {
                        element.appendChild(node)
                    });
                }
                else {
                    element.innerHTML = content.stripScripts();
                }
            }
            else {
                element.innerHTML = content.stripScripts();
            }

            content.evalScripts.bind(content).defer();
            return element;
        }

        return update;
    })(),

    replace: function (element, content) {
        element = $(element);
        if (content && content.toElement) content = content.toElement();
        else if (!Object.isElement(content)) {
            content = Object.toHTML(content);
            var range = element.ownerDocument.createRange();
            range.selectNode(element);
            content.evalScripts.bind(content).defer();
            content = range.createContextualFragment(content.stripScripts());
        }
        element.parentNode.replaceChild(content, element);
        return element;
    },

    insert: function (element, insertions) {
        element = $(element);

        if (Object.isString(insertions) || Object.isNumber(insertions) ||
            Object.isElement(insertions) || (insertions && (insertions.toElement || insertions.toHTML)))
            insertions = {bottom: insertions};

        var content, insert, tagName, childNodes;

        for (var position in insertions) {
            content = insertions[position];
            position = position.toLowerCase();
            insert = Element._insertionTranslations[position];

            if (content && content.toElement) content = content.toElement();
            if (Object.isElement(content)) {
                insert(element, content);
                continue;
            }

            content = Object.toHTML(content);

            tagName = ((position == 'before' || position == 'after')
                ? element.parentNode : element).tagName.toUpperCase();

            childNodes = Element._getContentFromAnonymousElement(tagName, content.stripScripts());

            if (position == 'top' || position == 'after') childNodes.reverse();
            childNodes.each(insert.curry(element));

            content.evalScripts.bind(content).defer();
        }

        return element;
    },

    wrap: function (element, wrapper, attributes) {
        element = $(element);
        if (Object.isElement(wrapper))
            $(wrapper).writeAttribute(attributes || {});
        else if (Object.isString(wrapper)) wrapper = new Element(wrapper, attributes);
        else wrapper = new Element('div', wrapper);
        if (element.parentNode)
            element.parentNode.replaceChild(wrapper, element);
        wrapper.appendChild(element);
        return wrapper;
    },

    inspect: function (element) {
        element = $(element);
        var result = '<' + element.tagName.toLowerCase();
        $H({'id': 'id', 'className': 'class'}).each(function (pair) {
            var property = pair.first(),
                attribute = pair.last(),
                value = (element[property] || '').toString();
            if (value) result += ' ' + attribute + '=' + value.inspect(true);
        });
        return result + '>';
    },

    recursivelyCollect: function (element, property, maximumLength) {
        element = $(element);
        maximumLength = maximumLength || -1;
        var elements = [];

        while (element = element[property]) {
            if (element.nodeType == 1)
                elements.push(Element.extend(element));
            if (elements.length == maximumLength)
                break;
        }

        return elements;
    },

    ancestors: function (element) {
        return Element.recursivelyCollect(element, 'parentNode');
    },

    descendants: function (element) {
        return Element.select(element, "*");
    },

    firstDescendant: function (element) {
        element = $(element).firstChild;
        while (element && element.nodeType != 1) element = element.nextSibling;
        return $(element);
    },

    immediateDescendants: function (element) {
        var results = [], child = $(element).firstChild;
        while (child) {
            if (child.nodeType === 1) {
                results.push(Element.extend(child));
            }
            child = child.nextSibling;
        }
        return results;
    },

    previousSiblings: function (element, maximumLength) {
        return Element.recursivelyCollect(element, 'previousSibling');
    },

    nextSiblings: function (element) {
        return Element.recursivelyCollect(element, 'nextSibling');
    },

    siblings: function (element) {
        element = $(element);
        return Element.previousSiblings(element).reverse()
            .concat(Element.nextSiblings(element));
    },

    match: function (element, selector) {
        element = $(element);
        if (Object.isString(selector))
            return Prototype.Selector.match(element, selector);
        return selector.match(element);
    },

    up: function (element, expression, index) {
        element = $(element);
        if (arguments.length == 1) return $(element.parentNode);
        var ancestors = Element.ancestors(element);
        return Object.isNumber(expression) ? ancestors[expression] :
            Prototype.Selector.find(ancestors, expression, index);
    },

    down: function (element, expression, index) {
        element = $(element);
        if (arguments.length == 1) return Element.firstDescendant(element);
        return Object.isNumber(expression) ? Element.descendants(element)[expression] :
            Element.select(element, expression)[index || 0];
    },

    previous: function (element, expression, index) {
        element = $(element);
        if (Object.isNumber(expression)) index = expression, expression = false;
        if (!Object.isNumber(index)) index = 0;

        if (expression) {
            return Prototype.Selector.find(element.previousSiblings(), expression, index);
        } else {
            return element.recursivelyCollect("previousSibling", index + 1)[index];
        }
    },

    next: function (element, expression, index) {
        element = $(element);
        if (Object.isNumber(expression)) index = expression, expression = false;
        if (!Object.isNumber(index)) index = 0;

        if (expression) {
            return Prototype.Selector.find(element.nextSiblings(), expression, index);
        } else {
            var maximumLength = Object.isNumber(index) ? index + 1 : 1;
            return element.recursivelyCollect("nextSibling", index + 1)[index];
        }
    },


    select: function (element) {
        element = $(element);
        var expressions = Array.prototype.slice.call(arguments, 1).join(', ');
        return Prototype.Selector.select(expressions, element);
    },

    adjacent: function (element) {
        element = $(element);
        var expressions = Array.prototype.slice.call(arguments, 1).join(', ');
        return Prototype.Selector.select(expressions, element.parentNode).without(element);
    },

    identify: function (element) {
        element = $(element);
        var id = Element.readAttribute(element, 'id');
        if (id) return id;
        do {
            id = 'anonymous_element_' + Element.idCounter++
        } while ($(id));
        Element.writeAttribute(element, 'id', id);
        return id;
    },

    readAttribute: function (element, name) {
        element = $(element);
        if (Prototype.Browser.IE) {
            var t = Element._attributeTranslations.read;
            if (t.values[name]) return t.values[name](element, name);
            if (t.names[name]) name = t.names[name];
            if (name.include(':')) {
                return (!element.attributes || !element.attributes[name]) ? null :
                    element.attributes[name].value;
            }
        }
        return element.getAttribute(name);
    },

    writeAttribute: function (element, name, value) {
        element = $(element);
        var attributes = {}, t = Element._attributeTranslations.write;

        if (typeof name == 'object') attributes = name;
        else attributes[name] = Object.isUndefined(value) ? true : value;

        for (var attr in attributes) {
            name = t.names[attr] || attr;
            value = attributes[attr];
            if (t.values[attr]) name = t.values[attr](element, value);
            if (value === false || value === null)
                element.removeAttribute(name);
            else if (value === true)
                element.setAttribute(name, name);
            else element.setAttribute(name, value);
        }
        return element;
    },

    getHeight: function (element) {
        return Element.getDimensions(element).height;
    },

    getWidth: function (element) {
        return Element.getDimensions(element).width;
    },

    classNames: function (element) {
        return new Element.ClassNames(element);
    },

    hasClassName: function (element, className) {
        if (!(element = $(element))) return;
        var elementClassName = element.className;
        return (elementClassName.length > 0 && (elementClassName == className ||
        new RegExp("(^|\\s)" + className + "(\\s|$)").test(elementClassName)));
    },

    addClassName: function (element, className) {
        if (!(element = $(element))) return;
        if (!Element.hasClassName(element, className))
            element.className += (element.className ? ' ' : '') + className;
        return element;
    },

    removeClassName: function (element, className) {
        if (!(element = $(element))) return;
        element.className = element.className.replace(
            new RegExp("(^|\\s+)" + className + "(\\s+|$)"), ' ').strip();
        return element;
    },

    toggleClassName: function (element, className) {
        if (!(element = $(element))) return;
        return Element[Element.hasClassName(element, className) ?
            'removeClassName' : 'addClassName'](element, className);
    },

    cleanWhitespace: function (element) {
        element = $(element);
        var node = element.firstChild;
        while (node) {
            var nextNode = node.nextSibling;
            if (node.nodeType == 3 && !/\S/.test(node.nodeValue))
                element.removeChild(node);
            node = nextNode;
        }
        return element;
    },

    empty: function (element) {
        return $(element).innerHTML.blank();
    },

    descendantOf: function (element, ancestor) {
        element = $(element), ancestor = $(ancestor);

        if (element.compareDocumentPosition)
            return (element.compareDocumentPosition(ancestor) & 8) === 8;

        if (ancestor.contains)
            return ancestor.contains(element) && ancestor !== element;

        while (element = element.parentNode)
            if (element == ancestor) return true;

        return false;
    },

    scrollTo: function (element) {
        element = $(element);
        var pos = Element.cumulativeOffset(element);
        window.scrollTo(pos[0], pos[1]);
        return element;
    },

    getStyle: function (element, style) {
        element = $(element);
        style = style == 'float' ? 'cssFloat' : style.camelize();
        var value = element.style[style];
        if (!value || value == 'auto') {
            var css = document.defaultView.getComputedStyle(element, null);
            value = css ? css[style] : null;
        }
        if (style == 'opacity') return value ? parseFloat(value) : 1.0;
        return value == 'auto' ? null : value;
    },

    getOpacity: function (element) {
        return $(element).getStyle('opacity');
    },

    setStyle: function (element, styles) {
        element = $(element);
        var elementStyle = element.style, match;
        if (Object.isString(styles)) {
            element.style.cssText += ';' + styles;
            return styles.include('opacity') ?
                element.setOpacity(styles.match(/opacity:\s*(\d?\.?\d*)/)[1]) : element;
        }
        for (var property in styles)
            if (property == 'opacity') element.setOpacity(styles[property]);
            else
                elementStyle[(property == 'float' || property == 'cssFloat') ?
                    (Object.isUndefined(elementStyle.styleFloat) ? 'cssFloat' : 'styleFloat') :
                    property] = styles[property];

        return element;
    },

    setOpacity: function (element, value) {
        element = $(element);
        element.style.opacity = (value == 1 || value === '') ? '' :
            (value < 0.00001) ? 0 : value;
        return element;
    },

    makePositioned: function (element) {
        element = $(element);
        var pos = Element.getStyle(element, 'position');
        if (pos == 'static' || !pos) {
            element._madePositioned = true;
            element.style.position = 'relative';
            if (Prototype.Browser.Opera) {
                element.style.top = 0;
                element.style.left = 0;
            }
        }
        return element;
    },

    undoPositioned: function (element) {
        element = $(element);
        if (element._madePositioned) {
            element._madePositioned = undefined;
            element.style.position =
                element.style.top =
                    element.style.left =
                        element.style.bottom =
                            element.style.right = '';
        }
        return element;
    },

    makeClipping: function (element) {
        element = $(element);
        if (element._overflow) return element;
        element._overflow = Element.getStyle(element, 'overflow') || 'auto';
        if (element._overflow !== 'hidden')
            element.style.overflow = 'hidden';
        return element;
    },

    undoClipping: function (element) {
        element = $(element);
        if (!element._overflow) return element;
        element.style.overflow = element._overflow == 'auto' ? '' : element._overflow;
        element._overflow = null;
        return element;
    },

    clonePosition: function (element, source) {
        var options = Object.extend({
            setLeft: true,
            setTop: true,
            setWidth: true,
            setHeight: true,
            offsetTop: 0,
            offsetLeft: 0
        }, arguments[2] || {});

        source = $(source);
        var p = Element.viewportOffset(source), delta = [0, 0], parent = null;

        element = $(element);

        if (Element.getStyle(element, 'position') == 'absolute') {
            parent = Element.getOffsetParent(element);
            delta = Element.viewportOffset(parent);
        }

        if (parent == document.body) {
            delta[0] -= document.body.offsetLeft;
            delta[1] -= document.body.offsetTop;
        }

        if (options.setLeft) element.style.left = (p[0] - delta[0] + options.offsetLeft) + 'px';
        if (options.setTop) element.style.top = (p[1] - delta[1] + options.offsetTop) + 'px';
        if (options.setWidth) element.style.width = source.offsetWidth + 'px';
        if (options.setHeight) element.style.height = source.offsetHeight + 'px';
        return element;
    }
};

Object.extend(Element.Methods, {
    getElementsBySelector: Element.Methods.select,

    childElements: Element.Methods.immediateDescendants
});

Element._attributeTranslations = {
    write: {
        names: {
            className: 'class',
            htmlFor: 'for'
        },
        values: {}
    }
};

if (Prototype.Browser.Opera) {
    Element.Methods.getStyle = Element.Methods.getStyle.wrap(
        function (proceed, element, style) {
            switch (style) {
                case 'height':
                case 'width':
                    if (!Element.visible(element)) return null;

                    var dim = parseInt(proceed(element, style), 10);

                    if (dim !== element['offset' + style.capitalize()])
                        return dim + 'px';

                    var properties;
                    if (style === 'height') {
                        properties = ['border-top-width', 'padding-top',
                            'padding-bottom', 'border-bottom-width'];
                    }
                    else {
                        properties = ['border-left-width', 'padding-left',
                            'padding-right', 'border-right-width'];
                    }
                    return properties.inject(dim, function (memo, property) {
                            var val = proceed(element, property);
                            return val === null ? memo : memo - parseInt(val, 10);
                        }) + 'px';
                default:
                    return proceed(element, style);
            }
        }
    );

    Element.Methods.readAttribute = Element.Methods.readAttribute.wrap(
        function (proceed, element, attribute) {
            if (attribute === 'title') return element.title;
            return proceed(element, attribute);
        }
    );
}

else if (Prototype.Browser.IE) {
    Element.Methods.getStyle = function (element, style) {
        element = $(element);
        style = (style == 'float' || style == 'cssFloat') ? 'styleFloat' : style.camelize();
        var value = element.style[style];
        if (!value && element.currentStyle) value = element.currentStyle[style];

        if (style == 'opacity') {
            if (value = (element.getStyle('filter') || '').match(/alpha\(opacity=(.*)\)/))
                if (value[1]) return parseFloat(value[1]) / 100;
            return 1.0;
        }

        if (value == 'auto') {
            if ((style == 'width' || style == 'height') && (element.getStyle('display') != 'none'))
                return element['offset' + style.capitalize()] + 'px';
            return null;
        }
        return value;
    };

    Element.Methods.setOpacity = function (element, value) {
        function stripAlpha(filter) {
            return filter.replace(/alpha\([^\)]*\)/gi, '');
        }

        element = $(element);
        var currentStyle = element.currentStyle;
        if ((currentStyle && !currentStyle.hasLayout) ||
            (!currentStyle && element.style.zoom == 'normal'))
            element.style.zoom = 1;

        var filter = element.getStyle('filter'), style = element.style;
        if (value == 1 || value === '') {
            (filter = stripAlpha(filter)) ?
                style.filter = filter : style.removeAttribute('filter');
            return element;
        } else if (value < 0.00001) value = 0;
        style.filter = stripAlpha(filter) +
            'alpha(opacity=' + (value * 100) + ')';
        return element;
    };

    Element._attributeTranslations = (function () {

        var classProp = 'className',
            forProp = 'for',
            el = document.createElement('div');

        el.setAttribute(classProp, 'x');

        if (el.className !== 'x') {
            el.setAttribute('class', 'x');
            if (el.className === 'x') {
                classProp = 'class';
            }
        }
        el = null;

        el = document.createElement('label');
        el.setAttribute(forProp, 'x');
        if (el.htmlFor !== 'x') {
            el.setAttribute('htmlFor', 'x');
            if (el.htmlFor === 'x') {
                forProp = 'htmlFor';
            }
        }
        el = null;

        return {
            read: {
                names: {
                    'class': classProp,
                    'className': classProp,
                    'for': forProp,
                    'htmlFor': forProp
                },
                values: {
                    _getAttr: function (element, attribute) {
                        return element.getAttribute(attribute);
                    },
                    _getAttr2: function (element, attribute) {
                        return element.getAttribute(attribute, 2);
                    },
                    _getAttrNode: function (element, attribute) {
                        var node = element.getAttributeNode(attribute);
                        return node ? node.value : "";
                    },
                    _getEv: (function () {

                        var el = document.createElement('div'), f;
                        el.onclick = Prototype.emptyFunction;
                        var value = el.getAttribute('onclick');

                        if (String(value).indexOf('{') > -1) {
                            f = function (element, attribute) {
                                attribute = element.getAttribute(attribute);
                                if (!attribute) return null;
                                attribute = attribute.toString();
                                attribute = attribute.split('{')[1];
                                attribute = attribute.split('}')[0];
                                return attribute.strip();
                            };
                        }
                        else if (value === '') {
                            f = function (element, attribute) {
                                attribute = element.getAttribute(attribute);
                                if (!attribute) return null;
                                return attribute.strip();
                            };
                        }
                        el = null;
                        return f;
                    })(),
                    _flag: function (element, attribute) {
                        return $(element).hasAttribute(attribute) ? attribute : null;
                    },
                    style: function (element) {
                        return element.style.cssText.toLowerCase();
                    },
                    title: function (element) {
                        return element.title;
                    }
                }
            }
        }
    })();

    Element._attributeTranslations.write = {
        names: Object.extend({
            cellpadding: 'cellPadding',
            cellspacing: 'cellSpacing'
        }, Element._attributeTranslations.read.names),
        values: {
            checked: function (element, value) {
                element.checked = !!value;
            },

            style: function (element, value) {
                element.style.cssText = value ? value : '';
            }
        }
    };

    Element._attributeTranslations.has = {};

    $w('colSpan rowSpan vAlign dateTime accessKey tabIndex ' +
        'encType maxLength readOnly longDesc frameBorder').each(function (attr) {
        Element._attributeTranslations.write.names[attr.toLowerCase()] = attr;
        Element._attributeTranslations.has[attr.toLowerCase()] = attr;
    });

    (function (v) {
        Object.extend(v, {
            href: v._getAttr2,
            src: v._getAttr2,
            type: v._getAttr,
            action: v._getAttrNode,
            disabled: v._flag,
            checked: v._flag,
            readonly: v._flag,
            multiple: v._flag,
            onload: v._getEv,
            onunload: v._getEv,
            onclick: v._getEv,
            ondblclick: v._getEv,
            onmousedown: v._getEv,
            onmouseup: v._getEv,
            onmouseover: v._getEv,
            onmousemove: v._getEv,
            onmouseout: v._getEv,
            onfocus: v._getEv,
            onblur: v._getEv,
            onkeypress: v._getEv,
            onkeydown: v._getEv,
            onkeyup: v._getEv,
            onsubmit: v._getEv,
            onreset: v._getEv,
            onselect: v._getEv,
            onchange: v._getEv
        });
    })(Element._attributeTranslations.read.values);

    if (Prototype.BrowserFeatures.ElementExtensions) {
        (function () {
            function _descendants(element) {
                var nodes = element.getElementsByTagName('*'), results = [];
                for (var i = 0, node; node = nodes[i]; i++)
                    if (node.tagName !== "!") // Filter out comment nodes.
                        results.push(node);
                return results;
            }

            Element.Methods.down = function (element, expression, index) {
                element = $(element);
                if (arguments.length == 1) return element.firstDescendant();
                return Object.isNumber(expression) ? _descendants(element)[expression] :
                    Element.select(element, expression)[index || 0];
            }
        })();
    }

}

else if (Prototype.Browser.Gecko && /rv:1\.8\.0/.test(navigator.userAgent)) {
    Element.Methods.setOpacity = function (element, value) {
        element = $(element);
        element.style.opacity = (value == 1) ? 0.999999 :
            (value === '') ? '' : (value < 0.00001) ? 0 : value;
        return element;
    };
}

else if (Prototype.Browser.WebKit) {
    Element.Methods.setOpacity = function (element, value) {
        element = $(element);
        element.style.opacity = (value == 1 || value === '') ? '' :
            (value < 0.00001) ? 0 : value;

        if (value == 1)
            if (element.tagName.toUpperCase() == 'IMG' && element.width) {
                element.width++;
                element.width--;
            } else try {
                var n = document.createTextNode(' ');
                element.appendChild(n);
                element.removeChild(n);
            } catch (e) {
            }

        return element;
    };
}

if ('outerHTML' in document.documentElement) {
    Element.Methods.replace = function (element, content) {
        element = $(element);

        if (content && content.toElement) content = content.toElement();
        if (Object.isElement(content)) {
            element.parentNode.replaceChild(content, element);
            return element;
        }

        content = Object.toHTML(content);
        var parent = element.parentNode, tagName = parent.tagName.toUpperCase();

        if (Element._insertionTranslations.tags[tagName]) {
            var nextSibling = element.next(),
                fragments = Element._getContentFromAnonymousElement(tagName, content.stripScripts());
            parent.removeChild(element);
            if (nextSibling)
                fragments.each(function (node) {
                    parent.insertBefore(node, nextSibling)
                });
            else
                fragments.each(function (node) {
                    parent.appendChild(node)
                });
        }
        else element.outerHTML = content.stripScripts();

        content.evalScripts.bind(content).defer();
        return element;
    };
}

Element._returnOffset = function (l, t) {
    var result = [l, t];
    result.left = l;
    result.top = t;
    return result;
};

Element._getContentFromAnonymousElement = function (tagName, html, force) {
    var div = new Element('div'),
        t = Element._insertionTranslations.tags[tagName];

    var workaround = false;
    if (t) workaround = true;
    else if (force) {
        workaround = true;
        t = ['', '', 0];
    }

    if (workaround) {
        div.innerHTML = '&nbsp;' + t[0] + html + t[1];
        div.removeChild(div.firstChild);
        for (var i = t[2]; i--;) {
            div = div.firstChild;
        }
    }
    else {
        div.innerHTML = html;
    }
    return $A(div.childNodes);
};

Element._insertionTranslations = {
    before: function (element, node) {
        element.parentNode.insertBefore(node, element);
    },
    top: function (element, node) {
        element.insertBefore(node, element.firstChild);
    },
    bottom: function (element, node) {
        element.appendChild(node);
    },
    after: function (element, node) {
        element.parentNode.insertBefore(node, element.nextSibling);
    },
    tags: {
        TABLE: ['<table>', '</table>', 1],
        TBODY: ['<table><tbody>', '</tbody></table>', 2],
        TR: ['<table><tbody><tr>', '</tr></tbody></table>', 3],
        TD: ['<table><tbody><tr><td>', '</td></tr></tbody></table>', 4],
        SELECT: ['<select>', '</select>', 1]
    }
};

(function () {
    var tags = Element._insertionTranslations.tags;
    Object.extend(tags, {
        THEAD: tags.TBODY,
        TFOOT: tags.TBODY,
        TH: tags.TD
    });
})();

Element.Methods.Simulated = {
    hasAttribute: function (element, attribute) {
        attribute = Element._attributeTranslations.has[attribute] || attribute;
        var node = $(element).getAttributeNode(attribute);
        return !!(node && node.specified);
    }
};

Element.Methods.ByTag = {};

Object.extend(Element, Element.Methods);

(function (div) {

    if (!Prototype.BrowserFeatures.ElementExtensions && div['__proto__']) {
        window.HTMLElement = {};
        window.HTMLElement.prototype = div['__proto__'];
        Prototype.BrowserFeatures.ElementExtensions = true;
    }

    div = null;

})(document.createElement('div'));

Element.extend = (function () {

    function checkDeficiency(tagName) {
        if (typeof window.Element != 'undefined') {
            var proto = window.Element.prototype;
            if (proto) {
                var id = '_' + (Math.random() + '').slice(2),
                    el = document.createElement(tagName);
                proto[id] = 'x';
                var isBuggy = (el[id] !== 'x');
                delete proto[id];
                el = null;
                return isBuggy;
            }
        }
        return false;
    }

    function extendElementWith(element, methods) {
        for (var property in methods) {
            var value = methods[property];
            if (Object.isFunction(value) && !(property in element))
                element[property] = value.methodize();
        }
    }

    var HTMLOBJECTELEMENT_PROTOTYPE_BUGGY = checkDeficiency('object');

    if (Prototype.BrowserFeatures.SpecificElementExtensions) {
        if (HTMLOBJECTELEMENT_PROTOTYPE_BUGGY) {
            return function (element) {
                if (element && typeof element._extendedByPrototype == 'undefined') {
                    var t = element.tagName;
                    if (t && (/^(?:object|applet|embed)$/i.test(t))) {
                        extendElementWith(element, Element.Methods);
                        extendElementWith(element, Element.Methods.Simulated);
                        extendElementWith(element, Element.Methods.ByTag[t.toUpperCase()]);
                    }
                }
                return element;
            }
        }
        return Prototype.K;
    }

    var Methods = {}, ByTag = Element.Methods.ByTag;

    var extend = Object.extend(function (element) {
        if (!element || typeof element._extendedByPrototype != 'undefined' ||
            element.nodeType != 1 || element == window) return element;

        var methods = Object.clone(Methods),
            tagName = element.tagName.toUpperCase();

        if (ByTag[tagName]) Object.extend(methods, ByTag[tagName]);

        extendElementWith(element, methods);

        element._extendedByPrototype = Prototype.emptyFunction;
        return element;

    }, {
        refresh: function () {
            if (!Prototype.BrowserFeatures.ElementExtensions) {
                Object.extend(Methods, Element.Methods);
                Object.extend(Methods, Element.Methods.Simulated);
            }
        }
    });

    extend.refresh();
    return extend;
})();

if (document.documentElement.hasAttribute) {
    Element.hasAttribute = function (element, attribute) {
        return element.hasAttribute(attribute);
    };
}
else {
    Element.hasAttribute = Element.Methods.Simulated.hasAttribute;
}

Element.addMethods = function (methods) {
    var F = Prototype.BrowserFeatures, T = Element.Methods.ByTag;

    if (!methods) {
        Object.extend(Form, Form.Methods);
        Object.extend(Form.Element, Form.Element.Methods);
        Object.extend(Element.Methods.ByTag, {
            "FORM": Object.clone(Form.Methods),
            "INPUT": Object.clone(Form.Element.Methods),
            "SELECT": Object.clone(Form.Element.Methods),
            "TEXTAREA": Object.clone(Form.Element.Methods),
            "BUTTON": Object.clone(Form.Element.Methods)
        });
    }

    if (arguments.length == 2) {
        var tagName = methods;
        methods = arguments[1];
    }

    if (!tagName) Object.extend(Element.Methods, methods || {});
    else {
        if (Object.isArray(tagName)) tagName.each(extend);
        else extend(tagName);
    }

    function extend(tagName) {
        tagName = tagName.toUpperCase();
        if (!Element.Methods.ByTag[tagName])
            Element.Methods.ByTag[tagName] = {};
        Object.extend(Element.Methods.ByTag[tagName], methods);
    }

    function copy(methods, destination, onlyIfAbsent) {
        onlyIfAbsent = onlyIfAbsent || false;
        for (var property in methods) {
            var value = methods[property];
            if (!Object.isFunction(value)) continue;
            if (!onlyIfAbsent || !(property in destination))
                destination[property] = value.methodize();
        }
    }

    function findDOMClass(tagName) {
        var klass;
        var trans = {
            "OPTGROUP": "OptGroup",
            "TEXTAREA": "TextArea",
            "P": "Paragraph",
            "FIELDSET": "FieldSet",
            "UL": "UList",
            "OL": "OList",
            "DL": "DList",
            "DIR": "Directory",
            "H1": "Heading",
            "H2": "Heading",
            "H3": "Heading",
            "H4": "Heading",
            "H5": "Heading",
            "H6": "Heading",
            "Q": "Quote",
            "INS": "Mod",
            "DEL": "Mod",
            "A": "Anchor",
            "IMG": "Image",
            "CAPTION": "TableCaption",
            "COL": "TableCol",
            "COLGROUP": "TableCol",
            "THEAD": "TableSection",
            "TFOOT": "TableSection",
            "TBODY": "TableSection",
            "TR": "TableRow",
            "TH": "TableCell",
            "TD": "TableCell",
            "FRAMESET": "FrameSet",
            "IFRAME": "IFrame"
        };
        if (trans[tagName]) klass = 'HTML' + trans[tagName] + 'Element';
        if (window[klass]) return window[klass];
        klass = 'HTML' + tagName + 'Element';
        if (window[klass]) return window[klass];
        klass = 'HTML' + tagName.capitalize() + 'Element';
        if (window[klass]) return window[klass];

        var element = document.createElement(tagName),
            proto = element['__proto__'] || element.constructor.prototype;

        element = null;
        return proto;
    }

    var elementPrototype = window.HTMLElement ? HTMLElement.prototype :
        Element.prototype;

    if (F.ElementExtensions) {
        copy(Element.Methods, elementPrototype);
        copy(Element.Methods.Simulated, elementPrototype, true);
    }

    if (F.SpecificElementExtensions) {
        for (var tag in Element.Methods.ByTag) {
            var klass = findDOMClass(tag);
            if (Object.isUndefined(klass)) continue;
            copy(T[tag], klass.prototype);
        }
    }

    Object.extend(Element, Element.Methods);
    delete Element.ByTag;

    if (Element.extend.refresh) Element.extend.refresh();
    Element.cache = {};
};


document.viewport = {

    getDimensions: function () {
        return {width: this.getWidth(), height: this.getHeight()};
    },

    getScrollOffsets: function () {
        return Element._returnOffset(
            window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
            window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop);
    }
};

(function (viewport) {
    var B = Prototype.Browser, doc = document, element, property = {};

    function getRootElement() {
        if (B.WebKit && !doc.evaluate)
            return document;

        if (B.Opera && window.parseFloat(window.opera.version()) < 9.5)
            return document.body;

        return document.documentElement;
    }

    function define(D) {
        if (!element) element = getRootElement();

        property[D] = 'client' + D;

        viewport['get' + D] = function () {
            return element[property[D]]
        };
        return viewport['get' + D]();
    }

    viewport.getWidth = define.curry('Width');

    viewport.getHeight = define.curry('Height');
})(document.viewport);


Element.Storage = {
    UID: 1
};

Element.addMethods({
    getStorage: function (element) {
        if (!(element = $(element))) return;

        var uid;
        if (element === window) {
            uid = 0;
        } else {
            if (typeof element._prototypeUID === "undefined")
                element._prototypeUID = Element.Storage.UID++;
            uid = element._prototypeUID;
        }

        if (!Element.Storage[uid])
            Element.Storage[uid] = $H();

        return Element.Storage[uid];
    },

    store: function (element, key, value) {
        if (!(element = $(element))) return;

        if (arguments.length === 2) {
            Element.getStorage(element).update(key);
        } else {
            Element.getStorage(element).set(key, value);
        }

        return element;
    },

    retrieve: function (element, key, defaultValue) {
        if (!(element = $(element))) return;
        var hash = Element.getStorage(element), value = hash.get(key);

        if (Object.isUndefined(value)) {
            hash.set(key, defaultValue);
            value = defaultValue;
        }

        return value;
    },

    clone: function (element, deep) {
        if (!(element = $(element))) return;
        var clone = element.cloneNode(deep);
        clone._prototypeUID = void 0;
        if (deep) {
            var descendants = Element.select(clone, '*'),
                i = descendants.length;
            while (i--) {
                descendants[i]._prototypeUID = void 0;
            }
        }
        return Element.extend(clone);
    },

    purge: function (element) {
        if (!(element = $(element))) return;
        var purgeElement = Element._purgeElement;

        purgeElement(element);

        var descendants = element.getElementsByTagName('*'),
            i = descendants.length;

        while (i--) purgeElement(descendants[i]);

        return null;
    }
});

(function () {

    function toDecimal(pctString) {
        var match = pctString.match(/^(\d+)%?$/i);
        if (!match) return null;
        return (Number(match[1]) / 100);
    }

    function getPixelValue(value, property, context) {
        var element = null;
        if (Object.isElement(value)) {
            element = value;
            value = element.getStyle(property);
        }

        if (value === null) {
            return null;
        }

        if ((/^(?:-)?\d+(\.\d+)?(px)?$/i).test(value)) {
            return window.parseFloat(value);
        }

        var isPercentage = value.include('%'), isViewport = (context === document.viewport);

        if (/\d/.test(value) && element && element.runtimeStyle && !(isPercentage && isViewport)) {
            var style = element.style.left, rStyle = element.runtimeStyle.left;
            element.runtimeStyle.left = element.currentStyle.left;
            element.style.left = value || 0;
            value = element.style.pixelLeft;
            element.style.left = style;
            element.runtimeStyle.left = rStyle;

            return value;
        }

        if (element && isPercentage) {
            context = context || element.parentNode;
            var decimal = toDecimal(value);
            var whole = null;
            var position = element.getStyle('position');

            var isHorizontal = property.include('left') || property.include('right') ||
                property.include('width');

            var isVertical = property.include('top') || property.include('bottom') ||
                property.include('height');

            if (context === document.viewport) {
                if (isHorizontal) {
                    whole = document.viewport.getWidth();
                } else if (isVertical) {
                    whole = document.viewport.getHeight();
                }
            } else {
                if (isHorizontal) {
                    whole = $(context).measure('width');
                } else if (isVertical) {
                    whole = $(context).measure('height');
                }
            }

            return (whole === null) ? 0 : whole * decimal;
        }

        return 0;
    }

    function toCSSPixels(number) {
        if (Object.isString(number) && number.endsWith('px')) {
            return number;
        }
        return number + 'px';
    }

    function isDisplayed(element) {
        var originalElement = element;
        while (element && element.parentNode) {
            var display = element.getStyle('display');
            if (display === 'none') {
                return false;
            }
            element = $(element.parentNode);
        }
        return true;
    }

    var hasLayout = Prototype.K;
    if ('currentStyle' in document.documentElement) {
        hasLayout = function (element) {
            if (!element.currentStyle.hasLayout) {
                element.style.zoom = 1;
            }
            return element;
        };
    }

    function cssNameFor(key) {
        if (key.include('border')) key = key + '-width';
        return key.camelize();
    }

    Element.Layout = Class.create(Hash, {
        initialize: function ($super, element, preCompute) {
            $super();
            this.element = $(element);

            Element.Layout.PROPERTIES.each(function (property) {
                this._set(property, null);
            }, this);

            if (preCompute) {
                this._preComputing = true;
                this._begin();
                Element.Layout.PROPERTIES.each(this._compute, this);
                this._end();
                this._preComputing = false;
            }
        },

        _set: function (property, value) {
            return Hash.prototype.set.call(this, property, value);
        },

        set: function (property, value) {
            throw "Properties of Element.Layout are read-only.";
        },

        get: function ($super, property) {
            var value = $super(property);
            return value === null ? this._compute(property) : value;
        },

        _begin: function () {
            if (this._prepared) return;

            var element = this.element;
            if (isDisplayed(element)) {
                this._prepared = true;
                return;
            }

            var originalStyles = {
                position: element.style.position || '',
                width: element.style.width || '',
                visibility: element.style.visibility || '',
                display: element.style.display || ''
            };

            element.store('prototype_original_styles', originalStyles);

            var position = element.getStyle('position'),
                width = element.getStyle('width');

            if (width === "0px" || width === null) {
                element.style.display = 'block';
                width = element.getStyle('width');
            }

            var context = (position === 'fixed') ? document.viewport :
                element.parentNode;

            element.setStyle({
                position: 'absolute',
                visibility: 'hidden',
                display: 'block'
            });

            var positionedWidth = element.getStyle('width');

            var newWidth;
            if (width && (positionedWidth === width)) {
                newWidth = getPixelValue(element, 'width', context);
            } else if (position === 'absolute' || position === 'fixed') {
                newWidth = getPixelValue(element, 'width', context);
            } else {
                var parent = element.parentNode, pLayout = $(parent).getLayout();

                newWidth = pLayout.get('width') -
                    this.get('margin-left') -
                    this.get('border-left') -
                    this.get('padding-left') -
                    this.get('padding-right') -
                    this.get('border-right') -
                    this.get('margin-right');
            }

            element.setStyle({width: newWidth + 'px'});

            this._prepared = true;
        },

        _end: function () {
            var element = this.element;
            var originalStyles = element.retrieve('prototype_original_styles');
            element.store('prototype_original_styles', null);
            element.setStyle(originalStyles);
            this._prepared = false;
        },

        _compute: function (property) {
            var COMPUTATIONS = Element.Layout.COMPUTATIONS;
            if (!(property in COMPUTATIONS)) {
                throw "Property not found.";
            }

            return this._set(property, COMPUTATIONS[property].call(this, this.element));
        },

        toObject: function () {
            var args = $A(arguments);
            var keys = (args.length === 0) ? Element.Layout.PROPERTIES :
                args.join(' ').split(' ');
            var obj = {};
            keys.each(function (key) {
                if (!Element.Layout.PROPERTIES.include(key)) return;
                var value = this.get(key);
                if (value != null) obj[key] = value;
            }, this);
            return obj;
        },

        toHash: function () {
            var obj = this.toObject.apply(this, arguments);
            return new Hash(obj);
        },

        toCSS: function () {
            var args = $A(arguments);
            var keys = (args.length === 0) ? Element.Layout.PROPERTIES :
                args.join(' ').split(' ');
            var css = {};

            keys.each(function (key) {
                if (!Element.Layout.PROPERTIES.include(key)) return;
                if (Element.Layout.COMPOSITE_PROPERTIES.include(key)) return;

                var value = this.get(key);
                if (value != null) css[cssNameFor(key)] = value + 'px';
            }, this);
            return css;
        },

        inspect: function () {
            return "#<Element.Layout>";
        }
    });

    Object.extend(Element.Layout, {
        PROPERTIES: $w('height width top left right bottom border-left border-right border-top border-bottom padding-left padding-right padding-top padding-bottom margin-top margin-bottom margin-left margin-right padding-box-width padding-box-height border-box-width border-box-height margin-box-width margin-box-height'),

        COMPOSITE_PROPERTIES: $w('padding-box-width padding-box-height margin-box-width margin-box-height border-box-width border-box-height'),

        COMPUTATIONS: {
            'height': function (element) {
                if (!this._preComputing) this._begin();

                var bHeight = this.get('border-box-height');
                if (bHeight <= 0) {
                    if (!this._preComputing) this._end();
                    return 0;
                }

                var bTop = this.get('border-top'),
                    bBottom = this.get('border-bottom');

                var pTop = this.get('padding-top'),
                    pBottom = this.get('padding-bottom');

                if (!this._preComputing) this._end();

                return bHeight - bTop - bBottom - pTop - pBottom;
            },

            'width': function (element) {
                if (!this._preComputing) this._begin();

                var bWidth = this.get('border-box-width');
                if (bWidth <= 0) {
                    if (!this._preComputing) this._end();
                    return 0;
                }

                var bLeft = this.get('border-left'),
                    bRight = this.get('border-right');

                var pLeft = this.get('padding-left'),
                    pRight = this.get('padding-right');

                if (!this._preComputing) this._end();

                return bWidth - bLeft - bRight - pLeft - pRight;
            },

            'padding-box-height': function (element) {
                var height = this.get('height'),
                    pTop = this.get('padding-top'),
                    pBottom = this.get('padding-bottom');

                return height + pTop + pBottom;
            },

            'padding-box-width': function (element) {
                var width = this.get('width'),
                    pLeft = this.get('padding-left'),
                    pRight = this.get('padding-right');

                return width + pLeft + pRight;
            },

            'border-box-height': function (element) {
                if (!this._preComputing) this._begin();
                var height = element.offsetHeight;
                if (!this._preComputing) this._end();
                return height;
            },

            'border-box-width': function (element) {
                if (!this._preComputing) this._begin();
                var width = element.offsetWidth;
                if (!this._preComputing) this._end();
                return width;
            },

            'margin-box-height': function (element) {
                var bHeight = this.get('border-box-height'),
                    mTop = this.get('margin-top'),
                    mBottom = this.get('margin-bottom');

                if (bHeight <= 0) return 0;

                return bHeight + mTop + mBottom;
            },

            'margin-box-width': function (element) {
                var bWidth = this.get('border-box-width'),
                    mLeft = this.get('margin-left'),
                    mRight = this.get('margin-right');

                if (bWidth <= 0) return 0;

                return bWidth + mLeft + mRight;
            },

            'top': function (element) {
                var offset = element.positionedOffset();
                return offset.top;
            },

            'bottom': function (element) {
                var offset = element.positionedOffset(),
                    parent = element.getOffsetParent(),
                    pHeight = parent.measure('height');

                var mHeight = this.get('border-box-height');

                return pHeight - mHeight - offset.top;
            },

            'left': function (element) {
                var offset = element.positionedOffset();
                return offset.left;
            },

            'right': function (element) {
                var offset = element.positionedOffset(),
                    parent = element.getOffsetParent(),
                    pWidth = parent.measure('width');

                var mWidth = this.get('border-box-width');

                return pWidth - mWidth - offset.left;
            },

            'padding-top': function (element) {
                return getPixelValue(element, 'paddingTop');
            },

            'padding-bottom': function (element) {
                return getPixelValue(element, 'paddingBottom');
            },

            'padding-left': function (element) {
                return getPixelValue(element, 'paddingLeft');
            },

            'padding-right': function (element) {
                return getPixelValue(element, 'paddingRight');
            },

            'border-top': function (element) {
                return getPixelValue(element, 'borderTopWidth');
            },

            'border-bottom': function (element) {
                return getPixelValue(element, 'borderBottomWidth');
            },

            'border-left': function (element) {
                return getPixelValue(element, 'borderLeftWidth');
            },

            'border-right': function (element) {
                return getPixelValue(element, 'borderRightWidth');
            },

            'margin-top': function (element) {
                return getPixelValue(element, 'marginTop');
            },

            'margin-bottom': function (element) {
                return getPixelValue(element, 'marginBottom');
            },

            'margin-left': function (element) {
                return getPixelValue(element, 'marginLeft');
            },

            'margin-right': function (element) {
                return getPixelValue(element, 'marginRight');
            }
        }
    });

    if ('getBoundingClientRect' in document.documentElement) {
        Object.extend(Element.Layout.COMPUTATIONS, {
            'right': function (element) {
                var parent = hasLayout(element.getOffsetParent());
                var rect = element.getBoundingClientRect(),
                    pRect = parent.getBoundingClientRect();

                return (pRect.right - rect.right).round();
            },

            'bottom': function (element) {
                var parent = hasLayout(element.getOffsetParent());
                var rect = element.getBoundingClientRect(),
                    pRect = parent.getBoundingClientRect();

                return (pRect.bottom - rect.bottom).round();
            }
        });
    }

    Element.Offset = Class.create({
        initialize: function (left, top) {
            this.left = left.round();
            this.top = top.round();

            this[0] = this.left;
            this[1] = this.top;
        },

        relativeTo: function (offset) {
            return new Element.Offset(
                this.left - offset.left,
                this.top - offset.top
            );
        },

        inspect: function () {
            return "#<Element.Offset left: #{left} top: #{top}>".interpolate(this);
        },

        toString: function () {
            return "[#{left}, #{top}]".interpolate(this);
        },

        toArray: function () {
            return [this.left, this.top];
        }
    });

    function getLayout(element, preCompute) {
        return new Element.Layout(element, preCompute);
    }

    function measure(element, property) {
        return $(element).getLayout().get(property);
    }

    function getDimensions(element) {
        element = $(element);
        var display = Element.getStyle(element, 'display');

        if (display && display !== 'none') {
            return {width: element.offsetWidth, height: element.offsetHeight};
        }

        var style = element.style;
        var originalStyles = {
            visibility: style.visibility,
            position: style.position,
            display: style.display
        };

        var newStyles = {
            visibility: 'hidden',
            display: 'block'
        };

        if (originalStyles.position !== 'fixed')
            newStyles.position = 'absolute';

        Element.setStyle(element, newStyles);

        var dimensions = {
            width: element.offsetWidth,
            height: element.offsetHeight
        };

        Element.setStyle(element, originalStyles);

        return dimensions;
    }

    function getOffsetParent(element) {
        element = $(element);

        if (isDocument(element) || isDetached(element) || isBody(element) || isHtml(element))
            return $(document.body);

        var isInline = (Element.getStyle(element, 'display') === 'inline');
        if (!isInline && element.offsetParent) return $(element.offsetParent);

        while ((element = element.parentNode) && element !== document.body) {
            if (Element.getStyle(element, 'position') !== 'static') {
                return isHtml(element) ? $(document.body) : $(element);
            }
        }

        return $(document.body);
    }


    function cumulativeOffset(element) {
        element = $(element);
        var valueT = 0, valueL = 0;
        if (element.parentNode) {
            do {
                valueT += element.offsetTop || 0;
                valueL += element.offsetLeft || 0;
                element = element.offsetParent;
            } while (element);
        }
        return new Element.Offset(valueL, valueT);
    }

    function positionedOffset(element) {
        element = $(element);

        var layout = element.getLayout();

        var valueT = 0, valueL = 0;
        do {
            valueT += element.offsetTop || 0;
            valueL += element.offsetLeft || 0;
            element = element.offsetParent;
            if (element) {
                if (isBody(element)) break;
                var p = Element.getStyle(element, 'position');
                if (p !== 'static') break;
            }
        } while (element);

        valueL -= layout.get('margin-top');
        valueT -= layout.get('margin-left');

        return new Element.Offset(valueL, valueT);
    }

    function cumulativeScrollOffset(element) {
        var valueT = 0, valueL = 0;
        do {
            valueT += element.scrollTop || 0;
            valueL += element.scrollLeft || 0;
            element = element.parentNode;
        } while (element);
        return new Element.Offset(valueL, valueT);
    }

    function viewportOffset(forElement) {
        element = $(element);
        var valueT = 0, valueL = 0, docBody = document.body;

        var element = forElement;
        do {
            valueT += element.offsetTop || 0;
            valueL += element.offsetLeft || 0;
            if (element.offsetParent == docBody &&
                Element.getStyle(element, 'position') == 'absolute') break;
        } while (element = element.offsetParent);

        element = forElement;
        do {
            if (element != docBody) {
                valueT -= element.scrollTop || 0;
                valueL -= element.scrollLeft || 0;
            }
        } while (element = element.parentNode);
        return new Element.Offset(valueL, valueT);
    }

    function absolutize(element) {
        element = $(element);

        if (Element.getStyle(element, 'position') === 'absolute') {
            return element;
        }

        var offsetParent = getOffsetParent(element);
        var eOffset = element.viewportOffset(),
            pOffset = offsetParent.viewportOffset();

        var offset = eOffset.relativeTo(pOffset);
        var layout = element.getLayout();

        element.store('prototype_absolutize_original_styles', {
            left: element.getStyle('left'),
            top: element.getStyle('top'),
            width: element.getStyle('width'),
            height: element.getStyle('height')
        });

        element.setStyle({
            position: 'absolute',
            top: offset.top + 'px',
            left: offset.left + 'px',
            width: layout.get('width') + 'px',
            height: layout.get('height') + 'px'
        });

        return element;
    }

    function relativize(element) {
        element = $(element);
        if (Element.getStyle(element, 'position') === 'relative') {
            return element;
        }

        var originalStyles =
            element.retrieve('prototype_absolutize_original_styles');

        if (originalStyles) element.setStyle(originalStyles);
        return element;
    }

    if (Prototype.Browser.IE) {
        getOffsetParent = getOffsetParent.wrap(
            function (proceed, element) {
                element = $(element);

                if (isDocument(element) || isDetached(element) || isBody(element) || isHtml(element))
                    return $(document.body);

                var position = element.getStyle('position');
                if (position !== 'static') return proceed(element);

                element.setStyle({position: 'relative'});
                var value = proceed(element);
                element.setStyle({position: position});
                return value;
            }
        );

        positionedOffset = positionedOffset.wrap(function (proceed, element) {
            element = $(element);
            if (!element.parentNode) return new Element.Offset(0, 0);
            var position = element.getStyle('position');
            if (position !== 'static') return proceed(element);

            var offsetParent = element.getOffsetParent();
            if (offsetParent && offsetParent.getStyle('position') === 'fixed')
                hasLayout(offsetParent);

            element.setStyle({position: 'relative'});
            var value = proceed(element);
            element.setStyle({position: position});
            return value;
        });
    } else if (Prototype.Browser.Webkit) {
        cumulativeOffset = function (element) {
            element = $(element);
            var valueT = 0, valueL = 0;
            do {
                valueT += element.offsetTop || 0;
                valueL += element.offsetLeft || 0;
                if (element.offsetParent == document.body)
                    if (Element.getStyle(element, 'position') == 'absolute') break;

                element = element.offsetParent;
            } while (element);

            return new Element.Offset(valueL, valueT);
        };
    }


    Element.addMethods({
        getLayout: getLayout,
        measure: measure,
        getDimensions: getDimensions,
        getOffsetParent: getOffsetParent,
        cumulativeOffset: cumulativeOffset,
        positionedOffset: positionedOffset,
        cumulativeScrollOffset: cumulativeScrollOffset,
        viewportOffset: viewportOffset,
        absolutize: absolutize,
        relativize: relativize
    });

    function isBody(element) {
        return element.nodeName.toUpperCase() === 'BODY';
    }

    function isHtml(element) {
        return element.nodeName.toUpperCase() === 'HTML';
    }

    function isDocument(element) {
        return element.nodeType === Node.DOCUMENT_NODE;
    }

    function isDetached(element) {
        return element !== document.body && !Element.descendantOf(element, document.body);
    }

    if ('getBoundingClientRect' in document.documentElement) {
        Element.addMethods({
            viewportOffset: function (element) {
                element = $(element);
                if (isDetached(element)) return new Element.Offset(0, 0);

                var rect = element.getBoundingClientRect(),
                    docEl = document.documentElement;
                return new Element.Offset(rect.left - docEl.clientLeft,
                    rect.top - docEl.clientTop);
            }
        });
    }
})();
window.$$ = function () {
    var expression = $A(arguments).join(', ');
    return Prototype.Selector.select(expression, document);
};

Prototype.Selector = (function () {

    function select() {
        throw new Error('Method "Prototype.Selector.select" must be defined.');
    }

    function match() {
        throw new Error('Method "Prototype.Selector.match" must be defined.');
    }

    function find(elements, expression, index) {
        index = index || 0;
        var match = Prototype.Selector.match, length = elements.length, matchIndex = 0, i;

        for (i = 0; i < length; i++) {
            if (match(elements[i], expression) && index == matchIndex++) {
                return Element.extend(elements[i]);
            }
        }
    }

    function extendElements(elements) {
        for (var i = 0, length = elements.length; i < length; i++) {
            Element.extend(elements[i]);
        }
        return elements;
    }


    var K = Prototype.K;

    return {
        select: select,
        match: match,
        find: find,
        extendElements: (Element.extend === K) ? K : extendElements,
        extendElement: Element.extend
    };
})();
Prototype._original_property = window.Sizzle;
/*!
 * Sizzle CSS Selector Engine - v1.0
 *  Copyright 2009, The Dojo Foundation
 *  Released under the MIT, BSD, and GPL Licenses.
 *  More information: http://sizzlejs.com/
 */
(function () {

    var chunker = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
        done = 0,
        toString = Object.prototype.toString,
        hasDuplicate = false,
        baseHasDuplicate = true;

    [0, 0].sort(function () {
        baseHasDuplicate = false;
        return 0;
    });

    var Sizzle = function (selector, context, results, seed) {
        results = results || [];
        var origContext = context = context || document;

        if (context.nodeType !== 1 && context.nodeType !== 9) {
            return [];
        }

        if (!selector || typeof selector !== "string") {
            return results;
        }

        var parts = [], m, set, checkSet, check, mode, extra, prune = true, contextXML = isXML(context),
            soFar = selector;

        while ((chunker.exec(""), m = chunker.exec(soFar)) !== null) {
            soFar = m[3];

            parts.push(m[1]);

            if (m[2]) {
                extra = m[3];
                break;
            }
        }

        if (parts.length > 1 && origPOS.exec(selector)) {
            if (parts.length === 2 && Expr.relative[parts[0]]) {
                set = posProcess(parts[0] + parts[1], context);
            } else {
                set = Expr.relative[parts[0]] ?
                    [context] :
                    Sizzle(parts.shift(), context);

                while (parts.length) {
                    selector = parts.shift();

                    if (Expr.relative[selector])
                        selector += parts.shift();

                    set = posProcess(selector, set);
                }
            }
        } else {
            if (!seed && parts.length > 1 && context.nodeType === 9 && !contextXML &&
                Expr.match.ID.test(parts[0]) && !Expr.match.ID.test(parts[parts.length - 1])) {
                var ret = Sizzle.find(parts.shift(), context, contextXML);
                context = ret.expr ? Sizzle.filter(ret.expr, ret.set)[0] : ret.set[0];
            }

            if (context) {
                var ret = seed ?
                    {expr: parts.pop(), set: makeArray(seed)} :
                    Sizzle.find(parts.pop(), parts.length === 1 && (parts[0] === "~" || parts[0] === "+") && context.parentNode ? context.parentNode : context, contextXML);
                set = ret.expr ? Sizzle.filter(ret.expr, ret.set) : ret.set;

                if (parts.length > 0) {
                    checkSet = makeArray(set);
                } else {
                    prune = false;
                }

                while (parts.length) {
                    var cur = parts.pop(), pop = cur;

                    if (!Expr.relative[cur]) {
                        cur = "";
                    } else {
                        pop = parts.pop();
                    }

                    if (pop == null) {
                        pop = context;
                    }

                    Expr.relative[cur](checkSet, pop, contextXML);
                }
            } else {
                checkSet = parts = [];
            }
        }

        if (!checkSet) {
            checkSet = set;
        }

        if (!checkSet) {
            throw "Syntax error, unrecognized expression: " + (cur || selector);
        }

        if (toString.call(checkSet) === "[object Array]") {
            if (!prune) {
                results.push.apply(results, checkSet);
            } else if (context && context.nodeType === 1) {
                for (var i = 0; checkSet[i] != null; i++) {
                    if (checkSet[i] && (checkSet[i] === true || checkSet[i].nodeType === 1 && contains(context, checkSet[i]))) {
                        results.push(set[i]);
                    }
                }
            } else {
                for (var i = 0; checkSet[i] != null; i++) {
                    if (checkSet[i] && checkSet[i].nodeType === 1) {
                        results.push(set[i]);
                    }
                }
            }
        } else {
            makeArray(checkSet, results);
        }

        if (extra) {
            Sizzle(extra, origContext, results, seed);
            Sizzle.uniqueSort(results);
        }

        return results;
    };

    Sizzle.uniqueSort = function (results) {
        if (sortOrder) {
            hasDuplicate = baseHasDuplicate;
            results.sort(sortOrder);

            if (hasDuplicate) {
                for (var i = 1; i < results.length; i++) {
                    if (results[i] === results[i - 1]) {
                        results.splice(i--, 1);
                    }
                }
            }
        }

        return results;
    };

    Sizzle.matches = function (expr, set) {
        return Sizzle(expr, null, null, set);
    };

    Sizzle.find = function (expr, context, isXML) {
        var set, match;

        if (!expr) {
            return [];
        }

        for (var i = 0, l = Expr.order.length; i < l; i++) {
            var type = Expr.order[i], match;

            if ((match = Expr.leftMatch[type].exec(expr))) {
                var left = match[1];
                match.splice(1, 1);

                if (left.substr(left.length - 1) !== "\\") {
                    match[1] = (match[1] || "").replace(/\\/g, "");
                    set = Expr.find[type](match, context, isXML);
                    if (set != null) {
                        expr = expr.replace(Expr.match[type], "");
                        break;
                    }
                }
            }
        }

        if (!set) {
            set = context.getElementsByTagName("*");
        }

        return {set: set, expr: expr};
    };

    Sizzle.filter = function (expr, set, inplace, not) {
        var old = expr, result = [], curLoop = set, match, anyFound,
            isXMLFilter = set && set[0] && isXML(set[0]);

        while (expr && set.length) {
            for (var type in Expr.filter) {
                if ((match = Expr.match[type].exec(expr)) != null) {
                    var filter = Expr.filter[type], found, item;
                    anyFound = false;

                    if (curLoop == result) {
                        result = [];
                    }

                    if (Expr.preFilter[type]) {
                        match = Expr.preFilter[type](match, curLoop, inplace, result, not, isXMLFilter);

                        if (!match) {
                            anyFound = found = true;
                        } else if (match === true) {
                            continue;
                        }
                    }

                    if (match) {
                        for (var i = 0; (item = curLoop[i]) != null; i++) {
                            if (item) {
                                found = filter(item, match, i, curLoop);
                                var pass = not ^ !!found;

                                if (inplace && found != null) {
                                    if (pass) {
                                        anyFound = true;
                                    } else {
                                        curLoop[i] = false;
                                    }
                                } else if (pass) {
                                    result.push(item);
                                    anyFound = true;
                                }
                            }
                        }
                    }

                    if (found !== undefined) {
                        if (!inplace) {
                            curLoop = result;
                        }

                        expr = expr.replace(Expr.match[type], "");

                        if (!anyFound) {
                            return [];
                        }

                        break;
                    }
                }
            }

            if (expr == old) {
                if (anyFound == null) {
                    throw "Syntax error, unrecognized expression: " + expr;
                } else {
                    break;
                }
            }

            old = expr;
        }

        return curLoop;
    };

    var Expr = Sizzle.selectors = {
        order: ["ID", "NAME", "TAG"],
        match: {
            ID: /#((?:[\w\u00c0-\uFFFF-]|\\.)+)/,
            CLASS: /\.((?:[\w\u00c0-\uFFFF-]|\\.)+)/,
            NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF-]|\\.)+)['"]*\]/,
            ATTR: /\[\s*((?:[\w\u00c0-\uFFFF-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,
            TAG: /^((?:[\w\u00c0-\uFFFF\*-]|\\.)+)/,
            CHILD: /:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/,
            POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/,
            PSEUDO: /:((?:[\w\u00c0-\uFFFF-]|\\.)+)(?:\((['"]*)((?:\([^\)]+\)|[^\2\(\)]*)+)\2\))?/
        },
        leftMatch: {},
        attrMap: {
            "class": "className",
            "for": "htmlFor"
        },
        attrHandle: {
            href: function (elem) {
                return elem.getAttribute("href");
            }
        },
        relative: {
            "+": function (checkSet, part, isXML) {
                var isPartStr = typeof part === "string",
                    isTag = isPartStr && !/\W/.test(part),
                    isPartStrNotTag = isPartStr && !isTag;

                if (isTag && !isXML) {
                    part = part.toUpperCase();
                }

                for (var i = 0, l = checkSet.length, elem; i < l; i++) {
                    if ((elem = checkSet[i])) {
                        while ((elem = elem.previousSibling) && elem.nodeType !== 1) {
                        }

                        checkSet[i] = isPartStrNotTag || elem && elem.nodeName === part ?
                            elem || false :
                            elem === part;
                    }
                }

                if (isPartStrNotTag) {
                    Sizzle.filter(part, checkSet, true);
                }
            },
            ">": function (checkSet, part, isXML) {
                var isPartStr = typeof part === "string";

                if (isPartStr && !/\W/.test(part)) {
                    part = isXML ? part : part.toUpperCase();

                    for (var i = 0, l = checkSet.length; i < l; i++) {
                        var elem = checkSet[i];
                        if (elem) {
                            var parent = elem.parentNode;
                            checkSet[i] = parent.nodeName === part ? parent : false;
                        }
                    }
                } else {
                    for (var i = 0, l = checkSet.length; i < l; i++) {
                        var elem = checkSet[i];
                        if (elem) {
                            checkSet[i] = isPartStr ?
                                elem.parentNode :
                                elem.parentNode === part;
                        }
                    }

                    if (isPartStr) {
                        Sizzle.filter(part, checkSet, true);
                    }
                }
            },
            "": function (checkSet, part, isXML) {
                var doneName = done++, checkFn = dirCheck;

                if (!/\W/.test(part)) {
                    var nodeCheck = part = isXML ? part : part.toUpperCase();
                    checkFn = dirNodeCheck;
                }

                checkFn("parentNode", part, doneName, checkSet, nodeCheck, isXML);
            },
            "~": function (checkSet, part, isXML) {
                var doneName = done++, checkFn = dirCheck;

                if (typeof part === "string" && !/\W/.test(part)) {
                    var nodeCheck = part = isXML ? part : part.toUpperCase();
                    checkFn = dirNodeCheck;
                }

                checkFn("previousSibling", part, doneName, checkSet, nodeCheck, isXML);
            }
        },
        find: {
            ID: function (match, context, isXML) {
                if (typeof context.getElementById !== "undefined" && !isXML) {
                    var m = context.getElementById(match[1]);
                    return m ? [m] : [];
                }
            },
            NAME: function (match, context, isXML) {
                if (typeof context.getElementsByName !== "undefined") {
                    var ret = [], results = context.getElementsByName(match[1]);

                    for (var i = 0, l = results.length; i < l; i++) {
                        if (results[i].getAttribute("name") === match[1]) {
                            ret.push(results[i]);
                        }
                    }

                    return ret.length === 0 ? null : ret;
                }
            },
            TAG: function (match, context) {
                return context.getElementsByTagName(match[1]);
            }
        },
        preFilter: {
            CLASS: function (match, curLoop, inplace, result, not, isXML) {
                match = " " + match[1].replace(/\\/g, "") + " ";

                if (isXML) {
                    return match;
                }

                for (var i = 0, elem; (elem = curLoop[i]) != null; i++) {
                    if (elem) {
                        if (not ^ (elem.className && (" " + elem.className + " ").indexOf(match) >= 0)) {
                            if (!inplace)
                                result.push(elem);
                        } else if (inplace) {
                            curLoop[i] = false;
                        }
                    }
                }

                return false;
            },
            ID: function (match) {
                return match[1].replace(/\\/g, "");
            },
            TAG: function (match, curLoop) {
                for (var i = 0; curLoop[i] === false; i++) {
                }
                return curLoop[i] && isXML(curLoop[i]) ? match[1] : match[1].toUpperCase();
            },
            CHILD: function (match) {
                if (match[1] == "nth") {
                    var test = /(-?)(\d*)n((?:\+|-)?\d*)/.exec(
                        match[2] == "even" && "2n" || match[2] == "odd" && "2n+1" ||
                        !/\D/.test(match[2]) && "0n+" + match[2] || match[2]);

                    match[2] = (test[1] + (test[2] || 1)) - 0;
                    match[3] = test[3] - 0;
                }

                match[0] = done++;

                return match;
            },
            ATTR: function (match, curLoop, inplace, result, not, isXML) {
                var name = match[1].replace(/\\/g, "");

                if (!isXML && Expr.attrMap[name]) {
                    match[1] = Expr.attrMap[name];
                }

                if (match[2] === "~=") {
                    match[4] = " " + match[4] + " ";
                }

                return match;
            },
            PSEUDO: function (match, curLoop, inplace, result, not) {
                if (match[1] === "not") {
                    if (( chunker.exec(match[3]) || "" ).length > 1 || /^\w/.test(match[3])) {
                        match[3] = Sizzle(match[3], null, null, curLoop);
                    } else {
                        var ret = Sizzle.filter(match[3], curLoop, inplace, true ^ not);
                        if (!inplace) {
                            result.push.apply(result, ret);
                        }
                        return false;
                    }
                } else if (Expr.match.POS.test(match[0]) || Expr.match.CHILD.test(match[0])) {
                    return true;
                }

                return match;
            },
            POS: function (match) {
                match.unshift(true);
                return match;
            }
        },
        filters: {
            enabled: function (elem) {
                return elem.disabled === false && elem.type !== "hidden";
            },
            disabled: function (elem) {
                return elem.disabled === true;
            },
            checked: function (elem) {
                return elem.checked === true;
            },
            selected: function (elem) {
                elem.parentNode.selectedIndex;
                return elem.selected === true;
            },
            parent: function (elem) {
                return !!elem.firstChild;
            },
            empty: function (elem) {
                return !elem.firstChild;
            },
            has: function (elem, i, match) {
                return !!Sizzle(match[3], elem).length;
            },
            header: function (elem) {
                return /h\d/i.test(elem.nodeName);
            },
            text: function (elem) {
                return "text" === elem.type;
            },
            radio: function (elem) {
                return "radio" === elem.type;
            },
            checkbox: function (elem) {
                return "checkbox" === elem.type;
            },
            file: function (elem) {
                return "file" === elem.type;
            },
            password: function (elem) {
                return "password" === elem.type;
            },
            submit: function (elem) {
                return "submit" === elem.type;
            },
            image: function (elem) {
                return "image" === elem.type;
            },
            reset: function (elem) {
                return "reset" === elem.type;
            },
            button: function (elem) {
                return "button" === elem.type || elem.nodeName.toUpperCase() === "BUTTON";
            },
            input: function (elem) {
                return /input|select|textarea|button/i.test(elem.nodeName);
            }
        },
        setFilters: {
            first: function (elem, i) {
                return i === 0;
            },
            last: function (elem, i, match, array) {
                return i === array.length - 1;
            },
            even: function (elem, i) {
                return i % 2 === 0;
            },
            odd: function (elem, i) {
                return i % 2 === 1;
            },
            lt: function (elem, i, match) {
                return i < match[3] - 0;
            },
            gt: function (elem, i, match) {
                return i > match[3] - 0;
            },
            nth: function (elem, i, match) {
                return match[3] - 0 == i;
            },
            eq: function (elem, i, match) {
                return match[3] - 0 == i;
            }
        },
        filter: {
            PSEUDO: function (elem, match, i, array) {
                var name = match[1], filter = Expr.filters[name];

                if (filter) {
                    return filter(elem, i, match, array);
                } else if (name === "contains") {
                    return (elem.textContent || elem.innerText || "").indexOf(match[3]) >= 0;
                } else if (name === "not") {
                    var not = match[3];

                    for (var i = 0, l = not.length; i < l; i++) {
                        if (not[i] === elem) {
                            return false;
                        }
                    }

                    return true;
                }
            },
            CHILD: function (elem, match) {
                var type = match[1], node = elem;
                switch (type) {
                    case 'only':
                    case 'first':
                        while ((node = node.previousSibling)) {
                            if (node.nodeType === 1) return false;
                        }
                        if (type == 'first') return true;
                        node = elem;
                    case 'last':
                        while ((node = node.nextSibling)) {
                            if (node.nodeType === 1) return false;
                        }
                        return true;
                    case 'nth':
                        var first = match[2], last = match[3];

                        if (first == 1 && last == 0) {
                            return true;
                        }

                        var doneName = match[0],
                            parent = elem.parentNode;

                        if (parent && (parent.sizcache !== doneName || !elem.nodeIndex)) {
                            var count = 0;
                            for (node = parent.firstChild; node; node = node.nextSibling) {
                                if (node.nodeType === 1) {
                                    node.nodeIndex = ++count;
                                }
                            }
                            parent.sizcache = doneName;
                        }

                        var diff = elem.nodeIndex - last;
                        if (first == 0) {
                            return diff == 0;
                        } else {
                            return ( diff % first == 0 && diff / first >= 0 );
                        }
                }
            },
            ID: function (elem, match) {
                return elem.nodeType === 1 && elem.getAttribute("id") === match;
            },
            TAG: function (elem, match) {
                return (match === "*" && elem.nodeType === 1) || elem.nodeName === match;
            },
            CLASS: function (elem, match) {
                return (" " + (elem.className || elem.getAttribute("class")) + " ")
                        .indexOf(match) > -1;
            },
            ATTR: function (elem, match) {
                var name = match[1],
                    result = Expr.attrHandle[name] ?
                        Expr.attrHandle[name](elem) :
                        elem[name] != null ?
                            elem[name] :
                            elem.getAttribute(name),
                    value = result + "",
                    type = match[2],
                    check = match[4];

                return result == null ?
                    type === "!=" :
                    type === "=" ?
                        value === check :
                        type === "*=" ?
                            value.indexOf(check) >= 0 :
                            type === "~=" ?
                                (" " + value + " ").indexOf(check) >= 0 :
                                !check ?
                                    value && result !== false :
                                    type === "!=" ?
                                        value != check :
                                        type === "^=" ?
                                            value.indexOf(check) === 0 :
                                            type === "$=" ?
                                                value.substr(value.length - check.length) === check :
                                                type === "|=" ?
                                                    value === check || value.substr(0, check.length + 1) === check + "-" :
                                                    false;
            },
            POS: function (elem, match, i, array) {
                var name = match[2], filter = Expr.setFilters[name];

                if (filter) {
                    return filter(elem, i, match, array);
                }
            }
        }
    };

    var origPOS = Expr.match.POS;

    for (var type in Expr.match) {
        Expr.match[type] = new RegExp(Expr.match[type].source + /(?![^\[]*\])(?![^\(]*\))/.source);
        Expr.leftMatch[type] = new RegExp(/(^(?:.|\r|\n)*?)/.source + Expr.match[type].source);
    }

    var makeArray = function (array, results) {
        array = Array.prototype.slice.call(array, 0);

        if (results) {
            results.push.apply(results, array);
            return results;
        }

        return array;
    };

    try {
        Array.prototype.slice.call(document.documentElement.childNodes, 0);

    } catch (e) {
        makeArray = function (array, results) {
            var ret = results || [];

            if (toString.call(array) === "[object Array]") {
                Array.prototype.push.apply(ret, array);
            } else {
                if (typeof array.length === "number") {
                    for (var i = 0, l = array.length; i < l; i++) {
                        ret.push(array[i]);
                    }
                } else {
                    for (var i = 0; array[i]; i++) {
                        ret.push(array[i]);
                    }
                }
            }

            return ret;
        };
    }

    var sortOrder;

    if (document.documentElement.compareDocumentPosition) {
        sortOrder = function (a, b) {
            if (!a.compareDocumentPosition || !b.compareDocumentPosition) {
                if (a == b) {
                    hasDuplicate = true;
                }
                return 0;
            }

            var ret = a.compareDocumentPosition(b) & 4 ? -1 : a === b ? 0 : 1;
            if (ret === 0) {
                hasDuplicate = true;
            }
            return ret;
        };
    } else if ("sourceIndex" in document.documentElement) {
        sortOrder = function (a, b) {
            if (!a.sourceIndex || !b.sourceIndex) {
                if (a == b) {
                    hasDuplicate = true;
                }
                return 0;
            }

            var ret = a.sourceIndex - b.sourceIndex;
            if (ret === 0) {
                hasDuplicate = true;
            }
            return ret;
        };
    } else if (document.createRange) {
        sortOrder = function (a, b) {
            if (!a.ownerDocument || !b.ownerDocument) {
                if (a == b) {
                    hasDuplicate = true;
                }
                return 0;
            }

            var aRange = a.ownerDocument.createRange(), bRange = b.ownerDocument.createRange();
            aRange.setStart(a, 0);
            aRange.setEnd(a, 0);
            bRange.setStart(b, 0);
            bRange.setEnd(b, 0);
            var ret = aRange.compareBoundaryPoints(Range.START_TO_END, bRange);
            if (ret === 0) {
                hasDuplicate = true;
            }
            return ret;
        };
    }

    (function () {
        var form = document.createElement("div"),
            id = "script" + (new Date).getTime();
        form.innerHTML = "<a name='" + id + "'/>";

        var root = document.documentElement;
        root.insertBefore(form, root.firstChild);

        if (!!document.getElementById(id)) {
            Expr.find.ID = function (match, context, isXML) {
                if (typeof context.getElementById !== "undefined" && !isXML) {
                    var m = context.getElementById(match[1]);
                    return m ? m.id === match[1] || typeof m.getAttributeNode !== "undefined" && m.getAttributeNode("id").nodeValue === match[1] ? [m] : undefined : [];
                }
            };

            Expr.filter.ID = function (elem, match) {
                var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
                return elem.nodeType === 1 && node && node.nodeValue === match;
            };
        }

        root.removeChild(form);
        root = form = null; // release memory in IE
    })();

    (function () {

        var div = document.createElement("div");
        div.appendChild(document.createComment(""));

        if (div.getElementsByTagName("*").length > 0) {
            Expr.find.TAG = function (match, context) {
                var results = context.getElementsByTagName(match[1]);

                if (match[1] === "*") {
                    var tmp = [];

                    for (var i = 0; results[i]; i++) {
                        if (results[i].nodeType === 1) {
                            tmp.push(results[i]);
                        }
                    }

                    results = tmp;
                }

                return results;
            };
        }

        div.innerHTML = "<a href='#'></a>";
        if (div.firstChild && typeof div.firstChild.getAttribute !== "undefined" &&
            div.firstChild.getAttribute("href") !== "#") {
            Expr.attrHandle.href = function (elem) {
                return elem.getAttribute("href", 2);
            };
        }

        div = null; // release memory in IE
    })();

    if (document.querySelectorAll) (function () {
        var oldSizzle = Sizzle, div = document.createElement("div");
        div.innerHTML = "<p class='TEST'></p>";

        if (div.querySelectorAll && div.querySelectorAll(".TEST").length === 0) {
            return;
        }

        Sizzle = function (query, context, extra, seed) {
            context = context || document;

            if (!seed && context.nodeType === 9 && !isXML(context)) {
                try {
                    return makeArray(context.querySelectorAll(query), extra);
                } catch (e) {
                }
            }

            return oldSizzle(query, context, extra, seed);
        };

        for (var prop in oldSizzle) {
            Sizzle[prop] = oldSizzle[prop];
        }

        div = null; // release memory in IE
    })();

    if (document.getElementsByClassName && document.documentElement.getElementsByClassName) (function () {
        var div = document.createElement("div");
        div.innerHTML = "<div class='test e'></div><div class='test'></div>";

        if (div.getElementsByClassName("e").length === 0)
            return;

        div.lastChild.className = "e";

        if (div.getElementsByClassName("e").length === 1)
            return;

        Expr.order.splice(1, 0, "CLASS");
        Expr.find.CLASS = function (match, context, isXML) {
            if (typeof context.getElementsByClassName !== "undefined" && !isXML) {
                return context.getElementsByClassName(match[1]);
            }
        };

        div = null; // release memory in IE
    })();

    function dirNodeCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
        var sibDir = dir == "previousSibling" && !isXML;
        for (var i = 0, l = checkSet.length; i < l; i++) {
            var elem = checkSet[i];
            if (elem) {
                if (sibDir && elem.nodeType === 1) {
                    elem.sizcache = doneName;
                    elem.sizset = i;
                }
                elem = elem[dir];
                var match = false;

                while (elem) {
                    if (elem.sizcache === doneName) {
                        match = checkSet[elem.sizset];
                        break;
                    }

                    if (elem.nodeType === 1 && !isXML) {
                        elem.sizcache = doneName;
                        elem.sizset = i;
                    }

                    if (elem.nodeName === cur) {
                        match = elem;
                        break;
                    }

                    elem = elem[dir];
                }

                checkSet[i] = match;
            }
        }
    }

    function dirCheck(dir, cur, doneName, checkSet, nodeCheck, isXML) {
        var sibDir = dir == "previousSibling" && !isXML;
        for (var i = 0, l = checkSet.length; i < l; i++) {
            var elem = checkSet[i];
            if (elem) {
                if (sibDir && elem.nodeType === 1) {
                    elem.sizcache = doneName;
                    elem.sizset = i;
                }
                elem = elem[dir];
                var match = false;

                while (elem) {
                    if (elem.sizcache === doneName) {
                        match = checkSet[elem.sizset];
                        break;
                    }

                    if (elem.nodeType === 1) {
                        if (!isXML) {
                            elem.sizcache = doneName;
                            elem.sizset = i;
                        }
                        if (typeof cur !== "string") {
                            if (elem === cur) {
                                match = true;
                                break;
                            }

                        } else if (Sizzle.filter(cur, [elem]).length > 0) {
                            match = elem;
                            break;
                        }
                    }

                    elem = elem[dir];
                }

                checkSet[i] = match;
            }
        }
    }

    var contains = document.compareDocumentPosition ? function (a, b) {
            return a.compareDocumentPosition(b) & 16;
        } : function (a, b) {
            return a !== b && (a.contains ? a.contains(b) : true);
        };

    var isXML = function (elem) {
        return elem.nodeType === 9 && elem.documentElement.nodeName !== "HTML" ||
            !!elem.ownerDocument && elem.ownerDocument.documentElement.nodeName !== "HTML";
    };

    var posProcess = function (selector, context) {
        var tmpSet = [], later = "", match,
            root = context.nodeType ? [context] : context;

        while ((match = Expr.match.PSEUDO.exec(selector))) {
            later += match[0];
            selector = selector.replace(Expr.match.PSEUDO, "");
        }

        selector = Expr.relative[selector] ? selector + "*" : selector;

        for (var i = 0, l = root.length; i < l; i++) {
            Sizzle(selector, root[i], tmpSet);
        }

        return Sizzle.filter(later, tmpSet);
    };


    window.Sizzle = Sizzle;

})();

;(function (engine) {
    var extendElements = Prototype.Selector.extendElements;

    function select(selector, scope) {
        return extendElements(engine(selector, scope || document));
    }

    function match(element, selector) {
        return engine.matches(selector, [element]).length == 1;
    }

    Prototype.Selector.engine = engine;
    Prototype.Selector.select = select;
    Prototype.Selector.match = match;
})(Sizzle);

window.Sizzle = Prototype._original_property;
delete Prototype._original_property;

var Form = {
    reset: function (form) {
        form = $(form);
        form.reset();
        return form;
    },

    serializeElements: function (elements, options) {
        if (typeof options != 'object') options = {hash: !!options};
        else if (Object.isUndefined(options.hash)) options.hash = true;
        var key, value, submitted = false, submit = options.submit, accumulator, initial;

        if (options.hash) {
            initial = {};
            accumulator = function (result, key, value) {
                if (key in result) {
                    if (!Object.isArray(result[key])) result[key] = [result[key]];
                    result[key].push(value);
                } else result[key] = value;
                return result;
            };
        } else {
            initial = '';
            accumulator = function (result, key, value) {
                return result + (result ? '&' : '') + encodeURIComponent(key) + '=' + encodeURIComponent(value);
            }
        }

        return elements.inject(initial, function (result, element) {
            if (!element.disabled && element.name) {
                key = element.name;
                value = $(element).getValue();
                if (value != null && element.type != 'file' && (element.type != 'submit' || (!submitted &&
                    submit !== false && (!submit || key == submit) && (submitted = true)))) {
                    result = accumulator(result, key, value);
                }
            }
            return result;
        });
    }
};

Form.Methods = {
    serialize: function (form, options) {
        return Form.serializeElements(Form.getElements(form), options);
    },

    getElements: function (form) {
        var elements = $(form).getElementsByTagName('*'),
            element,
            arr = [],
            serializers = Form.Element.Serializers;
        for (var i = 0; element = elements[i]; i++) {
            arr.push(element);
        }
        return arr.inject([], function (elements, child) {
            if (serializers[child.tagName.toLowerCase()])
                elements.push(Element.extend(child));
            return elements;
        })
    },

    getInputs: function (form, typeName, name) {
        form = $(form);
        var inputs = form.getElementsByTagName('input');

        if (!typeName && !name) return $A(inputs).map(Element.extend);

        for (var i = 0, matchingInputs = [], length = inputs.length; i < length; i++) {
            var input = inputs[i];
            if ((typeName && input.type != typeName) || (name && input.name != name))
                continue;
            matchingInputs.push(Element.extend(input));
        }

        return matchingInputs;
    },

    disable: function (form) {
        form = $(form);
        Form.getElements(form).invoke('disable');
        return form;
    },

    enable: function (form) {
        form = $(form);
        Form.getElements(form).invoke('enable');
        return form;
    },

    findFirstElement: function (form) {
        var elements = $(form).getElements().findAll(function (element) {
            return 'hidden' != element.type && !element.disabled;
        });
        var firstByIndex = elements.findAll(function (element) {
            return element.hasAttribute('tabIndex') && element.tabIndex >= 0;
        }).sortBy(function (element) {
            return element.tabIndex
        }).first();

        return firstByIndex ? firstByIndex : elements.find(function (element) {
                return /^(?:input|select|textarea)$/i.test(element.tagName);
            });
    },

    focusFirstElement: function (form) {
        form = $(form);
        var element = form.findFirstElement();
        if (element) element.activate();
        return form;
    },

    request: function (form, options) {
        form = $(form), options = Object.clone(options || {});

        var params = options.parameters, action = form.readAttribute('action') || '';
        if (action.blank()) action = window.location.href;
        options.parameters = form.serialize(true);

        if (params) {
            if (Object.isString(params)) params = params.toQueryParams();
            Object.extend(options.parameters, params);
        }

        if (form.hasAttribute('method') && !options.method)
            options.method = form.method;

        return new Ajax.Request(action, options);
    }
};

/*--------------------------------------------------------------------------*/


Form.Element = {
    focus: function (element) {
        $(element).focus();
        return element;
    },

    select: function (element) {
        $(element).select();
        return element;
    }
};

Form.Element.Methods = {

    serialize: function (element) {
        element = $(element);
        if (!element.disabled && element.name) {
            var value = element.getValue();
            if (value != undefined) {
                var pair = {};
                pair[element.name] = value;
                return Object.toQueryString(pair);
            }
        }
        return '';
    },

    getValue: function (element) {
        element = $(element);
        var method = element.tagName.toLowerCase();
        return Form.Element.Serializers[method](element);
    },

    setValue: function (element, value) {
        element = $(element);
        var method = element.tagName.toLowerCase();
        Form.Element.Serializers[method](element, value);
        return element;
    },

    clear: function (element) {
        $(element).value = '';
        return element;
    },

    present: function (element) {
        return $(element).value != '';
    },

    activate: function (element) {
        element = $(element);
        try {
            element.focus();
            if (element.select && (element.tagName.toLowerCase() != 'input' || !(/^(?:button|reset|submit)$/i.test(element.type))))
                element.select();
        } catch (e) {
        }
        return element;
    },

    disable: function (element) {
        element = $(element);
        element.disabled = true;
        return element;
    },

    enable: function (element) {
        element = $(element);
        element.disabled = false;
        return element;
    }
};

/*--------------------------------------------------------------------------*/

var Field = Form.Element;

var $F = Form.Element.Methods.getValue;

/*--------------------------------------------------------------------------*/

Form.Element.Serializers = (function () {
    function input(element, value) {
        switch (element.type.toLowerCase()) {
            case 'checkbox':
            case 'radio':
                return inputSelector(element, value);
            default:
                return valueSelector(element, value);
        }
    }

    function inputSelector(element, value) {
        if (Object.isUndefined(value))
            return element.checked ? element.value : null;
        else element.checked = !!value;
    }

    function valueSelector(element, value) {
        if (Object.isUndefined(value)) return element.value;
        else element.value = value;
    }

    function select(element, value) {
        if (Object.isUndefined(value))
            return (element.type === 'select-one' ? selectOne : selectMany)(element);

        var opt, currentValue, single = !Object.isArray(value);
        for (var i = 0, length = element.length; i < length; i++) {
            opt = element.options[i];
            currentValue = this.optionValue(opt);
            if (single) {
                if (currentValue == value) {
                    opt.selected = true;
                    return;
                }
            }
            else opt.selected = value.include(currentValue);
        }
    }

    function selectOne(element) {
        var index = element.selectedIndex;
        return index >= 0 ? optionValue(element.options[index]) : null;
    }

    function selectMany(element) {
        var values, length = element.length;
        if (!length) return null;

        for (var i = 0, values = []; i < length; i++) {
            var opt = element.options[i];
            if (opt.selected) values.push(optionValue(opt));
        }
        return values;
    }

    function optionValue(opt) {
        return Element.hasAttribute(opt, 'value') ? opt.value : opt.text;
    }

    return {
        input: input,
        inputSelector: inputSelector,
        textarea: valueSelector,
        select: select,
        selectOne: selectOne,
        selectMany: selectMany,
        optionValue: optionValue,
        button: valueSelector
    };
})();

/*--------------------------------------------------------------------------*/


Abstract.TimedObserver = Class.create(PeriodicalExecuter, {
    initialize: function ($super, element, frequency, callback) {
        $super(callback, frequency);
        this.element = $(element);
        this.lastValue = this.getValue();
    },

    execute: function () {
        var value = this.getValue();
        if (Object.isString(this.lastValue) && Object.isString(value) ?
                this.lastValue != value : String(this.lastValue) != String(value)) {
            this.callback(this.element, value);
            this.lastValue = value;
        }
    }
});

Form.Element.Observer = Class.create(Abstract.TimedObserver, {
    getValue: function () {
        return Form.Element.getValue(this.element);
    }
});

Form.Observer = Class.create(Abstract.TimedObserver, {
    getValue: function () {
        return Form.serialize(this.element);
    }
});

/*--------------------------------------------------------------------------*/

Abstract.EventObserver = Class.create({
    initialize: function (element, callback) {
        this.element = $(element);
        this.callback = callback;

        this.lastValue = this.getValue();
        if (this.element.tagName.toLowerCase() == 'form')
            this.registerFormCallbacks();
        else
            this.registerCallback(this.element);
    },

    onElementEvent: function () {
        var value = this.getValue();
        if (this.lastValue != value) {
            this.callback(this.element, value);
            this.lastValue = value;
        }
    },

    registerFormCallbacks: function () {
        Form.getElements(this.element).each(this.registerCallback, this);
    },

    registerCallback: function (element) {
        if (element.type) {
            switch (element.type.toLowerCase()) {
                case 'checkbox':
                case 'radio':
                    Event.observe(element, 'click', this.onElementEvent.bind(this));
                    break;
                default:
                    Event.observe(element, 'change', this.onElementEvent.bind(this));
                    break;
            }
        }
    }
});

Form.Element.EventObserver = Class.create(Abstract.EventObserver, {
    getValue: function () {
        return Form.Element.getValue(this.element);
    }
});

Form.EventObserver = Class.create(Abstract.EventObserver, {
    getValue: function () {
        return Form.serialize(this.element);
    }
});
(function () {

    var Event = {
        KEY_BACKSPACE: 8,
        KEY_TAB: 9,
        KEY_RETURN: 13,
        KEY_ESC: 27,
        KEY_LEFT: 37,
        KEY_UP: 38,
        KEY_RIGHT: 39,
        KEY_DOWN: 40,
        KEY_DELETE: 46,
        KEY_HOME: 36,
        KEY_END: 35,
        KEY_PAGEUP: 33,
        KEY_PAGEDOWN: 34,
        KEY_INSERT: 45,

        cache: {}
    };

    var docEl = document.documentElement;
    var MOUSEENTER_MOUSELEAVE_EVENTS_SUPPORTED = 'onmouseenter' in docEl
        && 'onmouseleave' in docEl;


    var isIELegacyEvent = function (event) {
        return false;
    };

    if (window.attachEvent) {
        if (window.addEventListener) {
            isIELegacyEvent = function (event) {
                return !(event instanceof window.Event);
            };
        } else {
            isIELegacyEvent = function (event) {
                return true;
            };
        }
    }

    var _isButton;

    function _isButtonForDOMEvents(event, code) {
        return event.which ? (event.which === code + 1) : (event.button === code);
    }

    var legacyButtonMap = {0: 1, 1: 4, 2: 2};

    function _isButtonForLegacyEvents(event, code) {
        return event.button === legacyButtonMap[code];
    }

    function _isButtonForWebKit(event, code) {
        switch (code) {
            case 0:
                return event.which == 1 && !event.metaKey;
            case 1:
                return event.which == 2 || (event.which == 1 && event.metaKey);
            case 2:
                return event.which == 3;
            default:
                return false;
        }
    }

    if (window.attachEvent) {
        if (!window.addEventListener) {
            _isButton = _isButtonForLegacyEvents;
        } else {
            _isButton = function (event, code) {
                return isIELegacyEvent(event) ? _isButtonForLegacyEvents(event, code) :
                    _isButtonForDOMEvents(event, code);
            }
        }
    } else if (Prototype.Browser.WebKit) {
        _isButton = _isButtonForWebKit;
    } else {
        _isButton = _isButtonForDOMEvents;
    }

    function isLeftClick(event) {
        return _isButton(event, 0)
    }

    function isMiddleClick(event) {
        return _isButton(event, 1)
    }

    function isRightClick(event) {
        return _isButton(event, 2)
    }

    function element(event) {
        event = Event.extend(event);

        var node = event.target, type = event.type,
            currentTarget = event.currentTarget;

        if (currentTarget && currentTarget.tagName) {
            if (type === 'load' || type === 'error' ||
                (type === 'click' && currentTarget.tagName.toLowerCase() === 'input'
                && currentTarget.type === 'radio'))
                node = currentTarget;
        }

        if (node.nodeType == Node.TEXT_NODE)
            node = node.parentNode;

        return Element.extend(node);
    }

    function findElement(event, expression) {
        var element = Event.element(event);

        if (!expression) return element;
        while (element) {
            if (Object.isElement(element) && Prototype.Selector.match(element, expression)) {
                return Element.extend(element);
            }
            element = element.parentNode;
        }
    }

    function pointer(event) {
        return {x: pointerX(event), y: pointerY(event)};
    }

    function pointerX(event) {
        var docElement = document.documentElement,
            body = document.body || {scrollLeft: 0};

        return event.pageX || (event.clientX +
            (docElement.scrollLeft || body.scrollLeft) -
            (docElement.clientLeft || 0));
    }

    function pointerY(event) {
        var docElement = document.documentElement,
            body = document.body || {scrollTop: 0};

        return event.pageY || (event.clientY +
            (docElement.scrollTop || body.scrollTop) -
            (docElement.clientTop || 0));
    }


    function stop(event) {
        Event.extend(event);
        event.preventDefault();
        event.stopPropagation();

        event.stopped = true;
    }


    Event.Methods = {
        isLeftClick: isLeftClick,
        isMiddleClick: isMiddleClick,
        isRightClick: isRightClick,

        element: element,
        findElement: findElement,

        pointer: pointer,
        pointerX: pointerX,
        pointerY: pointerY,

        stop: stop
    };

    var methods = Object.keys(Event.Methods).inject({}, function (m, name) {
        m[name] = Event.Methods[name].methodize();
        return m;
    });

    if (window.attachEvent) {
        function _relatedTarget(event) {
            var element;
            switch (event.type) {
                case 'mouseover':
                case 'mouseenter':
                    element = event.fromElement;
                    break;
                case 'mouseout':
                case 'mouseleave':
                    element = event.toElement;
                    break;
                default:
                    return null;
            }
            return Element.extend(element);
        }

        var additionalMethods = {
            stopPropagation: function () {
                this.cancelBubble = true
            },
            preventDefault: function () {
                this.returnValue = false
            },
            inspect: function () {
                return '[object Event]'
            }
        };

        Event.extend = function (event, element) {
            if (!event) return false;

            if (!isIELegacyEvent(event)) return event;

            if (event._extendedByPrototype) return event;
            event._extendedByPrototype = Prototype.emptyFunction;

            var pointer = Event.pointer(event);

            Object.extend(event, {
                target: event.srcElement || element,
                relatedTarget: _relatedTarget(event),
                pageX: pointer.x,
                pageY: pointer.y
            });

            Object.extend(event, methods);
            Object.extend(event, additionalMethods);

            return event;
        };
    } else {
        Event.extend = Prototype.K;
    }

    if (window.addEventListener) {
        Event.prototype = window.Event.prototype || document.createEvent('HTMLEvents').__proto__;
        Object.extend(Event.prototype, methods);
    }

    function _createResponder(element, eventName, handler) {
        var registry = Element.retrieve(element, 'prototype_event_registry');

        if (Object.isUndefined(registry)) {
            CACHE.push(element);
            registry = Element.retrieve(element, 'prototype_event_registry', $H());
        }

        var respondersForEvent = registry.get(eventName);
        if (Object.isUndefined(respondersForEvent)) {
            respondersForEvent = [];
            registry.set(eventName, respondersForEvent);
        }

        if (respondersForEvent.pluck('handler').include(handler)) return false;

        var responder;
        if (eventName.include(":")) {
            responder = function (event) {
                if (Object.isUndefined(event.eventName))
                    return false;

                if (event.eventName !== eventName)
                    return false;

                Event.extend(event, element);
                handler.call(element, event);
            };
        } else {
            if (!MOUSEENTER_MOUSELEAVE_EVENTS_SUPPORTED &&
                (eventName === "mouseenter" || eventName === "mouseleave")) {
                if (eventName === "mouseenter" || eventName === "mouseleave") {
                    responder = function (event) {
                        Event.extend(event, element);

                        var parent = event.relatedTarget;
                        while (parent && parent !== element) {
                            try {
                                parent = parent.parentNode;
                            }
                            catch (e) {
                                parent = element;
                            }
                        }

                        if (parent === element) return;

                        handler.call(element, event);
                    };
                }
            } else {
                responder = function (event) {
                    Event.extend(event, element);
                    handler.call(element, event);
                };
            }
        }

        responder.handler = handler;
        respondersForEvent.push(responder);
        return responder;
    }

    function _destroyCache() {
        for (var i = 0, length = CACHE.length; i < length; i++) {
            Event.stopObserving(CACHE[i]);
            CACHE[i] = null;
        }
    }

    var CACHE = [];

    if (Prototype.Browser.IE)
        window.attachEvent('onunload', _destroyCache);

    if (Prototype.Browser.WebKit)
        window.addEventListener('unload', Prototype.emptyFunction, false);


    var _getDOMEventName = Prototype.K,
        translations = {mouseenter: "mouseover", mouseleave: "mouseout"};

    if (!MOUSEENTER_MOUSELEAVE_EVENTS_SUPPORTED) {
        _getDOMEventName = function (eventName) {
            return (translations[eventName] || eventName);
        };
    }

    function observe(element, eventName, handler) {
        element = $(element);

        var responder = _createResponder(element, eventName, handler);

        if (!responder) return element;

        if (eventName.include(':')) {
            if (element.addEventListener)
                element.addEventListener("dataavailable", responder, false);
            else {
                element.attachEvent("ondataavailable", responder);
                element.attachEvent("onlosecapture", responder);
            }
        } else {
            var actualEventName = _getDOMEventName(eventName);

            if (element.addEventListener)
                element.addEventListener(actualEventName, responder, false);
            else
                element.attachEvent("on" + actualEventName, responder);
        }

        return element;
    }

    function stopObserving(element, eventName, handler) {
        element = $(element);

        var registry = Element.retrieve(element, 'prototype_event_registry');
        if (!registry) return element;

        if (!eventName) {
            registry.each(function (pair) {
                var eventName = pair.key;
                stopObserving(element, eventName);
            });
            return element;
        }

        var responders = registry.get(eventName);
        if (!responders) return element;

        if (!handler) {
            responders.each(function (r) {
                stopObserving(element, eventName, r.handler);
            });
            return element;
        }

        var i = responders.length, responder;
        while (i--) {
            if (responders[i].handler === handler) {
                responder = responders[i];
                break;
            }
        }
        if (!responder) return element;

        if (eventName.include(':')) {
            if (element.removeEventListener)
                element.removeEventListener("dataavailable", responder, false);
            else {
                element.detachEvent("ondataavailable", responder);
                element.detachEvent("onlosecapture", responder);
            }
        } else {
            var actualEventName = _getDOMEventName(eventName);
            if (element.removeEventListener)
                element.removeEventListener(actualEventName, responder, false);
            else
                element.detachEvent('on' + actualEventName, responder);
        }

        registry.set(eventName, responders.without(responder));

        return element;
    }

    function fire(element, eventName, memo, bubble) {
        element = $(element);

        if (Object.isUndefined(bubble))
            bubble = true;

        if (element == document && document.createEvent && !element.dispatchEvent)
            element = document.documentElement;

        var event;
        if (document.createEvent) {
            event = document.createEvent('HTMLEvents');
            event.initEvent('dataavailable', bubble, true);
        } else {
            event = document.createEventObject();
            event.eventType = bubble ? 'ondataavailable' : 'onlosecapture';
        }

        event.eventName = eventName;
        event.memo = memo || {};

        if (document.createEvent)
            element.dispatchEvent(event);
        else
            element.fireEvent(event.eventType, event);

        return Event.extend(event);
    }

    Event.Handler = Class.create({
        initialize: function (element, eventName, selector, callback) {
            this.element = $(element);
            this.eventName = eventName;
            this.selector = selector;
            this.callback = callback;
            this.handler = this.handleEvent.bind(this);
        },

        start: function () {
            Event.observe(this.element, this.eventName, this.handler);
            return this;
        },

        stop: function () {
            Event.stopObserving(this.element, this.eventName, this.handler);
            return this;
        },

        handleEvent: function (event) {
            var element = Event.findElement(event, this.selector);
            if (element) this.callback.call(this.element, event, element);
        }
    });

    function on(element, eventName, selector, callback) {
        element = $(element);
        if (Object.isFunction(selector) && Object.isUndefined(callback)) {
            callback = selector, selector = null;
        }

        return new Event.Handler(element, eventName, selector, callback).start();
    }

    Object.extend(Event, Event.Methods);

    Object.extend(Event, {
        fire: fire,
        observe: observe,
        stopObserving: stopObserving,
        on: on
    });

    Element.addMethods({
        fire: fire,

        observe: observe,

        stopObserving: stopObserving,

        on: on
    });

    Object.extend(document, {
        fire: fire.methodize(),

        observe: observe.methodize(),

        stopObserving: stopObserving.methodize(),

        on: on.methodize(),

        loaded: false
    });

    if (window.Event) Object.extend(window.Event, Event);
    else window.Event = Event;
})();

(function () {
    /* Support for the DOMContentLoaded event is based on work by Dan Webb,
     Matthias Miller, Dean Edwards, John Resig, and Diego Perini. */

    var timer;

    function fireContentLoadedEvent() {
        if (document.loaded) return;
        if (timer) window.clearTimeout(timer);
        document.loaded = true;
        document.fire('dom:loaded');
    }

    function checkReadyState() {
        if (document.readyState === 'complete') {
            document.stopObserving('readystatechange', checkReadyState);
            fireContentLoadedEvent();
        }
    }

    function pollDoScroll() {
        try {
            document.documentElement.doScroll('left');
        }
        catch (e) {
            timer = pollDoScroll.defer();
            return;
        }
        fireContentLoadedEvent();
    }

    if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', fireContentLoadedEvent, false);
    } else {
        document.observe('readystatechange', checkReadyState);
        if (window == top)
            timer = pollDoScroll.defer();
    }

    Event.observe(window, 'load', fireContentLoadedEvent);
})();

Element.addMethods();

/*------------------------------- DEPRECATED -------------------------------*/

Hash.toQueryString = Object.toQueryString;

var Toggle = {display: Element.toggle};

Element.Methods.childOf = Element.Methods.descendantOf;

var Insertion = {
    Before: function (element, content) {
        return Element.insert(element, {before: content});
    },

    Top: function (element, content) {
        return Element.insert(element, {top: content});
    },

    Bottom: function (element, content) {
        return Element.insert(element, {bottom: content});
    },

    After: function (element, content) {
        return Element.insert(element, {after: content});
    }
};

var $continue = new Error('"throw $continue" is deprecated, use "return" instead');

var Position = {
    includeScrollOffsets: false,

    prepare: function () {
        this.deltaX = window.pageXOffset
            || document.documentElement.scrollLeft
            || document.body.scrollLeft
            || 0;
        this.deltaY = window.pageYOffset
            || document.documentElement.scrollTop
            || document.body.scrollTop
            || 0;
    },

    within: function (element, x, y) {
        if (this.includeScrollOffsets)
            return this.withinIncludingScrolloffsets(element, x, y);
        this.xcomp = x;
        this.ycomp = y;
        this.offset = Element.cumulativeOffset(element);

        return (y >= this.offset[1] &&
        y < this.offset[1] + element.offsetHeight &&
        x >= this.offset[0] &&
        x < this.offset[0] + element.offsetWidth);
    },

    withinIncludingScrolloffsets: function (element, x, y) {
        var offsetcache = Element.cumulativeScrollOffset(element);

        this.xcomp = x + offsetcache[0] - this.deltaX;
        this.ycomp = y + offsetcache[1] - this.deltaY;
        this.offset = Element.cumulativeOffset(element);

        return (this.ycomp >= this.offset[1] &&
        this.ycomp < this.offset[1] + element.offsetHeight &&
        this.xcomp >= this.offset[0] &&
        this.xcomp < this.offset[0] + element.offsetWidth);
    },

    overlap: function (mode, element) {
        if (!mode) return 0;
        if (mode == 'vertical')
            return ((this.offset[1] + element.offsetHeight) - this.ycomp) /
                element.offsetHeight;
        if (mode == 'horizontal')
            return ((this.offset[0] + element.offsetWidth) - this.xcomp) /
                element.offsetWidth;
    },


    cumulativeOffset: Element.Methods.cumulativeOffset,

    positionedOffset: Element.Methods.positionedOffset,

    absolutize: function (element) {
        Position.prepare();
        return Element.absolutize(element);
    },

    relativize: function (element) {
        Position.prepare();
        return Element.relativize(element);
    },

    realOffset: Element.Methods.cumulativeScrollOffset,

    offsetParent: Element.Methods.getOffsetParent,

    page: Element.Methods.viewportOffset,

    clone: function (source, target, options) {
        options = options || {};
        return Element.clonePosition(target, source, options);
    }
};

/*--------------------------------------------------------------------------*/

if (!document.getElementsByClassName) document.getElementsByClassName = function (instanceMethods) {
    function iter(name) {
        return name.blank() ? null : "[contains(concat(' ', @class, ' '), ' " + name + " ')]";
    }

    instanceMethods.getElementsByClassName = Prototype.BrowserFeatures.XPath ?
        function (element, className) {
            className = className.toString().strip();
            var cond = /\s/.test(className) ? $w(className).map(iter).join('') : iter(className);
            return cond ? document._getElementsByXPath('.//*' + cond, element) : [];
        } : function (element, className) {
            className = className.toString().strip();
            var elements = [], classNames = (/\s/.test(className) ? $w(className) : null);
            if (!classNames && !className) return elements;

            var nodes = $(element).getElementsByTagName('*');
            className = ' ' + className + ' ';

            for (var i = 0, child, cn; child = nodes[i]; i++) {
                if (child.className && (cn = ' ' + child.className + ' ') && (cn.include(className) ||
                    (classNames && classNames.all(function (name) {
                        return !name.toString().blank() && cn.include(' ' + name + ' ');
                    }))))
                    elements.push(Element.extend(child));
            }
            return elements;
        };

    return function (className, parentElement) {
        return $(parentElement || document.body).getElementsByClassName(className);
    };
}(Element.Methods);

/*--------------------------------------------------------------------------*/

Element.ClassNames = Class.create();
Element.ClassNames.prototype = {
    initialize: function (element) {
        this.element = $(element);
    },

    _each: function (iterator) {
        this.element.className.split(/\s+/).select(function (name) {
            return name.length > 0;
        })._each(iterator);
    },

    set: function (className) {
        this.element.className = className;
    },

    add: function (classNameToAdd) {
        if (this.include(classNameToAdd)) return;
        this.set($A(this).concat(classNameToAdd).join(' '));
    },

    remove: function (classNameToRemove) {
        if (!this.include(classNameToRemove)) return;
        this.set($A(this).without(classNameToRemove).join(' '));
    },

    toString: function () {
        return $A(this).join(' ');
    }
};

Object.extend(Element.ClassNames.prototype, Enumerable);

/*--------------------------------------------------------------------------*/

(function () {
    window.Selector = Class.create({
        initialize: function (expression) {
            this.expression = expression.strip();
        },

        findElements: function (rootElement) {
            return Prototype.Selector.select(this.expression, rootElement);
        },

        match: function (element) {
            return Prototype.Selector.match(element, this.expression);
        },

        toString: function () {
            return this.expression;
        },

        inspect: function () {
            return "#<Selector: " + this.expression + ">";
        }
    });

    Object.extend(Selector, {
        matchElements: function (elements, expression) {
            var match = Prototype.Selector.match,
                results = [];

            for (var i = 0, length = elements.length; i < length; i++) {
                var element = elements[i];
                if (match(element, expression)) {
                    results.push(Element.extend(element));
                }
            }
            return results;
        },

        findElement: function (elements, expression, index) {
            index = index || 0;
            var matchIndex = 0, element;
            for (var i = 0, length = elements.length; i < length; i++) {
                element = elements[i];
                if (Prototype.Selector.match(element, expression) && index === matchIndex++) {
                    return Element.extend(element);
                }
            }
        },

        findChildElements: function (element, expressions) {
            var selector = expressions.toArray().join(', ');
            return Prototype.Selector.select(selector, element || document);
        }
    });
})();

/**
 * Prototype-Effects
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
var Effect = {
    _elementDoesNotExistError: {
        name: 'ElementDoesNotExistError',
        message: 'The specified DOM element does not exist, but is required for this effect to operate'
    },
    Transitions: {
        linear: Prototype.K,
        sinoidal: function (pos) {
            return (-Math.cos(pos * Math.PI) / 2) + .5;
        },
        reverse: function (pos) {
            return 1 - pos;
        },
        flicker: function (pos) {
            var pos = ((-Math.cos(pos * Math.PI) / 4) + .75) + Math.random() / 4;
            return pos > 1 ? 1 : pos;
        },
        wobble: function (pos) {
            return (-Math.cos(pos * Math.PI * (9 * pos)) / 2) + .5;
        },
        pulse: function (pos, pulses) {
            return (-Math.cos((pos * ((pulses || 5) - .5) * 2) * Math.PI) / 2) + .5;
        },
        spring: function (pos) {
            return 1 - (Math.cos(pos * 4.5 * Math.PI) * Math.exp(-pos * 6));
        },
        none: function (pos) {
            return 0;
        },
        full: function (pos) {
            return 1;
        }
    },
    DefaultOptions: {
        duration: 1.0,   // seconds
        fps: 100,   // 100= assume 66fps max.
        sync: false, // true for combining
        from: 0.0,
        to: 1.0,
        delay: 0.0,
        queue: 'parallel'
    },
    multiple: function (element, effect) {
        var elements;
        if (((typeof element == 'object') ||
            Object.isFunction(element)) &&
            (element.length))
            elements = element;
        else
            elements = $(element).childNodes;

        var options = Object.extend({
            speed: 0.1,
            delay: 0.0
        }, arguments[2] || {});
        var masterDelay = options.delay;

        $A(elements).each(function (element, index) {
            new effect(element, Object.extend(options, {delay: index * options.speed + masterDelay}));
        });
    },
    PAIRS: {
        'slide': ['SlideDown', 'SlideUp'],
        'blind': ['BlindDown', 'BlindUp'],
        'appear': ['Appear', 'Fade']
    },
    toggle: function (element, effect, options) {
        element = $(element);
        effect = (effect || 'appear').toLowerCase();

        return Effect[Effect.PAIRS[effect][element.visible() ? 1 : 0]](element, Object.extend({
            queue: {position: 'end', scope: (element.id || 'global'), limit: 1}
        }, options || {}));
    }
};

Effect.DefaultOptions.transition = Effect.Transitions.sinoidal;

Effect.ScopedQueue = Class.create(Enumerable, {
    initialize: function () {
        this.effects = [];
        this.interval = null;
    },
    _each: function (iterator) {
        this.effects._each(iterator);
    },
    add: function (effect) {
        var timestamp = new Date().getTime();

        var position = Object.isString(effect.options.queue) ?
            effect.options.queue : effect.options.queue.position;

        switch (position) {
            case 'front':
                // move unstarted effects after this effect
                this.effects.findAll(function (e) {
                    return e.state == 'idle'
                }).each(function (e) {
                    e.startOn += effect.finishOn;
                    e.finishOn += effect.finishOn;
                });
                break;
            case 'with-last':
                timestamp = this.effects.pluck('startOn').max() || timestamp;
                break;
            case 'end':
                // start effect after last queued effect has finished
                timestamp = this.effects.pluck('finishOn').max() || timestamp;
                break;
        }

        effect.startOn += timestamp;
        effect.finishOn += timestamp;

        if (!effect.options.queue.limit || (this.effects.length < effect.options.queue.limit))
            this.effects.push(effect);

        if (!this.interval)
            this.interval = setInterval(this.loop.bind(this), 15);
    },
    remove: function (effect) {
        this.effects = this.effects.reject(function (e) {
            return e == effect
        });
        if (this.effects.length == 0) {
            clearInterval(this.interval);
            this.interval = null;
        }
    },
    loop: function () {
        var timePos = new Date().getTime();
        for (var i = 0, len = this.effects.length; i < len; i++)
            this.effects[i] && this.effects[i].loop(timePos);
    }
});

Effect.Queues = {
    instances: $H(),
    get: function (queueName) {
        if (!Object.isString(queueName)) return queueName;

        return this.instances.get(queueName) ||
            this.instances.set(queueName, new Effect.ScopedQueue());
    }
};
Effect.Queue = Effect.Queues.get('global');

Effect.Base = Class.create({
    position: null,
    start: function (options) {
        if (options && options.transition === false) options.transition = Effect.Transitions.linear;
        this.options = Object.extend(Object.extend({}, Effect.DefaultOptions), options || {});
        this.currentFrame = 0;
        this.state = 'idle';
        this.startOn = this.options.delay * 1000;
        this.finishOn = this.startOn + (this.options.duration * 1000);
        this.fromToDelta = this.options.to - this.options.from;
        this.totalTime = this.finishOn - this.startOn;
        this.totalFrames = this.options.fps * this.options.duration;

        this.render = (function () {
            function dispatch(effect, eventName) {
                if (effect.options[eventName + 'Internal'])
                    effect.options[eventName + 'Internal'](effect);
                if (effect.options[eventName])
                    effect.options[eventName](effect);
            }

            return function (pos) {
                if (this.state === "idle") {
                    this.state = "running";
                    dispatch(this, 'beforeSetup');
                    if (this.setup) this.setup();
                    dispatch(this, 'afterSetup');
                }
                if (this.state === "running") {
                    pos = (this.options.transition(pos) * this.fromToDelta) + this.options.from;
                    this.position = pos;
                    dispatch(this, 'beforeUpdate');
                    if (this.update) this.update(pos);
                    dispatch(this, 'afterUpdate');
                }
            };
        })();

        this.event('beforeStart');
        if (!this.options.sync)
            Effect.Queues.get(Object.isString(this.options.queue) ?
                'global' : this.options.queue.scope).add(this);
    },
    loop: function (timePos) {
        if (timePos >= this.startOn) {
            if (timePos >= this.finishOn) {
                this.render(1.0);
                this.cancel();
                this.event('beforeFinish');
                if (this.finish) this.finish();
                this.event('afterFinish');
                return;
            }
            var pos = (timePos - this.startOn) / this.totalTime,
                frame = (pos * this.totalFrames).round();
            if (frame > this.currentFrame) {
                this.render(pos);
                this.currentFrame = frame;
            }
        }
    },
    cancel: function () {
        if (!this.options.sync)
            Effect.Queues.get(Object.isString(this.options.queue) ?
                'global' : this.options.queue.scope).remove(this);
        this.state = 'finished';
    },
    event: function (eventName) {
        if (this.options[eventName + 'Internal']) this.options[eventName + 'Internal'](this);
        if (this.options[eventName]) this.options[eventName](this);
    },
    inspect: function () {
        var data = $H();
        for (property in this)
            if (!Object.isFunction(this[property])) data.set(property, this[property]);
        return '#<Effect:' + data.inspect() + ',options:' + $H(this.options).inspect() + '>';
    }
});


Effect.Tween = Class.create(Effect.Base, {
    initialize: function (object, from, to) {
        object = Object.isString(object) ? $(object) : object;
        var args = $A(arguments), method = args.last(),
            options = args.length == 5 ? args[3] : null;
        this.method = Object.isFunction(method) ? method.bind(object) :
            Object.isFunction(object[method]) ? object[method].bind(object) :
                function (value) {
                    object[method] = value
                };
        this.start(Object.extend({from: from, to: to}, options || {}));
    },
    update: function (position) {
        this.method(position);
    }
});

Effect.Event = Class.create(Effect.Base, {
    initialize: function () {
        this.start(Object.extend({duration: 0}, arguments[0] || {}));
    },
    update: Prototype.emptyFunction
});

Effect.ScrollTo = function (element) {
    var options = arguments[1] || {},
        scrollOffsets = document.viewport.getScrollOffsets(),
        elementOffsets = $(element).cumulativeOffset();

    if (options.offset) elementOffsets[1] += options.offset;

    return new Effect.Tween(null,
        scrollOffsets.top,
        elementOffsets[1],
        options,
        function (p) {
            scrollTo(scrollOffsets.left, p.round());
        }
    );
};

/*!
 Chosen, a Select Box Enhancer for jQuery and Prototype
 by Patrick Filler for Harvest, http://getharvest.com

 Version 1.2.0
 Full source at https://github.com/harvesthq/chosen
 Copyright (c) 2011-2014 Harvest http://getharvest.com

 MIT License, https://github.com/harvesthq/chosen/blob/master/LICENSE.md
 This file is generated by `grunt build`, do not edit it by hand.
 */

(function () {
    var AbstractChosen, SelectParser, _ref,
        __hasProp = {}.hasOwnProperty,
        __extends = function (child, parent) {
            for (var key in parent) {
                if (__hasProp.call(parent, key)) child[key] = parent[key];
            }
            function ctor() {
                this.constructor = child;
            }

            ctor.prototype = parent.prototype;
            child.prototype = new ctor();
            child.__super__ = parent.prototype;
            return child;
        };

    SelectParser = (function () {
        function SelectParser() {
            this.options_index = 0;
            this.parsed = [];
        }

        SelectParser.prototype.add_node = function (child) {
            if (child.nodeName.toUpperCase() === "OPTGROUP") {
                return this.add_group(child);
            } else {
                return this.add_option(child);
            }
        };

        SelectParser.prototype.add_group = function (group) {
            var group_position, option, _i, _len, _ref, _results;
            group_position = this.parsed.length;
            this.parsed.push({
                array_index: group_position,
                group: true,
                label: this.escapeExpression(group.label),
                children: 0,
                disabled: group.disabled
            });
            _ref = group.childNodes;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                option = _ref[_i];
                _results.push(this.add_option(option, group_position, group.disabled));
            }
            return _results;
        };

        SelectParser.prototype.add_option = function (option, group_position, group_disabled) {
            if (option.nodeName.toUpperCase() === "OPTION") {
                if (option.text !== "") {
                    if (group_position != null) {
                        this.parsed[group_position].children += 1;
                    }
                    this.parsed.push({
                        array_index: this.parsed.length,
                        options_index: this.options_index,
                        value: option.value,
                        text: option.text,
                        html: option.innerHTML,
                        selected: option.selected,
                        disabled: group_disabled === true ? group_disabled : option.disabled,
                        group_array_index: group_position,
                        classes: option.className,
                        style: option.style.cssText
                    });
                } else {
                    this.parsed.push({
                        array_index: this.parsed.length,
                        options_index: this.options_index,
                        empty: true
                    });
                }
                return this.options_index += 1;
            }
        };

        SelectParser.prototype.escapeExpression = function (text) {
            var map, unsafe_chars;
            if ((text == null) || text === false) {
                return "";
            }
            if (!/[\&\<\>\"\'\`]/.test(text)) {
                return text;
            }
            map = {
                "<": "&lt;",
                ">": "&gt;",
                '"': "&quot;",
                "'": "&#x27;",
                "`": "&#x60;"
            };
            unsafe_chars = /&(?!\w+;)|[\<\>\"\'\`]/g;
            return text.replace(unsafe_chars, function (chr) {
                return map[chr] || "&amp;";
            });
        };

        return SelectParser;

    })();

    SelectParser.select_to_array = function (select) {
        var child, parser, _i, _len, _ref;
        parser = new SelectParser();
        _ref = select.childNodes;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            child = _ref[_i];
            parser.add_node(child);
        }
        return parser.parsed;
    };

    AbstractChosen = (function () {
        function AbstractChosen(form_field, options) {
            this.form_field = form_field;
            this.options = options != null ? options : {};
            if (!AbstractChosen.browser_is_supported() || !this.form_field) {
                return;
            }
            this.is_multiple = this.form_field.multiple;
            this.set_default_text();
            this.set_default_values();
            this.setup();
            this.set_up_html();
            this.register_observers();
        }

        AbstractChosen.prototype.set_default_values = function () {
            var _this = this;
            this.click_test_action = function (evt) {
                return _this.test_active_click(evt);
            };
            this.activate_action = function (evt) {
                return _this.activate_field(evt);
            };
            this.active_field = false;
            this.mouse_on_container = false;
            this.results_showing = false;
            this.result_highlighted = null;
            this.allow_single_deselect = (this.options.allow_single_deselect != null) && (this.form_field.options[0] != null) && this.form_field.options[0].text === "" ? this.options.allow_single_deselect : false;
            this.disable_search_threshold = this.options.disable_search_threshold || 0;
            this.disable_search = this.options.disable_search || false;
            this.enable_split_word_search = this.options.enable_split_word_search != null ? this.options.enable_split_word_search : true;
            this.group_search = this.options.group_search != null ? this.options.group_search : true;
            this.search_contains = this.options.search_contains || false;
            this.single_backstroke_delete = this.options.single_backstroke_delete != null ? this.options.single_backstroke_delete : true;
            this.max_selected_options = this.options.max_selected_options || Infinity;
            this.inherit_select_classes = this.options.inherit_select_classes || false;
            this.display_selected_options = this.options.display_selected_options != null ? this.options.display_selected_options : true;
            return this.display_disabled_options = this.options.display_disabled_options != null ? this.options.display_disabled_options : true;
        };

        AbstractChosen.prototype.set_default_text = function () {
            if (this.form_field.getAttribute("data-placeholder")) {
                this.default_text = this.form_field.getAttribute("data-placeholder");
            } else if (this.is_multiple) {
                this.default_text = this.options.placeholder_text_multiple || this.options.placeholder_text || AbstractChosen.default_multiple_text;
            } else {
                this.default_text = this.options.placeholder_text_single || this.options.placeholder_text || AbstractChosen.default_single_text;
            }
            return this.results_none_found = this.form_field.getAttribute("data-no_results_text") || this.options.no_results_text || AbstractChosen.default_no_result_text;
        };

        AbstractChosen.prototype.mouse_enter = function () {
            return this.mouse_on_container = true;
        };

        AbstractChosen.prototype.mouse_leave = function () {
            return this.mouse_on_container = false;
        };

        AbstractChosen.prototype.input_focus = function (evt) {
            var _this = this;
            if (this.is_multiple) {
                if (!this.active_field) {
                    return setTimeout((function () {
                        return _this.container_mousedown();
                    }), 50);
                }
            } else {
                if (!this.active_field) {
                    return this.container_mousedown();
                }
            }
        };

        AbstractChosen.prototype.input_blur = function (evt) {
            var _this = this;
            if (!this.mouse_on_container) {
                this.active_field = false;
                return setTimeout((function () {
                    return _this.blur_test();
                }), 100);
            }
        };

        AbstractChosen.prototype.results_option_build = function (options) {
            var content, data, _i, _len, _ref;
            content = '';
            _ref = this.results_data;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                data = _ref[_i];
                if (data.group) {
                    content += this.result_add_group(data);
                } else {
                    content += this.result_add_option(data);
                }
                if (options != null ? options.first : void 0) {
                    if (data.selected && this.is_multiple) {
                        this.choice_build(data);
                    } else if (data.selected && !this.is_multiple) {
                        this.single_set_selected_text(data.text);
                    }
                }
            }
            return content;
        };

        AbstractChosen.prototype.result_add_option = function (option) {
            var classes, option_el;
            if (!option.search_match) {
                return '';
            }
            if (!this.include_option_in_results(option)) {
                return '';
            }
            classes = [];
            if (!option.disabled && !(option.selected && this.is_multiple)) {
                classes.push("active-result");
            }
            if (option.disabled && !(option.selected && this.is_multiple)) {
                classes.push("disabled-result");
            }
            if (option.selected) {
                classes.push("result-selected");
            }
            if (option.group_array_index != null) {
                classes.push("group-option");
            }
            if (option.classes !== "") {
                classes.push(option.classes);
            }
            option_el = document.createElement("li");
            option_el.className = classes.join(" ");
            option_el.style.cssText = option.style;
            option_el.setAttribute("data-option-array-index", option.array_index);
            option_el.innerHTML = option.search_text;
            return this.outerHTML(option_el);
        };

        AbstractChosen.prototype.result_add_group = function (group) {
            var group_el;
            if (!(group.search_match || group.group_match)) {
                return '';
            }
            if (!(group.active_options > 0)) {
                return '';
            }
            group_el = document.createElement("li");
            group_el.className = "group-result";
            group_el.innerHTML = group.search_text;
            return this.outerHTML(group_el);
        };

        AbstractChosen.prototype.results_update_field = function () {
            this.set_default_text();
            if (!this.is_multiple) {
                this.results_reset_cleanup();
            }
            this.result_clear_highlight();
            this.results_build();
            if (this.results_showing) {
                return this.winnow_results();
            }
        };

        AbstractChosen.prototype.reset_single_select_options = function () {
            var result, _i, _len, _ref, _results;
            _ref = this.results_data;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                result = _ref[_i];
                if (result.selected) {
                    _results.push(result.selected = false);
                } else {
                    _results.push(void 0);
                }
            }
            return _results;
        };

        AbstractChosen.prototype.results_toggle = function () {
            if (this.results_showing) {
                return this.results_hide();
            } else {
                return this.results_show();
            }
        };

        AbstractChosen.prototype.results_search = function (evt) {
            if (this.results_showing) {
                return this.winnow_results();
            } else {
                return this.results_show();
            }
        };

        AbstractChosen.prototype.winnow_results = function () {
            var escapedSearchText, option, regex, results, results_group, searchText, startpos, text, zregex, _i, _len, _ref, searchVersion, escapedSearchVersion, vregex;
            this.no_results_clear();
            results = 0;
            searchText = this.get_search_text();
            searchVersion = Corrector._changeLayout(searchText);
            escapedSearchText = searchText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
            escapedSearchVersion = searchVersion.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
            zregex = new RegExp(escapedSearchText, 'i');
            vregex = new RegExp(escapedSearchVersion, 'i');
            regex = this.get_search_regex(escapedSearchText, escapedSearchVersion);
            _ref = this.results_data;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                option = _ref[_i];
                option.search_match = false;
                results_group = null;
                if (this.include_option_in_results(option)) {
                    if (option.group) {
                        option.group_match = false;
                        option.active_options = 0;
                    }
                    if ((option.group_array_index != null) && this.results_data[option.group_array_index]) {
                        results_group = this.results_data[option.group_array_index];
                        if (results_group.active_options === 0 && results_group.search_match) {
                            results += 1;
                        }
                        results_group.active_options += 1;
                    }
                    if (!(option.group && !this.group_search)) {
                        option.search_text = option.group ? option.label : option.text;
                        option.search_match = this.search_string_match(option.search_text, regex);
                        if (option.search_match && !option.group) {
                            results += 1;
                        }
                        if (option.search_match) {
                            if (searchText.length) {
                                startpos = option.search_text.search(zregex);
                                if (startpos == -1) {
                                    startpos = option.search_text.search(vregex);
                                }
                                text = option.search_text.substr(0, startpos + searchText.length) + '</em>' + option.search_text.substr(startpos + searchText.length);
                                option.search_text = text.substr(0, startpos) + '<em>' + text.substr(startpos);
                            }
                            if (results_group != null) {
                                results_group.group_match = true;
                            }
                        } else if ((option.group_array_index != null) && this.results_data[option.group_array_index].search_match) {
                            option.search_match = true;
                        }
                    }
                }
            }
            this.result_clear_highlight();
            if (results < 1 && searchText.length) {
                this.update_results_content("");
                return this.no_results(searchText);
            } else {
                this.update_results_content(this.results_option_build());
                return this.winnow_results_set_highlight();
            }
        };

        AbstractChosen.prototype.get_search_regex = function (escaped_search_string, escaped_search_version) {
            var regex_anchor;
            regex_anchor = this.search_contains ? "" : "^";
            escaped_search_string = escaped_search_string + "|" + escaped_search_version;
            return new RegExp(regex_anchor + escaped_search_string, 'i');
        };

        AbstractChosen.prototype.search_string_match = function (search_string, regex) {
            var part, parts, _i, _len;
            if (regex.test(search_string)) {
                return true;
            } else if (this.enable_split_word_search && (search_string.indexOf(" ") >= 0 || search_string.indexOf("[") === 0)) {
                parts = search_string.replace(/\[|\]/g, "").split(" ");
                if (parts.length) {
                    for (_i = 0, _len = parts.length; _i < _len; _i++) {
                        part = parts[_i];
                        if (regex.test(part)) {
                            return true;
                        }
                    }
                }
            }
        };

        AbstractChosen.prototype.choices_count = function () {
            var option, _i, _len, _ref;
            if (this.selected_option_count != null) {
                return this.selected_option_count;
            }
            this.selected_option_count = 0;
            _ref = this.form_field.options;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                option = _ref[_i];
                if (option.selected) {
                    this.selected_option_count += 1;
                }
            }
            return this.selected_option_count;
        };

        AbstractChosen.prototype.choices_click = function (evt) {
            evt.preventDefault();
            if (!(this.results_showing || this.is_disabled)) {
                return this.results_show();
            }
        };

        AbstractChosen.prototype.keyup_checker = function (evt) {
            var stroke, _ref;
            stroke = (_ref = evt.which) != null ? _ref : evt.keyCode;
            this.search_field_scale();
            switch (stroke) {
                case 8:
                    if (this.is_multiple && this.backstroke_length < 1 && this.choices_count() > 0) {
                        return this.keydown_backstroke();
                    } else if (!this.pending_backstroke) {
                        this.result_clear_highlight();
                        return this.results_search();
                    }
                    break;
                case 13:
                    evt.preventDefault();
                    if (this.results_showing) {
                        return this.result_select(evt);
                    }
                    break;
                case 27:
                    if (this.results_showing) {
                        this.results_hide();
                    }
                    return true;
                case 9:
                case 38:
                case 40:
                case 16:
                case 91:
                case 17:
                    break;
                default:
                    return this.results_search();
            }
        };

        AbstractChosen.prototype.clipboard_event_checker = function (evt) {
            var _this = this;
            return setTimeout((function () {
                return _this.results_search();
            }), 50);
        };

        AbstractChosen.prototype.container_style = function () {
            return this.form_field.style.cssText;
        };

        AbstractChosen.prototype.include_option_in_results = function (option) {
            if (this.is_multiple && (!this.display_selected_options && option.selected)) {
                return false;
            }
            if (!this.display_disabled_options && option.disabled) {
                return false;
            }
            if (option.empty) {
                return false;
            }
            return true;
        };

        AbstractChosen.prototype.search_results_touchstart = function (evt) {
            this.touch_started = true;
            return this.search_results_mouseover(evt);
        };

        AbstractChosen.prototype.search_results_touchmove = function (evt) {
            this.touch_started = false;
            return this.search_results_mouseout(evt);
        };

        AbstractChosen.prototype.search_results_touchend = function (evt) {
            if (this.touch_started) {
                return this.search_results_mouseup(evt);
            }
        };

        AbstractChosen.prototype.outerHTML = function (element) {
            var tmp;
            if (element.outerHTML) {
                return element.outerHTML;
            }
            tmp = document.createElement("div");
            tmp.appendChild(element);
            return tmp.innerHTML;
        };

        AbstractChosen.browser_is_supported = function () {
            if (window.navigator.appName === "Microsoft Internet Explorer") {
                return document.documentMode >= 8;
            }
            if (/iP(od|hone)/i.test(window.navigator.userAgent)) {
                return false;
            }
            if (/Android/i.test(window.navigator.userAgent)) {
                if (/Mobile/i.test(window.navigator.userAgent)) {
                    return false;
                }
            }
            return true;
        };

        AbstractChosen.default_multiple_text = "Select Some Options";

        AbstractChosen.default_single_text = "Select an Option";

        AbstractChosen.default_no_result_text = "No results match";

        return AbstractChosen;

    })();

    this.Chosen = (function (_super) {
        __extends(Chosen, _super);

        function Chosen() {
            _ref = Chosen.__super__.constructor.apply(this, arguments);
            return _ref;
        }

        Chosen.prototype.setup = function () {
            this.current_selectedIndex = this.form_field.selectedIndex;
            return this.is_rtl = this.form_field.hasClassName("chosen-rtl");
        };

        Chosen.prototype.set_default_values = function () {
            Chosen.__super__.set_default_values.call(this);
            this.single_temp = new Template('<a class="chosen-single chosen-default" tabindex="-1"><span>#{default}</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" /></div><ul class="chosen-results"></ul></div>');
            this.multi_temp = new Template('<ul class="chosen-choices"><li class="search-field"><input type="text" value="#{default}" class="default" autocomplete="off" style="width:25px;" /></li></ul><div class="chosen-drop"><ul class="chosen-results"></ul></div>');
            return this.no_results_temp = new Template('<li class="no-results">' + this.results_none_found + ' "<span>#{terms}</span>"</li>');
        };

        Chosen.prototype.set_up_html = function () {
            var container_classes, container_props;
            container_classes = ["chosen-container"];
            container_classes.push("chosen-container-" + (this.is_multiple ? "multi" : "single"));
            if (this.inherit_select_classes && this.form_field.className) {
                container_classes.push(this.form_field.className);
            }
            if (this.is_rtl) {
                container_classes.push("chosen-rtl");
            }
            if (this.form_field.className.indexOf('rSelectParam') != -1) {
                container_classes.push("rSelectParam");
            }
            container_props = {
                'class': container_classes.join(' '),
                'style': this.container_style(),
                'title': this.form_field.title
            };
            if (this.form_field.id.length) {
                container_props.id = this.form_field.id.replace(/[^\w]/g, '_') + "_chosen";
            }
            this.container = this.is_multiple ? new Element('div', container_props).update(this.multi_temp.evaluate({
                    "default": this.default_text
                })) : new Element('div', container_props).update(this.single_temp.evaluate({
                    "default": this.default_text
                }));
            this.form_field.hide().insert({
                after: this.container
            });
            this.dropdown = this.container.down('div.chosen-drop');
            this.search_field = this.container.down('input');
            this.search_results = this.container.down('ul.chosen-results');
            this.search_field_scale();
            this.search_no_results = this.container.down('li.no-results');
            if (this.is_multiple) {
                this.search_choices = this.container.down('ul.chosen-choices');
                this.search_container = this.container.down('li.search-field');
            } else {
                this.search_container = this.container.down('div.chosen-search');
                this.selected_item = this.container.down('.chosen-single');
            }
            this.results_build();
            this.set_tab_index();
            this.set_label_behavior();
            return this.form_field.fire("chosen:ready", {
                chosen: this
            });
        };

        Chosen.prototype.register_observers = function () {
            var _this = this;
            this.container.observe("touchstart", function (evt) {
                return;//n _this.container_mousedown(evt);
            });
            this.container.observe("touchend", function (evt) {
                return _this.container_mouseup(evt);
            });
            this.container.observe("mousedown", function (evt) {
                return _this.container_mousedown(evt);
            });
            this.container.observe("mouseup", function (evt) {
                return _this.container_mouseup(evt);
            });
            this.container.observe("mouseenter", function (evt) {
                return _this.mouse_enter(evt);
            });
            this.container.observe("mouseleave", function (evt) {
                return _this.mouse_leave(evt);
            });
            this.search_results.observe("mouseup", function (evt) {
                return _this.search_results_mouseup(evt);
            });
            this.search_results.observe("mouseover", function (evt) {
                return _this.search_results_mouseover(evt);
            });
            this.search_results.observe("mouseout", function (evt) {
                return _this.search_results_mouseout(evt);
            });
            this.search_results.observe("mousewheel", function (evt) {
                return _this.search_results_mousewheel(evt);
            });
            this.search_results.observe("DOMMouseScroll", function (evt) {
                return _this.search_results_mousewheel(evt);
            });
            this.search_results.observe("touchstart", function (evt) {
                return _this.search_results_touchstart(evt);
            });
            this.search_results.observe("touchmove", function (evt) {
                return _this.search_results_touchmove(evt);
            });
            this.search_results.observe("touchend", function (evt) {
                return _this.search_results_touchend(evt);
            });
            this.form_field.observe("chosen:updated", function (evt) {
                return _this.results_update_field(evt);
            });
            this.form_field.observe("chosen:activate", function (evt) {
                return _this.activate_field(evt);
            });
            this.form_field.observe("chosen:open", function (evt) {
                return _this.container_mousedown(evt);
            });
            this.form_field.observe("chosen:close", function (evt) {
                return _this.input_blur(evt);
            });
            this.search_field.observe("blur", function (evt) {
                return _this.input_blur(evt);
            });
            this.search_field.observe("keyup", function (evt) {
                return _this.keyup_checker(evt);
            });
            this.search_field.observe("keydown", function (evt) {
                return _this.keydown_checker(evt);
            });
            this.search_field.observe("focus", function (evt) {
                return _this.input_focus(evt);
            });
            this.search_field.observe("cut", function (evt) {
                return _this.clipboard_event_checker(evt);
            });
            this.search_field.observe("paste", function (evt) {
                return _this.clipboard_event_checker(evt);
            });
            if (this.is_multiple) {
                return this.search_choices.observe("click", function (evt) {
                    return _this.choices_click(evt);
                });
            } else {
                return this.container.observe("click", function (evt) {
                    return evt.preventDefault();
                });
            }
        };

        Chosen.prototype.destroy = function () {
            this.container.ownerDocument.stopObserving("click", this.click_test_action);
            this.form_field.stopObserving();
            this.container.stopObserving();
            this.search_results.stopObserving();
            this.search_field.stopObserving();
            if (this.form_field_label != null) {
                this.form_field_label.stopObserving();
            }
            if (this.is_multiple) {
                this.search_choices.stopObserving();
                this.container.select(".search-choice-close").each(function (choice) {
                    return choice.stopObserving();
                });
            } else {
                this.selected_item.stopObserving();
            }
            if (this.search_field.tabIndex) {
                this.form_field.tabIndex = this.search_field.tabIndex;
            }
            this.container.remove();
            return this.form_field.show();
        };

        Chosen.prototype.search_field_disabled = function () {
            this.is_disabled = this.form_field.disabled;
            if (this.is_disabled) {
                this.container.addClassName('chosen-disabled');
                this.search_field.disabled = true;
                if (!this.is_multiple) {
                    this.selected_item.stopObserving("focus", this.activate_action);
                }
                return this.close_field();
            } else {
                this.container.removeClassName('chosen-disabled');
                this.search_field.disabled = false;
                if (!this.is_multiple) {
                    return this.selected_item.observe("focus", this.activate_action);
                }
            }
        };

        Chosen.prototype.container_mousedown = function (evt) {
            if (!this.is_disabled) {
                if (evt && evt.type === "mousedown" && !this.results_showing) {
                    evt.stop();
                }
                if (!((evt != null) && evt.target.hasClassName("search-choice-close"))) {
                    if (!this.active_field) {
                        if (this.is_multiple) {
                            this.search_field.clear();
                        }
                        this.container.ownerDocument.observe("click", this.click_test_action);
                        this.results_show();
                    } else if (!this.is_multiple && evt && (evt.target === this.selected_item || evt.target.up("a.chosen-single"))) {
                        this.results_toggle();
                    }
                    return this.activate_field();
                }
            }
        };

        Chosen.prototype.container_mouseup = function (evt) {
            if (evt.target.nodeName === "ABBR" && !this.is_disabled) {
                return this.results_reset(evt);
            }
        };

        Chosen.prototype.search_results_mousewheel = function (evt) {
            var delta;
            delta = evt.deltaY || -evt.wheelDelta || evt.detail;
            if (delta != null) {
                evt.preventDefault();
                if (evt.type === 'DOMMouseScroll') {
                    delta = delta * 40;
                }
                delta /= 2;
                return this.search_results.scrollTop = delta + this.search_results.scrollTop;
            }
        };

        Chosen.prototype.blur_test = function (evt) {
            if (!this.active_field && this.container.hasClassName("chosen-container-active")) {
                return this.close_field();
            }
        };

        Chosen.prototype.close_field = function () {
            this.container.ownerDocument.stopObserving("click", this.click_test_action);
            this.active_field = false;
            this.results_hide();
            this.container.removeClassName("chosen-container-active");
            this.clear_backstroke();
            this.show_search_field_default();
            return this.search_field_scale();
        };

        Chosen.prototype.activate_field = function () {
            this.container.addClassName("chosen-container-active");
            this.active_field = true;
            this.search_field.value = this.search_field.value;
            return this.search_field.focus();
        };

        Chosen.prototype.test_active_click = function (evt) {
            if (evt.target.up('.chosen-container') === this.container) {
                return this.active_field = true;
            } else {
                return this.close_field();
            }
        };

        Chosen.prototype.results_build = function () {
            this.parsing = true;
            this.selected_option_count = null;
            this.results_data = SelectParser.select_to_array(this.form_field);
            if (this.is_multiple) {
                this.search_choices.select("li.search-choice").invoke("remove");
            } else if (!this.is_multiple) {
                this.single_set_selected_text();
                if (this.disable_search || this.form_field.options.length <= this.disable_search_threshold) {
                    this.search_field.readOnly = true;
                    this.container.addClassName("chosen-container-single-nosearch");
                } else {
                    this.search_field.readOnly = false;
                    this.container.removeClassName("chosen-container-single-nosearch");
                }
            }
            this.update_results_content(this.results_option_build({
                first: true
            }));
            this.search_field_disabled();
            this.show_search_field_default();
            this.search_field_scale();
            return this.parsing = false;
        };

        Chosen.prototype.result_do_highlight = function (el) {
            var high_bottom, high_top, maxHeight, visible_bottom, visible_top;
            this.result_clear_highlight();
            this.result_highlight = el;
            this.result_highlight.addClassName("highlighted");
            maxHeight = parseInt(this.search_results.getStyle('maxHeight'), 10);
            visible_top = this.search_results.scrollTop;
            visible_bottom = maxHeight + visible_top;
            high_top = this.result_highlight.positionedOffset().top;
            high_bottom = high_top + this.result_highlight.getHeight();
            if (high_bottom >= visible_bottom) {
                return this.search_results.scrollTop = (high_bottom - maxHeight) > 0 ? high_bottom - maxHeight : 0;
            } else if (high_top < visible_top) {
                return this.search_results.scrollTop = high_top;
            }
        };

        Chosen.prototype.result_clear_highlight = function () {
            if (this.result_highlight) {
                this.result_highlight.removeClassName('highlighted');
            }
            return this.result_highlight = null;
        };

        Chosen.prototype.results_show = function () {
            if (this.is_multiple && this.max_selected_options <= this.choices_count()) {
                this.form_field.fire("chosen:maxselected", {
                    chosen: this
                });
                return false;
            }
            this.container.addClassName("chosen-with-drop");
            this.results_showing = true;
            this.search_field.focus();
            this.search_field.value = this.search_field.value;
            this.winnow_results();
            return this.form_field.fire("chosen:showing_dropdown", {
                chosen: this
            });
        };

        Chosen.prototype.update_results_content = function (content) {
            return this.search_results.update(content);
        };

        Chosen.prototype.results_hide = function () {
            if (this.results_showing) {
                this.result_clear_highlight();
                this.container.removeClassName("chosen-with-drop");
                this.form_field.fire("chosen:hiding_dropdown", {
                    chosen: this
                });
            }
            return this.results_showing = false;
        };

        Chosen.prototype.set_tab_index = function (el) {
            var ti;
            if (this.form_field.tabIndex) {
                ti = this.form_field.tabIndex;
                this.form_field.tabIndex = -1;
                return this.search_field.tabIndex = ti;
            }
        };

        Chosen.prototype.set_label_behavior = function () {
            var _this = this;
            this.form_field_label = this.form_field.up("label");
            if (this.form_field_label == null) {
                this.form_field_label = $$("label[for='" + this.form_field.id + "']").first();
            }
            if (this.form_field_label != null) {
                return this.form_field_label.observe("click", function (evt) {
                    if (_this.is_multiple) {
                        return _this.container_mousedown(evt);
                    } else {
                        return _this.activate_field();
                    }
                });
            }
        };

        Chosen.prototype.show_search_field_default = function () {
            if (this.is_multiple && this.choices_count() < 1 && !this.active_field) {
                this.search_field.value = this.default_text;
                return this.search_field.addClassName("default");
            } else {
                this.search_field.value = "";
                return this.search_field.removeClassName("default");
            }
        };

        Chosen.prototype.search_results_mouseup = function (evt) {
            var target;
            target = evt.target.hasClassName("active-result") ? evt.target : evt.target.up(".active-result");
            if (target) {
                this.result_highlight = target;
                this.result_select(evt);
                return this.search_field.focus();
            }
        };

        Chosen.prototype.search_results_mouseover = function (evt) {
            var target;
            target = evt.target.hasClassName("active-result") ? evt.target : evt.target.up(".active-result");
            if (target) {
                return this.result_do_highlight(target);
            }
        };

        Chosen.prototype.search_results_mouseout = function (evt) {
            if (evt.target.hasClassName('active-result') || evt.target.up('.active-result')) {
                return this.result_clear_highlight();
            }
        };

        Chosen.prototype.choice_build = function (item) {
            var choice, close_link,
                _this = this;
            choice = new Element('li', {
                "class": "search-choice"
            }).update("<span>" + item.html + "</span>");
            if (item.disabled) {
                choice.addClassName('search-choice-disabled');
            } else {
                close_link = new Element('a', {
                    href: '#',
                    "class": 'search-choice-close',
                    rel: item.array_index
                });
                close_link.observe("click", function (evt) {
                    return _this.choice_destroy_link_click(evt);
                });
                choice.insert(close_link);
            }
            return this.search_container.insert({
                before: choice
            });
        };

        Chosen.prototype.choice_destroy_link_click = function (evt) {
            evt.preventDefault();
            evt.stopPropagation();
            if (!this.is_disabled) {
                return this.choice_destroy(evt.target);
            }
        };

        Chosen.prototype.choice_destroy = function (link) {
            if (this.result_deselect(link.readAttribute("rel"))) {
                this.show_search_field_default();
                if (this.is_multiple && this.choices_count() > 0 && this.search_field.value.length < 1) {
                    this.results_hide();
                }
                link.up('li').remove();
                return this.search_field_scale();
            }
        };

        Chosen.prototype.results_reset = function () {
            this.reset_single_select_options();
            this.form_field.options[0].selected = true;
            this.single_set_selected_text();
            this.show_search_field_default();
            this.results_reset_cleanup();
            if (typeof Event.simulate === 'function') {
                this.form_field.simulate("change");
            }
            if (this.active_field) {
                return this.results_hide();
            }
        };

        Chosen.prototype.results_reset_cleanup = function () {
            var deselect_trigger;
            this.current_selectedIndex = this.form_field.selectedIndex;
            deselect_trigger = this.selected_item.down("abbr");
            if (deselect_trigger) {
                return deselect_trigger.remove();
            }
        };

        Chosen.prototype.result_select = function (evt) {
            var high, item;
            if (this.result_highlight) {
                high = this.result_highlight;
                this.result_clear_highlight();
                if (this.is_multiple && this.max_selected_options <= this.choices_count()) {
                    this.form_field.fire("chosen:maxselected", {
                        chosen: this
                    });
                    return false;
                }
                if (this.is_multiple) {
                    high.removeClassName("active-result");
                } else {
                    this.reset_single_select_options();
                }
                high.addClassName("result-selected");
                item = this.results_data[high.getAttribute("data-option-array-index")];
                item.selected = true;
                this.form_field.options[item.options_index].selected = true;
                this.selected_option_count = null;
                if (this.is_multiple) {
                    this.choice_build(item);
                } else {
                    this.single_set_selected_text(item.text);
                }
                if (!((evt.metaKey || evt.ctrlKey) && this.is_multiple)) {
                    this.results_hide();
                }
                this.search_field.value = "";
                if (typeof Event.simulate === 'function' && (this.is_multiple || this.form_field.selectedIndex !== this.current_selectedIndex)) {
                    this.form_field.simulate("change");
                }
                this.current_selectedIndex = this.form_field.selectedIndex;
                return this.search_field_scale();
            }
        };

        Chosen.prototype.single_set_selected_text = function (text) {
            if (text == null) {
                text = this.default_text;
            }
            if (text === this.default_text) {
                this.selected_item.addClassName("chosen-default");
            } else {
                this.single_deselect_control_build();
                this.selected_item.removeClassName("chosen-default");
            }
            return this.selected_item.down("span").update(text);
        };

        Chosen.prototype.result_deselect = function (pos) {
            var result_data;
            result_data = this.results_data[pos];
            if (!this.form_field.options[result_data.options_index].disabled) {
                result_data.selected = false;
                this.form_field.options[result_data.options_index].selected = false;
                this.selected_option_count = null;
                this.result_clear_highlight();
                if (this.results_showing) {
                    this.winnow_results();
                }
                if (typeof Event.simulate === 'function') {
                    this.form_field.simulate("change");
                }
                this.search_field_scale();
                return true;
            } else {
                return false;
            }
        };

        Chosen.prototype.single_deselect_control_build = function () {
            if (!this.allow_single_deselect) {
                return;
            }
            if (!this.selected_item.down("abbr")) {
                this.selected_item.down("span").insert({
                    after: "<abbr class=\"search-choice-close\"></abbr>"
                });
            }
            return this.selected_item.addClassName("chosen-single-with-deselect");
        };

        Chosen.prototype.get_search_text = function () {
            if (this.search_field.value === this.default_text) {
                return "";
            } else {
                return this.search_field.value.strip().escapeHTML();
            }
        };

        Chosen.prototype.winnow_results_set_highlight = function () {
            var do_high;
            if (!this.is_multiple) {
                do_high = this.search_results.down(".result-selected.active-result");
            }
            if (do_high == null) {
                do_high = this.search_results.down(".active-result");
            }
            if (do_high != null) {
                return this.result_do_highlight(do_high);
            }
        };

        Chosen.prototype.no_results = function (terms) {
            this.search_results.insert(this.no_results_temp.evaluate({
                terms: terms
            }));
            return this.form_field.fire("chosen:no_results", {
                chosen: this
            });
        };

        Chosen.prototype.no_results_clear = function () {
            var nr, _results;
            nr = null;
            _results = [];
            while (nr = this.search_results.down(".no-results")) {
                _results.push(nr.remove());
            }
            return _results;
        };

        Chosen.prototype.keydown_arrow = function () {
            var next_sib;
            if (this.results_showing && this.result_highlight) {
                next_sib = this.result_highlight.next('.active-result');
                if (next_sib) {
                    return this.result_do_highlight(next_sib);
                }
            } else {
                return this.results_show();
            }
        };

        Chosen.prototype.keyup_arrow = function () {
            var actives, prevs, sibs;
            if (!this.results_showing && !this.is_multiple) {
                return this.results_show();
            } else if (this.result_highlight) {
                sibs = this.result_highlight.previousSiblings();
                actives = this.search_results.select("li.active-result");
                prevs = sibs.intersect(actives);
                if (prevs.length) {
                    return this.result_do_highlight(prevs.first());
                } else {
                    if (this.choices_count() > 0) {
                        this.results_hide();
                    }
                    return this.result_clear_highlight();
                }
            }
        };

        Chosen.prototype.keydown_backstroke = function () {
            var next_available_destroy;
            if (this.pending_backstroke) {
                this.choice_destroy(this.pending_backstroke.down("a"));
                return this.clear_backstroke();
            } else {
                next_available_destroy = this.search_container.siblings().last();
                if (next_available_destroy && next_available_destroy.hasClassName("search-choice") && !next_available_destroy.hasClassName("search-choice-disabled")) {
                    this.pending_backstroke = next_available_destroy;
                    if (this.pending_backstroke) {
                        this.pending_backstroke.addClassName("search-choice-focus");
                    }
                    if (this.single_backstroke_delete) {
                        return this.keydown_backstroke();
                    } else {
                        return this.pending_backstroke.addClassName("search-choice-focus");
                    }
                }
            }
        };

        Chosen.prototype.clear_backstroke = function () {
            if (this.pending_backstroke) {
                this.pending_backstroke.removeClassName("search-choice-focus");
            }
            return this.pending_backstroke = null;
        };

        Chosen.prototype.keydown_checker = function (evt) {
            var stroke, _ref1;
            stroke = (_ref1 = evt.which) != null ? _ref1 : evt.keyCode;
            this.search_field_scale();
            if (stroke !== 8 && this.pending_backstroke) {
                this.clear_backstroke();
            }
            switch (stroke) {
                case 8:
                    this.backstroke_length = this.search_field.value.length;
                    break;
                case 9:
                    if (this.results_showing && !this.is_multiple) {
                        this.result_select(evt);
                    }
                    this.mouse_on_container = false;
                    break;
                case 13:
                    if (this.results_showing) {
                        evt.preventDefault();
                    }
                    break;
                case 32:
                    if (this.disable_search) {
                        evt.preventDefault();
                    }
                    break;
                case 38:
                    evt.preventDefault();
                    this.keyup_arrow();
                    break;
                case 40:
                    evt.preventDefault();
                    this.keydown_arrow();
                    break;
            }
        };

        Chosen.prototype.search_field_scale = function () {
            var div, f_width, h, style, style_block, styles, w, _i, _len;
            if (this.is_multiple) {
                h = 0;
                w = 0;
                style_block = "position:absolute; left: -1000px; top: -1000px; display:none;";
                styles = ['font-size', 'font-style', 'font-weight', 'font-family', 'line-height', 'text-transform', 'letter-spacing'];
                for (_i = 0, _len = styles.length; _i < _len; _i++) {
                    style = styles[_i];
                    style_block += style + ":" + this.search_field.getStyle(style) + ";";
                }
                div = new Element('div', {
                    'style': style_block
                }).update(this.search_field.value.escapeHTML());
                document.body.appendChild(div);
                w = Element.measure(div, 'width') + 25;
                div.remove();
                f_width = this.container.getWidth();
                if (w > f_width - 10) {
                    w = f_width - 10;
                }
                return this.search_field.setStyle({
                    'width': w + 'px'
                });
            }
        };

        return Chosen;

    })(AbstractChosen);

}).call(this);


/**
 * Simulate event class
 */
(function () {

    var eventMatchers = {
        'HTMLEvents': new RegExp('^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$', ''),
        'MouseEvents': new RegExp('^(?:click|mouse(?:down|up|over|move|out))$', '')
    }
    var defaultOptions = {
        pointerX: 0,
        pointerY: 0,
        button: 0,
        ctrlKey: false,
        altKey: false,
        shiftKey: false,
        metaKey: false,
        bubbles: true,
        cancelable: true
    }

    Event.simulate = function (element, eventName) {
        var options = Object.extend(defaultOptions, arguments[2] || {});
        var oEvent, eventType = null;

        element = $(element);

        for (var name in eventMatchers) {
            if (eventMatchers[name].test(eventName)) {
                eventType = name;
                break;
            }
        }

        if (!eventType)
            throw new SyntaxError('Only HTMLEvents and MouseEvents interfaces are supported');

        if (document.createEvent) {
            oEvent = document.createEvent(eventType);
            if (eventType == 'HTMLEvents') {
                oEvent.initEvent(eventName, options.bubbles, options.cancelable);
            }
            else {
                oEvent.initMouseEvent(eventName, options.bubbles, options.cancelable, document.defaultView,
                    options.button, options.pointerX, options.pointerY, options.pointerX, options.pointerY,
                    options.ctrlKey, options.altKey, options.shiftKey, options.metaKey, options.button, element);
            }
            element.dispatchEvent(oEvent);
        }
        else {
            options.clientX = options.pointerX;
            options.clientY = options.pointerY;
            oEvent = Object.extend(document.createEventObject(), options);
            element.fireEvent('on' + eventName, oEvent);
        }
        return element;
    }

    Element.addMethods({simulate: Event.simulate});
})()

/**
 *   Validator.js
 */
/******************************************************************
 * �?�����������?���? ���������� �?���?���?.
 * �?�?�? �?�?�?�?���?�?�?�� ���?������������ ���� �����?������ getAttribute
 * inspired by dojo.html.getAttribute
 *
 * �?�?�� ���?�?�?�����?�?�? case sensitive! �������?���? �����?���?�? vldTYPE ������ vldtype !!!
 *
 * �?�?�?�?�����?�?�?:
 * vldType=[int|float|string|text|email|url] - �?���� �������?; text===string
 * vldRequired - �������� �����?�����?�����?������ �����? �������������������?
 * vldMin=[int] �� vldMax=[int] - �?�������������?������|�������?���������?������ �������?�?�?�������� �������?�������� �����? �?�?�?���� �� �?���?����
 * vldTitle=[string] - ���������?�����?�����?���?�� ���?�?�?�����?�?, ���?�������?���?���?�?�? �������?�?�� �?�����?�����?�?�?���?�?�?������ label �?�����������?��
 * vldErrorText=[string] - ���������?�����?�����?���?�� ���?�?�?�����?�?, �?�������?�������� ���� ���?��������, ���?�����?������ ���? �?�?���������?�?��������
 *    (���?�����?���?���? �?�������?�������? ���� ���?�������� �?��������: vldErrorText, ���?���� ������ �����? �?�?���������?�?������ �?�������?�������� �? ������������������ �������? = vldTitle,
 *        ���?���� vldTitle ���� ����������, �?�?���������?�?������ �?�������?�������� �? ������������������ �������?, �������������?�� �� �?�����?�����?�?�?���?�?�?���� �?�?���� label)
 *
 * ���?�������?���� ���?���������������?�?�? ���?���� ���������� ���?�?�����?�? vldType ������ vldRequired
 *
 * ������?�?���������� ���?�������?���� String|Email|Integer|Float ���?���������������?�?�? �?�����?���� �����? ����������
 * �����?���?�?�� �������?�? ���?�?�? ���������������? �������?���������?�������� (textarea, input type=text|hidden|password)
 * ���?�?�����?���?�� �?�����������?�?, �������?�������? �����?���?�?�? ���������?�?�?�? �� html ��������, ���?�������?�?�?�?�?�? �?�����?���� ���?�� ���������?���� vldRequired
 *
 * �?���? ���?�?�����? radio ���������?���������� ���������?�? vldRequired �?�����?���� �����? �����?�������� �?�����������?��
 * �?�?�?�?���? �����?���? �?�����������?�� select ������������ �������?�? value="" (�������?�� ���?�����? ���?�����?�?�� �������?�������� ���������? �?���������� <option></option>)
 *
 * USAGE:
 *    var validator = new ValidatorUtils('formName');
 *    if (validator.validateForm())
 *        process_data
 *    else
 *        alert('�?�? ���?���������?�? �� �������������������� �?���?���?:\n' + validator.message.join('\n'));
 * ������
 *    var validator = new ValidatorUtils('formName');
 *    validator.validateInteger('int_field_id', 'Field Label');
 *    validator.validateEmail('mail_field_id', '�?���?�?��');
 *    validator.validateString('string_field_id', '�������?�?');
 *    if (validator.validate())
 *        success!!
 *    else
 *        alert('�?�? ���?���������?�? �� �������������������� �?���?���?:\n' + validator.message.join('\n'));
 *
 * �? �����?������ �?���?�?���� ���?�������?�?�?�?�?�? �?�����?���� �������? �? ���?�?�?�����?�?���� vldType ������ vldRequired
 * ���� ���?���?���� �?���?�?���� ���?�?�?�����?�? ���������?���������� �?�������?�����?�? label �?�����������?�� ���?�� ���?�������� �����?������
 *
 * �?�?�������?�? VLDML �����? �?���?���?:
 * <label for="abbreviation">string not required</label>:<input name="abbreviation" id="abbreviation"  vldType="string" vldMin="3" vldMax="64">
 * <label for="mail">mail</label>: <input name="mail" id="mail"  vldType="email">
 * <label for="info">textarea</label>: <textarea name="info" id="info" vldMin="5" vldType="string" vldRequired></textarea>
 * <label for="interval_second">int</label>: <input name="interval_second" id="interval_second" vldRequired vldType="int" vldMin="0">
 * <label for="interval_second2">float</label>: <input name="interval_second2" id="interval_second2" vldRequired vldType="float" vldMin="0">
 * <label for="is_base">checkbox required</label><input type="checkbox" name="is_base" id="is_base" vldType="string" vldRequired>
 * <label for="radio">radio required</label><br />
 * <input type="radio" name="radio" id="radio" value="1" vldRequired> 1
 * <input type="radio" name="radio" id="radio" value="2"> 2
 * <input type="radio" name="radio" id="radio" value="3"> 3
 *
 * �� ���?�?�?�����?�?�� for �?�?���� label �����?���?�? id �������? �� �����?���?�����? ���?�����?���?�?�? ������������
 *
 * TODO: 1. �?�?�������?���� �������?/���������� ���?���� ������������������ ���?�?������ ��������
 *        2. �?�?�������?���� ���?�?���� �?�����������?���� (�?�?�����?���������� radio)
 *        3. � ���?�����?���?������ - �����?�����������?�����?�? �?�������?�? �? labels; �?�����?������ ���������� ���?���������� $(), value.trim() �� �?.��.
 *        4. �?�����������?�? �����?���� �������������������? �?�������?�������? ���� ���?��������, �����?���?�?�� ���?�������?�?�� ���? ���������? �?�������?�������? (�������� �����?�?������ �?�������?��������) �� �������������?�� �?�����?���� �������?�?�?�?�� �������?�������?
 */
var ValidatorUtils = Class.create();

ValidatorUtils.prototype = {

    form: null,
    labels: null,
    message: null,
    inputs: null,

    initialize: function (formElement) {
        this.message = new Array();
        this.form = $(formElement);
        this.labels = $A(this.form.getElementsByTagName("label"));
    },

    validateForm: function () {
        with (this) {
            this.inputs = Form.getElements(form);

            for (i = 0; inLn = this.inputs.length, i < inLn; ++i) {
                this.validateElem(this.inputs[i]);
            }
        }
        return this.validate();
    },

    validateElem: function (elem, label) {
        var type;

        if (elem = $(elem)) {
            //�� �������?�� 7 �� 8 getAttribute ���?�� ���?�?�?���� ���?�?�?�����?�?�� ���������?���?�����? ���?�?�?�?�? �?�?�?�����?
            //�� ���?�?�����? ���?���?�����?���? null
            if (
                ((type = elem.getAttribute('vldType')) && type != '')
                || (elem.getAttribute('vldRequired') != null)
            ) {
                errorText = '';
                customErrorText = elem.getAttribute('vldErrorText');

                //get label names
                if (!label)
                    label = elem.getAttribute('vldTitle');

                if (!label) {
                    label = null;
                    for (j = 0; lbLn = this.labels.length, j < lbLn; ++j) {
                        if (this.labels[j].htmlFor == elem.id) {
                            label = this.labels[j].firstChild.nodeValue;
                            break;
                        }
                    }
                }

                switch (elem.type) {
                    case "radio":
                    case "checkbox":
                        errorText = this._checkRequired(elem, label);
                        break;
                    case "select-one":
                    case "select-many":
                        value = $F(elem);
                        if (this._isEmpty(value)) {
                            errorText = this._checkRequired(elem, label);
                        }
                        break;
                    case "text":
                    case "textarea":
                    case "password":
                    case "hidden":
                    case "file":
                        //vldType
                        switch (type) {
                            case 'int':
                                errorText = this.validateInteger(elem, label);
                                break;
                            case 'float':
                                errorText = this.validateFloat(elem, label);
                                break;
                            case 'email':
                                errorText = this.validateEmail(elem, label);
                                break;
                            case 'date':
                                errorText = this.validateDate(elem, label);
                                break;
                            case 'url':
                                errorText = this.validateUrl(elem, label);
                                break;
                            case 'string':
                            case 'text':
                                errorText = this.validateString(elem, label);
                                break;
                            default :
                                errorText = langId(12) + type + langId(13) + label + " (id " + elem.id + ")";
                                break;
                        }
                        break;
                }
                if (typeof errorText != 'undefined' && errorText)
                    this.message.push(customErrorText ? customErrorText : errorText)
            }
        }
    },

    validate: function () {
        return (!this.message.length);
    },

    /**
     * Validate group of elements
     */
    validateGroup: function (name, label) {
        var chk = 0;

        elements = Form.getInputs(this.form, 'checkbox', name)

        if (elements.length == null) {
            if (elements.checked)
                chk++;
        }
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].checked)
                chk++;
        }
        if (chk == 0)
            return langId(14) + ' "' + label + '": ' + langId(15);
    },

    validateString: function (name, label) {
        value = $F(name);

        min = this._getMin(name);
        max = this._getMax(name);

        if (this._isEmpty(value) && !(min > 0)) {
            return this._checkRequired(name, label);
        }

        value = value.trim();

        if (min && (value.length < min))
            return langId(14) + ' "' + label + '" ' + langId(16) + ' ' + min + ' ' + langId(18);
        else if (max && (value.length > max))
            return langId(14) + ' "' + label + '" ' + langId(17) + ' ' + max + ' ' + langId(18);
        else {
            elem = $(name);
            if (elem.type == "text" || elem.type == "textarea") {//�����?���?�?�? trimmed �������?�������� �� ��������
                elem.value = value;
            }
        }
    },

    validateUrl: function (name, label) {
        value = $F(name);

        //���� ���?�����?������ �� validateForm, �?.��. ���������? ���?���?�����?�?�?�? ���?�������?����
        if (this._isEmpty(value)) {
            return this._checkRequired(name, label);
        }
        value = value.trim();
        //TODO: �������?�� ���?���������?���?�� regex!
        var regexp = /(ftp|http|https):\/\/?(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i;
        //alert(regexp.test(value));
        if (!regexp.test(value)) {
            return langId(14) + ' "' + label + '" ���� �?�����?���?�?�? �����?���?�� URL';
        } else {
            elem = $(name);
            if (elem.type == "text" || elem.type == "textarea")//�����?���?�?�? trimmed �������?�������� �� ��������
                elem.value = value;
        }
    },

    validateEmail: function (name, label) {
        value = $F(name);

        //���� ���?�����?������ �� validateForm, �?.��. ���������? ���?���?�����?�?�?�? ���?�������?����
        if (this._isEmpty(value)) {
            return this._checkRequired(name, label);
        }
        value = value.trim();
        if (!value.match(/^[a-z0-9\._-]+@[a-z0-9\._-]+\.[a-z]{2,6}$/i)) {
            return langId(14) + ' "' + label + '" ���� �?�����?���?�?�? �����?���?�� �����?���?���� �?�������?�?���������� �����?�?�?';
        } else {
            elem = $(name);
            if (elem.type == "text" || elem.type == "textarea")//�����?���?�?�? trimmed �������?�������� �� ��������
                elem.value = value;
        }
    },

    validateDate: function (name, label) {
        var err = langId(14) + ' "' + label + '" ���� �?�����?���?�?�? �����?������ �����?����.\n�?���������?���?�?��, �����������?�� �����?�? �� �?���?�����?�� "2014-01-25"\n';
        value = $F(name);

        //���� ���?�����?������ �� validateForm, �?.��. ���������? ���?���?�����?�?�?�? ���?�������?����
        if (this._isEmpty(value)) {
            return this._checkRequired(name, label);
        }
        value = value.trim();

        dArray = value.split('-');
        if (dArray.length != 3 || isNaN(dArray[0]) || isNaN(dArray[1]) || isNaN(dArray[2])) {
            return err;
        }

        if (
            dArray[0] < 1900 || dArray[0] > 2100 ||
            parseInt(dArray[1], 10) < 1 || parseInt(dArray[1], 10) > 12 ||
            parseInt(dArray[2], 10) < 1 || parseInt(dArray[2], 10) > getDayInMonth(dArray[0], dArray[1])
        ) {
            return err;
        }

        elem = $(name);
        if (elem.type == "text" || elem.type == "textarea") {//�����?���?�?�? trimmed �������?�������� �� ��������
            elem.value = value;
        }
    },

    validateInteger: function (name, label) {
        value = $F(name);

        //���� ���?�����?������ �� validateForm, �?.��. ���������? ���?���?�����?�?�?�? ���?�������?����
        if (this._isEmpty(value)) {
            return this._checkRequired(name, label);
        }

        value = parseInt(value);
        if (isNaN(value))
            return langId(14) + ' "' + label + '" ���� �?�����?���?�?�? �?�����?�� �?���?������';
        else {
            min = this._getMin(name);
            max = this._getMax(name);

            if (min && (value < min)) {
                return langId(14) + ' "' + label + '" ������������ ���?�?�? �������?�?�� ������ �?�������� ' + min;
            } else if (max && (value > max)) {
                return langId(14) + ' "' + label + '" ������������ ���?�?�? �������?�?�� ' + max;
            } else {
                elem = $(name);
                if (elem.type == "text" || elem.type == "textarea")//�����?���?�?�? parsed �������?�������� �� ��������
                    elem.value = value;
            }
        }
    },

    validateFloat: function (name, label) {
        value = $F(name);

        //���� ���?�����?������ �� validateForm, �?.��. ���������? ���?���?�����?�?�?�? ���?�������?����
        if (this._isEmpty(value)) {
            return this._checkRequired(name, label);
        }

        value = parseFloat(value);
        if (isNaN(value))
            return langId(14) + ' "' + label + '" ���� �?�����?���?�?�? �?���?������';
        else {
            min = this._getMin(name);
            max = this._getMax(name);

            if (min && (value < min)) {
                return langId(14) + ' "' + label + '" ������������ ���?�?�? �������?�?�� ' + min;
            } else if (max && (value > max)) {
                return langId(14) + ' "' + label + '" ������������ ���?�?�? �������?�?�� ' + max;
            } else {
                elem = $(name);
                if (elem.type == "text" || elem.type == "textarea")//�����?���?�?�? parsed �������?�������� �� ��������
                    elem.value = value;
            }
        }
    },

    /**
     * �?�?�������?�?���? �?�����?���?�?�? ���� �?�����������? �����?�����?�����?���?��
     */
    _checkRequired: function (name, label) {
        elem = $(name);
        if (elem.getAttribute('vldRequired') != null) {
            //���?�����?������ �?�������?�������? �� ���������?�������?�?�� ���? �������� �?�����������?��.
            switch (elem.type) {
                //�����? radio ���?�����?�� �����?�������?�?����
                case 'radio':
                    chk = 0;
                    elements = Form.getInputs(this.form, 'radio', elem.name);
                    for (var i = 0; i < elements.length; i++) {
                        if (elements[i].checked)
                            chk++;
                    }
                    if (chk == 0)
                        return langId(14) + ' "' + label + '": ' + langId(15);
                    break;
                case 'select-one':
                case 'select':
                    return langId(14) + ' "' + label + '": ������������ ���?�?�? ���?���?������ �������?��������';
                    break;
                default:
                    return langId(14) + ' "' + label + '" �����?�����?�����?���� �����? �������������������?';
                    break;
            }
        }
    },

    _isEmpty: function (value) {
        if (value === null || typeof value == 'undefined')
            return true;
        if (typeof value == "string" &&
            value.trim() == "")
            return true;

        return false;
    },

    _getMin: function (name) {
        min = parseFloat($(name).getAttribute('vldMin'));
        return (typeof min == 'number') ? min : null;
    },

    _getMax: function (name) {
        max = parseFloat($(name).getAttribute('vldMax'));
        return (typeof max == 'number') ? max : null;
    }
}

/**
 * Global functions
 */
function fixEvent(e) {
    // �������?�?���?�? �����?�����? �?�����?�?���� �����? IE
    e = e || window.event

    // �������������?�? pageX/pageY �����? IE
    if (e.pageX == null && e.clientX != null) {
        var html = document.documentElement
        var body = document.body
        e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0)
        e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0)
    }

    // �������������?�? which �����? IE
    if (!e.which && e.button) {
        e.which = e.button & 1 ? 1 : ( e.button & 2 ? 3 : ( e.button & 4 ? 2 : 0 ) )
    }

    return e
}

function checkHideMenu(ev) {
    var is_ie = ( /msie/i.test(navigator.userAgent) && !/opera/i.test(navigator.userAgent) );
    var el = is_ie ? window.event.srcElement : ev.target;
    while (el.nodeType != 1) {
        if (typeof(el.parentNode) == 'undefined') { // IE bug
            return;
        }
        el = el.parentNode;
    }
    var elSrc = el;

    for (var i = 0; i < menuArrayIds.length; i++) {
        for (; el != null && el != $(menuArrayIds[i]); el = el.parentNode);
        if (el == null && $(menuArrayIds[i]) && $(menuArrayIds[i]).visible()) {
            if (menuArrayIds[i] == 'smartInputFloaterContent') {
                $('smartInputFloater').hide();
                if (elSrc.hasClassName('noHideWick')) {
                    return;
                }
                $('w_comment_off') ? $('w_comment_off').hide() : '';
                $('i_comment_off') ? $('i_comment_off').hide() : '';
            }
            else {
                $(menuArrayIds[i]).hide();
            }
            if ('help' == menuArrayIds[i] || 'grapth' == menuArrayIds[i] || 'to_group' == menuArrayIds[i] || 'add_description' == menuArrayIds[i]) {
                $('scrShadow').hide();
            }
            else if ('rest_plan_block_div' == menuArrayIds[i] && $('rest_plan_block_text')) {
                var plObj = $('rest_plan_block_text');
                plObj.setAttribute('id', plObj.getAttribute('iPlanId'));
            }
            break;
        }
        el = elSrc;
    }
}

function addMenuId(id) {
    for (var i = 0; i < menuArrayIds.length; i++) {
        if (menuArrayIds[i] == id) {
            return;
        }
    }
    menuArrayIds.push(id);
}

function langId(id) {
    return langData[id][langIdx];
}

/* 
 * ��?�����?���? removeSymbols �?�������?���? ���� �?�?�?������ value ���?�� ���?�������������? �?��������������
 * ���� �����?�?������ symbols.
 * �?�?���� �����?�������?�? symbols ���� ����������, ���� �?�������?�������? �?�������?�?�?�?�? �?�������?�� �������? ���?������������. 
 */
function removeSymbols(value, symbols) {
    if (value.length == 0) {
        return value;
    }

    if (!symbols || symbols.length == 0) {
        //unicode �������? �?�������������� �����? �?�����?�?�?���?���� ���� �?�������?�������? (�?�������?�� �������? ���?������������)
        symbols = [0x20, 0x2000, 0x2001, 0x2002, 0x2003, 0x2004, 0x2005, 0x2006, 0x2007,
            0x2008, 0x2009, 0x200A, 0x200B, 0x202F, 0x205F, 0x2060, 0x3000];
    }

    return value.split('')
        .filter(function (item) {
            return symbols.indexOf(item.charCodeAt(0)) == -1
        })
        .join('');
}

function calcSum(sum_elem_id, isNullAllowed, isObj) {
    var elemObj = ( isObj == true ? sum_elem_id : $(sum_elem_id));
    var value = elemObj.value;

    elemObj.focus();

    try {
        value = value.split(',').join('.');
        value = removeSymbols(value);

        // �����?���?�� ���?���� ���?�?����������, �?�?�����? ���� �?�?�����?�������?�? ������ �����?�?�����?���?������
        while (value.indexOf('0') == 0) {
            value = value.substr(1);
        }
        if (value.match(/,/i)) {
            value = 'ss'; // ��?�� �� ���?�����?�?�������� �?���������?���?�?.
        }
        value = value.replace(new RegExp('--', 'g'), '+');
        elemObj.value = eval(value);
        if (isNullAllowed == true && (elemObj.value == 0 || elemObj.value == 'undefined')) {
            elemObj.value = '';
            return true;
        }
        // �?����, �����?���?�?�� ������������ ���?���?�����?���?�?�? - 33.4*3 = 100.19999999999999
        var pPos = elemObj.value.indexOf(".");
        if (pPos > 0) {
            var mant = elemObj.value.substr(pPos);
            if (mant.length > 2) {
                var num = new Number(elemObj.value);
                elemObj.value = num.toFixed(2);
            }
        }
        // end of bug

        if (elemObj.value == '' || elemObj.value == 'undefined' || elemObj.value == 0) {
            elemObj.value = '';
            eval('ss'); // ���������?���� ���?�����?�?��������.
        }
    }
    catch (e) {
        if (!isNullAllowed) {
            alert(langId(20));
        }
        return false;
    }
    return true;
}

function toggleRoundDiv(divId) {
    Element.toggle(divId);

    var isVisible = Element.visible(divId);

    /**
     * ��?�� �?�?����
     */
    setVisible($(divId + '_form_tr'), isVisible);
    setVisible($('div_' + divId), !isVisible);
    setVisible($('div_open_' + divId), !isVisible);
    if ($('a_min_' + divId)) { // �������? �? �����?���?�?�? _ ���?�����?���?�����?�?�? �� "���?���?�?�?�?"
        setVisible($('a_min_' + divId), isVisible);
        setVisible($('a_cls_' + divId), isVisible);
    }
    if ('last_block' == divId) { // �?���?���?�? �����?���� ���?������������ #110
        displayFilterValue(isVisible);
    }

    /**
     * �? �?�?�� ���?����
     */
    setCookie(divId, isVisible ? "0" : "1", 3650);
    return false;
}

function setVisible(obj, isShow) {
    if (!obj) {
        return;
    }
    if (isShow) {
        obj.show();
        return;
    }
    obj.hide();
}

// �?�������?���? �?�����������?�� ���� �?���?������
function getOffset(elem) {
    if (elem.getBoundingClientRect) {
        // "���?���������?���?��" �����?�������?
        return getOffsetRect(elem)
    } else {
        // ���?�?�?�? �?�������?�����? �?���?�? ������-�?��
        return getOffsetSum(elem)
    }
}

function getOffsetSum(elem) {
    var top = 0, left = 0
    while (elem) {
        top = top + parseInt(elem.offsetTop)
        left = left + parseInt(elem.offsetLeft)
        elem = elem.offsetParent
    }

    return {top: top, left: left}
}

function getOffsetRect(elem) {
    // (1)
    var box = elem.getBoundingClientRect()

    // (2)
    var body = document.body
    var docElem = document.documentElement

    // (3)
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft

    // (4)
    var clientTop = docElem.clientTop || body.clientTop || 0
    var clientLeft = docElem.clientLeft || body.clientLeft || 0

    // (5)
    var top = box.top + scrollTop - clientTop
    var left = box.left + scrollLeft - clientLeft

    return {top: Math.round(top), left: Math.round(left)}
}

function screenSize() {
    var w, h; // �?���?�?�����?���� �����?�����������?��, w - ����������, h - ���?�?���?��
    w = (window.innerWidth ? window.innerWidth : (document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.offsetWidth));
    h = (window.innerHeight ? window.innerHeight : (document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.offsetHeight));
    return {w: w, h: h};
}

function positionElement(elemName) {
    $(elemName).style.top = document.body.scrollTop + screenSize().h / 2 - $(elemName).offsetHeight / 2 + 'px';
    $(elemName).style.left = screenSize().w / 2 - $(elemName).offsetWidth / 2 + 'px';
}

//drop down menu
function chSumDown(sumId) {
    if (sumId == 'let_title') {
        $(sumId).className = sumId + ' wickEnabled';
        return;
    }
    if (sumId.indexOf('var_own_') != -1) {
        $(sumId).className = 'let_title';
        return;
    }
    $(sumId).className = 'sum_input';
}
function chSumUp(sumId) {
    if (sumId == 'let_title') {
        $(sumId).className = $(sumId).value.length ? sumId + ' wickEnabled' : sumId + ' wickEnabled let_bg';
        return;
    }
    if (sumId.indexOf('var_own_') != -1) {
        $(sumId).className = $(sumId).value.length ? 'let_title' : 'let_title let_bg';
        var tVisible = $(sumId).value.length ? '' : 'none';
        $(sumId).nextSibling.nextSibling.nextSibling.style.display = tVisible;
        if (!isVoted) {
            $('vote_but').disabled = $(sumId).value.length ? false : true;
        }
        return;
    }
    $(sumId).className = $(sumId).value.length ? 'sum_input' : 'sum_input ' + langId(19);
}

function toggleDashList(divId, isHide, customTextEl) {
    typeof(isHide) != 'undefined' && isHide ? Element.hide(divId + '_div') : Element.toggle(divId + '_div');
    $(divId + '_div').visible() ? positionDash(divId, customTextEl) : '';

    $(divId + '_div').visible() ? positionDash(divId, customTextEl) : '';
    return false;
}

function positionDash(divId, customTextEl) {
    /**
     * �?�?�����?�����?�?���� �������� ������������ �������?�? �?������ ���� �?�������?�����? ���� ���������?���������?�����������?,
     * �?�?�� �� "*_text" �?�����������? ���� �����?���?�?�� �����������?����. �?�?���� �?�?�� �?�����������? �? �������?�?����
     * limited, �?�� �����?�?�� �?�������?�?�?���� ���� ������.
     */
    var textEl = typeof(customTextEl) != 'undefined' ? customTextEl : divId + '_text';
    var offsetParent = $(textEl).offsetParent;
    var offsParPos = {top: 0, left: 0};
    if ($(textEl).offsetParent.className.indexOf('limited') != -1 || 'all_date' == divId) {
        offsetParent = $(textEl).offsetParent.offsetParent;
        offsParPos.top = $(textEl).offsetParent.offsetTop;
        offsParPos.left = $(textEl).offsetParent.offsetLeft;
    }

    offsetParent.appendChild($(divId + '_div'));

    /**
     * �?�������?���? �?�����������?�� ���� �����?���?�?�� �����������?����, ���?�����?���?�����?���� �������������?������
     * ���������?���������?�?�?�?������ �?�������?�����? (body, relative/absolute �� table/td/th ���?����
     * �?�����������? �?���� ���� relative/absolute).
     */
    var pos = {top: $(textEl).offsetTop + offsParPos.top, left: $(textEl).offsetLeft + offsParPos.left};

    /**
     * ������?�������� �?�����������?�� _sp ���?�����?���?�����?���� �?�����������?�� _div
     */
    var divPos = getOffset($(divId + '_div'));
    var spPos = getOffset($(divId + '_sp'));
    var offset = {top: (spPos.top - divPos.top), left: (spPos.left - divPos.left)};

    /**
     * �?���?�?�?���? �����? ���������������?�?.
     */
    if ('all_date' == divId) {
        offset.top -= (Prototype.Browser.Opera ? ($('tplLimedDiv') ? -1 : 0) : ($('tplLimedDiv') ? 0 : 1));
        offset.left -= (Prototype.Browser.Opera ? -1 : ($('tplLimedDiv') ? 0 : 1));
    }
    /**
     * �?���?�?�?���? �����? �?���?�������������� �?���������?�? �? ������������
     */
    if ('rest_plan_block' == divId || divId.indexOf('_limed') > 0) {
        offset.left += $('plnd_toscroll').scrollLeft;
    }
    $(divId + '_div').style.left = pos.left - offset.left + 'px';
    $(divId + '_div').style.top = pos.top - offset.top + 'px';
}

function selDashItem(divId, elemId) {
    var elName = $(divId + '_a_' + elemId).innerHTML;
    if (divId.indexOf('currency') != -1) {
        elName = elName.substr(0, 3);
    }
    $(divId + '_text').innerHTML = elName;
    $(divId + '_sp').innerHTML = elName;
    Element.hide(divId + '_div');
    $(divId).value = elemId;
    var pref = (
        divId.indexOf('w_') == 0 ? 'w' : (
                divId.indexOf('i_') == 0 ? 'i' : (
                        divId.indexOf('m_') == 0 ? 'm' : ''
                    )
            )
    );
    if (pref) {
        setFocusTimeout(pref);
    }
    if ($(divId + '_i1') && $(divId + '_i2')) {
        var plIcon = ( (typeof placeIcons[elemId] == "undefined") ? '' : placeIcons[elemId] );
        var icSrc = plIcon <= 20 ? 'img/pl' + plIcon + '.png' : 'gif.htm?icId=' + plIcon;
        $(divId + '_i2').src = icSrc;
        $(divId + '_i1').src = icSrc;
    }
    if (divId == 'm_from' && $('m_komis_text')) {
        selDashItem('m_komis', elemId);
    }
    if (divId == 'rules_from_id') {
        selDashItem('rules_komissia_acc', elemId);
    }
    return false;
}
//change color of element of menu over which mouse is
function grayElem(id, isHide, color) {

    if (!$(id)) {
        return false;
    }
    var curBg = $(id).style.backgroundColor;
    if (curBg == '#999966' || curBg == '#996' || curBg == 'rgb(153, 153, 102)' || $(id).className.indexOf('tagSel') != -1) {
        return false;
    }
    if (typeof(color) == 'undefined') {
        color = '#eee';
    }
    $(id).style.backgroundColor = (isHide ? 'transparent' : color);
    return true;
}

function grayElemYell(id, isHide) {
    if (!grayElem(id, isHide)) {
        return;
    }
    $('lim' + id).style.backgroundImage = (isHide ? 'url(img/opacityYell.png)' : 'url(img/opacityGray.png)');
}

function ajaxRequest(param, successId, url, complete, success, failure) {
    url = typeof(url) == 'undefined' || !url ? gbPrivateUrl : (gbPrivateUrl + url);
    sId = typeof(successId) == 'undefined' ? null : successId;

    var allParam = {
        method: 'post',
        onFailure: function () {
            LangId(11);
        },
        evalScripts: true
    };
    typeof(param) == 'undefined' || !param ? '' : allParam['parameters'] = param;
    typeof(complete) == 'undefined' || !complete ? '' : allParam['onComplete'] = complete;
    typeof(success) == 'undefined' || !success ? '' : allParam['onSuccess'] = success;
    typeof(failure) == 'undefined' || !failure ? '' : allParam['onFailure'] = failure;

    var myAjax = sId ? new Ajax.Updater(sId, url, allParam) : new Ajax.Request(url, allParam);
}

/**
 * Common private functions
 */
var gbDactId = null;
var gbPrivateUrl = '';
var gbSettings = {plnChk: {}, tgldet: {}, wick: {}, autocomplete: {}};
var gbLastCorrId = null;
var gbTgBlockMouseOn = {wm_tg: 0, im_tg: 0, mm_tg: 0, chm_tg: 0, wm_tpl: 0, im_tpl: 0, mm_tpl: 0, chm_tpl: 0};
var gbTgBlockHeight = 0;
var moreLazyContainer = {w: '', i: '', m: '', ch: ''};
var gbDutyContainer = [];
var gbGrpContainer = [];
var gbIsMobile = false;
var gbTypedDutySrch = ['', ''];
var gbTypedGrpSrch = ['', ''];
var gbDutyHdnCount = [0, 0];
var gbEditRecordId = null;
var gbEditData = '';
var gbMoreParElems = {
    w_m_par_cloud: 'w_short_categ',
    w_m_par_cloud: 'w_short_categ',
    i_m_par_cloud: 'i_short_categ',
    w_m_par_tags: 'w_short_tags_form_tr',
    m_m_par_tags: 'm_short_tags_form_tr',
    ch_m_par_tags: 'ch_short_tags_form_tr',
    i_m_par_tags: 'i_short_tags_form_tr',
    w_m_par_tpl: 'w_short_tpl_form_tr',
    m_m_par_tpl: 'm_short_tpl_form_tr',
    ch_m_par_tpl: 'ch_short_tpl_form_tr',
    i_m_par_tpl: 'i_short_tpl_form_tr',
    w_m_par_checks: 'ord_chks',
    m_m_par_kom: 'm_short_kom_form_tr'
};
function updateDate(is_cancel) {
    var tab = getVisibleTab();
    var smPref = getPrefByTab(tab);

    Element.hide('all_date_div');
    toggleWaitMode(smPref, true);

    var plnEditRem = '';
    var save_par = '';
    if (is_cancel == 'undefined' || is_cancel == null) {
        // �������������?���?����
        var color = $(smPref + '_comment').style.color;
        if (color == 'gray' || color == '#808080') {
            $(smPref + '_comment').value = '';
        }
        var rDate = ( $('rest_date') ? '&restDate=' + $('rest_date').value : '');
        save_par = '&' + smPref + '_sum1=' + encodeURIComponent($('sum_tab_' + bigPref(smPref)).value) + '&' + smPref + '_from1=' + $(smPref + '_from').value + '&' + smPref + '_comment1=' + encodeURIComponent($(smPref + '_comment').value) + rDate + savePar(smPref, true) + futurePar(smPref, '1');
    }
    else {
        plnEditRem = ($(bigPref(smPref) + '_id').value && $('pln_edit_remember')) ? '&pln_edit_remember=' + $('pln_edit_remember').value : '';
    }

    var corrPar = '';
    if (gbLastCorrId && $('corr_rest_sum') && $('corr_rest_sum').value != '') {
        corrPar = '&corr_rest_sum=' + encodeURIComponent($('corr_rest_sum').value) + '&corr_rest_plid=' + gbLastCorrId;
    }

    var calTfObj = $(getVisibleTab() + 'CalDay');
    var isTypeFuture = false;
    if (calTfObj && calTfObj.getAttribute('istf') == 1) {
        isTypeFuture = true;
    }
    var successId = 'operBlock';
    var urlPref = '';
    if ($('tplLimedDiv')) {
        var plnBlkPref = $('tplLimedDiv').getAttribute('pref');
        successId = plnBlkPref + '_ptadd';
        plnEditRem += '&is_force_planned=true';
        urlPref = bigPref(smPref, true);
        $(plnBlkPref + '_limed_text').setAttribute('date', dateToStr(gbFormCalander.date));
    }

    ajaxRequest(
        'action=show_date&all_date=' + dateToStr(gbFormCalander.date) + '&all_date_h=' + $('all_date_h').value + '&all_date_m=' + $('all_date_m').value + '&all_date_close=' + $('all_date_close').checked + save_par + corrPar + plnEditRem,
        successId,
        urlPref,
        function () {
            Element.hide(smPref + '_wait');
            if ($('observe_date') && $('observe_date').checked) {
                toggle_div(bigPref(smPref));
            }
            if ($(smPref + '_future_type_plan') && isTypeFuture) {
                $(smPref + '_future_type_plan').checked = true;
                $(smPref + '_planned_param').show();
            }
            setFocusTimeout(smPref);
        }
    );

    return false;
}
function hourFocus(e) {
    switch (e.keyCode) {
        case 9:
        case 39:
        case 37:
            return;
    }
    $('all_date_m').value = $('all_date_m').value.replace(new RegExp("[^0-9]", 'gi'), '');
    if ($('all_date_m').value > 59) {
        $('all_date_m').value = 59;
    }
    if (13 == e.keyCode) {
        return setFormTime();
    }
    if (8 == e.keyCode && 0 == $('all_date_m').value.length && !gbIsMobile) {
        selInputText('all_date_h', -1);
    }
}
function minFocus(e) {
    switch (e.keyCode) {
        case 9:
        case 8:
        case 39:
        case 37:
            return;
    }
    $('all_date_h').value = $('all_date_h').value.replace(new RegExp("[^0-9]", 'gi'), '');
    if ($('all_date_h').value > 23) {
        $('all_date_h').value = 23;
    }
    if ($('all_date_h').value.length == 2 && !gbIsMobile) {
        selInputText('all_date_m', 0);
    }
    if (13 == e.keyCode) {
        return setFormTime();
    }
}
function setFormTime() {
    if ($('all_date_m').value && !$('all_date_h').value) {
        alert('�?���������?�� �?���?')
        return;
    }
    if ($('all_date_h').value && !$('all_date_m').value) {
        alert('�?���������?�� �������?�?�?')
        return;
    }
    var min = $('all_date_m').value.length == 1 ? '0' + $('all_date_m').value : $('all_date_m').value;
    var spVal = ', ' + $('all_date_h').value + ':' + min;
    if (!$('all_date_m').value && !$('all_date_h').value) {
        spVal = '';
    }
    $$('span[id$=_set_time]').each(function (element) {
        element.innerHTML = spVal;
    });
    Element.hide('all_date_div');
    setFocusTimeout(getPrefByTab(getVisibleTab()));
    if (!$('all_date_close').checked) {
        updateDate();
    }
    return;
}
function getVisibleTab() {
    if ($('tplLimedDiv')) {
        var smPref = $('tplLimedDiv').getAttribute('smPref')
        return bigPref(smPref);
    }
    for (var key in img_tab_names) {
        if (!img_tab_names.hasOwnProperty(key)) {
            continue;
        }
        if (Element.visible('tab_' + img_tab_names[key] + '_div')) {
            return img_tab_names[key];
        }
    }
    return '';
}
function getPrefByTab(tab) {
    switch (tab) {
        case 'waste':
            return 'w';
        case 'income':
            return 'i';
        case 'move':
            return 'm';
        case 'change':
            return 'ch';
        default:
            alert('��?��-�?�� �����?���� ���� �?����.\n ��������?���?��, �����������?���?�?��, ���� �?�?���� �?�����?�������?�?�������� ���� �?���?�?����.');
            return false;
    }
}
function setAllDate(increment, isPopup) {
    var date = new Date();
    if (0 != increment) {
        date = new Date(gbFormCalander.date.getTime() + increment * 1000 * 60 * 60 * 24);
    }
    gbFormCalander.setDate(date);
    if (!(isPopup && !$('all_date_close').checked)) {
        return updateDate();
    }
    return false;
}
function allDateChanged(calendar) {
    if (calendar.dateClicked && $('all_date_close').checked) {
        updateDate();
    }
}
function calendarBlock(obj, isTypeFuture) {
    if (gbFormCalander) {
        gbFormCalander.refresh();
    }
    if ($('all_date_text')) {
        $('all_date_text').removeAttribute('id');
    }
    if ($(obj.parentNode).getAttribute('id') != 'all_date_sp') {
        $(obj.parentNode).setAttribute('id', 'all_date_text');
    }

    $('calDay').innerHTML = gbCalDay['day'];
    $('calMonth').innerHTML = gbCalDay['month'];

    toggleDashList('all_date');

    obj.removeAttribute('istf');
    if (Element.visible('all_date_div')) {
        if (typeof(isTypeFuture) != 'undefined' && isTypeFuture) {
            obj.setAttribute('istf', '1');
        }
        !gbIsMobile ? selInputText('all_date_h', 0) : '';
    }
}
function screenCheck(checkId) {
    var fullWidth = screenSize().w - (Prototype.Browser.IE ? 25 : 20);
    var pos = getOffset($('chk_divr_' + checkId));

    if ($('chk_imr_' + checkId).width == fullWidth) {
        $('chk_imr_' + checkId).style.border = '0px solid #ccc';
        $('chk_imr_' + checkId).style.position = 'relative';
        $('chk_imr_' + checkId).style.left = 0;
        $('chk_imr_' + checkId).width = 380;
    }
    else {
        $('chk_imr_' + checkId).style.border = '1px solid #ccc';
        $('chk_imr_' + checkId).style.position = 'absolute';
        $('chk_imr_' + checkId).style.left = '-' + pos.left + 'px';
        $('chk_imr_' + checkId).width = fullWidth;
    }
    return false;
}

function bigPref(smPref, isMod) {
    switch (smPref) {
        case 'w':
            return (isMod == true ? 'Waste' : 'waste');
        case 'i':
            return (isMod == true ? 'Income' : 'income');
        case 'm':
            return (isMod == true ? 'Move' : 'move');
        case 'ch':
            return (isMod == true ? 'Change' : 'change');
        default:
            return 'undef';
    }
}

function span_check(smPref, id, noUnsel) {
    if ($(smPref + '_duty_person').value.length > 0) {
        return false;
    }
    var allSpans = $(smPref + '_short_categ').getElementsByTagName('span');
    var curSp = $(smPref + '_sh' + id);
    var curBg = '';
    if (curSp) {
        curBg = curSp.style.background;
    }
    for (var i = 0; i < allSpans.length; i++) {
        allSpans[i].style.background = '';
        allSpans[i].style.color = 'gray';
    }

    if (noUnsel != true && (curBg == '#999966' || curBg == '#996' || curBg == 'rgb(153, 153, 102)')) {
        id = '';
    }
    if (id && curSp) {
        $(smPref + '_sh' + id).style.color = '#fff';
        $(smPref + '_sh' + id).style.background = '#996';
    }
    $(smPref + '_category').value = id;
    Event.fire($(smPref + '_category'), "chosen:updated");
    setFocusTimeout(smPref);
    return false;
}

function duty_check(smPref) {
    var dutyPerson = $(smPref + '_duty_person').value.trim();
    dutySrchHlt(dutyPerson, smPref);
    if (dutyPerson.length > 0) {
        if ($(smPref + '_duty_fttl')) {
            $(smPref + '_duty_fspn').innerHTML = dutyPerson;
            if ($(smPref + '_duty_fttl').visible()) {
                return;
            }
        }
        $(smPref + '_category_chosen').hide();
        $(smPref + '_category').removeAttribute('vldRequired');
        toggleAddCategory(smPref);
        var htmTpl = '<div id="' + smPref +
            '_duty_fttl" style="width:332px; height:23px; padding-top:2px; overflow:hidden; white-space: nowrap;">' + (smPref == 'w' ? '�?���?��' : '�?���?�?��') +
            ' �� ��������' + (smPref == 'i' ? ' �?' : '') + ': <span class="dash tGray" onclick="toggleDutyPanel(\'' + smPref + '\')" id="' + smPref + '_duty_fspn">' + dutyPerson + '</span></div>';
        $(smPref + '_category').insert({after: htmTpl});
    }
    else {
        $(smPref + '_category').setAttribute('vldRequired', 'vldRequired');
        $(smPref + '_duty_fttl') ? $(smPref + '_duty_fttl').hide() && $(smPref + '_category_chosen').show() : '';
    }
}

function dutySrchHlt(typed, smPref) {
    if (gbDutyContainer.length == 0 || gbTypedDutySrch[0] == typed || (gbTypedDutySrch[1] == typed && typed)) {
        return;
    }
    var posStart, dontSelect, visHidden = 0, hdnHidden = 0;
    var curSpan, curSubstr;
    dontSelect = (gbTypedDutySrch[0].length > typed.length);
    gbTypedDutySrch[0] = typed;

    gbDutyContainer.each(function (item) {
        posStart = item['name'].toLowerCase().indexOf(typed.toLowerCase());
        curSpan = $(smPref + '_duty_item_' + item['id']);
        if (posStart != 0 && typed) {
            $(curSpan.parentNode).hide();
            item['is_hidden'] == 't' ? hdnHidden++ : visHidden++;
        }
        else {
            $(curSpan.parentNode).show();
        }
        if (posStart == 0) {
            curSubstr = item['name'].substr(posStart, typed.length);
            curSpan.innerHTML = item['name'].replace(curSubstr, '<span class="found">' + curSubstr + '</span>');
            if (!dontSelect) {
                gbTypedDutySrch[1] = typed + item['name'].substr(posStart + typed.length);
                $(smPref + '_duty_person').value = gbTypedDutySrch[1];
                selInputText(smPref + '_duty_person', posStart + typed.length);
                dontSelect = true;
            }
        }
    });
    if (!gbDutyHdnCount[1]) {
        return;
    }
    if (visHidden == gbDutyHdnCount[0] && hdnHidden == gbDutyHdnCount[1]) {
        $('hdn_duty_' + smPref + '_ttl').hide();
        $('arrow_hdn_duty_' + smPref).hide();
    }
    else {
        $('hdn_duty_' + smPref + '_ttl').show();
        $('arrow_hdn_duty_' + smPref).show();
    }
    if (visHidden == gbDutyHdnCount[0] && hdnHidden != gbDutyHdnCount[1] && !$('hdn_duty_' + smPref).visible()) {
        toggleDetails('hdn_duty_' + smPref, true);
    }
}

function reloadAjaxModule(divId, url) {
    Element.show('retry_ind');
    $('retry_but').disabled = true;

    url = url.substr(url.indexOf('v2_homeBuhPrivate') + ('v2_homeBuhPrivate').length);

    ajaxRequest(null, divId, url, divId, function () {
        Element.hide('error');
        Element.hide('retry_ind');
        $('retry_but').disabled = false;
    });
}

function setFocusTimeout(smPref) {
    setTimeout('my_focus("' + smPref + '")', 300);
    if (!$(smPref + '_category')) {
        return;
    }
    toggleAddCategory(smPref)
}
function toggleAddCategory(smPref) {
    var addTr = 'add_' + smPref + '_category';
    var addName = 'add_' + smPref + '_category_name';
    if ($(smPref + '_category').value == -5) {
        Element.show(addTr);
        $(addName).setAttribute('vldRequired', 'vldRequired');
        $(addName).setAttribute('vldMax', '32');
        $(addName).setAttribute('vldMin', '2');
    }
    else {
        Element.hide(addTr);
        $(addName).removeAttribute('vldRequired');
        $(addName).removeAttribute('vldMax');
        $(addName).removeAttribute('vldMin');
    }
}

function my_focus(smPref) {
    var elemName = 'sum_tab_' + bigPref(smPref);
    try {
        if ($('sum_tab_' + bigPref(smPref)).value) {
            elemName = smPref + '_comment';
        }
        if ($(smPref + '_category') && $(smPref + '_category').value == -5) {
            elemName = 'add_' + smPref + '_category_name';
        }
        if ($(elemName).visible()) {
            $(elemName).focus();
        }
    }
    catch (e) {/*�?�?���� �����?�������?�?������ ���� ���?�?���?�? �������������? ������ �����? �������? �? �������������?����.*/
    }
}

function focusComment(smPref) {
    var elName = (smPref == 'm_komissia_sum' ? smPref : smPref + '_comment');
    var color = $(elName).style.color;
    if (color == 'gray' || color == '#808080') {
        $(elName).style.color = 'black';
        $(elName).value = '';
    }
    return true;
}

function komisValidation() {
    if (!$('m_komissia_category')) {
        return false;
    }

    $('m_komissia_category').removeAttribute('vldRequired');

    if ($('m_short_kom_form_tr') && $('m_short_kom_form_tr').visible()) {
        calcSum('m_komissia_sum', true);
        if ($('m_komissia_sum').value) {
            $('m_komissia_category').setAttribute('vldRequired', 'vldRequired');
        }
    }

    return false;
}
function setMoreParText(id, checked, pref) {
    if (!$(id)) {
        return;
    }
    $(id + '_lb').innerHTML = '<span class="ch"></span>' + (checked ? langId(21) : langId(22));
    $(id + '_lb').title = (checked ? langId(23) : langId(24));
    $(id).checked = checked;

    switch (id) {
        case 'i_m_par_tags':
        case 'w_m_par_tags':
        case 'm_m_par_tags':
        case 'ch_m_par_tags':
            if (checked) {
                $(pref + '_form_tags_td').appendChild($(pref + '_short_tags'));
                $('m_tg_h_div_' + pref).show();
                $('b_mnmx_' + pref + '_short_tags').show();
                $(pref + '_short_tags').hide();
                toggleRoundDiv(pref + '_short_tags');
            }
            else {
                if ($(pref + '_short_tags').parentNode.id != pref + '_more_tags_td') {
                    $(pref + '_more_tags_td').appendChild($(pref + '_short_tags'));
                    $('m_tg_h_div_' + pref).hide();
                    if ($(pref + '_short_tags_form_tr').visible()) {
                        toggleRoundDiv(pref + '_short_tags');
                    }
                }
                $('b_mnmx_' + pref + '_short_tags').hide();
                $(pref + '_short_tags').show();
                updateCheckedTags(pref);
            }
            break;
        case 'i_m_par_tpl':
        case 'w_m_par_tpl':
        case 'm_m_par_tpl':
        case 'ch_m_par_tpl':
            if (checked) {
                $(pref + '_form_tpl_td').appendChild($(pref + '_short_tpl'));
                $('m_tpl_h_div_' + pref).show();
                $('b_mnmx_' + pref + '_short_tpl').show();
                $(pref + '_short_tpl').hide();
                toggleRoundDiv(pref + '_short_tpl');
            }
            else {
                if ($(pref + '_short_tpl').parentNode.id != pref + '_more_tpl_td') {
                    $(pref + '_more_tpl_td').appendChild($(pref + '_short_tpl'));
                    $('m_tpl_h_div_' + pref).hide();
                    if ($(pref + '_short_tpl_form_tr').visible()) {
                        toggleRoundDiv(pref + '_short_tpl');
                    }
                }
                $('b_mnmx_' + pref + '_short_tpl').hide();
                $(pref + '_short_tpl').show();
            }
            break;
        case 'm_m_par_kom':
            if (checked) {
                $(pref + '_form_kom_td').appendChild($(pref + '_short_kom'));
                $('m_kom_h_div_' + pref).show();
                $('b_mnmx_' + pref + '_short_kom').show();
                $(pref + '_short_kom').hide();
                toggleRoundDiv(pref + '_short_kom');
            }
            else {
                if ($(pref + '_short_kom').parentNode.id != pref + '_more_kom_td') {
                    $(pref + '_more_kom_td').appendChild($(pref + '_short_kom'));
                    $('m_kom_h_div_' + pref).hide();
                    if ($(pref + '_short_kom_form_tr').visible()) {
                        toggleRoundDiv(pref + '_short_kom');
                    }
                }
                $('b_mnmx_' + pref + '_short_kom').hide();
                $(pref + '_short_kom').show();
            }
            break;
    }
}

function updateMoreParMenu(pref, isDontCall) {
    if (!$(pref + '_more_lazy_div').innerHTML) {
        getLazyMore(pref);
        return;
    }
    if (
        typeof(isDontCall) === 'undefined' &&
        (!$(pref + '_short_tags') || !$(pref + '_short_tpl') || (pref == 'm' && !$(pref + '_short_kom')))
    ) {
        // �?�����?: �������?�?���� ���� ���?���?�?�����? �������� �� �?���?����, ���������� �?�?���?�?, ���?���?�?���� �������? - �������� �����?���?�?��.
        $(pref + '_more_lazy_div').innerHTML = '';
        moreLazyContainer[pref] = '';
        getLazyMore(pref, true);
        return;
    }
    var checked;
    for (key in gbMoreParElems) {
        if (key.indexOf(pref + '_m_par_') != 0) continue;
        checked = $(gbMoreParElems[key]).visible();
        setMoreParText(key, checked, pref);
    }
}

function updateCheckedTags(pref) {
    // ���?�������?���� �?������
    var comment = $(pref + '_comment').value;
    for (var i = 0; i < tagList.length; i++) {
        var key = tagList[i].id;
        if (!$('m_tg' + pref + key)) {
            continue;
        }
        if (comment.indexOf('[' + tagList[i].name + ']') != -1) {
            colorElement($('m_tg' + pref + key), true);
        }
        else {
            colorElement($('m_tg' + pref + key), false);
        }
    }
}

function tgTdHeight(pref, isIncrease, event, parPref) {
    var totPref = pref + parPref;
    var el, curDivH = $(parPref + '_div_' + pref).getHeight();
    var tgOrTpl = (parPref == 'm_tg' ? 'tags' : 'tpl');
    var parNode = $($(pref + '_short_' + tgOrTpl).parentNode);

    if (isIncrease && curDivH <= maxListHeight) {
        gbTgBlockMouseOn[totPref] = true;
        setTimeout(function () {
            var totHeight = $(parPref + '_div_' + pref).scrollHeight;
            if (gbTgBlockMouseOn[totPref] && totHeight >= maxListHeight) {
                var parHeight = parNode.getHeight();
                if (!isNaN(parseInt(parNode.style.paddingBottom))) {
                    parHeight -= parseInt(parNode.style.paddingBottom);
                }
                if (!isNaN(parseInt(parNode.style.paddingTop))) {
                    parHeight -= parseInt(parNode.style.paddingTop);
                }

                parNode.style.height = parHeight + 'px';
                $(pref + '_short_' + tgOrTpl).style.position = 'absolute';
                $(parPref + '_div_' + pref).style.height = totHeight + 'px';
                $(parPref + '_div_' + pref).style.maxHeight = totHeight + 'px';
                gbTgBlockHeight = totHeight;
            }
        }, 500);
    }
    else if (!isIncrease) {
        el = event.toElement || event.relatedTarget;
        while (el) {
            if (el.id == (parPref + '_td_' + pref)) {
                return;
            }
            el = el.parentNode;
        }
        gbTgBlockMouseOn[totPref] = false;
        if (curDivH > maxListHeight) {
            $(parPref + '_div_' + pref).style.height = maxListHeight + 'px';
            $(parPref + '_div_' + pref).style.maxHeight = maxListHeight + 'px';
            $(pref + '_short_' + tgOrTpl).style.position = 'relative';
            parNode.style.height = '';
            gbTgBlockHeight = 0;
        }
    }
}

function toggleOnOff(obj, pref) {
    setMoreParText(obj.id, obj.checked, pref);
    moreLazyContainer[pref] = '';

    switch (obj.id) {
        case 'w_m_par_checks':
            toggleRoundDiv('ord_chks');
            if ($('ord_chks').visible() && $('ord_chks').innerHTML == '') {
                showChkBlock();
            }
            break;
        case 'w_m_par_cloud':
        case 'i_m_par_cloud':
            toggleRoundDiv(pref + '_short_categ');
            break;
    }
}

//function toggleDplc(pref, clkObj)
//{
//	var addHref = $('addPlcA' + pref).href;
//	$('div_dplc_list_' + pref).toggle();
//	if ($('div_dplc_list_' + pref).visible()) {
//		$(clkObj).className = 'down';
//		$('addPlcA' + pref).innerHTML = langId(26);
//		$('addPlcA' + pref).href = addHref.replace('tab=place','tab=duty');
//	}
//	else {
//		$(clkObj).className = 'right';
//		$('addPlcA' + pref).innerHTML = langId(25);
//		$('addPlcA' + pref).href = addHref.replace('tab=duty','tab=place');
//	}
//}

function focusKomSum() {
    if (!$('m_short_kom_form_tr').visible() && $('m_short_kom').parentNode.id == 'm_more_kom_td') {
        $('m_m_par_kom').checked = true;
        toggleOnOff($('m_m_par_kom'), 'm');
        toggleDashList('m_more_param', true);
    }
    focusComment('m_komissia_sum');
    setTimeout(function () {
        $('m_komissia_sum').focus();
    }, 100);
    return true;
}

function savePar(smPref, isUpdate) {
    var pref1 = '';
    if (isUpdate == true) {
        pref1 = '1';
    }
    switch (smPref) {
        case 'w':
        case 'i':
            return '&' + smPref + '_currency' + pref1 + '=' + $(smPref + '_currency').value + '&' + smPref + '_category' + pref1 + '=' + $(smPref + '_category').value + '&' + smPref + '_duty_person' + pref1 + '=' + encodeURIComponent($(smPref + '_duty_person').value);
        case 'm':
            var komiss = '';
            if ($('m_short_kom_form_tr').visible() && (isUpdate || $('m_komissia_category').value)) {
                komiss = '&m_komissia_category' + pref1 + '=' + $('m_komissia_category').value + '&m_komissia_sum' + pref1 + '=' + encodeURIComponent($('m_komissia_sum').value) + '&m_komis' + pref1 + '=' + $('m_komis').value;
            }
            return '&m_currency' + pref1 + '=' + $('m_currency').value + '&m_to' + pref1 + '=' + $('m_to').value + komiss;
        case 'ch':
            var chTo = '';
            if ($('ch_to')) {
                chTo = '&ch_to' + pref1 + '=' + $('ch_to').value;
            }
            return '&ch_sum_to' + pref1 + '=' + $('ch_sum_to').value + '&ch_to_currency' + pref1 + '=' + $('ch_to_currency').value + '&ch_from_currency' + pref1 + '=' + $('ch_from_currency').value + chTo;
        default:
            return 'undef';
    }
}

function futurePar(smPref, p1) {
    var futurePar = $(smPref + '_edit_ftype') ? '&edit_ftype=' + $(smPref + '_edit_ftype').value : '';
    if ($(smPref + '_ftype_div').visible()) {
        var ftype = $(smPref + '_future_type_plan').checked ? 'plan' : ( $(smPref + '_future_type_real').checked ? 'real' : '' );
        ftype = ftype ? '&' + smPref + '_future_type' + p1 + '=' + ftype : '';
        futurePar += ftype + '&' + smPref + '_days_before' + p1 + '=' + $(smPref + '_days_before').value + '&' + smPref + '_pln_period' + p1 + '=' + $(smPref + '_pln_period').value + '&' + smPref + '_period_n' + p1 + '=' + $(smPref + '_period_n').value + '&' + smPref + '_is_remind' + p1 + '=' + $(smPref + '_is_remind').checked;
    }
    return futurePar;
}

function toggleWaitMode(smPref, isShowWait) {
    if (isShowWait) {
        $(smPref + '_but').disabled = true;
        Element.show(smPref + '_ind');
        Element.show(smPref + '_wait');
        Element.hide(smPref + '_edit');
    }
    else {
        $(smPref + '_but').disabled = false;
        Element.hide(smPref + '_ind');
        Element.hide(smPref + '_wait');
    }
}

function colorElement(el, active) {
    el = el.firstChild;
    if (active) {
        el.style.color = '#fff';
        el.style.background = '#996';
    }
    else {
        el.style.color = '#555';
        el.style.background = '';
    }
}

function pushTpl(smPref, id) {
    if (typeof(tplList[id]) === 'undefined') {
        getTplList(smPref, id);
        return false;
    }
    // ���?�������������� ���������������? �?������������ �� �?�����?����
    for (var tplId in tplList) {
        if (tplId == id) {
            colorElement($('m_tpl' + smPref + id), true);
        }
        else if ($('m_tpl' + smPref + tplId)) {
            colorElement($('m_tpl' + smPref + tplId), false);
        }
    }
    // ���?���?�?�������?���� ���?�?�����?�?�? �?������������ �� �������?
    var isKomFound = false;
    for (var prop in tplList[id]) {
        if (tplList[id].hasOwnProperty(prop)) {
            // ���?�?�?�?�� �������?�������? �������� �?�? ���?�?�?�������?����
            if (!tplList[id][prop]) {
                continue;
            }
            if (prop.indexOf('m_komissia_sum') == 0) {
                $('m_komissia_sum').style.color = 'black';
            }
            if (prop.indexOf('m_komis') == 0) {
                isKomFound = true;
                if (!$('m_short_kom') && !$('m_more_lazy_div').innerHTML) {
                    getLazyTplParam(smPref, id);
                    return;
                }
            }
            if (prop == 'ch_to') {
                if (!$('ch_to') && !$('ch_more_lazy_div').innerHTML) {
                    getLazyTplParam(smPref, id);
                    return;
                }
            }
            if ($(prop + '_text')) {
                selDashItem(prop, tplList[id][prop]);
            }
            if (prop.indexOf('_comment') != -1) {
                $(smPref + '_comment').style.color = 'black';
                $(smPref + '_comment').value = '';
            }
            $(prop).value = tplList[id][prop];
            if (prop.indexOf('sum_') != -1) {
                $(prop).className = 'sum_input';
            }
            prop.indexOf('m_komissia_category') != -1 ? Event.fire($('m_komissia_category'), "chosen:updated") : '';
            if (prop.indexOf('_category') != -1 && (smPref == 'w' || smPref == 'i')) {
                span_check(smPref, tplList[id][prop], true);
            }
        }
    }
    // �?�?���� �?�?�� �����?�������?�������? �� ���?�?�? ���������?�?���? - ���? �������� ���?�����?�������?�? �� �?���?����
    if (smPref == 'm' && isKomFound && !$('m_short_kom_form_tr').visible()) {
        if ($('m_short_kom')) {
            if ($('m_short_kom').parentNode.id == 'm_form_kom_td') {
                toggleRoundDiv('m_short_kom');
            }
            else {
                $('m_m_par_kom').checked = true;
                toggleOnOff($('m_m_par_kom'), 'm');
            }
        }
    }
    $(smPref + '_comment').focus();

    // ����?�?�������� �������?�?�? �����?���� ���?�����?�� �?������������, ���?���� ������ ���� �� �?���?����
    if ($(smPref + '_more_param_div').visible()) {
        toggleDashList(smPref + '_more_param');
    }
    return false;
}

function toggleTag(pref, name, id, index) {
    focusComment(pref);

    var comObj = $(pref + '_comment');
    var comment = comObj.value;
    var cName = "[" + name + "]";
    var cPref = comment ? ' ' : '';
    var el = $('m_tg' + pref + id);
    var active = comment.indexOf(cName) == -1;
    colorElement(el, active);
    if (active) {
        comment += cPref + cName;
    }
    else {
        comment = comment.replace('' + cName, '');
    }
    comObj.value = comment.trim();

    if (comObj.createTextRange) {
        var r = comObj.createTextRange();
        r.collapse(false);
        r.select();
    }

    if (gbTgBlockHeight < 200) {
        comObj.focus()
    }
    return false;
}

// �����? ���?�?�������� �?���������?�? �?��������
function pushTag(smPref, name) {
    focusComment(smPref);
    my_focus(smPref);
    var pref = ' ';
    var comment = $(smPref + '_comment').value;
    if (comment == '') {
        pref = '';
    }
    if (comment.indexOf("[" + name + "]") == -1) {
        $(smPref + '_comment').value += pref + "[" + name + "]";
    }
    //Element.hide(smPref + '_tags');
    return false;
}

function updateMenuContext(smPref, isDontCall) {
    $(smPref + '_more_lazy_div').innerHTML = moreLazyContainer[smPref];
    smPref == 'm' ? new Chosen($('m_komissia_category'), gbChosenParam) : '';
    clearSecondBlock(smPref, 'tags');
    clearSecondBlock(smPref, 'tpl');
    clearSecondBlock(smPref, 'kom');
    updateMoreParMenu(smPref, isDontCall);
    if (smPref == 'm' && $('m_short_kom_form_tr') && !Element.visible('m_short_kom_form_tr') && $('m_from').value > 0) {
        selDashItem('m_komis', $('m_from').value);
    }
}

function clearSecondBlock(pref, parPref) {
    try {
        if (
            $(pref + '_short_' + parPref + '_form_tr') && !Element.visible(pref + '_short_' + parPref + '_form_tr') &&
            $(pref + '_more_' + parPref + '_td') &&
            $(pref + '_more_' + parPref + '_td').firstChild.id == (pref + '_short_' + parPref)
        ) {
            $(pref + '_form_' + parPref + '_td').innerHTML = '';
        }
    }
    catch (e) {
        console.log(pref + '_short_' + parPref + '_form_tr');
        console.log(e);
    }
}

function dateToStr(dateObj) {
    return dateObj.getFullYear() + '-' + (dateObj.getMonth() + 1) + '-' + dateObj.getDate();
}

function selInputText(inputId, startPos) {
    $(inputId).focus();
    var pos = $(inputId).value.length;
    if (-1 == startPos) { // �?�������� �������� ���?���?�?�� �����?�?�������?�? ���?�?�?���? �� ���������?
        startPos = pos;
    }
    if ($(inputId).setSelectionRange) {
        $(inputId).setSelectionRange(startPos, pos);
    }
    else if ($(inputId).createTextRange) {
        var range = $(inputId).createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', startPos);
        range.select();
    }
}

function togglePlus(id) {
    $(id).toggle();
    var ic = $(id).visible() ? 'Minus' : 'Plus'
    $(id + 'Img').src = "img/v3_ic" + ic + '.png';
    return false;
}

function showPopup(elemId, width, param) {
    var div = $(elemId);
    width = typeof(width) == 'undefined' ? 550 : width;

    if (div.visible()) {
        return true;
    }
    div.innerHTML = '<div style="text-align: center; width: 300px; height: 200px; display: table-cell; vertical-align: middle;">' + langId(27) + ' ...</div>';
    div.show();
    positionElem();
    $('scrShadow').show();
    var url = elemId == 'help' ? '?module=help' + param : gbPrivateUrl + param;

    var myAjax = new Ajax.Updater(
        {
            success: elemId
        },
        url,
        {
            method: 'post',
            onComplete: function () {
                div.style.width = width + 'px';
                positionElem();
            },
            onFailure: function () {
                window.alert(langId(11));
            },
            evalScripts: (elemId == 'help' ? false : true)
        }
    );

    function positionElem() {
        var scrTop = (window.pageYOffset || document.body.scrollTop);
        var scrLft = (window.pageXOffset || document.body.scrollLeft);
        div.style.left = '50%';
        div.style.top = '45%';
        div.style.marginLeft = scrLft - div.getWidth() / 2 + 'px';
        div.style.marginTop = scrTop - div.getHeight() / 2 + 'px';
        if (div.getHeight() > screenSize().h || getOffset(div).top < scrTop) {
            div.style.marginTop = '0px';
            div.style.top = scrTop + 'px';
        }
    }
}

function help(module, width) {
    return showPopup('help', width, '&p=' + module);
}

function lkMenuClick(div) {
    div = $(div);
    var hrf = div.getAttribute('hrf');
    if (hrf == 'administration'
    //|| hrf=='v2_homeBuhPrivateOther' || hrf == 'v2_homeBuhPrivateSettings'
    ) {
        var tab = div.getAttribute('tab');
        if (tab != '-' && tab) {
            window.location = hrf + '&tab=' + div.getAttribute('tab');
            return false;
        }
        div.className = (div.className == 'right' ? 'down' : 'right');
        div.parentNode.select('[tab]').each(function (el) {
            if (
                (hrf == 'administration' && el.getAttribute('tab') == '-')
            //||
            //(hrf == 'v2_homeBuhPrivateOther' && el.getAttribute('tab') == '-') ||
            //(hrf == 'v2_homeBuhPrivateSettings' && el.getAttribute('tab') != '-')


            ) {
                el.toggle();
            }
        });
        return false;
    }
    window.location = '' + hrf;
    return false;
}

function hltMinClose(obj, isHide) {
    $(obj).select("img").each(function (el) {
        el.toggle();
    });
    return true;
}
function toggleMenu(isNoCookie) {
    $('menuWide').toggle();
    $('menuNarrow').toggle();

    if (!isNoCookie) {
        setCookie('menuWide', $('menuWide').visible() ? "1" : "0", 3650);
    }
    $('menuWide').parentNode.style.width = $('menuWide').visible() ? '275px' : '20px';
}

function toggleTopMenu() {
    $('topMenu').toggle();
    if ($('topMenu').visible()) {
        $('lkMenu').className = 'rc box_shadow lkMenu lkMenuTop';
        $('topMenuLck').show();
        $('topMenuMin').hide();
        $('topMinExit').hide();
        $('mainTd').style.paddingTop = '23px';
    }
    else {
        $('lkMenu').className = 'rc box_shadow lkMenu';
        $('topMenuLck').hide();
        $('topMenuMin').show();
        $('topMinExit').show();
        $('mainTd').style.paddingTop = '28px';
    }
}
function topMenuLock() {
    var unl = '';
    if ($('v3_lock').src.indexOf('v3_lock.png') != -1) {
        unl = 'un';
        $('topMenuLck').title = '����?�?�����?�? �����?�������?���?���?����';
        setTimeout('toggleTopMenu()', 500);
    }
    else {
        $('topMenuLck').title = '�?�� �?���?�?�����?�? �����?�������?���?���?����';
    }
    setCookie('topMenuWide', unl ? "1" : "0", 3650);

    $('v3_lock').src = 'img/v3_' + unl + 'lock.png';
    $('v3_lock_h').src = 'img/v3_' + unl + 'lock_h.png';
}

function toggleDetails(blkId, isDontSave) {
    $(blkId).toggle();
    var isVis = $(blkId).visible();
    $('arrow_' + blkId).src = "img/v3_arr_" + (isVis ? 'd' : 'r') + ".png";
    if ($(blkId + '_ttl')) {
        $(blkId + '_ttl').title = isVis ? '������?���?�?�?' : '� ���������?���?�?�?';
    }
    isDontSave ? '' : saveSetting('tgldet', blkId, isVis);
    return false;
}

function getCaretPosition(obj) {
    var cursorPos = null;
    if (document.selection) {
        var range = document.selection.createRange();
        range.moveStart('textedit', -1);
        cursorPos = range.text.length;
    }
    else {
        cursorPos = obj.selectionStart;
    }
    return cursorPos;
}
function getChar(event) {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
    }

    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which)   // ���?�?�����?���?��
    }
    return null; // �?�����?�������?�����? �����������?��
}


function sumObserver(el) {
    Event.observe(el, 'keypress', function (e) {

        if (e.ctrlKey || e.altKey || e.metaKey) return;

        var allow = ',.+*-()%';
        var chr = getChar(e);

        if (chr == null || (chr >= '0' && chr <= '9') || allow.indexOf(chr) != -1) {
            return;
        }

        var insert = null;
        var replaceBeforeLength = 0;
        var replaceAfterLength = 0;

        if (chr == '/') {
            if (el.selectionStart > 0 && el.value.charAt(el.selectionStart - 1) == '.') {
                insert = '/';
                replaceBeforeLength = 1;
            }
            else if (el.selectionStart > 0 && el.value.charAt(el.selectionStart - 1) == '/') {
                insert = '.';
                replaceBeforeLength = 1;
            }
            else {
                insert = '.';
            }
        }
        else if (chr == '�?' || chr == '�') {
            insert = '.';
        }
        else if (chr == '��' || chr == '�?') {
            insert = ',';
        }

        if (insert) {
            var before = 0 < el.selectionStart - replaceBeforeLength ?
                el.value.substring(0, el.selectionStart - replaceBeforeLength) : "";
            var after = el.selectionEnd + replaceAfterLength < el.value.length ?
                el.value.substring(el.selectionEnd + replaceAfterLength, el.value.length) : "";
            var selectionStart = el.selectionStart;

            el.value = before + insert + after;
            el.selectionStart = el.selectionEnd = selectionStart - replaceBeforeLength + insert.length;
        }
        Event.stop(e);
        return false;
    });
}

function getDayInMonth(year, month) {
    return new Date(year, month, 0).getDate();
}

function toggleNdays(pref, chkVal) {
    if ($(pref + '_pln_period').value == chkVal) {
        $(pref + '_div_period_n').show();
        $(pref + '_period_n').focus();
        $(pref + '_period_n').setAttribute('vldRequired', 'vldRequired');
    }
    else {
        $(pref + '_div_period_n').hide();
        $(pref + '_period_n').removeAttribute('vldRequired');
    }
}

function daysBeforeFocus(pref) {
    if ($(pref + '_is_remind').checked) {
        $(pref + '_days_before').focus();
        $(pref + '_days_before').setAttribute('vldRequired', 'vldRequired');
    }
    else {
        $(pref + '_days_before').blur();
        $(pref + '_days_before').removeAttribute('vldRequired');
        $(pref + '_days_before').value = '';
    }
}

function saveSetting(setPref, id, value) {
    var name = setPref + '_' + id;
    gbSettings[setPref][id] = value;
    $(name) ? (value == true ? $(name).show() : $(name).hide()) : '';

    ajaxRequest({
        action: 'saveSetting',
        setPref: setPref,
        setName: id,
        value: value ? 'on' : 'off'
    });
    return false;
}

function sumFromKop(totSum) {
    // �?�?�������?���?�? ���?���� ���?�����? ��-���? 33.4*3 = 100.19999999999999, �?�� �����?�������?���?�? ������ �?�?�?�����?
    return totSum / 100;
}
function tokenizeSum(sum) {
    var sign = sum < 0 ? '-' : '';
    var revSumS = new Array();
    var kop = new Array(3);
    var i = 0, j = 0;

    sum = '' + Math.abs(sum);
    sum = sum.split('').reverse();
    for (i in sum) {
        if (isNaN(sum[i])) continue;
        if (i < 2) {
            kop[i] = sum[i];
            continue;
        }
        ((i - 2) > 0 && (i - 2) % 3 == 0) ? revSumS[j++] = '&nbsp;' : '';
        revSumS[j++] = sum[i];
    }
    kop = kop.reverse().join('');
    sum = revSumS.reverse().join('');
    sum = sum == '' ? '0' : sum;
    sum = sign + sum;

    return {sum: sum, kop: kop};
}
function toReadableSum(totSum, isColor) {
    isColor = typeof(isColor) != 'undefined' ? isColor : false;
    var num = new Number(totSum);
    var sumkop = tokenizeSum(num.toFixed(0));
    var sumStr = isColor ? '<span style="color:' + (totSum > 0 ? 'green' : '#CC3300') + '">' : '';
    sumStr += sumkop.sum;
    if (gbIsShowKop) {
        sumStr += '<span style="font-size:9px">.' + sumkop.kop + '</span>';
    }
    return sumStr + (isColor ? '</span>' : '');
}
function autoTextArea(id) {
    var text = $(id);
    if (!text) {
        return;
    }
    text.addClassName('auto_height');
    function resize() {
        text.style.height = 'auto';
        text.style.height = text.scrollHeight + 'px';
    }

    /* 0-timeout to get the already changed text */
    function delayedResize() {
        window.setTimeout(resize, 0);
    }

    Event.observe(text, 'change', resize);
    Event.observe(text, 'cut', delayedResize);
    Event.observe(text, 'paste', delayedResize);
    Event.observe(text, 'drop', delayedResize);
    Event.observe(text, 'keydown', delayedResize);

    resize();
}

/**
 * Global init
 */
var gbDefCategoryId = null;
var tagList = [];
var tplList = {};
var altTplList = {};
var placeIcons = {};
var gbMenuIdx = 0;
var gbFormCalander = null;
var gbIsShowKop = true;
var gbChosenParam = {
    disable_search_threshold: 8,
    search_contains: true,
    allow_single_deselect: true,
    no_results_text: '�?�� ��������������:'
};
var menuArrayIds = [
    'rest_menu_div',
    'remind_menu_div',
    'help',
    'w_currency_div',
    'i_currency_div',
    'm_currency_div',
    'ch_from_currency_div',
    'ch_to_currency_div',
    'is_i_duty_div',
    'is_w_duty_div',
    'all_date_div',
    'w_from_div',
    'i_from_div',
    'tplMenu_div',
    'spravo4nikMenu',
    'otherMenu',
    'filter_menu_div',
    'w_more_param_div',
    'i_more_param_div',
    'm_more_param_div',
    'ch_more_param_div',
    'm_to_div',
    'm_from_div',
    'm_komis_div',
    'rest_plan_block_div',
    'add_plnd_div',
    'to_group',
    'add_description',
    'last_enter_div',
    'smartInputFloaterContent',
    'ch_from_div'
];

function searchTagById(id) {
    for (var i = 0; i < tagList.length; i++) {
        if (tagList[i].id == id) {
            return tagList[i];
        }
    }
    return false;
}

function autoChosen() {
    $$('select.chosen').each(function (el) {
        el.hasClassName('auto') ? new Chosen(el, gbChosenParam) : '';
    });
}

Event.observe(document, 'mousedown', checkHideMenu);
Event.observe(window, 'load', autoChosen);

/**
 * Lang settings
 */
var langIdx = ( window.location.pathname.indexOf('/en/') != -1 ? 1 : 0 );
var langData = {
    1: ['�? �����?������ �?�?�?������ �����?�������������?�? �������?.\n�?�?�?�?�� ���� �����������?�� �����?���������?�? �?�?���?�? �� �������?�? �?�?�?�����?, �� ���?�������?���?���?�� �����? �����? �����?���?�?.', 'There is zero sum in the first line.\nIt\'s better to use first line for the last expense in the list.'],
    2: ['�?���������?���?�?��, �?���������?�� �����?������ �������?�������� �?����������, �� ���?���?�����?���?, ���? 0 ���� 99', 'Please, enter valid discount value in percent from 0 to 99'],
    3: ['�?���?�?�����?���?�������� �����������?��', 'Adjusting the balance'],
    4: ['�? �?�?���� �����?�?�� �?�?�����������?, ���������� �� �?�?���� �������?�?�� �����?.\n������?�?�����?���?�������?�? �����?������ ���� ����������.', 'There is no money in that currency.\nNo correction is possible.'],
    5: ['�?�����������?�? �����?������', 'Show detail'],
    6: ['����?�?�?�? �����?������', 'Hide detail'],
    7: ['����?�?�?�?', 'Hide'],
    8: ['�?�?���?�?�?�?', 'Show'],
    9: ['�?o�?o', 'photo'],
    10: ['�?�������?���?�?�?', 'Rotate'],
    11: ['�?���? �?�����������������? �? �?���?�����?����', 'Not connected'],
    12: ['�?�������?���?�� �?���� ���?�������?���� ', 'Not valid type '],
    13: [' �����? �������? ', ' for the field '],
    14: ['�?������', 'Field'],
    15: ['������������ ���?�?�? ���?���?���� �?���?�? ���? ��������', 'must be selected'],
    16: ['������������ ���?�?�? ���� �������?�?��', 'must be less than'],
    17: ['������������ ���?�?�? ���� �������?�?��', 'must be greater than'],
    18: ['�?��������������', 'symbols'],
    19: ['sum_bg', 'sum_bg_en'],
    20: ['�?������ �����? ���������� �?�?�����? ������������ �?�������?�����?�? �����?������ �����?�������?���?���?������ ���?�?������������,\n�?���?�?���������������� �?�����?���� ���� �?���?�? �� �?��������������: + - * . /', 'The "amount" field must be valid math. expression with digits and following symbols:  + - * . /'],
    21: ['�?�?����', 'Off'],
    22: ['�?����', 'On'],
    23: ['�?�?�����?�?���?�? ���� �?���?���?', 'Remove from form'],
    24: ['�?�����������?�? �� �?���?���?', 'Add to form'],
    25: ['�?�����������?�? �?�?�?�?', 'Add account'],
    26: ['�?�����������?�? ��������', 'Add duty'],
    27: ['������?�����?', 'One moment'],
    28: ['� �������?�����? �?�����?���� �� �?���������� \'����������?�? ���� ���?���?�?�?���� ����������������\'', 'Need \'Watch for the open tab\''],
    29: ['�?�?�����? ���������� �?���?���� �������������?�? �������?���?����', 'Shows planned trans. number'],
    30: ['', '']
};

/**
 * Corrector
 */
var Corrector = {
    counter: 0,
    elements: [],
    selectors: ['input', 'textarea'],
    timeout: 300,
    minWordLen: 6,
    uniqueId: function () {
        return 'uid_' + this.counter++;
    },
    _statTables: {
        ru: [
            00, 10, 24, 05, 16, 10, 00, 09, 25, 01, 07, 38, 73, 28, 41, 01, 07, 16, 30, 39, 01, 01, 07, 04, 08, 07, 03, 00, 00, 00, 00, 09, 17,
            06, 00, 00, 00, 00, 13, 00, 00, 00, 08, 00, 01, 08, 00, 03, 21, 00, 09, 03, 00, 08, 00, 00, 00, 00, 00, 01, 00, 12, 01, 00, 00, 03,
            51, 00, 00, 01, 01, 45, 00, 00, 02, 28, 00, 03, 07, 01, 10, 54, 01, 07, 22, 03, 06, 00, 01, 00, 01, 07, 00, 01, 19, 00, 00, 00, 02,
            11, 00, 00, 00, 07, 03, 00, 00, 00, 06, 00, 01, 10, 00, 02, 52, 00, 09, 00, 00, 04, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            36, 00, 08, 00, 00, 36, 00, 00, 00, 18, 00, 02, 04, 00, 16, 28, 01, 08, 03, 01, 11, 00, 00, 02, 00, 00, 00, 05, 07, 00, 00, 00, 03,
            02, 10, 13, 17, 22, 09, 00, 07, 12, 01, 17, 15, 44, 37, 63, 03, 06, 50, 40, 46, 01, 01, 03, 02, 12, 06, 05, 00, 00, 00, 00, 01, 01,
            00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            06, 01, 00, 00, 08, 27, 00, 00, 00, 15, 00, 01, 00, 00, 06, 00, 00, 00, 00, 00, 03, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            34, 01, 07, 04, 06, 05, 00, 00, 00, 05, 00, 01, 02, 05, 16, 06, 00, 03, 00, 00, 04, 00, 00, 00, 00, 00, 00, 01, 03, 00, 00, 00, 01,
            04, 05, 20, 04, 13, 21, 00, 02, 22, 08, 16, 19, 32, 21, 30, 03, 02, 12, 26, 36, 00, 01, 14, 05, 12, 05, 01, 00, 00, 00, 00, 04, 15,
            00, 00, 00, 00, 02, 00, 00, 00, 00, 00, 00, 01, 00, 00, 03, 01, 00, 00, 04, 02, 00, 00, 00, 00, 03, 00, 00, 00, 00, 00, 00, 00, 00,
            56, 00, 03, 00, 00, 05, 00, 00, 00, 27, 00, 00, 07, 00, 02, 64, 00, 15, 03, 08, 15, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            33, 01, 00, 01, 00, 45, 00, 01, 01, 58, 00, 03, 04, 00, 03, 44, 00, 00, 11, 00, 09, 00, 00, 00, 01, 00, 00, 34, 03, 00, 00, 10, 12,
            20, 01, 00, 00, 00, 34, 00, 00, 00, 25, 00, 01, 02, 01, 15, 37, 02, 01, 01, 00, 15, 00, 00, 00, 00, 00, 00, 00, 12, 00, 00, 00, 03,
            75, 00, 00, 02, 03, 74, 00, 00, 00, 67, 00, 05, 00, 00, 26, 87, 00, 00, 05, 12, 21, 01, 00, 04, 02, 00, 00, 10, 39, 00, 00, 01, 12,
            00, 30, 60, 38, 38, 19, 00, 15, 13, 08, 39, 19, 39, 46, 36, 03, 13, 44, 55, 50, 00, 03, 05, 02, 14, 06, 03, 00, 00, 00, 01, 02, 06,
            12, 00, 00, 00, 00, 17, 00, 00, 00, 09, 00, 00, 10, 00, 01, 74, 01, 51, 01, 00, 09, 00, 00, 00, 00, 00, 00, 02, 03, 00, 00, 00, 03,
            65, 00, 04, 03, 01, 50, 00, 02, 00, 32, 00, 03, 00, 05, 10, 66, 01, 00, 03, 05, 20, 00, 02, 00, 01, 02, 00, 03, 12, 00, 00, 01, 08,
            15, 00, 16, 00, 01, 28, 00, 00, 00, 14, 00, 28, 21, 07, 09, 25, 19, 03, 08, 95, 09, 01, 01, 00, 04, 01, 00, 23, 02, 00, 00, 01, 33,
            62, 00, 25, 00, 01, 43, 00, 00, 00, 32, 00, 06, 02, 00, 12, 99, 01, 26, 12, 00, 12, 00, 00, 00, 00, 00, 00, 46, 13, 00, 00, 00, 03,
            02, 05, 03, 07, 16, 02, 00, 12, 02, 00, 00, 05, 10, 07, 03, 00, 05, 06, 10, 13, 00, 00, 03, 00, 07, 05, 05, 00, 00, 00, 00, 13, 00,
            01, 00, 00, 00, 00, 02, 00, 00, 00, 05, 00, 00, 00, 00, 00, 04, 00, 01, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            04, 00, 01, 00, 00, 00, 00, 00, 00, 02, 00, 00, 02, 00, 03, 14, 00, 03, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            04, 00, 01, 00, 00, 07, 00, 00, 00, 09, 00, 00, 00, 00, 00, 01, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 01, 00, 00, 00, 00,
            16, 00, 00, 00, 00, 34, 00, 00, 00, 13, 00, 04, 00, 00, 12, 01, 00, 00, 00, 20, 04, 00, 00, 00, 00, 01, 00, 02, 00, 00, 00, 00, 00,
            05, 00, 00, 00, 00, 17, 00, 00, 00, 13, 00, 03, 02, 00, 02, 02, 00, 00, 00, 01, 02, 00, 00, 00, 00, 00, 00, 04, 00, 00, 00, 00, 00,
            04, 00, 00, 00, 00, 15, 00, 00, 00, 05, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00, 02, 00, 00, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00,
            00, 00, 00, 01, 00, 02, 00, 00, 01, 00, 00, 08, 00, 03, 15, 00, 00, 00, 08, 01, 00, 00, 00, 01, 00, 04, 00, 00, 00, 00, 00, 05, 03,
            00, 00, 10, 01, 01, 12, 00, 01, 01, 00, 15, 01, 05, 12, 01, 00, 01, 02, 06, 03, 00, 00, 12, 00, 01, 05, 00, 00, 00, 00, 00, 00, 01,
            00, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 02, 01, 00, 00, 00, 00, 00, 00, 16, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            00, 02, 00, 00, 03, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 02, 04, 00, 00, 01, 00, 02, 00, 06, 00, 00, 00, 00, 00, 00,
            00, 00, 02, 01, 04, 01, 00, 01, 02, 00, 00, 02, 05, 03, 05, 00, 00, 00, 04, 11, 00, 00, 01, 01, 02, 00, 03, 00, 00, 00, 00, 01, 01
        ],
        en: [
            00, 06, 09, 10, 00, 01, 05, 00, 13, 00, 04, 16, 07, 55, 00, 05, 00, 28, 32, 33, 03, 08, 02, 00, 08, 00,
            02, 00, 00, 00, 14, 00, 00, 00, 02, 01, 00, 05, 00, 00, 09, 00, 00, 02, 01, 00, 04, 00, 00, 00, 02, 00,
            11, 00, 01, 00, 13, 00, 00, 13, 05, 00, 10, 03, 00, 00, 18, 00, 00, 03, 00, 05, 03, 00, 00, 00, 01, 00,
            02, 00, 00, 01, 16, 00, 00, 00, 08, 00, 00, 02, 00, 01, 08, 00, 00, 02, 05, 00, 01, 00, 00, 00, 01, 00,
            16, 00, 08, 37, 09, 04, 01, 01, 06, 00, 00, 12, 11, 28, 01, 03, 00, 55, 21, 09, 00, 06, 02, 03, 07, 00,
            04, 00, 00, 00, 06, 02, 00, 00, 05, 00, 00, 04, 00, 00, 08, 00, 00, 05, 00, 01, 03, 00, 00, 00, 00, 00,
            07, 00, 00, 00, 09, 00, 03, 08, 01, 00, 00, 01, 00, 01, 04, 00, 00, 04, 01, 00, 01, 00, 00, 00, 01, 00,
            35, 00, 00, 00, 88, 00, 00, 00, 26, 00, 00, 00, 00, 00, 13, 00, 00, 02, 00, 05, 01, 00, 00, 00, 01, 00,
            02, 02, 17, 13, 05, 03, 09, 00, 00, 00, 01, 07, 04, 58, 10, 03, 00, 10, 23, 34, 00, 03, 00, 00, 00, 00,
            00, 00, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00,
            00, 00, 00, 00, 07, 00, 00, 00, 04, 00, 00, 00, 00, 02, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00,
            15, 00, 00, 08, 21, 03, 00, 00, 11, 00, 01, 14, 00, 00, 09, 00, 00, 00, 01, 02, 05, 01, 01, 00, 09, 00,
            11, 01, 00, 00, 26, 00, 00, 00, 08, 00, 00, 00, 01, 00, 08, 03, 00, 00, 02, 00, 04, 00, 00, 00, 11, 00,
            05, 00, 09, 33, 18, 01, 33, 00, 11, 00, 02, 01, 00, 04, 13, 00, 00, 00, 08, 17, 02, 01, 00, 00, 03, 00,
            02, 02, 03, 04, 01, 26, 01, 00, 04, 00, 04, 07, 15, 37, 08, 03, 00, 20, 06, 11, 33, 04, 12, 01, 01, 00,
            08, 00, 00, 00, 10, 00, 00, 01, 05, 00, 00, 04, 00, 00, 09, 03, 00, 05, 01, 02, 02, 00, 00, 00, 00, 00,
            00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 04, 00, 00, 00, 00, 00,
            12, 01, 01, 08, 37, 00, 01, 01, 14, 00, 02, 01, 02, 04, 15, 02, 00, 03, 08, 08, 04, 02, 00, 00, 06, 00,
            13, 00, 03, 00, 24, 00, 00, 12, 13, 00, 01, 01, 02, 01, 08, 07, 01, 00, 10, 26, 06, 00, 01, 00, 00, 00,
            10, 00, 01, 00, 23, 00, 00, 99, 15, 00, 00, 05, 00, 00, 27, 00, 00, 07, 04, 07, 06, 00, 04, 00, 04, 00,
            03, 01, 04, 02, 02, 01, 06, 00, 02, 00, 00, 12, 02, 09, 00, 06, 00, 13, 10, 11, 00, 00, 00, 00, 00, 00,
            01, 00, 00, 00, 24, 00, 00, 00, 03, 00, 00, 00, 00, 00, 03, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            22, 00, 00, 00, 12, 00, 00, 14, 09, 00, 00, 00, 00, 04, 09, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00,
            00, 00, 01, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
            01, 00, 00, 00, 03, 00, 00, 00, 01, 00, 00, 00, 00, 00, 06, 01, 00, 00, 03, 01, 00, 00, 00, 00, 00, 00,
            00, 00, 00, 00, 01, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00
        ],
    },
    _layoutTable: {
        ru: "���?�?���������?�?���?�?�?�?�������?���������?�?�?�?�����?�?���?�?����?�?�?�?����?�������?�?�?� �?�?�?�?������?�?�����?�",
        en: "qwertyuiop[]asdfghjkl;'zxcvbnm,.QWERTYUIOP{}ASDFGHJKL:" + '"' + "ZXCVBNM<>"
    },
    _alphabet: {
        ru: '�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?� ����������������������',
        en: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    },
    template: '<div class="{class}"><span class="label">{label}</span><span class="version" onclick="Corrector.setValue(this)">{version}</span></div>',
    options: {
        class: 'hint',
        label: '�?�������������� �?�? ���������� ���������?: '
    },
    timerId: null,
    run: function (config) {
        var config = (arguments.length > 0) ? arguments[0] : {};
        this.selectors = config.selectors || this.selectors;
        this.timeout = config.timeout || this.timeout;
        this.template = config.template || this.template;
        this.minWordLen = config.minWordLen || this.minWordLen;
        if (typeof config.options != 'undefined') {
            this.options.class = config.options.class || this.options.class;
            this.options.label = config.options.label || this.options.label;
        }

        var elements = $$(this.selectors);
        var hintHtml = this._getHintBlock('');
        for (var idx = 0; idx < elements.length; idx++) {
            var virtualWrapper = document.createElement('div');
            virtualWrapper.innerHTML = hintHtml;
            var hintElement = virtualWrapper.firstChild;
            if (!elements[idx].id) {
                elements[idx].id = this.uniqueId();
            }
            hintElement.id = 'hintfor_' + elements[idx].id;
            hintElement.style.display = 'none';
            document.body.appendChild(hintElement);
        }
        this.elements = $$(this.selectors);
        this.timerId = setInterval(function () {
            Corrector.execute.call(Corrector);
        }, this.timeout);
    },
    stop: function () {
        clearInterval(this.timerId);
    },
    execute: function () {
        var version = '', hint;

        for (var idx = 0; idx < this.elements.length; idx++) {
            if ((document.activeElement == this.elements[idx]) &&
                (this.elements[idx].value.length > this.minWordLen)) {
                version = this._changeLayout(this.elements[idx].value);
                if (this._getPredicate(this.elements[idx].value) < this._getPredicate(version)) {
                    this._showHint(this.elements[idx], version);
                    continue;
                }
            }
            this._hideHint(this.elements[idx]);
        }
    },
    _changeLayout: function (str) {
        var newStr = '';
        var char = '';
        var pos = -1;
        var lt = this._layoutTable; //�?���? ���?���?�����?�?��

        for (var idx = 0; idx < str.length; idx++) {
            char = str[idx];
            if (typeof str[idx] == 'undefined')
                continue;
            if ((pos = lt.ru.indexOf(char)) != -1) {
                char = lt.en[pos];
            } else if ((pos = lt.en.indexOf(char)) != -1) {
                char = lt.ru[pos];
            }
            newStr = newStr + char;
        }

        return newStr;
    },
    _getPredicate: function (str) {
        var calc = 0, lang;

        for (var idx = 0; idx < str.length; idx++) {
            if ((typeof str[idx] == 'undefined') ||
                ((lang = this._getCharLang(str[idx])) == null))
                continue;
            if ((typeof str[idx + 1] == 'undefined') ||
                (lang != this._getCharLang(str[idx + 1])))
                continue;

            calc += this._getStat(str[idx], str[idx + 1], lang);
        }

        return calc;
    },
    _getCharLang: function (char) {
        var ch = char.toUpperCase();
        if (this._alphabet.ru.indexOf(ch) >= 0)
            return 'ru';
        if (this._alphabet.en.indexOf(ch) >= 0)
            return 'en';
        return null;
    },
    _getStat: function (char1, char2, lang) {
        var index = this._getCharNum(char1, lang) * this._alphabet[lang].length + this._getCharNum(char2, lang);
        return this._statTables[lang][index] || 0;
    },
    _getCharNum: function (char, lang) {
        return this._alphabet[lang].indexOf(char.toUpperCase());
    },
    _getHintBlock: function (version) {
        var block = this.template;

        block = block.replace(/\{class\}/g, this.options.class);
        block = block.replace(/\{version\}/g, version);
        block = block.replace(/\{label\}/g, this.options.label);
        return block;
    },
    _getInputHint: function (element) {
        var hint;
        hint = document.getElementById("hintfor_" + element.id);
        if (typeof hint == 'undefined')
            return null;
        return hint;
    },
    _showHint: function (element, versionText) {
        var hint = this._getInputHint(element);
        var versions = hint.getElementsByClassName('version');
        for (var idx = 0; idx < versions.length; idx++) {
            versions[idx].innerHTML = versionText;
        }
        hint.style.display = 'block';
    },
    _hideHint: function (element) {
        var hint = this._getInputHint(element);
        if (hint) {
            hint.style.display = 'none';
        }
    },
    setValue: function (element) {
        var input = this._getInputElement(element);

        if (input != null) {
            input.value = element.innerHTML;
        }
    },
    _getInputElement: function (element) {
        var hintEl = element;
        while (hintEl.id.indexOf("hintfor_") < 0) {
            hintEl = hintEl.parentNode;
        }
        var input = document.getElementById(hintEl.id.replace("hintfor_", ""));
        if (typeof input == 'undefined')
            return null;
        return input;
    }
}