var csrfParameter = jQuery("meta[name='_csrf_parameter']").attr("content");
var csrfToken = jQuery("meta[name='_csrf']").attr("content");
var csrfHeader = jQuery("meta[name='_csrf_header']").attr("content");



jQuery(document).ready(function () {
    jQuery(function () {


        jQuery('#catTree').jstree({

                "checkbox": {
                    "whole_node": false,
                    "keep_selected_style": true,
                    "three_state": true,
                    "tie_selection": false

                },


                'core': {
                    // 'expand_selected_onload': true,
                    // 'animation': 200,
                    //"check_callback": true,
                    // 'check_callback': function (op) {},
                    // 'force_text': true,
                    'data': {
                        'url': function (node) {
                            return '/getAllCategoriesWithRootJSON';
                        },
                        'data': function (node) {
                            return {'text': node.text};
                        }
                    }
                },
                "checkbox": {
                    "keep_selected_style": false
                },
                "plugins": ["checkbox", "sort", "wholerow"]


                // plugins: ["checkbox", "json_data", "ui", "crrm", "hotkeys", "types", "sort"],
                // "sort": function (a, b) {
                //     return this.get_text(a) > this.get_text(b) ? 1 : -1;
                // },

            });

    })




        jQuery("#whatToShowSelect").change(function () {
            var chosenParam = jQuery("#whatToShowOption").val();
            //all
            if (chosenParam == "1") {
                jQuery("#howToShowSelect").hide();
                jQuery("#showAmountAfterEveryOperation").show();
                jQuery("#periodSelect").show();
                jQuery("#periodChooser").show();
                jQuery("#currencySelect").show();
                jQuery("#accountSelect").show();
                jQuery("#sourcesSelectDiv").hide();
                jQuery("#selectDiv").hide();
                jQuery("#amountSelect").show();
                jQuery("#roundModSelect").hide();

            }
            //costs
            else if (chosenParam == "2") {
                jQuery("#howToShowSelect").show();
                //hide sources show categories
                jQuery("#howToShowOption option[value='sources']").hide();
                jQuery("#howToShowOption option[value='cat']").show();
                jQuery("#howToShowOption option[value='cat']").prop("selected", true);
                jQuery("#howToShowOption option[value='subcat']").show();
                jQuery("#showAmountAfterEveryOperation").show();
                jQuery("#periodSelect").show();
                jQuery("#periodChooser").show();
                jQuery("#currencySelect").show();
                jQuery("#accountSelect").show();
                jQuery("#selectDiv").show();
                jQuery("#sourcesSelectDiv").hide();
                jQuery("#amountSelect").show();
                jQuery("#roundModSelect").show();
            }
            //income
            else if (chosenParam == "3") {
                jQuery("#howToShowSelect").show();
                //hide categories show sources
                jQuery("#howToShowOption option[value='cat']").hide();
                jQuery("#howToShowOption option[value='subcat']").hide();
                jQuery("#howToShowOption option[value='sources']").show();
                jQuery("#howToShowOption option[value='sources']").prop("selected", true);
                jQuery("#showAmountAfterEveryOperation").show();
                jQuery("#periodSelect").show();
                jQuery("#periodChooser").show();
                jQuery("#currencySelect").show();
                jQuery("#accountSelect").show();
                jQuery("#selectDiv").hide();
                jQuery("#sourcesSelectDiv").show();
                jQuery("#amountSelect").show();
                jQuery("#roundModSelect").hide();
            }
            //transaction
            else if (chosenParam == "4") {
                jQuery("#howToShowSelect").show();
                jQuery("#showAmountAfterEveryOperation").show();
                jQuery("#periodSelect").show();
                jQuery("#periodChooser").show();
                jQuery("#currencySelect").show();
                jQuery("#accountSelect").show();
                jQuery("#selectDiv").hide();
                jQuery("#sourcesSelectDiv").hide();
                jQuery("#amountSelect").show();
                jQuery("#roundModSelect").hide();
            }
            //exchange
            else if (chosenParam == "5") {
                jQuery("#howToShowSelect").show();
                jQuery("#showAmountAfterEveryOperation").show();
                jQuery("#periodSelect").show();
                jQuery("#periodChooser").show();
                jQuery("#currencySelect").hide();
                jQuery("#accountSelect").show();
                jQuery("#selectDiv").hide();
                jQuery("#sourcesSelectDiv").hide();
                jQuery("#amountSelect").hide();
                jQuery("#roundModSelect").hide();
            }
        });
        jQuery("#periodOption").change(function () {
            if(jQuery("#periodOption").val()!="11"){
                jQuery("#periodChooser").hide();
            }else{
                jQuery("#periodChooser").show();
            }

        });
        jQuery("#amountOption").change(function () {
            var value=jQuery("#amountOption").val();
            if(value=="1"){
                jQuery("#amountMerger").hide();
                jQuery("#amountValue").hide();
            }
            else if(value=="2"||value=="3"||value=="4"){
                jQuery("#amountValue").show();
                jQuery("#amountMerger").hide();
            }
            else if(value=="5"){
                jQuery("#amountMerger").show();
                jQuery("#amountValue").hide();

            }
        })
        jQuery(function () {

            var date=new Date();
            date.setDate(date.getDate()+1);

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);

            var year=date.getFullYear();
            var month=date.getMonth();
            var day=date.getDate();

            jQuery("#datepickerStart").datepicker();
            jQuery("#datepickerStart").datepicker('setDate', new Date(date.getFullYear(), 0,1));
            jQuery("#datepickerEnd").datepicker();
            jQuery("#datepickerEnd").datepicker('setDate', new Date(year, month, day));

        });
    })




function getCategories() {
    // var selectedElmsIds = $('#tree').jstree("get_selected");
    var selectedElmsIds = [];
    var selectedElms = jQuery('#catTree').jstree("get_selected", true);
    jQuery.each(selectedElms, function() {
        var x=this.id.substr(0, 1);
        if(x=='c'){
            selectedElmsIds.push(this.id.substr(1));
        }
    });
    return selectedElmsIds;
}
;


function getSubcategories() {
    // var selectedElmsIds = $('#tree').jstree("get_selected");
    var selectedElmsIds = [];
    var selectedElms = jQuery('#catTree').jstree("get_selected", true);
    jQuery.each(selectedElms, function() {
            var x=this.id.substr(0, 1);
            if(x=='s'){
                selectedElmsIds.push(this.id.substr(1));
            }
    });
    return selectedElmsIds;
}
;

function getCurrencies() {

    return jQuery("#currencyOption option:selected").val();

}
;


function getStartTime() {
    return jQuery("#datepickerStart").val();

}
;
function getEndTime() {
    return jQuery("#datepickerEnd").val();

}
;
function getAccounts() {
    var accounts = [];
    jQuery("#accountSelect option:selected").each(function () {
            accounts.push(this.value);
        }
    );
    return accounts;
}
;

function getExpenses(){
    var details=jQuery("#showAmountAfterEveryOperation").is(':checked');
    var categories;
    var isReportByCategory;
    var optionCategory=jQuery("#howToShowOption :selected").val();
    if(optionCategory=='cat'){
        isReportByCategory=true;
        categories= getCategories();
    }
        else if(optionCategory=='subcat'){
        isReportByCategory=false;
        categories= getSubcategories();
    }

    var currencies = getCurrencies();
    var start = getStartTime();
    var end = getEndTime();
    var accounts = getAccounts();
    var roundMod=jQuery("#roundOption :selected").val();
    var period=jQuery("#periodOption :selected").val();
    if(details){



    }else{

        if(roundMod=="5") {
            var dataToJson = {
                categories: categories,
                currencies: currencies,
                start: start,
                end: end,
                accounts: accounts,
                periodOption:period,
                reportByCategory: isReportByCategory
        }

            var headers = {};
            var data = {};
            data[csrfParameter] = csrfToken;
            headers[csrfHeader] = csrfToken;

            jQuery(document).ajaxSend(function (e, xhr, options) {
                xhr.setRequestHeader(csrfHeader, csrfToken);
                xhr.setRequestHeader('Access-Control-Allow-Origin', "*");
            });

            jQuery.ajax({
                crossOrigin: true,
                url: '/balanceOfOperations',
                type: "post",
                headers: headers,
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(dataToJson),

                success: function (data) {
                    console.log(data);
                    console.log(data.message);
                    var json = JSON.parse(data.message);
                    // var stat = jQuery.parseJSON(data.message.stats);
var stat = json["stats"];
                    chartData = [];

                    for (var key in stat) {

                        chartData.push({
                            name: key,
                            balance: stat[key]
                        });
                    }

                    chart.dataProvider = (chartData);
                    chart.validateData();


                    jQuery("#tableDiv").html('');

                    var operations= json["operations"];
                    console.log(operations);

                    var htmlOperation="";
                    for(var i=0; i<operations.length; i++){
                        var d = new Date(operations[i].date);
                        var formatDate = ('0' + (d.getDate())).slice(-2) + '.' + ('0' + (d.getMonth() + 1)).slice(-2) + '.' + d.getFullYear() + ' ' + ('0' + (d.getHours())).slice(-2) + ':' + ('0' + (d.getMinutes())).slice(-2) + ':' + ('0' + (d.getSeconds())).slice(-2);

                        htmlOperation=htmlOperation+"<div class='row'>"
                            +"<div class='col-sm-4'>"+formatDate+"</div>"
                            +"<div class='col-sm-2'> <span style='color: #00dd00'>"+parseFloat(operations[i].sum).toFixed(2)+"</span><span style='color: #0a6aa1' >"+operations[i].currency+"</span></div>"
                            +"<div class='col-sm-2'>"+operations[i].subcategory.category.name+"</div>"
                            +"<div class='col-sm-2'>"+operations[i].subcategory.name+"</div>"
                            +"<div class='col-sm-2'>"+operations[i].account.name+"</div></div>";
                    };

                    jQuery("#tableDiv").html(htmlOperation);

               },

                error: function (jqXHR, textStatus, errorThrown) {
                    jQuery("#errorField").html("This item is already exists. Please, change name!");
                }
            });
        }
        else{
            var dataToJson = {
                categories: categories,
                currencies: currencies,
                start: start,
                end: end,
                accounts: accounts,
                roundMod: roundMod
            }

            var headers = {};
            var data = {};
            data[csrfParameter] = csrfToken;
            headers[csrfHeader] = csrfToken;

            jQuery(document).ajaxSend(function (e, xhr, options) {
                xhr.setRequestHeader(csrfHeader, csrfToken);
                xhr.setRequestHeader('Access-Control-Allow-Origin', "*");
            });

            jQuery.ajax({
                crossOrigin: true,
                url: '/balanceOfOperations',
                type: "post",
                headers: headers,
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(dataToJson),

                success: function (data) {
                    console.log(data);
                    console.log(data.message);
                    var stat = JSON.parse(data.message);
                    chartData = [];

                    for (var key in stat) {

                        chartData.push({
                            name: key,
                            balance: stat[key]
                        });
                    }

                    chart.dataProvider = (chartData);
                    chart.validateData();

                    var table='<table class="tablesorter" style="width: 100%;" id="reportTable" border="1"'+
                    ' cellpadding="5" cellspacing="1" width="100%" align="center" summary="operations">'+
                        '<thead class="text-center">'+
                        '<th>Category</th>'+
                        '<th>Amount</th>'+
                        '</thead>'+
                        '<tbody>';
                    for (var key in stat) {
                        table+='<tr  onmouseover="showMenu()" onmouseout="hideMenu()">' +
                            '<td>'+key+'</td>' +
                            '<td>'+stat[key]+'</td>' +
                            '</tr>';


                    }

                    table+='</tbody></table>';

                    jQuery("#tableDiv").html(table);

                },

                error: function (jqXHR, textStatus, errorThrown) {
                    jQuery("#errorField").html("This item is already exists. Please, change name!");
                }
            });

        }
        }
}

function getAllOperations() {

}
function getIncomes() {

}
function getTransactions() {

}
function getExchanges() {

}
function refreshDiagram() {
    var chosenParam = jQuery("#whatToShowOption").val();
    if(chosenParam=="1"){
        getAllOperations();
    }
    else if (chosenParam == "2") {
        getExpenses();
    }
    else if (chosenParam == "3") {
        getIncomes();
    }
    else if (chosenParam == "4") {
        getTransactions();
    }
    else if (chosenParam == "5") {
        getExchanges();
    }




}

var chart;
var chartData = [];


chart = AmCharts.makeChart("chartdiv",
    {
        "type": "pie",
        "theme": "light",
        "dataProvider": [],
        "valueField": "balance",
        "titleField": "name",
        "balloon": {
            "fixedPosition": true
        },

        "export": {
            "enabled": true
        }
    });


