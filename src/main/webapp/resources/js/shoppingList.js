function summaryAmount() {
    jQuery()
}

var csrfParameter = jQuery("meta[name='_csrf_parameter']").attr("content");
var csrfToken = jQuery("meta[name='_csrf']").attr("content");
var csrfHeader = jQuery("meta[name='_csrf_header']").attr("content");
var addItemLink;
var listId;


//++
function createItemForm() {

    var select = '<select id="selectList" style="width: 100%">';
    jQuery(".listName").each(function (i) {
        var element = jQuery(this).closest("div").prop("id");
        select += '<option value="' + element.replace("query_info_", "") + '">' + jQuery(this).text() + '</option>';
    })
    select += '</select>';


    var selectCurrency = '<select id="selectCurrency" style="width: 100%">';

    var fav_curr = "${def_curr}";
    jQuery.each(currencies, function (i) {
        if (fav_curr == currencies[i]) {

            selectCurrency += '<option selected value="' + currencies[i] + '">' + currencies[i] + '</option>';
        } else {
            selectCurrency += '<option value="' + currencies[i] + '">' + currencies[i] + '</option>';
        }

    });
    selectCurrency += '</select>';

    var innerHtmlForm =
        '<div class="row" >' +
        '<div class="col-sm-5">List</div>' +
        '<div class="col-sm-7">' + select + '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-5">Item\'s name</div>' +
        '<div class="col-sm-7">' +
        '<input id="itemName" type="text" style="width: 100%" name="itemName"/>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-5">Item\'s description</div>' +
        '<div class="col-sm-7">' +
        '<input id="itemDesc" type="text" style="width: 100%" name="itemDesc"/>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-5">Item\'s price</div>' +
        '<div class="col-sm-7">' +
        '<input id="itemPrice" type="text" style="width: 100%" name="itemPrice"/>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-5">Currency</div>' +
        '<div class="col-sm-7">' + selectCurrency +
        '</div>' +
        '</div>' + '</div>' +
        '<div class="row">' +
        '<div class="col-sm-6"></div><div class="col-sm-3">' +
        '<input type="button" value="Cancel"  onclick="cancelCreateList()"/>' +
        '</div>' +
        '<div class="col-sm-3">' +
        '<input id="createItemButton" type="button" value="Create" onclick="createItem()"/>' +
        '</div>' +
        '</div>';
    jQuery("#createOffer").html(innerHtmlForm);
}

jQuery(document).ready(function () {
    //+

    jQuery('#createList').click(function () {
        createListForm();
        return false;
    });

    //+
    jQuery('#createItem').click(function () {
        createItemForm();
        return false;
    });

    sortItems();
});
function sortItems() {
    var $wrapper = jQuery(".items");
    $wrapper.each(function () {
            jQuery(this).find('.itemOfList').sort(function (a, b) {
                return +a.dataset.sort - +b.dataset.sort;
            })
                .appendTo(jQuery(this));
        }
    );
}
//+
function createListForm() {
    var innerHtmlForm =
        '<div class="row" >' +
        '<div class="col-sm-5">List\'s name</div>' +
        '<div class="col-sm-7"><input id="listName" type="text" name="listName"/>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row" >' +
        '<div class="col-sm-5">Description</div>' +
        '<div class="col-sm-7"><input id="listDesc" type="text" name="listDesc"/>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-6"></div><div class="col-sm-3">' +
        '<input type="button" value="Cancel" onclick="cancelCreateList()"/></div>' +
        '<div class="col-sm-3">' +
        '<input id="createListButton" type="button" value="Create" onclick="createList()"/></div></div>';
    jQuery("#createOffer").html(innerHtmlForm);
}

//+
function createList() {

    var listName = jQuery('#listName').val();
    var listDescription = jQuery('#listDesc').val();
    var list = {
        "name": listName,
        "description": listDescription,
    };

    jQuery.getJSON("/addNewShoppingList", {
            "listName": listName,
            "listDescription": listDescription,
        },
        function (responseEntity) {
            console.log("...");
        })
        .done(function (responseEntity) {
            console.log(responseEntity.id);
            var html =
                '<div id="query_' + responseEntity.id + '" class="row no-gutter listQuery well" style="margin-top:0px; margin-bottom: 0px; background-color: #00dd00" onclick="showItems(this.id, ' + responseEntity.id + ')"' +
                'onmouseover="showMenu(' + responseEntity.id + ')" onmouseout="hideMenu(' + responseEntity.id + ')" class="btn" data-toggle="collapse" data-target="#query_list_' + responseEntity.id + '">' +
                '<div id="menuShopping' + responseEntity.id + '" class="rc dopMenu box_shadow popup" style="z-index: 10; display: none; font-size: 10px">' +
                '<div style="padding: 0px 8px 5px 8px">' +
                '<a id="editShoppingListLink' + responseEntity.id + '" href="#" onclick="editShoppingList(' + responseEntity.id + ')">' +
                '<span>Edit</span> <img border="0" src="/resources/img/edit.png" width="15" height="15">' +
                '</a>' +
                '</div>' +
                '<div style="padding: 5px 8px; ">' +
                '<a id="deleteShoppingListLink' + responseEntity.id + '" class="deleteShoppingListLink" href="#" onclick="deleteShoppingList(' + responseEntity.id + ')">' +
                '<span>Удалить</span> <img src="/resources/img/del.gif" height="15" width="15" border="0"></a>' +
                '</div>' +
                '</div>' +
                '<div id="query_info_' + responseEntity.id + '" class="col-md-10 text-left">' +
                '<span id="query_name_span_' + responseEntity.id + '" class="listName">' +
                responseEntity.name +
                '</span>' +
                '<span id="query_info_span_' + responseEntity.id + '" style="font-size: 10px">' +
                ' (No contains items)' +
                '</span>' +
                '<br/>' +
                '<span id="query_desc_span_' + responseEntity.id + '" >' +
                responseEntity.desc +
                '</span>' +
                '</div>' +
                '<div class="col-md-2" >' +
                '<span id="img_expand_query_' + responseEntity.id + '" style="cursor:pointer;display:block" title="Expand">' +
                '<img src="/resources/img/v3_expand.png" style="" border="0" vspace="8" width="15" class="btn" data-toggle="collapse" data-target="#query_list_' + responseEntity.id + '"/>' +
                '</span>' +
                '<span id="img_min_query_' + responseEntity.id + '" style="cursor:pointer;display:none" title="Minimize">' +
                '<img src="/resources/img/v3_minim.png" style="" border="0" vspace="8" width="15" class="btn" data-toggle="collapse" data-target="#query_list_' + responseEntity.id + '"/>' +
                '</span>' +
                '</div></div>' +
                '<div id="query_list_' + responseEntity.id + '" class="bBody collapse well " style="margin-top:0px; margin-bottom: 0px; background-color: #9acfea;padding:0px;">' +
                //'<div id="query_list_' + responseEntity.id + '" class="bBody collapse well "></div>' +
                '<div id="items' + responseEntity.id + '" class="items"></div>' +
                '<div id="absent_items_' + responseEntity.id + '" style="display: none">' +
                'You haven`t had items in shopping list yet.<br>' +
                'You can add it simply by click the button below<br>' +
                '</div>' +
                '<div id="addItem_' + responseEntity.id + '" class="btn-success" style="display:none;">' +
                '<span style="cursor: pointer;" onclick="showAddNewItemForm(' + responseEntity.id + ')">Add new item</span>' +
                '</div>' +
                '</div>' +
                '</br>';


            //get div with id shopping_queries
            var shoppingQueryElement = jQuery('#shopping_queries');

            //if shoppingQueryElement exists that add div with new list div
            if (shoppingQueryElement.length != 0) {
                jQuery('#shopping_queries').append(html);

            }
            // else add div with id shopping_queries and append new list div
            else {
                //create new top part off div that contains shopping_query div
                var header = '<div style="text-align: center">' +
                    '<h4>Shopping Lists</h4>' +
                    '</div>' +
                    '<div id="shopping_queries" align="center">' + html +
                    '</div>' +
                    '<div class="btn">' +
                    '<a href="/addShoppingList.html">Add new list</a>' +
                    '</div>';
                // add this part to DOM
                jQuery('#main').html(header);

            }
            // delete form of creating list
            cancelCreateList();

        })
        .fail(function (xhr, status, error) {
            console.log("...0..");
            jQuery("#errorField").html("This list is already exists. Please, change name!");
        });

}

//+
function deleteShoppingList(id) {
    $.getJSON('shoppingList/delete/' + id, {
        ajax: 'true'
    }, function (data) {
        var result = JSON.parse(data.message);
        alert(result);
        jQuery("#query_" + result).remove();
        jQuery("#query_list_" + result).next("br").remove();
        jQuery("#query_list_" + result).remove();

        //jQuery()
        var list = jQuery(".listQuery");
        if (list.length == 0)
            jQuery("#main").html('<br><br><br>You haven`t had shopping list query yet.<br>' +
                'You can add it simply by click the button below' +
                '<br>' +
                '<hr><br><a href="/addShoppingList.html">Add new list</a> <br><br><br><hr/>');

        cancelCreateList();

    });

}

function cancelCreateList() {
    var listQuery = jQuery(".listQuery");
    if (listQuery.length == 0) {
        var innerHtmlForm = '<div class="row">Create new <a id="createList" href="#">List</a><br></div>' +
            '</div>';
        jQuery("#createOffer").html(innerHtmlForm);
        jQuery("#errorField").html("");
    } else {
        var innerHtmlForm = '<div class="row">Select list or item for edit</div>' +
            '<div class="row">or create new:<br>' +
            '<a id="createList" href="#">List</a><br>' +
            '<a id="createItem" href="#" >Item</a>' +
            '</div>';
        jQuery("#createOffer").html(innerHtmlForm);
        jQuery("#errorField").html("");
        jQuery('#createItem').click(function () {

            createItemForm();
            return false;
        });
    }


    jQuery('#createList').click(function () {
        createListForm();
        return false;
    });


}


function editList(id) {
    var name = jQuery("#listName").val().trim();
    var desc = jQuery("#listDesc").val().trim();

    var dataToJson = {
        id: id,
        name: name,
        desc: desc,

    }
    var headers = {};
    var data = {};
    data[csrfParameter] = csrfToken;
    //            data[]
    headers[csrfHeader] = csrfToken;
    console.log(data);
    jQuery(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(csrfHeader, csrfToken);
        xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

    });


    jQuery.ajax({
        crossOrigin: true,
        url: '/editList',
        type: "post",
        headers: headers,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(dataToJson),

        success: function (data) {

            var result = (data.id);
            console.log(result);
            jQuery("#query_name_span_" + data.id).text(data.name);
            jQuery("#query_desc_span_" + data.id).text(data.desc);

            cancelCreateList();
        },
        error: function (jqXHR, textStatus, errorThrown) {

            jQuery("#errorField").html("Error has occured when edit the list");

        }
    });


}


function editItem(id) {

    var listId = jQuery("#selectList :selected").val();

    var name = jQuery("#itemName").val();
    var desc = jQuery("#itemDesc").val().trim();
    var price = jQuery("#itemPrice").val().trim();
    var currency = jQuery("#selectCurrency :selected").val();


    var dataToJson = {
        id: id,
        name: name,
        desc: desc,
        price: price,
        currencyType: currency,
        listId: listId
    }

    var headers = {};
    var data = {};
    data[csrfParameter] = csrfToken;
    //            data[]
    headers[csrfHeader] = csrfToken;
    console.log(data);
    jQuery(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(csrfHeader, csrfToken);
        xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

    });


    jQuery.ajax({
        crossOrigin: true,
        url: '/editItem',
        type: "post",
        headers: headers,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(dataToJson),

        success: function (data) {
            var elementItem = jQuery("#item" + data.id);
            jQuery("#item_name_" + data.id).text(data.name);
            jQuery("#item_desc_" + data.id).text(data.desc);
            jQuery("#item_price_" + data.id).text(data.price.toFixed(2));
            jQuery("#item_currency_" + data.id).text(data.currencyType);
            var currentListId = elementItem.parent().attr("id").replace("items", "");
            var itemsInNewList = jQuery("" +
                "query_list_" + data.shoppingListQuery.id + " .itemOfList");
            var countOfItemsInNewList = itemsInNewList.length;
            changeListInfo(data.shoppingListQuery.id,  data.shoppingListQuery.sum, data.shoppingListQuery.type);
            cancelCreateList();
        },
        error: function (jqXHR, textStatus, errorThrown) {

            jQuery("#errorField").html("Error has occured when edit the item");

        }
    });


}


function editItemWithMovingToAnotherList(id) {

    var listId = jQuery("#selectList :selected").val();

    var name = jQuery("#itemName").val();
    var desc = jQuery("#itemDesc").val().trim();
    var price = jQuery("#itemPrice").val().trim();
    var currency = jQuery("#selectCurrency :selected").val();


    var dataToJson = {
        id: id,
        name: name,
        desc: desc,
        price: price,
        currencyType: currency,
        listId: listId
    }

    var headers = {};
    var data = {};
    data[csrfParameter] = csrfToken;
    //            data[]
    headers[csrfHeader] = csrfToken;
    console.log(data);
    jQuery(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(csrfHeader, csrfToken);
        xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

    });


    jQuery.ajax({
        crossOrigin: true,
        url: '/editItemWithMoving',
        type: "post",
        headers: headers,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(dataToJson),

        success: function (data) {
            var oldItem;
            var newItem;
            if(data[1].shoppingListQuery.id==listId){
                newItem=data[1];
                oldItem=data[0];

            }else{
                newItem=data[0];
                oldItem=data[1];
            }

            var elementItem = jQuery("#item" + newItem.id);
            jQuery("#item_name_" + newItem.id).text(newItem.name);
            jQuery("#item_desc_" + newItem.id).text(newItem.desc);
            jQuery("#item_price_" + newItem.id).text(newItem.price.toFixed(2));
            jQuery("#item_currency_" + newItem.id).text(newItem.currencyType);

            var currentListId = elementItem.parent().attr("id").replace("items", "");
            if (currentListId != newItem.shoppingListQuery.id) {
                jQuery("#items" + newItem.shoppingListQuery.id).append(elementItem);
            }

            var itemsInNewList = jQuery("#query_list_" + newItem.shoppingListQuery.id + " .itemOfList");
            var countOfItemsInNewList = itemsInNewList.length;
            var itemsInOldList = jQuery("#query_list_" + oldItem.shoppingListQuery.id + " .itemOfList");
            var countOfItemsInOldList = itemsInOldList.length;

            if(countOfItemsInOldList==0){
                var x='<div id="absent_items_' + currentListId+ '" style="display: block">' +
                    'You haven`t had items in shopping list yet.<br>' +
                    'You can add it simply by click the button below<br>' +
                    '</div>';
                jQuery("#addItem_" + currentListId).before(x);

            }

            jQuery("#absent_items_" + newItem.shoppingListQuery.id).hide();


            changeListInfo(oldItem.shoppingListQuery.id, oldItem.shoppingListQuery.sum, oldItem.shoppingListQuery.type);
            changeListInfo(newItem.shoppingListQuery.id,  newItem.shoppingListQuery.sum, newItem.shoppingListQuery.type);
            cancelCreateList();
        },
        error: function (jqXHR, textStatus, errorThrown) {

            jQuery("#errorField").html("Error has occured when edit the item");

        }
    });


}

function  changeListInfo(listId, sum, currency) {
    var criteria = "#query_list_" + listId+ " .itemOfList";
    var items = jQuery(criteria);
    var count = items.length;

if(count!=0){

    jQuery('#query_info_span_' + listId).text(" (Contains " + count + " item. Total price is " + sum.toFixed(2) + " " + currency+ ") ");
}else{
    jQuery('#query_info_span_' + listId).text("(No contains items)");
}

}

function editShoppingList(id) {
    createListForm();
    jQuery("#createListButton").val("Edit");

    jQuery("#createListButton").attr("onclick", "editList(" + id + ")");


    var listName = jQuery("#query_name_span_" + id).text().trim();
    jQuery("#listName").val(listName.trim());
    var listDesc = jQuery("#query_desc_span_" + id).text().trim();
    jQuery("#listDesc").val(listDesc.trim());

    jQuery("#createListButton").attr("disabled", "disabled");
    jQuery("#listName").on("input", function () {

        checkField(listName, listDesc)
    });
    jQuery("#listDesc").on("input", function () {

        checkField(listName, listDesc)
    });

}


function checkField(textName, textDesc) {
    var newNameValue = jQuery("#listName").val();
    var newDescValue = jQuery("#listDesc").val().trim();
    if (newNameValue.trim() == "") {
        jQuery("#createListButton").prop("disabled", true);

    }
    else if (newNameValue == textName) {
        if (newDescValue == textDesc) {
            jQuery("#createListButton").prop("disabled", true);
        } else {
            jQuery("#createListButton").prop("disabled", false);
        }

    }
    else
        jQuery("#createListButton").prop("disabled", false);

}


function editShoppingItem(id) {
    createItemForm();
    jQuery("#createItemButton").val("Edit");

    jQuery("#createItemButton").attr("onclick", "editItem(" + id + ")");

    var itemList = jQuery("#item" + id).parent().attr("id").replace("items", "");
    var itemName = jQuery("#item_name_" + id).text().trim();
    var itemDesc = jQuery("#item_desc_" + id).text().trim();
    var itemPrice = jQuery("#item_price_" + id).text().trim();
    var itemCurrency = jQuery("#item_currency_" + id).text().trim();

    jQuery("#selectList").val(itemList.trim());
    jQuery("#itemName").val(itemName.trim());
    jQuery("#itemDesc").val(itemDesc.trim());
    jQuery("#itemPrice").val(itemPrice.trim());
    jQuery("#selectCurrency").val(itemCurrency);


    jQuery("#createItemButton").attr("disabled", "disabled");


    jQuery("#selectList").on("change", function () {
        checkFieldItem(itemList, itemName, itemDesc, itemPrice, itemCurrency)
    });
    jQuery("#selectCurrency").on("change", function () {

        checkFieldItem(itemList, itemName, itemDesc, itemPrice, itemCurrency)
    });
    jQuery("#itemName").on("input", function () {
        checkFieldItem(itemList, itemName, itemDesc, itemPrice, itemCurrency)
    });
    jQuery("#itemDesc").on("input", function () {
        checkFieldItem(itemList, itemName, itemDesc, itemPrice, itemCurrency)
    });
    jQuery("#itemPrice").on("input", function () {
        checkFieldItem(itemList, itemName, itemDesc, itemPrice, itemCurrency)
    });



}

function checkFieldItem(textList, textName, textDesc, textPrice, textCurrency) {


    if (isInputChanged("itemName", textName) ||
        isInputChanged("itemDesc", textDesc) ||
        isInputChanged("itemPrice", textPrice) ||
        isSelectChanged("selectCurrency", textCurrency) ||
        isSelectChanged("selectList", textList)) {
        jQuery("#createItemButton").prop("disabled", false);

    }
    else {
        jQuery("#createItemButton").prop("disabled", true);

    }

    var x= jQuery("#createItemButton").attr("onclick");
    var start=x.indexOf("(");
    var id=x.substring(start, x.length);

    if(isSelectChanged("selectList", textList)){
        // var x= jQuery("#createItemButton").attr("onclick");
        jQuery("#createItemButton").attr("onclick", "editItemWithMovingToAnotherList" + id);
    }else{
        jQuery("#createItemButton").attr("onclick", "editItem" + id);
    }



}

function isInputChanged(elementId, oldValue) {
    var newNameValue = jQuery("#" + elementId).val().trim();
    var a = newNameValue != oldValue;
    var b = newNameValue != "";
    return a && b;
}

function isSelectChanged(elementId, oldValue) {
    var newNameValue = jQuery("#" + elementId + " :selected").val();
    return newNameValue != oldValue && newNameValue != "";
}


function createItem() {
    var list = jQuery("#selectList option:selected").val();
    var listId = list.replace("query_info_", "");

    var itemName = jQuery('#itemName').val();
    var itemDescription = jQuery('#itemDesc').val();
    var itemPrice = jQuery('#itemPrice').val();
    var currName = jQuery("#selectCurrency option:selected").val();

    var dataToJson = {
            listId: listId,
            name: itemName,
            desc: itemDescription,
            price: itemPrice,
            currencyType: currName,
        }
        ;


    var headers = {};
    var data = {};
    data[csrfParameter] = csrfToken;
    //            data[]
    headers[csrfHeader] = csrfToken;
    console.log(data);
    jQuery(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(csrfHeader, csrfToken);
        xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

    });


    jQuery.ajax({
        crossOrigin: true,
        url: '/addNewShoppingItem1',
        type: "post",
        headers: headers,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(dataToJson),

        success: function (data) {

            var result = (data.id);
            console.log(result);
            var html =
                '<div id="item' + data.id + '" class="row itemOfList" data-sort="'+data.priority+'">' +
                '<div class="col-md-6 text-left" >' +
                '<span id="item_name_'+data.id+'" style="margin-left: 10px">' + data.name + '</span><br>' +
                '<div style="margin-left: 10px" class="tMini tGray">Description: <span id="item_desc_'+data.id+'"> ' + data.desc +
                '</span></div>' +
                '</div>' +
                '<div class="col-md-2 text-left">' +
                '<span onclick="deleteItem(' + data.id + ')">Удалить</span><br>' +
                '<span onclick="editShoppingItem(' + data.id + ')">Edit</span><br>' +
                '<span onclick="itemUp(' + data.id + ')">Поднять</span><br>' +
                '<span onclick="itemDown(' + data.id + ')">Опустить</span>' +
                '</div>' +
                '<div class="col-md-4 text-right" >' +
                '<div style="margin-right: 5px">' +
                '<span title="Price">' +
                '<div class="sum">' +
                '<span id="item_price_'+data.id+'" class="green">' + data.price.toFixed(2) + ' </span>' +
                '<span id="item_currency_'+data.id+'" class="tMini tGray">' + data.currencyType +
                '</span>' +
                '</div>' +
                '</span>' +
                '</div>' +
                '</div></div>';


            /*href="/account/' + result.id + '/delete.html"*/
            jQuery('#items' + data.shoppingListQuery.id).append(html);
            hideAddNewItemForm(data.shoppingListQuery.id);
            var absentItems = jQuery('#absent_items_' + data.shoppingListQuery.id);
            absentItems.hide();
            //jQuery(newItemField).insertBefore('#absent_items_' + data1.shoppingListQuery.id);
            //alert("прибыли данные " + data1.join("\n"));
            var span = jQuery('#query_info_span_' + data.shoppingListQuery.id);
            var x = span.text();
            var criteria = "#query_list_" + data.shoppingListQuery.id + " .itemOfList";
            var items = jQuery(criteria);
            var count = items.length;
            var sum = data.shoppingListQuery.sum;
            var type = data.shoppingListQuery.type;

            jQuery('#query_info_span_' + data.shoppingListQuery.id).text(" (Contains " + count + " item. Total price is " + sum.toFixed(2) + " " + type + ") ");


            cancelCreateList();
        },
        error: function (jqXHR, textStatus, errorThrown) {

            jQuery("#errorField").html("This item is already exists. Please, change name!");

        }
    });


}


function saveItem() {
    var name = jQuery('#newItemField').val();
    var description = jQuery('#itemCommentField').val();
    var price = jQuery('#newSumField').val();
    var currency = jQuery("#currencyElement option:selected").val();
    // var = currencyElement.options[currencyElement.selectedIndex].value;

    var dataToJson = {
            name: name,
            desc: description,
            price: price,
            currencyType: currency,
            listId: listId
//                        user:

        }
        ;

    var headers = {};
    var data = {};
    data[csrfParameter] = csrfToken;
    //            data[]
    headers[csrfHeader] = csrfToken;
    console.log(data);
    jQuery(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(csrfHeader, csrfToken);
        xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

    });
    jQuery.ajax({
        crossOrigin: true,
        url: 'shoppingList/save',
        type: "post",
        headers: headers,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(dataToJson),

        success: function (data) {
            var data1 = JSON.parse(data.message);
            var newItemField =
                '<div id="item' + data1.id + '" class="row itemOfList" data-sort="'+data1.priority+'">' +
                '<div class="col-md-6 text-left" >' +
                '<span style="margin-left: 10px">' + data1.name + '</span><br>' +
                '<div style="margin-left: 10px" class="tMini tGray">Description: ' + data1.desc +
                '</div>' +
                '</div>' +
                '<div class="col-md-2 text-left">' +
                '<span onclick="deleteItem(' + data1.id + ')">Удалить</span><br>' +
                '<span onclick="itemUp(' + data1.id + ')">Поднять</span><br>' +
                '<span onclick="itemDown(' + data1.id + ')">Опустить</span>' +
                '</div>' +
                '<div class="col-md-4 text-right" >' +
                '<div style="margin-right: 5px">' +
                '<span title="Price">' +
                '<div class="sum">' +
                '<span class="green">' + data1.price.toFixed(2) + ' </span>' +
                '<span class="tMini tGray">' + data1.currencyType +
                '</span>' +
                '</div>' +
                '</span>' +
                '</div>' +
                '</div></div>';
            jQuery('#items' + data1.shoppingListQuery.id).append(newItemField);
            hideAddNewItemForm(data1.shoppingListQuery.id);
            jQuery('#absent_items_' + data1.shoppingListQuery.id).hide();
            //jQuery(newItemField).insertBefore('#absent_items_' + data1.shoppingListQuery.id);
            //alert("прибыли данные " + data1.join("\n"));
            var x = jQuery('#query_info_span_' + data1.shoppingListQuery.id).text();
            var criteria = "#query_list_" + data1.shoppingListQuery.id + " .itemOfList";
            var items = jQuery(criteria);
            var count = items.length;
            var sum = data1.shoppingListQuery.sum;
            var type = data1.shoppingListQuery.type;
            jQuery('#query_info_span_' + data1.shoppingListQuery.id).text(" (Contains " + count + " item. Total price is " + sum.toFixed(2) + " " + type + ") ");

        },

        error: function (jqXHR, textStatus, errorThrown) {
            jQuery("#errorField").html("This item is already exists. Please, change name!");
        }
    });

}


function hideItems(elementName, id) {

    var elements = jQuery('.order_list_entity_' + id);
    for (var i = 0; i < elements.length; i++) {
        elements[i].hide();
    }

    var element = jQuery('#addItem_' + id);

    if (element) element.hide();

//            var listOfQuery = document.getElementById('query_list_' + id);
//            if (listOfQuery)listOfQuery.style.display = "none";

    var absentItemsMessage = jQuery('#absent_items_' + id);
    if (absentItemsMessage) {
        absentItemsMessage.show();
    }

    var tableHeader = jQuery('#query_name_' + id);
    if (tableHeader) tableHeader.onclick = function () {
        showItems(elementName, id);
    }

    changeSpans(elementName)
}

function showItems(elementName, id) {

    var elements = jQuery('.order_list_entity_' + id);

    for (var i = 0; i < elements.length; i++) {
        elements[i].show();
    }
    var element = jQuery('#addItem_' + id);
    if (element) element.show();

    var absentItemsMessage = jQuery('#absent_items_' + id);
    var x = jQuery("#items" + id + " .itemOfList");
    if (x.length == 0) absentItemsMessage.show();

    var tableHeader = jQuery('#query_name_' + id);
    if (tableHeader) tableHeader.onclick = function () {
        hideItems(elementName, id);
    }

    changeSpans(elementName);

}
function changeSpans(id) {
    var elementId;
    if (id.includes('img_expand_query_')) {
        elementId = id.replace('img_expand_query_', '');
        var element = jQuery("#" + id);
        if (element) element.hide();
        var element = jQuery("#img_min_query_" + elementId);
        if (element) element.show();
    }
    else if (id.includes('img_min_query_')) {
        elementId = id.replace('img_min_query_', '');
        var element = jQuery("#" + id);
        if (element) element.show();
        var element = jQuery("#img_expand_query_" + elementId);
        if (element) element.show();

    }

}

function showAddNewItemForm(id) {
    var element = jQuery("#addItem_" + id);
    if (element) {
        addItemLink = element.html();
        element.html(createFormForNewItem(id));
    }
    ;
}

function createFormForNewItem(id) {
    listId = id;

    var selectCurrency = '<select id="currencyElement">';
    var fav_curr = "${def_curr}";
    jQuery.each(currencies, function (i) {
        if (fav_curr == currencies[i]) {

            selectCurrency += '<option selected value="' + currencies[i] + '">' + currencies[i] + '</option>';
        } else {
            selectCurrency += '<option value="' + currencies[i] + '">' + currencies[i] + '</option>';
        }

    });
    selectCurrency += '</select>';


    var element =

        '<div  id="formForList_' + id + '">' +
        '<div class="row">' +
        '<div class="col-md-7" style="padding: 4px 0px 0px 20px;">' +
        '<input id="newItemField" style="width: 100%;" class="input-sm" maxlength="48" value="" title="Новая покупка (название)" tabindex="1" placeholder="Новая покупка (название)" type="text"</input>' +
        '</div>' +
        '<div class="col-md-3" style="padding: 4px 0px 0px 5px;">' +
        '<input id="newSumField" style="width: 100%; " class="input-sm" maxlength="48" value="" title="Summa" tabindex="2" placeholder="Cумма" type="text"</input>' +
        '</div>' +
        '<div class="col-md-2" style="padding: 4px 18px 5px 0px;">' +
        selectCurrency +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-md-9 text-left" style="padding-top: 4px">' +
        '<span id="itemCommentOption" class="tMini dash" onclick="addCommentField(this.id)"  style="margin-left:5px;">Добавить комментарий</span>' +
        '</div>' +
        '<div class="col-md-3">' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-md-7 " style="padding-left:20px">' +
        '<textarea id="itemCommentField" tabindex="3" style="margin-top: 10px; width: 350px; height: 100px; display: none"></textarea>' +
        '</div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-md-3">' +
        '</div>' +
        '<div class="col-md-2" style="padding-top: 4px; padding-bottom: 4px">' +
        '<input class="btn-success" id="saveItemButton" value="Сохранить" onclick="saveItem()" tabindex="4" type="button"</input>' +
        '</div>' +
        '<div class="col-md-2" style="padding-top: 4px; padding-bottom: 4px">' +
        '<span style="padding: 5px 40px; color: #555;" onclick="hideAddNewItemForm(' + id + ')">' +
        '<span class="dash btn-danger">Отмена</span></span>' +
        '</div>' +
        '</div>' +
        '</div>';

    return element;
}


function hideAddNewItemForm(id) {
    var element = jQuery('#addItem_' + id);
    if (element) element.html(addItemLink);
}
function addCommentField(id) {
    jQuery("#itemCommentField").show();
    jQuery("#" + id).show();
}

function showMenu(id) {
    var a = jQuery("#menuShopping" + id);
    a.show();


}
function hideMenu(id) {
    var a = jQuery("#menuShopping" + id);
    a.hide();

}
function deleteItem(id) {
    jQuery.ajax({
        url: '/shoppingList/item/' + id + '/delete',
        success: function (data) {
            console.log(data.message);
            var s = JSON.parse(data.message);

            var items = jQuery("#item" + s.id).parent().attr("id");
            var listId = items.replace("items", "");
            var x = jQuery("#" + items + " .itemOfList");
            var count = x.length;

            if (count > 1) {
                // var br=jQuery("#query_list_" + s.id).next("br");
                // br.remove();
                jQuery("#item" + s.id).remove();
                jQuery("#query_info_span_" + listId).html("(Contains " + count + " items. Total price is " + s.sum + " " + s.sum + " )");
            }
            else if (count <= 1) {
                // var br=jQuery("#query_list_" + s.id).next("br");
                // br.remove();
                jQuery("#item" + s.id).remove();
                jQuery("#absent_items_" + listId).show();
                jQuery("#query_info_span_" + listId).text("(No contains items)");
            } else {
                alert("Something goes wrong");
            }


        }

    })
}

function itemUp(id) {
    var itemDiv =jQuery("#item"+id);
    var oldDataSort=itemDiv.data("sort");
    var upperItem=itemDiv.prev();
    if(upperItem.length) {


        var dataToJson = {
            id: id,
        }

        var headers = {};
        var data = {};
        data[csrfParameter] = csrfToken;
        headers[csrfHeader] = csrfToken;
        console.log(data);
        jQuery(document).ajaxSend(function (e, xhr, options) {
            xhr.setRequestHeader(csrfHeader, csrfToken);
            xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

        });


        jQuery.ajax({
            crossOrigin: true,
            url: '/item_up',
            type: "post",
            headers: headers,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(dataToJson),

            success: function (data1) {
                itemDiv.insertBefore(upperItem);

                var newDataSort=data1.priority;
                console.log("newDataSort "+newDataSort)

                var x =jQuery("#item"+data1.id);

                    x.attr("data-sort",  newDataSort);
                x.next().attr("data-sort",  oldDataSort);
                console.log("oldDataSort "+oldDataSort);

            },
            error: function (jqXHR, textStatus, errorThrown) {

                // jQuery("#errorField").html("Error has occured when edit the item");

            }
        });
    } else{
     alert("this item is on the top");
    }

}


function itemDown(id) {
    var itemDiv =jQuery("#item"+id);
    var oldDataSort=itemDiv.data("sort");
    var lowerItem=itemDiv.next();
    if(lowerItem.length) {


        var dataToJson = {
            id: id,
        }

        var headers = {};
        var data = {};
        data[csrfParameter] = csrfToken;
        headers[csrfHeader] = csrfToken;
        console.log(data);
        jQuery(document).ajaxSend(function (e, xhr, options) {
            xhr.setRequestHeader(csrfHeader, csrfToken);
            xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

        });


        jQuery.ajax({
            crossOrigin: true,
            url: '/item_down',
            type: "post",
            headers: headers,
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(dataToJson),

            success: function (data1) {
                itemDiv.insertAfter(lowerItem);

                var newDataSort=data1.priority;
                var x =jQuery("#item"+data1.id);
                console.log("DOWN" +x);
                console.log(newDataSort);
                x.attr("data-sort", newDataSort);
                console.log("x"+x.data("sort"));
                x.prev().attr("data-sort",  oldDataSort);
                console.log("x.prev"+x.prev().data("sort"));



            },
            error: function (jqXHR, textStatus, errorThrown) {

                // jQuery("#errorField").html("Error has occured when edit the item");

            }
        });
    } else{
        alert("this item is on the down");
    }



}


