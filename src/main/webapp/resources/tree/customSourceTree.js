function createSource() {
    var newValue = document.getElementById('sourceName').value;
    var node = {
        "id": true,
        "text": newValue,
        "parent": '#'
    };

    $('#tree').jstree().create_node('#',
        node
        , "last", function () {
            jQuery('#tree').jstree(true).set_id(arguments[0], arguments[0].original.id);
            cancelCreateSource();
        });

    cancelCreateSource();

}

function createSourceForm() {
    var innerHtmlForm = '<div class="row" ><div class="col-sm-5">Source</div>' +
        '<div class="col-sm-7"><input id="sourceName" type="text" name="sourceName"/></div></div>' +
        '<div class="row">' +
        '<div class="col-sm-6"></div><div class="col-sm-3">' +
        '<input type="button" value="Cancel" onclick="cancelCreateSource()"/></div>' +
        '<div class="col-sm-3">' +
        '<input type="button" value="Create" onclick="createSource()"/></div></div>';
    jQuery("#createSourceOffer").html(innerHtmlForm);
}
function createSubsourceForm() {
    var sources = jQuery("#tree").jstree(true).get_json('#', {flat: false});
    var select = getSelectSources();
    var innerHtmlForm = '<div class="row" ><div class="col-sm-5">source</div>' +
        '<div class="col-sm-7">' + select + '</div></div>' +
        '<div class="row"><div class="col-sm-5">Subsource</div><div class="col-sm-7">' +
        '<input id="text" type="text" style="width: 100%" name="subsourceName"/></div></div><div class="row">' +
        '<div class="col-sm-6"></div><div class="col-sm-3">' +
        '<input type="button" value="Cancel"  onclick="cancelCreateSource()"/></div>' +
        '<div class="col-sm-3">' +
        '<input type="button" value="Create" onclick="createSubsource()"/></div></div>';
    jQuery("#createSourceOffer").html(innerHtmlForm);
}

function createSubsource() {
    var sourceElement = document.getElementById("selectSource");
    var source = sourceElement.options[sourceElement.selectedIndex].value;

    var subsource = document.getElementById('text').value;

    console.log(source);


    $('#tree').jstree().create_node(source, {
        "id": true,
        "parent": source,
        "text": subsource
    }, "last", function () {
        jQuery('#tree').jstree(true).set_id(arguments[0], arguments[0].original.id);
        cancelCreateSource();
    });
}

$(document).ready(function () {
    jQuery('#createSource').click(function () {
        createSourceForm();
        return false;
    });
    jQuery('#createSubsource').click(function () {
        createSubsourceForm();
        return false;
    });
    $(function () {
        $('#tree')
            .jstree({

                "themes": {
                    "theme": "default",
                    "dots": false,
                    "icons": false,
                    "url": "/css/jstree/themes/default/style.css"
                },

                'core': {
                    'expand_selected_onload': true,
                    'animation': 200,
                    //"check_callback": true,
                    'check_callback': function (op) {
                        var b = true;

                        if (op === "rename_node") {
                            console.log("callback_rename");
                            var newValue = arguments[3];
                            var oldId = arguments[1].id;
                            console.log("callback_rename new value" + newValue);
                            console.log("callback_rename new value" + oldId);
                            //var
                            var originalId = arguments[1].original.id;
                            if (originalId == null) {
                                originalId = true;
                            }
                            var ref = $('#tree').jstree(true),
                                sel = ref.get_selected();
                            $.getJSON("/change_source_name", {
                                    'id': originalId,
                                    'text': newValue,
                                    'parent': arguments[1].parent

                                },
                                function (responseEntity) {

                                })
                                .done(function (responseEntity) {
                                    console.log("callback_rename_done");
                                    var ref = $('#tree').jstree(true);

                                    ref.set_id(oldId, responseEntity.id);

                                    b = true;
                                    console.log("callback_rename_done" + b);

                                })
                                .fail(function (xhr, status, error) {
                                    console.log("callback_rename_fail");

                                    alert("This name is already used=" + newValue + ". Please, enter other one!");
                                    console.log("callback_rename_fail " + xhr.responseJSON.id);

                                    var ref = $('#tree').jstree(true),
                                        sel = ref.get_node(oldId, true);


                                    console.log(sel);

                                    if (sel) {
                                        ref.edit(sel);
                                    }

                                    b = false;
                                    console.log("callback_rename_fail" + b);

                                });


                        }


                        else if (op === "create_node") {
                            console.log("callback_create");
                            //var oldId=arguments[1].node.id;
                            var node = arguments[1];
                            console.log("callback_create " + node);

                            if (arguments[2].id == '#') {
                                console.log("callback_create_source");
                                $.ajaxSetup({
                                    async: false
                                });
                                $.getJSON("/create_source", {
                                        //true
                                        'id': arguments[1].id,
                                        //new node
                                        'text': arguments[1].text,
                                        //#
                                        'parent': arguments[2].id

                                    },
                                    function (responseEntity) {
                                        console.log("...");
                                    })
                                    .done(function (responseEntity) {
                                        console.log("callback_create_source_done");
                                        console.log("callback_create_source id=" + responseEntity.id);

                                        //new id
                                        node.id = responseEntity.id;
                                        console.log("callback_create_source " + node);

                                        var ref = $('#tree').jstree(true),
                                            sel = ref.get_node(responseEntity.id, true);

                                        //var selected= $('#tree').jstree(true).find();
                                        console.log(sel);
                                        if (sel) {
                                            console.log("callback_create_source if sel");

                                            ref.edit(sel);


                                        }

                                        console.log(true);
                                        b = true;
                                        console.log("callback_create_source " + b);


                                    })
                                    .fail(function (xhr, status, error) {
                                        console.log("callback_create_source_fail");

                                        alert("This source is already exists=" + newValue + ". Please, enter other one!");

                                        console.log("to be deleted");

                                        b = false;
                                        console.log("callback_create_source " + b);


                                    });

                            }
                            else {
                                console.log("callback_create_subsource");
                                $.ajaxSetup({
                                    async: false
                                });
                                $.getJSON("/create_subsource", {
                                        //true
                                        'id': arguments[1].id,
                                        //new node
                                        'text': arguments[1].text,
                                        //c123
                                        'parent': arguments[2].id

                                    },
                                    function (responseEntity) {

                                    })
                                    .done(function (responseEntity) {
                                        console.log("callback_create_subsource_done");
                                        console.log("callback_create_subsource_done id =" + responseEntity.id);

                                        node.id = responseEntity.id;
                                        console.log("callback_create_subsource " + node.id);

                                        var ref = $('#tree').jstree(true),
                                            sel = ref.get_node(responseEntity.id, true);

                                        //var selected= $('#tree').jstree(true).find();
                                        console.log(sel);
                                        if (sel) {
                                            console.log("callback_create_subsource if sel");

                                            ref.edit(sel);


                                        }


                                        ////$("tree").jstree(true).set_id(obj.node, responseEntity.id);
                                        //$('#tree').jstree(true).set_id(arguments[1].id, responseEntity.id);
                                        //// /new id
                                        ////node.id = responseEntity.id;

                                        console.log(true);
                                        b = true;

                                    })
                                    .fail(function (xhr, status, error) {
                                        console.log("callback_create_subsource_fail");

                                        //console.log("error");
                                        alert("This subsource is already exists=" + newValue + ". Please, enter other one!");
                                        //console.log(xhr.responseJSON.text);
                                        //console.log('c' + xhr.responseJSON.id);
                                        console.log("callback_create_subsource_fail");

                                        b = false;

                                    });


                            }


                        }


                        else if (op === "delete_node") {
                            console.log("callback_delete");

                            $.getJSON("/delete_source", {
                                    'id': arguments[1].id

                                },
                                function (responseEntity) {

                                })
                                .done(function (responseEntity) {
                                    console.log("callback_delete_done");

                                    b = true;
                                    cancelCreateSource();
                                })
                                .fail(function (xhr, status, error) {

                                    console.log("callback_delete_fail");
                                    alert("Error. To delete source try to delete all subsources and operations that use it")
                                    b = false;
                                    return false;
                                });


                        }

                        return b;

                    },
                    'force_text': true,
                    'data': {
                        'url': function (node) {
                            return '/getAllSourcesJSON';
                        },
                        'data': function (node) {
                            return {'text': node.text};
                        }
                    }
                },

                plugins: ["themes", "json_data", "ui", "crrm", "hotkeys", "types", "contextmenu", "sort", "wholerow"],
                "sort": function (a, b) {
                    return this.get_text(a) > this.get_text(b) ? 1 : -1;
                },
                "contextmenu": {
                    "items": function () {
                        return {
                            "create": {
                                "separator_before": false,
                                "separator_after": true,
                                "label": "Create",
                                "action": false,
                                "submenu": {
                                    "create_source": {
                                        "seperator_before": false,
                                        "seperator_after": false,
                                        "label": "source",
                                        action: function (obj) {
                                            console.log("action_create_source");

                                            var ref = $('#tree').jstree(true),
                                                sel = ref.get_selected();
                                            console.log("action_create_source init vars");
                                            if (!sel.length) {
                                                return false;
                                            }
                                            sel = sel[0];
                                            sel = ref.create_node('#');

                                            console.log("action_create_source after create node");
                                            if (sel) {

                                                ref.edit(sel);


                                            }
                                            console.log("action_create_source_end");

                                        }
                                    },
                                    "create_subsource": {
                                        "seperator_before": false,
                                        "seperator_after": false,
                                        "label": "Subsource",
                                        action: function (obj) {
                                            console.log("action_create_subsource");
                                            var ref = $('#tree').jstree(true),
                                                //selected node id
                                                sel = ref.get_selected();
                                            //parent of sel
                                            var parent = ref.get_parent(sel);
                                            if (!sel.length) {
                                                return false;
                                            }
                                            sel = sel[0];
                                            if (sel.indexOf("c") === 0) {
                                                sel = ref.create_node(sel, false);
                                                if (sel) {
                                                    ref.edit(sel);
                                                }
                                            } else {
                                                sel = ref.create_node(parent, false);
                                                if (sel) {
                                                    ref.edit(sel);
                                                }
                                            }
                                            console.log("action_create_subsource_end");

                                        }
                                    }
                                }
                            },
                            "Rename": {
                                "label": "Rename",
                                "action": function (data) {
                                    console.log("action_rename");

                                    var inst = $.jstree.reference(data.reference);
                                    obj = inst.get_node(data.reference);
                                    inst.edit(obj);
                                    console.log("action_rename_end");

                                }
                            },
                            "Delete": {
                                "label": "Delete",
                                "action": function (data) {
                                    console.log("action_delete");

                                    console.log("delete start");
                                    var ref = $.jstree.reference(data.reference),
                                        sel = ref.get_selected();
                                    if (!sel.length) {
                                        return false;
                                    }
                                    ref.delete_node(sel);
                                    console.log("delete end");
                                }
                            }
                        };
                    }
                }
            }).bind("select_node.jstree", function (evt, data) {
            addNodeInfo(data);
        })

    });
});
function getSelectSources() {
    var sources = jQuery("#tree").jstree(true).get_json('#', {flat: false});
    var select = '<select id="selectSource" style="width: 100%">';
    jQuery.each(sources, function (key, value) {
        select += '<option value="' + value.id + '">' + value.text + '</option>';
    });
    select += '</select>';
    return select;
}
function addNodeInfo(data) {
    //removeNodeInfo(data);
    var nodeName = data.node.text;
    var id1 = data.node.id;
    var icon = data.node.icon;
    var parent = data.node;

    //if source is chosen
    if (id1[0] == "c") {

        document.getElementById('createSourceOffer').innerHTML =
            '<div class="row">' +
            '<div class="col-sm-3">' +
            'source\'s name' +
            '</div>' +
            '<div class="col-sm-9">' +
            ' <input id ="text" type="text" name="name" value="' + nodeName + '"/>' +
            '</div>' +
            '</div>' +
            '<br>' +
            '<div class="row">' +
            '<div class="col-sm-6">' +
            '</div>' +
            '<div class="col-sm-3">' +
            '<input type="button" value="Cancel" onclick="cancelCreateSource()"/>' +
            '</div>' +
            '<div class="col-sm-3">' +
            '<input type="button" value="Change" onclick="changeSource(\'' + id1.toString() + '\',\' ' + icon + '\', \'' + parent.parent + '\')"/>' +
            '</div>';


    }
    //if subsource is chosen
    else if (id1[0] = "s") {
        var parentName = jQuery("#" + parent.parent).children("a").text();
        var select = getSelectSources();
        document.getElementById('createSourceOffer').innerHTML =
            '<div class="row">' +
            '<div class="col-sm-5">' +
            'source ' +
            '</div>' +
            '<div class="col-sm-7">' + select + '</div></div>' +
            '<div class="row">' +
            '<div class="col-sm-5">' +
            'Subsource\'s Name' +
            '</div>' +
            '<div class="col-sm-7">' +
            '<input id ="text" type="text" name="subsourceName" value="' + nodeName + '"/>' +
            '</div>' +
            '<br>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-sm-6"></div>' +
            '<div class="col-sm-3">' +
            '<input type="button" value="Cancel" onclick="cancelCreateSource()"/>' + '</div>' +
            '<div class="col-sm-3">' +
            '<input type="button" value="Change" onclick="changeSource(\'' + id1.toString() + '\',\' ' + icon + '\', \'' + parent.parent + '\')"/>' + '</div>';


    }


}
function changeSource(identification, icon, parent) {

    var newValue = document.getElementById('text').value;

    var sourceName = jQuery('#sourceName').value;

    $.getJSON("/change_source_name", {
            'id': identification,
            'text': newValue,
            'parent': parent

        },
        function (responseEntity) {
            console.log(responseEntity);
        })
        .done(function (responseEntity) {
            console.log("callback_rename_done");
            var ref = $('#tree').jstree(true);
            ref.set_id(identification, responseEntity.id);
            $('#tree').jstree('rename_node', responseEntity.id, responseEntity.text);
        })
        .fail(function (xhr, status, error) {
            console.log("callback_rename_fail");

            alert("This name is already used=" + newValue + ". Please, enter other one!");
            console.log("callback_rename_fail " + xhr.responseJSON.id);

            var ref = $('#tree').jstree(true),
                sel = ref.get_node(identification, true);
            console.log(sel);

            if (sel) {
                ref.edit(sel);
            }


        });

    cancelCreateSource();
}

function cancelCreateSource() {
    var innerHtmlForm = '<div class="row">Select source or subsource for edit</div>' +
        '<div class="row">or create the new:<br>' +
        '<a id="createSource" href="#">Source</a><br>' +
        '<a id="createSubsource" href="#" >Subsource</a>' +
        '</div>';

    jQuery("#createSourceOffer").html(innerHtmlForm);
    jQuery('#createSource').click(function () {
        createSourceForm();
        return false;
    });
    jQuery('#createSubsource').click(function () {
        createSubsourceForm();
        return false;
    });

}
