function createCat() {
    var newValue = document.getElementById('categoryName').value;
    var node = {
        "id": true,
        "text": newValue,
        "parent": '#'
    };

    $('#tree').jstree().create_node('#',
        node
        , "last", function () {
            jQuery('#tree').jstree(true).set_id(arguments[0], arguments[0].original.id);
            cancelCreateCat();
        });

    cancelCreateCat();

}

function createCatForm() {
    var innerHtmlForm = '<div class="row" ><div class="col-sm-5">Category</div>' +
        '<div class="col-sm-7"><input id="categoryName" type="text" name="catname"/></div></div>' +
        '<div class="row">' +
        '<div class="col-sm-6"></div><div class="col-sm-3">' +
        '<input type="button" value="Cancel" onclick="cancelCreateCat()"/></div>' +
        '<div class="col-sm-3">' +
        '<input type="button" value="Create" onclick="createCat()"/></div></div>';
    jQuery("#createCatOffer").html(innerHtmlForm);
}
function createSubcatForm() {
    var cats = jQuery("#tree").jstree(true).get_json('#', {flat: false});
    var select = getSelectCategories();
    var innerHtmlForm = '<div class="row" ><div class="col-sm-5">Category</div>' +
        '<div class="col-sm-7">' + select + '</div></div>' +
        '<div class="row"><div class="col-sm-5">Subcategory</div><div class="col-sm-7">' +
        '<input id="text" type="text" style="width: 100%" name="subcatname"/></div></div><div class="row">' +
        '<div class="col-sm-6"></div><div class="col-sm-3">' +
        '<input type="button" value="Cancel"  onclick="cancelCreateCat()"/></div>' +
        '<div class="col-sm-3">' +
        '<input type="button" value="Create" onclick="createSubCat()"/></div></div>';
    jQuery("#createCatOffer").html(innerHtmlForm);
}

function createSubCat() {
    var categoryElement = document.getElementById("selectCat");
    var category = categoryElement.options[categoryElement.selectedIndex].value;

    var subcat = document.getElementById('text').value;


    $('#tree').jstree().create_node(category, {
        "id": true,
        "parent": category,
        "text": subcat
    }, "last", function () {
        jQuery('#tree').jstree(true).set_id(arguments[0], arguments[0].original.id);
        cancelCreateCat();
    });
}

$(document).ready(function () {
    jQuery('#createCategory').click(function () {
        createCatForm();
        return false;
    });
    jQuery('#createSubcategory').click(function () {
        createSubcatForm();
        return false;
    });
    $(function () {
        $('#tree')
            .jstree({

                "themes": {
                    "theme": "default",
                    "dots": false,
                    "icons": false,
                    "url": "/css/jstree/themes/default/style.css"
                },

                'core': {
                    'expand_selected_onload': true,
                    'animation': 200,
                    //"check_callback": true,
                    'check_callback': function (op) {
                        var b = true;

                        if (op === "rename_node") {
                            console.log("callback_rename");
                            var newValue = arguments[3];
                            var oldId = arguments[1].id;
                            console.log("callback_rename new value" + newValue);
                            console.log("callback_rename new value" + oldId);
                            //var
                            var originalId = arguments[1].original.id;
                            if (originalId == null) {
                                originalId = true;
                            }
                            var ref = $('#tree').jstree(true),
                                sel = ref.get_selected();
                            $.getJSON("/change_category_name", {
                                    'id': originalId,
                                    'text': newValue,
                                    'parent': arguments[1].parent

                                },
                                function (responseEntity) {

                                })
                                .done(function (responseEntity) {
                                    console.log("callback_rename_done");
                                    var ref = $('#tree').jstree(true);

                                    ref.set_id(oldId, responseEntity.id);

                                    b = true;
                                    console.log("callback_rename_done" + b);

                                })
                                .fail(function (xhr, status, error) {
                                    console.log("callback_rename_fail");

                                    alert("This name is already used=" + newValue + ". Please, enter other one!");
                                    console.log("callback_rename_fail " + xhr.responseJSON.id);

                                    var ref = $('#tree').jstree(true),
                                        sel = ref.get_node(oldId, true);


                                    console.log(sel);

                                    if (sel) {
                                        ref.edit(sel);
                                    }

                                    b = false;
                                    console.log("callback_rename_fail" + b);

                                });


                        }


                        else if (op === "create_node") {
                            console.log("callback_create");
                            //var oldId=arguments[1].node.id;
                            var node = arguments[1];
                            console.log("callback_create " + node);

                            if (arguments[2].id == '#') {
                                console.log("callback_create_cat");
                                $.ajaxSetup({
                                    async: false
                                });
                                $.getJSON("/create_category", {
                                        //true
                                        'id': arguments[1].id,
                                        //new node
                                        'text': arguments[1].text,
                                        //#
                                        'parent': arguments[2].id

                                    },
                                    function (responseEntity) {
                                        console.log("...");
                                    })
                                    .done(function (responseEntity) {
                                        console.log("callback_create_cat_done");
                                        console.log("callback_create_cat id=" + responseEntity.id);

                                        //new id
                                        node.id = responseEntity.id;
                                        console.log("callback_create_cat " + node);

                                        var ref = $('#tree').jstree(true),
                                            sel = ref.get_node(responseEntity.id, true);

                                        //var selected= $('#tree').jstree(true).find();
                                        console.log(sel);
                                        if (sel) {
                                            console.log("callback_create_cat if sel");

                                            ref.edit(sel);


                                        }

                                        console.log(true);
                                        b = true;
                                        console.log("callback_create_cat " + b);


                                    })
                                    .fail(function (xhr, status, error) {
                                        console.log("callback_create_cat_fail");

                                        alert("This category is already exists=" + newValue + ". Please, enter other one!");

                                        console.log("to be deleted");

                                        b = false;
                                        console.log("callback_create_cat " + b);


                                    });

                            }
                            else {
                                console.log("callback_create_subcat");
                                $.ajaxSetup({
                                    async: false
                                });
                                $.getJSON("/create_subcategory", {
                                        //true
                                        'id': arguments[1].id,
                                        //new node
                                        'text': arguments[1].text,
                                        //c123
                                        'parent': arguments[2].id

                                    },
                                    function (responseEntity) {

                                    })
                                    .done(function (responseEntity) {
                                        console.log("callback_create_subcat_done");
                                        console.log("callback_create_subcat_done id =" + responseEntity.id);

                                        node.id = responseEntity.id;
                                        console.log("callback_create_subcat " + node.id);

                                        var ref = $('#tree').jstree(true),
                                            sel = ref.get_node(responseEntity.id, true);

                                        //var selected= $('#tree').jstree(true).find();
                                        console.log(sel);
                                        if (sel) {
                                            console.log("callback_create_subcat if sel");

                                            ref.edit(sel);


                                        }


                                        ////$("tree").jstree(true).set_id(obj.node, responseEntity.id);
                                        //$('#tree').jstree(true).set_id(arguments[1].id, responseEntity.id);
                                        //// /new id
                                        ////node.id = responseEntity.id;

                                        console.log(true);
                                        b = true;

                                    })
                                    .fail(function (xhr, status, error) {
                                        console.log("callback_create_subcat_fail");

                                        //console.log("error");
                                        alert("This subcategory is already exists=" + newValue + ". Please, enter other one!");
                                        //console.log(xhr.responseJSON.text);
                                        //console.log('c' + xhr.responseJSON.id);
                                        console.log("callback_create_subcat_fail");

                                        b = false;

                                    });


                            }


                        }


                        else if (op === "delete_node") {
                            console.log("callback_delete");

                            $.getJSON("/delete_category", {
                                    'id': arguments[1].id

                                },
                                function (responseEntity) {

                                })
                                .done(function (responseEntity) {
                                    console.log("callback_delete_done");

                                    b = true;
                                    cancelCreateCat();
                                })
                                .fail(function (xhr, status, error) {

                                    console.log("callback_delete_fail");
                                    alert("Error. To delete category try to delete all subcategories and operations that use it")
                                    b = false;
                                    return false;
                                });


                        }

                        return b;

                    },
                    'force_text': true,
                    'data': {
                        'url': function (node) {
                            return '/getAllCategoriesJSON';
                        },
                        'data': function (node) {
                            return {'text': node.text};
                        }
                    }
                },

                plugins: ["themes", "json_data", "ui", "crrm", "hotkeys", "types", "contextmenu", "sort", "wholerow"],
                "sort": function (a, b) {
                    return this.get_text(a) > this.get_text(b) ? 1 : -1;
                },
                "contextmenu": {
                    "items": function () {
                        return {
                            "create": {
                                "separator_before": false,
                                "separator_after": true,
                                "label": "Create",
                                "action": false,
                                "submenu": {
                                    "create_category": {
                                        "seperator_before": false,
                                        "seperator_after": false,
                                        "label": "Category",
                                        action: function (obj) {
                                            console.log("action_create_cat");

                                            var ref = $('#tree').jstree(true),
                                                sel = ref.get_selected();
                                            console.log("action_create_cat init vars");
                                            if (!sel.length) {
                                                return false;
                                            }
                                            sel = sel[0];
                                            sel = ref.create_node('#');

                                            console.log("action_create_cat after create node");
                                            if (sel) {

                                                ref.edit(sel);


                                            }
                                            console.log("action_create_cat_end");

                                        }
                                    },
                                    "create_subcategory": {
                                        "seperator_before": false,
                                        "seperator_after": false,
                                        "label": "Subcategory",
                                        action: function (obj) {
                                            console.log("action_create_subcat");
                                            var ref = $('#tree').jstree(true),
                                                //selected node id
                                                sel = ref.get_selected();
                                            //parent of sel
                                            var parent = ref.get_parent(sel);
                                            if (!sel.length) {
                                                return false;
                                            }
                                            sel = sel[0];
                                            if (sel.indexOf("c") === 0) {
                                                sel = ref.create_node(sel, false);
                                                if (sel) {
                                                    ref.edit(sel);
                                                }
                                            } else {
                                                sel = ref.create_node(parent, false);
                                                if (sel) {
                                                    ref.edit(sel);
                                                }
                                            }
                                            console.log("action_create_subcat_end");

                                        }
                                    }
                                }
                            },
                            "Rename": {
                                "label": "Rename",
                                "action": function (data) {
                                    console.log("action_rename");

                                    var inst = $.jstree.reference(data.reference);
                                    obj = inst.get_node(data.reference);
                                    inst.edit(obj);
                                    console.log("action_rename_end");

                                }
                            },
                            "Delete": {
                                "label": "Delete",
                                "action": function (data) {
                                    console.log("action_delete");

                                    console.log("delete start");
                                    var ref = $.jstree.reference(data.reference),
                                        sel = ref.get_selected();
                                    if (!sel.length) {
                                        return false;
                                    }
                                    ref.delete_node(sel);
                                    console.log("delete end");
                                }
                            }
                        };
                    }
                }
            }).bind("select_node.jstree", function (evt, data) {
            addNodeInfo(data);
        })

    });
});
function getSelectCategories() {
    var cats = jQuery("#tree").jstree(true).get_json('#', {flat: false});
    var select = '<select id="selectCat" style="width: 100%">';
    jQuery.each(cats, function (key, value) {
        select += '<option value="' + value.id + '">' + value.text + '</option>';
    });
    select += '</select>';
    return select;
}
function addNodeInfo(data) {
    //removeNodeInfo(data);
    var nodeName = data.node.text;
    var id1 = data.node.id;
    var icon = data.node.icon;
    var parent = data.node;

    //if category is chosen
    if (id1[0] == "c") {

        document.getElementById('createCatOffer').innerHTML =
            '<div class="row">' +
            '<div class="col-sm-3">' +
            'Category\'s name' +
            '</div>' +
            '<div class="col-sm-9">' +
            ' <input id ="text" type="text" name="name" value="' + nodeName + '"/>' +
            '</div>' +
            '</div>' +
            '<br>' +
            '<div class="row">' +
            '<div class="col-sm-6">' +
            '</div>' +
            '<div class="col-sm-3">' +
            '<input type="button" value="Cancel" onclick="cancelCreateCat()"/>' +
            '</div>' +
            '<div class="col-sm-3">' +
            '<input type="button" value="Change" onclick="changeCat(\'' + id1.toString() + '\',\' ' + icon + '\', \'' + parent.parent + '\')"/>' +
            '</div>';


    }
    //if subcategory is chosen
    else if (id1[0] = "s") {
        var parentName = jQuery("#" + parent.parent).children("a").text();
        var select = getSelectCategories();
        document.getElementById('createCatOffer').innerHTML =
            '<div class="row">' +
            '<div class="col-sm-5">' +
            'Category ' +
            '</div>' +
            '<div class="col-sm-7">' + select + '</div></div>' +
            '<div class="row">' +
            '<div class="col-sm-5">' +
            'Subcategory\'s Name' +
            '</div>' +
            '<div class="col-sm-7">' +
            '<input id ="text" type="text" name="subcatname" value="' + nodeName + '"/>' +
            '</div>' +
            '<br>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-sm-6"></div>' +
            '<div class="col-sm-3">' +
            '<input type="button" value="Cancel" onclick="cancelCreateCat()"/>' + '</div>' +
            '<div class="col-sm-3">' +
            '<input type="button" value="Change" onclick="changeCat(\'' + id1.toString() + '\',\' ' + icon + '\', \'' + parent.parent + '\')"/>' + '</div>';


    }


}
function changeCat(identification, icon, parent) {

    var newValue = document.getElementById('text').value;

    var catName = jQuery('#categoryName').value;
    //alert(identification);
    //alert(newValue);
    //alert(parent);

    $.getJSON("/change_category_name", {
            'id': identification,
            'text': newValue,
            'parent': parent

        },
        function (responseEntity) {
            console.log(responseEntity);
        })
        .done(function (responseEntity) {
            console.log("callback_rename_done");
            var ref = $('#tree').jstree(true);
            ref.set_id(identification, responseEntity.id);
            $('#tree').jstree('rename_node', responseEntity.id, responseEntity.text);
        })
        .fail(function (xhr, status, error) {
            console.log("callback_rename_fail");

            alert("This name is already used=" + newValue + ". Please, enter other one!");
            console.log("callback_rename_fail " + xhr.responseJSON.id);

            var ref = $('#tree').jstree(true),
                sel = ref.get_node(identification, true);
            console.log(sel);

            if (sel) {
                ref.edit(sel);
            }


        });

    cancelCreateCat();
}

function cancelCreateCat() {
    var innerHtmlForm = '<div class="row">Select category or subcategory for edit</div>' +
        '<div class="row">or create the new:<br>' +
        '<a id="createCategory" href="#">Category</a><br>' +
        '<a id="createSubcategory" href="#" >Subcategory</a>' +
        '</div>';

    jQuery("#createCatOffer").html(innerHtmlForm);
    jQuery('#createCategory').click(function () {
        createCatForm();
        return false;
    });
    jQuery('#createSubcategory').click(function () {
        createSubcatForm();
        return false;
    });

}
