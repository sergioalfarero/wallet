jQuery(document).ready(function () {
    jQuery("#load_more_div").on("click", function () {
        loadMoreItems();
    })

    jQuery("#nextPage").on("click", function () {
        var nextPage = parseInt(currentPage) + 1;

        loadPage(nextPage);
    });

    jQuery("#prevPage").on("click", function () {
        var nextPage = currentPage - 1;
        loadPage(nextPage);
    });


    jQuery("#go_to_page_button").on("click", function () {
        var page = jQuery("#page_to_go").val();
        loadPage(page);
    });


    jQuery('#countSelect').change(
        function () {
            var chosenCount = jQuery(this).val();
            if (chosenCount != "") {
                jQuery.getJSON(findOperation+'?page=0&count=' + chosenCount, {
                    ajax: 'true'
                }, function (data) {
                    var result1 = JSON.parse(data.message);
                    jQuery('#operationTable>tbody').html("");

                    for (var i = 0; i < result1.length; i++) {
                        var result = result1[i];

                        var d = new Date(result.date);
                        var formatDate = ('0' + (d.getDate())).slice(-2) + '.' + ('0' + (d.getMonth() + 1)).slice(-2) + '.' + d.getFullYear() + ' ' + ('0' + (d.getHours())).slice(-2) + ':' + ('0' + (d.getMinutes())).slice(-2) + ':' + ('0' + (d.getSeconds())).slice(-2);

                        var operationRow = '<tr id="row' + result.id + '" onmouseover="showMenu(' + result.id + ')" onmouseout="hideMenu(' + result.id + ')">' +
                            '<td>' +
                            '<div id="menuOperation' + result.id + '" class="rc dopMenu box_shadow popup"' +
                            'style="z-index: 10; display: none">' +
                            '<div style="padding: 0px 8px 5px 8px">' +
                            '<a id="editOperationLink' + result.id + '" href="#" onclick="editOperation(' + result.id + ')">' +
                            '<span>Редактировать</span>' +
                            '<img border="0" src="/resources/img/edit.png" width="15" height="15">' +
                            '</a> </div><div style="padding: 5px 8px; "> <a id="deleteOperationLink' + result.id + '" class="deleteOperationLink"' +
                            'href="#" onclick="deleteOperation(' + result.id + ')"><span>Удалить</span><img border="0"src="/resources/img/del.gif" width="15" height="15"></a></div>' +
                            '</div>' + formatDate + '</td>' +
                            '<td>' + result.subcategory.category.name + '</td> ' +
                            '<td>' + result.subcategory.name + '</td>' +
                            '<td>' + result.account.name + '</td>' +
                            '<td>' + parseFloat(result.sum).toFixed(2) + '</td>' +
                            '<td>' + result.currency + '</td>' +
                            '</tr>';

                        jQuery('#operationTable > tbody:last-child').append(operationRow);

                    }

                });
            } else {
//                        jQuery('#accountBalance').html("");

            }
//                    checkFieldValues();


        }
    );

});

function loadPage(page) {
    alert(page);
    var jqxhr = jQuery.getJSON(findPageOfOperation+page,
        {
            ajax: 'true'
        }, function (data) {
            var result1 = JSON.parse(data.message);

            var x = jQuery('#operationTable > tbody').empty();
            for (var i = 0; i < result1.length; i++) {
                var result = result1[i];

                var d = new Date(result.date);
                var formatDate = ('0' + (d.getDate())).slice(-2) + '.' + ('0' + (d.getMonth() + 1)).slice(-2) + '.' + d.getFullYear() + ' ' + ('0' + (d.getHours())).slice(-2) + ':' + ('0' + (d.getMinutes())).slice(-2) + ':' + ('0' + (d.getSeconds())).slice(-2);

                var operationRow = '<tr id="row' + result.id + '" onmouseover="showMenu(' + result.id + ')" onmouseout="hideMenu(' + result.id + ')">' +
                    '<td>' +
                    '<div id="menuOperation' + result.id + '" class="rc dopMenu box_shadow popup"' +
                    'style="z-index: 10; display: none">' +
                    '<div style="padding: 0px 8px 5px 8px">' +
                    '<a id="editOperationLink' + result.id + '" href="#" onclick="editOperation(' + result.id + ')">' +
                    '<span>Редактировать</span>' +
                    '<img border="0" src="/resources/img/edit.png" width="15" height="15">' +
                    '</a> </div><div style="padding: 5px 8px; "> <a id="deleteOperationLink' + result.id + '" class="deleteOperationLink"' +
                    'href="#" onclick="deleteOperation(' + result.id + ')"><span>Удалить</span><img border="0"src="/resources/img/del.gif" width="15" height="15"></a></div>' +
                    '</div>' + formatDate + '</td>' +
                    '<td>' + result.subcategory.category.name + '</td> ' +
                    '<td>' + result.subcategory.name + '</td>' +
                    '<td>' + result.account.name + '</td>' +
                    '<td>' + parseFloat(result.sum).toFixed(2) + '</td>' +
                    '<td>' + result.currency + '</td>' +
                    '</tr>';

                jQuery('#operationTable > tbody').append(operationRow);

            }


        }).done(function () {

        currentPage = page;
        countOfLoadedPage = 1;

        if (currentPage == 4) {
            removePagesInRange();
            jQuery("#page3").next().
            before('<span id="page4" class="paginationSpan" onclick="loadPage(4)"> 4</span>').
            before('<span id="pageDiviner1" class="paginationSpan inactivePage">... </span>');

        }
        if (currentPage == 5) {
            removePagesInRange();
            jQuery("#page3").next().
            before('<span id="page4" class="paginationSpan" onclick="loadPage(4)"> 4</span>').
            before('<span id="page5" class="paginationSpan" onclick="loadPage(5)"> 5</span>').
            before('<span id="pageDiviner1" class="paginationSpan inactivePage">... </span>');

        }

        if (currentPage >= 6 && currentPage <= maxPageCount - 6) {
            removePagesInRange();

            var prevPage = currentPage - 1;
            var nextPage = parseInt(currentPage) + 1;

            jQuery("span#page3").
            after('<span id="pageDiviner2" class="paginationSpan inactivePage"> ... </span>').
            after('<span id="page' + nextPage + '" class="paginationSpan inactivePage">' + nextPage + '</span>').
            after('<span id="page' + currentPage + '" class="paginationSpan activePage">' + currentPage + '</span>').
            after('<span id="page' + prevPage + '" class="paginationSpan inactivePage">' + prevPage + '</span>').
            after('<span id="pageDiviner1" class="paginationSpan inactivePage"> ... </span>');

        }

        if(currentPage == maxPageCount - 5){
            var x=maxPageCount-3;
//                    var smallerPage=maxPageCount-5;
            var largerPage=maxPageCount-4;

            removePagesInRange();
            jQuery("#page"+x).
            before('<span id="pageDiviner2" class="paginationSpan inactivePage">... </span>').
            before('<span id="page'+currentPage+'" class="paginationSpan" onclick="loadPage('+currentPage+')">'+currentPage+'</span>').
            before('<span id="page'+largerPage+'" class="paginationSpan" onclick="loadPage('+largerPage+')">'+largerPage+'</span>');

        }
        if(currentPage == maxPageCount - 4){
            var x=maxPageCount-3;
            removePagesInRange();
            jQuery("#page"+x).
            before('<span id="pageDiviner2" class="paginationSpan inactivePage">... </span>').
            before('<span id="page'+currentPage+'" class="paginationSpan" onclick="loadPage('+currentPage+')">'+currentPage+'</span>');
//                    before('<span id="page'+(maxPageCount-5)+'" class="paginationSpan" onclick="loadPage('+maxPageCount-4+')"> 5</span>');

        }


        if (fullPage) {
            if (page == maxPageCount - 1) {
                jQuery("#nextPage").hide();
                jQuery("#load_more_div").hide();
            } else {
                jQuery("#nextPage").show();
                jQuery("#load_more_div").show();
            }
        } else {

            if (page == maxPageCount) {
                jQuery("#nextPage").hide();
                jQuery("#load_more_div").hide();
            } else {
                jQuery("#nextPage").show();
                jQuery("#load_more_div").show();
            }
        }




        if (page != 1) {
            jQuery("#prevPage").show();
        } else {
            jQuery("#prevPage").hide();
        }


        jQuery("#page_to_go").val("");

        var s = jQuery(".paginationSpan");
        jQuery(".paginationSpan").removeClass("activePage");
        var nextPageSpan = jQuery("#page" + currentPage + "");
        nextPageSpan.addClass("activePage");
    });

};

function removePagesInRange(){
    var index = maxPageCount - 3;
    jQuery("#page3").nextUntil("#page" + index).remove();

};

function loadMoreItems() {
    var pageToLoad = parseInt(currentPage) + countOfLoadedPage;
    var jqxhr = jQuery.getJSON(findPageOfOperation+pageToLoad,
        {
            ajax: 'true'
        }, function (data) {
            var result1 = JSON.parse(data.message);
            alert(result1);
//                    jQuery('#operationTable>tbody').html("");

            for (var i = 0; i < result1.length; i++) {
                var result = result1[i];

                var d = new Date(result.date);
                var formatDate = ('0' + (d.getDate())).slice(-2) + '.' + ('0' + (d.getMonth() + 1)).slice(-2) + '.' + d.getFullYear() + ' ' + ('0' + (d.getHours())).slice(-2) + ':' + ('0' + (d.getMinutes())).slice(-2) + ':' + ('0' + (d.getSeconds())).slice(-2);

                var operationRow = '<tr id="row' + result.id + '" onmouseover="showMenu(' + result.id + ')" onmouseout="hideMenu(' + result.id + ')">' +
                    '<td>' +
                    '<div id="menuOperation' + result.id + '" class="rc dopMenu box_shadow popup"' +
                    'style="z-index: 10; display: none">' +
                    '<div style="padding: 0px 8px 5px 8px">' +
                    '<a id="editOperationLink' + result.id + '" href="#" onclick="editOperation(' + result.id + ')">' +
                    '<span>Редактировать</span>' +
                    '<img border="0" src="/resources/img/edit.png" width="15" height="15">' +
                    '</a> </div><div style="padding: 5px 8px; "> <a id="deleteOperationLink' + result.id + '" class="deleteOperationLink"' +
                    'href="#" onclick="deleteOperation(' + result.id + ')"><span>Удалить</span><img border="0"src="/resources/img/del.gif" width="15" height="15"></a></div>' +
                    '</div>' + formatDate + '</td>' +
                    '<td>' + result.subcategory.category.name + '</td> ' +
                    '<td>' + result.subcategory.name + '</td>' +
                    '<td>' + result.account.name + '</td>' +
                    '<td>' + parseFloat(result.sum).toFixed(2) + '</td>' +
                    '<td>' + result.currency + '</td>' +
                    '</tr>';

                jQuery('#operationTable > tbody:last-child').append(operationRow);

            }


        }).done(function () {
//                currentPage;
        countOfLoadedPage++;
        alert("страница=" + currentPage + "; загружено страниц=" + countOfLoadedPage);
        hideLoadMoreAndNextButtonIfMaxPageShown(pageToLoad);


        var nextPageSpan = jQuery("#page" + (currentPage + countOfLoadedPage - 1));
        nextPageSpan.addClass("activePage");
    });
};

function hideLoadMoreAndNextButtonIfMaxPageShown(pageToLoad) {
    if (fullPage == 0) {
        if (pageToLoad == maxPageCount) {
            jQuery("#nextPage").hide();
            jQuery("#load_more_div").hide();

        }
    } else {
        if (pageToLoad == maxPageCount - 1) {
            jQuery("#nextPage").hide();
            jQuery("#load_more_div").hide();
        }
    }

};
