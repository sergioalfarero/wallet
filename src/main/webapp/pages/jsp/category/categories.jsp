<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">--%>
<head>

    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/tree/jstree.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/tree/customCategoryTree.js"/>"></script>
    <%--<script type="text/javascript" src="<c:url value="/resources/jsTree/vakata-jstree.js"/>"></script>--%>
    <link type="text/css" href="<c:url value="/resources/tree/themes/default/style.min.css"/>" rel="stylesheet"/>
    <%--<script type="text/javascript" src="<c:url value="/resources/js/categoriesActions.js"/>"></script>--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Categories</title>


</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>


<div class="container">
    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>
        <div id="main" class="col-sm-6 well text-center">
            <div id="tree" class="text-left">
            </div>
            <hr/>
            <a href="/add_category.html">Add new category</a><br>
            <a href="/addSubcategory.html">Add new subcategory</a><br>
            <hr/>

        </div>
        <div id="rightBlock" class="col-sm-4 well text-center">
            <div id="nodeInfo">
                <div id="createCatOffer">
                    <div class="row">
                        Select category or subcategory for edit
                    </div>
                    <div class="row">
                        or create the new:
                        <br>
                        <a id="createCategory" href="#">Category</a>
                        <br>
                        <a id="createSubcategory" href="#">Subcategory</a>

                    </div>
                </div>


            </div>
        </div>
    </div>
    <hr/>


    <jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>

</body>
</html>