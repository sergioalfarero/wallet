<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="func" %>

<%--<!DOCTYPE html>--%>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome!</title>


    <%--JQUERY--%>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>
    <link type="text/css" href="<c:url value="/resources/sorttable/style.css"/>" rel="stylesheet"/>

    <%--BOOTSTRAP--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

    <script type="text/javascript" src="<c:url value="/resources/js/getCategories.js"/>"></script>

</head>
<body>
<sec:authorize access="isAuthenticated()">
    <jsp:include page="menuHeader1.jsp"/>
    <div class="container">
        <div class="row">
            <div class="col-sm-2 side-nave well">
                <jsp:include page="sideMenu.jsp"/>
            </div>
            <div class="col-sm-6 well">
                <div class="well" id="mainWindow">
                    <div id="quickFunction" class="row text-center">
                        <ul class="list-inline nav nav-pills text-center ">
                            <li class="active text-center"><a data-toggle="tab" href="#outcome">Расходы</a></li>
                            <li><a data-toggle="tab" href="#income">Доходы</a></li>
                            <li><a data-toggle="tab" href="#transaction">Транзакции</a></li>
                            <li><a data-toggle="tab" href="#exchange">Обмен валют</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="outcome" class="tab-pane fade in active">
                                <span id="createNewOperationTitle" class="text-center"><span
                                        id="createNewOperationSpan">Create new operation</span></span>
                                <fmt:formatDate var="fmtDate" pattern="dd.MM.yyyy HH:mm:ss"
                                                value="<%=new java.util.Date()%>"/>

                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Date</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="date" type="text" value="${fmtDate}">
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Category</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="category" style="width: 100%">

                                        </select>

                                    </div>
                                </div>


                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Subcategory</span>
                                    </div>
                                    <div id="subcatSelect" class="col-sm-9">
                                        <select id="subcategory" style="width: 100%">

                                        </select>


                                    </div>
                                </div>


                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Account</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="accountSelect">
                                            <option value="">--Select--</option>
                                            <c:forEach items="${accounts}" var="acc">
                                                <option value="${acc.name}">${acc.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div id="accountBalance">

                                </div>

                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Amount</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="amount" type="text" value="">
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                    </div>
                                    <div class="col-sm-9">
                                        <span id="error" type="text" value=""></span>
                                    </div>


                                </div>


                                <div id="buttonsRow" class="row text-right">

                                    <input id="addNewOperationButton" <%--disabled="true"--%> type="button"
                                           onclick="addOperation()" value=
                                        <spring:message code="user.body.buttonlabel"/>>

                                </div>

                                <div id="output">

                                </div>
                            </div>
                            <div id="income" class="tab-pane fade">
                               <span id="createNewOperationTitle" class="text-center"><span
                                       id="createNewOperationSpan">Create new operation</span></span>
                                <fmt:formatDate var="fmtDate" pattern="dd.MM.yyyy HH:mm:ss"
                                                value="<%=new java.util.Date()%>"/>

                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Date</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="date" type="text" value="${fmtDate}">
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Category</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="category" style="width: 100%">

                                        </select>

                                    </div>
                                </div>


                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Subcategory</span>
                                    </div>
                                    <div id="subcatSelect" class="col-sm-9">
                                        <select id="subcategory" style="width: 100%">

                                        </select>


                                    </div>
                                </div>


                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Account</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="accountSelect">
                                            <option value="">--Select--</option>
                                            <c:forEach items="${accounts}" var="acc">
                                                <option value="${acc.name}">${acc.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div id="accountBalance">

                                </div>

                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Amount</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="amount" type="text" value="">
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                    </div>
                                    <div class="col-sm-9">
                                        <span id="error" type="text" value=""></span>
                                    </div>
                                </div>


                                <div id="buttonsRow" class="row text-right">

                                    <input id="addNewOperationButton" <%--disabled="true"--%> type="button"
                                           onclick="addOperation()" value=
                                        <spring:message code="user.body.buttonlabel"/>>

                                </div>

                                <div id="output">

                                </div>
                            </div>
                            <div id="transaction" class="tab-pane fade">
                                <span id="createNewTransactionTitle" class="text-center">Create new transaction</span>

                                <fmt:formatDate var="fmtDate" value="<%=new java.util.Date()%>" pattern="dd.MM.yyyy HH:mm:ss"/>

                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Date</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="dateTr" type="text" value="${fmtDate}">
                                    </div>
                                </div>


                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>OutcomingAccount</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="outcomingAccount" onchange="changeOutcome()">
                                            <option value="">--Select--</option>
                                            <c:forEach items="${accounts}" var="acc">
                                                <option value="${acc.id}">${acc.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div class="row" id="incomingAccountRow" style="margin-bottom: 3px; display:none">
                                    <div class="col-sm-3 text-right">
                                        <span>IncomingAccount</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="incomingAccount">
                                            <option value="">--Select--</option>
                                            <c:forEach items="${accounts}" var="acc">
                                                <option value="${acc.id}">${acc.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div id="accountBalance">

                                </div>

                                <div class="row" style="margin-bottom: 3px;">
                                    <div class="col-sm-3 text-right">
                                        <span>Ammount</span>
                                    </div>
                                    <div class="col-sm-9">
                                        <input id="amountTr" class="form-control" type="text" value="">
                                    </div>
                                </div>


                                <div class="row">
                                    <input type="button" value="Cancel" onclick="cancelCreateTransaction()">
                                    </input>

                                    <input id="addNewTransactionButton" <%--disabled="true"--%> type="button" onclick="addTransaction()"
                                           value=<spring:message code="user.body.buttonlabel"/>>
                                    </input>

                                </div>

                                <div id="output">

                                </div>
                            </div>
                            <div id="exchange" class="tab-pane fade">

                            </div>
                        </div>

                    </div>
                </div>
                <div id="operations" class="well">
                    <div class="text-center">
                        Operation
                    </div>
                    <div id="tableDiv">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="tablesorter" style="width: 100%;" id="operationTable" border="1"
                                       cellpadding="5"
                                       cellspacing="1"
                                       width="100%" align="center"
                                       summary="operations">
                                    <thead class="text-center">

                                    <c:if test="${path=='/administration/all_operations'}">
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <th>Operation_id</th>
                                        </sec:authorize>
                                    </c:if>
                                    <th>Date</th>
                                    <th>Category</th>
                                    <th>Subcategory</th>
                                    <th>Account</th>
                                    <th>Sum</th>
                                    <th>Currency</th>
                                    <c:if test="${path=='/administration/all_operations'}">
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <th>User_id</th>
                                        </sec:authorize>
                                    </c:if>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="oper" items="${operations}">
                                        <tr id="row${oper.id}" onmouseover="showMenu(${oper.id})"
                                            onmouseout="hideMenu(${oper.id})">
                                            <c:if test="${path=='/administration/all_operations'}">
                                                <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                    <td>${oper.id}</td>
                                                </sec:authorize>
                                            </c:if>
                                            <td>
                                                <div id="menuOperation${oper.id}" class="rc dopMenu box_shadow popup"
                                                     style="z-index: 10; display: none">
                                                    <div style="padding: 0px 8px 5px 8px">
                                                        <a id="editOperationLink${oper.id}"
                                                           href="#" onclick="editOperation(${oper.id})">
                                                            <span>Редактировать</span>
                                                            <img border="0" src="/resources/img/edit.png" width="15"
                                                                 height="15">
                                                        </a>
                                                    </div>
                                                    <div style="padding: 5px 8px; ">
                                                        <a id="deleteOperationLink${oper.id}" class="deleteOperationLink"
                                                            <%--href="<c:url value='/account/${account.id}/delete.html' />"--%>
                                                           href='#' onclick="deleteOperation(${oper.id})"/>
                                                        <span>Удалить</span>
                                                        <img border="0"
                                                             src="/resources/img/del.gif"
                                                             width="15"
                                                             height="15"></a>
                                                    </div>
                                                </div>
                                                <fmt:formatDate var="fmtDate" pattern="dd.MM.yyyy HH:mm:ss"
                                                                value="${oper.date}"/>
                                                    ${fmtDate}</td>

                                            <td>${oper.subcategory.category.name}</td>
                                            <td>${oper.subcategory.name}</td>
                                            <td>${oper.account.name}</td>
                                            <td>${oper.sum}</td>
                                            <td>${oper.currency}</td>

                                            <c:if test="${path=='/administration/all_operations'}">
                                                <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                    <td>${oper.user.id}</td>
                                                </sec:authorize>
                                            </c:if>

                                        </tr>
                                    </c:forEach>
                                    </tbody>

                                </table>
                            </div>

                        </div>


                    </div>
                </div>
            </div>


            <div class="col-sm-4 well">
                <div id="balances" class="well">
                    <div class="text-center">
                        Balance
                    </div>
                        <%--<div class="container">--%>
                    <div class="row">

                        <div class="col-sm-12 well">
                            <table class="tablesorter" style="width: 100%;" id="accTable" border="1"
                                   cellpadding="5"
                                   cellspacing="1"
                                   width="100%" align="center"
                                   summary="operations">
                                <tbody>
                                <c:forEach var="account" items="${accounts}">
                                    <tr>

                                        <td><img width="20" height="20" src="${account.icon}"/> ${account.name}</td>
                                        <td align="right">
                                            <c:choose>
                                                <c:when test="${func:compareCheck(account.balance)}">
                                                    <span style="color:blue;">${account.balance}</span>
                                                </c:when>
                                                <c:otherwise>
                                                    <span style="color:red;">${account.balance}</span>
                                                </c:otherwise>
                                            </c:choose>
                                            <span style="color:green; font-size: smaller">${account.currencyType}</span>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <hr>
                            <table class="tablesorter" style="width: 100%;" id="summaryCurrenciesTable" border="1"
                                   cellpadding="5"
                                   cellspacing="1"
                                   width="100%" align="center"
                                   summary="operations">
                                <tbody>
                                <tr>
                                    <td rowspan="${summary.map.size()}">
                                        Итого по валютам:
                                    </td>
                                    <c:forEach items="${summary.map}" var="account" varStatus="status">
                                    <td align="right">${account.value} <span
                                            style="color:green; font-size: smaller">${account.key}</span></td>
                                </tr>
                                </c:forEach>


                                </tr>
                                </tbody>

                            </table>
                            <hr>
                            <table class="tablesorter" style="width: 100%;" id="summaryAccountTable" border="1"
                                   cellpadding="5"
                                   cellspacing="1"
                                   width="100%" align="center"
                                   summary="operations">
                                <tbody>
                                <tr>
                                    <td>
                                        Итого в ${sum.key}:
                                    </td>
                                    <td align="center">
                                        <c:forEach items="${summary.total}" var="sum">
                                            ${sum.value}
                                            <span style="color:green; font-size: smaller">${sum.key}</span>
                                        </c:forEach>

                                    </td>
                                </tr>

                                </tbody>
                            </table>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


</sec:authorize>

<sec:authorize access="isAnonymous()">
    <jsp:include page="/login"/>
</sec:authorize>


<jsp:include page="footer.jsp"/>

</body>
</html>
