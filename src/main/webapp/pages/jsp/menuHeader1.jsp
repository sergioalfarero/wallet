<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <meta charset="utf-8">
</head>

<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"><img src="/resources/img/default/mainPhoto.png" height="22" width="22"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="/settings">Settings</a></li>
                <li><a href="/userInfo">Profile</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <%--<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>--%>
                <sec:authorize access="!isAuthenticated()">
                <li>
                    <p><a class="btn btn-sm btn-default" href="<c:url value="/user/registration" />"
                          role="button">Registration</a>
                    </p>
                    </sec:authorize>
                </li>
                <li>
                    <sec:authorize access="!isAuthenticated()">
                    <p><a class="btn btn-sm btn-success" href="<c:url value="/login" />"
                          role="button">Войти</a>
                    </p>
                </li>
                </sec:authorize>

                <sec:authorize access="isAuthenticated()">
                    <li>
                    <span>Hello, <sec:authentication property="principal.username"/>
                        <a class="btn btn-sm btn-default" href="<c:url value="/userInfo" />"
                           role="button">Information</a>
                        <a class="btn btn-sm btn-danger" href="<c:url value="/logout" />" role="button">Выйти</a>
                   </span>
                    </li>
                </sec:authorize>
            </ul>
        </div>
    </div>
</nav>


</body>
</html>
