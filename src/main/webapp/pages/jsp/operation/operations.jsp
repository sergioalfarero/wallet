<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Operations</title>

    <link type="text/css" href="<c:url value="/resources/sorttable/style.css"/>" rel="stylesheet"/>
    <link type="text/css" href="<c:url value="/resources/myStyle.css"/>" rel="stylesheet"/>

    <%--JQUERY--%>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
    <%--<script type="text/javascript" src="<c:url value="/resources/multiple-select/multiple-select.js"/>"></script>--%>
    <script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/getCategories.js"/>"></script>

    <%--BOOTSTRAP--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

<%--Pagination--%>
    <script type="text/javascript" src="<c:url value="/resources/pagination.js"/>"></script>
    <link type="text/css" href="<c:url value="/resources/pagination.css"/>" rel="stylesheet"/>

    <sec:csrfMetaTags/>


    <c:url var="findCategoriesURL" value="/findcategories"/>
    <c:url var="findSubcategoriesURL" value="/findsubcategories"/>
    <c:url var="findAccountSumURL" value="/findAccountSum"/>
    <c:url var="findOperation" value="/operations"/>
    <c:url var="findPageOfOperation" value="/operations?page="/>
    <c:url var="deleteOperation" value="/operations/delete/"/>
    <c:url var="changeOperation" value="/operations/change/"/>

    <script type="text/javascript">
        //current loaded page
        var currentPage = 1;
        //count of loaded pages
        var countOfLoadedPage = 1;
        //total count of pages
        var maxPageCount =${numberOfPages};
        //total number of items
        var maxCountOfItems =${numberOfItems};
        //number of items to show on page
        var itemsOnThePage=${numberToShowOnPage};

        var fullPage =${fullPage};
        var findOperation="/operations";
        var findPageOfOperation="/operations?page=";

        $(document).ready(function () {
                $("#subcatDiv").hide();
                $("#operationTable").tablesorter();

                jQuery("#date").on("input", function () {

                    checkFieldValues();
                });

                jQuery('#accountSelect').change(
                    function () {
                        var chosenAcc = jQuery(this).val();
                        if (chosenAcc != "") {
                            jQuery.getJSON('${findAccountSumURL}', {
                                accountName: chosenAcc,
                                ajax: 'true'
                            }, function (data) {
                                var html = "Available amount of money at " + data.name + " account= " + data.balance + " " + data.type;
                                jQuery('#accountBalance').html(html);
                                jQuery("#currencyOption").val(data.type);

                            });
                        } else {
                            jQuery('#accountBalance').html("");

                        }
                        checkFieldValues();
                    });
            }
        );


        function checkFieldValues() {
            var accText = jQuery("#accountSelect :selected").text().trim();
            var date = jQuery("#date").val().trim();
            var categoryName = jQuery("#category :selected").text().trim();
            var accBoolean = accText != "--Select--";
            var dateBoolean = date != "";
            var catBoolean = categoryName != "Category";
            var b = accBoolean && dateBoolean && catBoolean;
            if (b) {
                jQuery("#addNewOperationButton").attr("disabled", false);
            } else {

                jQuery("#addNewOperationButton").attr("disabled", true);
            }
        }

    </script>


    <script type="text/javascript">
        function showMenu(id) {
            var a = document.getElementById("menuOperation" + id);
            a.style.display = "";

        }
        function hideMenu(id) {
            var a = document.getElementById("menuOperation" + id);
            a.style.display = "none";


        }
    </script>


    <script type="text/javascript">
        function editOperation(id) {
            var newSelectedIndex = "";

            jQuery.getJSON('${findOperation}' + id, {ajax: 'true'},
                function (data) {
                    var result = JSON.parse(data.message);

                    document.getElementById('createNewOperationSpan').innerHTML = 'Change existing operation';

                    var d = new Date(result.date);
                    var formatDate = ('0' + (d.getDate())).slice(-2) + '.' + ('0' + (d.getMonth() + 1)).slice(-2) + '.' + d.getFullYear() + ' ' + ('0' + (d.getHours())).slice(-2) + ':' + ('0' + (d.getMinutes())).slice(-2) + ':' + ('0' + (d.getSeconds())).slice(-2);
                    jQuery("#date").val(formatDate);


                    newSelectedIndex = result.subcategory.category.id;
//                    findCategories();
                    jQuery("#category option:selected").removeAttr("selected");

                    jQuery("#category option[value='" + newSelectedIndex + "']").attr('selected', 'selected');


                    jQuery("#accountSelect option:selected").removeAttr("selected");
                    jQuery("#accountSelect option[value='" + result.account.name + "']").attr('selected', 'selected');


                    jQuery("#subcategory option:selected").removeAttr("selected");
                    newSelectedIndex = result.subcategory.id;
                    setSubcats(newSelectedIndex);

//                        jQuery("#subcategory").val(newSelectedIndex);
                    jQuery("#currencyOption option:selected").removeAttr("selected");
                    newSelectedIndex = result.currency;
                    jQuery("#currencyOption option[value='" + newSelectedIndex + "']").attr('selected', 'selected');


                    jQuery("#amount").val(result.sum);


                    jQuery('#addNewOperationButton').val("Change");

                    var buttons = '<input id="cancelChangeButton" type="button" onclick="cancelChanging()" value="Cancel"/>' +
                        '<input id="changeOperationButton" type="button" onclick="changeOperation(' + result.id + ')" value="Change"/>';
                    jQuery('#buttonsRow').empty().html(buttons);
                    jQuery("#subcategory").val(newSelectedIndex);

                });


        }

        function changeOperation(id) {
            var csrfParameter = jQuery("meta[name='_csrf_parameter']").attr("content");
            var csrfToken = jQuery("meta[name='_csrf']").attr("content");
            var csrfHeader = jQuery("meta[name='_csrf_header']").attr("content");

            var categoryElement = document.getElementById("category");
            var category = categoryElement.options[categoryElement.selectedIndex].value;

            var subcategoryElement = document.getElementById("subcategory");
            var subcategory = subcategoryElement.options[subcategoryElement.selectedIndex].value;
            var accountElement = document.getElementById("accountSelect");
            var account = accountElement.options[accountElement.selectedIndex].value;

            var operation = {
                date: document.getElementById('date').value,
                subcategory: subcategory,
                category: category,
                sum: document.getElementById("amount").value,
                account: account
            }

            jQuery(document).ajaxSend(function (e, xhr, options) {
                xhr.setRequestHeader(csrfHeader, csrfToken);
                xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

            });
            jQuery.ajax({
                type: 'POST',
                url: '${changeOperation}' + id,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                async: false,
                data: JSON.stringify(operation),
                success: function (data) {
                    console.log("SUCCESS: ", data);
                    jQuery("#error").text("");
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                    console.log("ERROR: ", e.responseJSON.message);
                    jQuery("#error").text(e.responseJSON.message);
                    jQuery("#error").style("color:red");

                },
                done: function (e) {
                    console.log("DONE");
                }

            })

        }


        function cancelChanging() {

            document.getElementById('createNewOperationSpan').innerHTML = 'Create new operation';

            jQuery("#date").val(${fmtDate});

            jQuery("#category option:selected").removeAttr("selected");

            jQuery("#subcatSelect").html('<select id="subcategory" style="width: 100%"></select>');

            jQuery("#accountSelect option:selected").removeAttr("selected");

            jQuery("#amount").val("");

            jQuery('#buttonsRow').empty().html('<input id="addNewOperationButton" type="button" onclick="addOperation()" value="Add"/>');


        }

        function editOperation1() {
        }

        function deleteOperation(id) {
            $.getJSON('${deleteOperation}' + id, {
                ajax: 'true'
            }, function (data) {
                var result = JSON.parse(data.message);
                alert(result);
                jQuery("#row" + result).remove();





            });


        }
    </script>


    <script type="application/javascript">
        function addOperation() {
            var csrfParameter = jQuery("meta[name='_csrf_parameter']").attr("content");
            var csrfToken = jQuery("meta[name='_csrf']").attr("content");
            var csrfHeader = jQuery("meta[name='_csrf_header']").attr("content");

            var date = document.getElementById('date').value;
            var categoryElement = document.getElementById("category");
            var category = categoryElement.options[categoryElement.selectedIndex].value;

            var subcategoryElement = document.getElementById("subcategory");
            var subcategory = subcategoryElement.options[subcategoryElement.selectedIndex].value;

            var accountElement = document.getElementById("accountSelect");
            var account = accountElement.options[accountElement.selectedIndex].value;

            var amount = document.getElementById("amount").value;
            var e = document.getElementById("currencyOption");
            var currency = e.options[e.selectedIndex].value;


            var operation = {
                date: date,
                subcategory: subcategory,
                category: category,
                sum: amount,
                account: account,
                currencyType: currency

            }
            var headers = {};
            var data = {};
            data[csrfParameter] = csrfToken;
            headers[csrfHeader] = csrfToken;
            jQuery(document).ajaxSend(function (e, xhr, options) {
                xhr.setRequestHeader(csrfHeader, csrfToken);
                xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

            });

            jQuery.ajax({
                url: 'operation/save',
                type: "post",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(operation),

                success: function (data) {

                    var result = JSON.parse(data.message);
                    alert(result.id);
                    var html = '<tr id="row' + result.id + '" onmouseover="showMenu(' + result.id + ')" onmouseout="hideMenu(' + result.id + ')">' +
                        '<td>' +
                        '<div id="menuOperation' + result.id + '" class="rc dopMenu box_shadow popup"' +
                        'style="z-index: 10; display: none">' +
                        '<div style="padding: 0px 8px 5px 8px">' +
                        '<a id="editOperationLink' + result.id + '" href="#" onclick="editOperation(' + result.id + ')">' +
                        '<span>Редактировать</span>' +
                        '<img border="0" src="/resources/img/edit.png" width="15" height="15">' +
                        '</a> </div><div style="padding: 5px 8px; "> <a id="deleteOperationLink' + result.id + '" class="deleteOperationLink"' +
                        'href="#" onclick="deleteOperation(' + result.id + ')"><span>Удалить</span><img border="0"src="/resources/img/del.gif" width="15" height="15"></a></div>' +
                        '</div>' + result.date + '</td>' +
                        '<td>' + result.category + '</td> ' +
                        '<td>' + result.subcategory + '</td>' +
                        '<td>' + result.account + '</td>' +
                        '<td>' + parseFloat(result.sum).toFixed(2) + '</td>' +
                        '<td>' + result.currencyType + '</td>' +
                        '</tr>';

                    maxCountOfItems++;
                    if(maxCountOfItems>itemsOnThePage){
                        alert("mas");
                     //удалить верхнюю строчкe
                        //добавить новую вниз
                        jQuery('#operationTable> tbody').empty();

                        jQuery('#operationTable> tbody').append(html);

                        //пересчитать кнопки паджинации

                    } else {

                        jQuery('#operationTable > tbody:last-child').append(html);
                    }
                    var inputElements = jQuery(".form-control");
                    for (var i = 0; i < inputElements.length; i++) {
                        inputElements[i].value = "";
                    }
                    document.getElementById("addNewOperationButton").disabled = true;



                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    alert(' произошла ошибка: ' + textStatus + '. сode=' + jqXHR.status);
                }
            });


        }
    </script>

    <script type="text/javascript">

        function findSubcats() {
            var html = '<option value="">Subcategory</option>';

            var jqxhr = jQuery.getJSON('${findSubcategoriesURL}', {
                category: jQuery("#category").val(),
                ajax: 'true'
            }, function (data) {
                var len = data.length;
                for (var i = 0; i < len; i++) {
                    html += '<option value="' + data[i].name + '">'
                        + data[i].name + '</option>';
                }
                html += '</option>';

            }).done(function () {
                jQuery('#subcategory').html(html);
                jQuery("#subcatDiv").show();

            });


        }


        function setSubcats(id) {
            var html = '<option value="">Subcategory</option>';

            var jqxhr = jQuery.getJSON('${findSubcategoriesURL}', {
                category: jQuery("#category").val(),
                ajax: 'true'
            }, function (data) {
                var len = data.length;
                for (var i = 0; i < len; i++) {
                    if (data[i].id == id) {
                        html += '<option selected="selected" value="' + data[i].id + '">'
                            + data[i].name + '</option>';

                    } else {
                        html += '<option value="' + data[i].id + '">'
                            + data[i].name + '</option>';
                    }
                }
                html += '</option>';

            }).done(function () {
                jQuery('#subcategory').html(html);
                jQuery("#subcatDiv").show();
            });


        }
    </script>


</head>
<body>

<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>
<div class="container">

    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>
        <div id="main" class="col-sm-6 well text-center">
            <p class="text-success">
                <c:if test="${fn:length(operations)==0}">
                You haven`t operations yet.
                <br>
                You can add it simply by click the button below
                <br>
                </c:if>
                <c:if test="${operationDto!=null}">
            <h5>You added new operation </h5>
            </c:if>
            </p>

            <c:if test="${fn:length(operations)!=0}">
                <c:if test="${path=='/administration/all_accounts'}">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <div id="selectDiv" style="width:300px; margin: 0 auto">

                            <form:select style="width:250px" multiple="multiple" data-placeholder="Choose user login"
                                         id="userSelect" path="users">

                                <form:options items="${users}" itemLabel="login"
                                              itemValue="login"/>

                            </form:select>

                        </div>

                    </sec:authorize>
                </c:if>
                <h3>Operations</h3>

                <div id="operation_count_div">
                        ${fn:length(operations)}
                </div>


                <div id="viewOption">
                    <div>Select count of operations to view
                        <select id="countSelect">
                            <option value="10" selected>10</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>


                <div id="tableDiv">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="tablesorter" style="width: 100%;" id="operationTable" border="1"
                                   cellpadding="5"
                                   cellspacing="1"
                                   width="100%" align="center"
                                   summary="operations">
                                <thead class="text-center">

                                <c:if test="${path=='/administration/all_operations'}">
                                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                                        <th>Operation_id</th>
                                    </sec:authorize>
                                </c:if>
                                <th>Date</th>
                                <th>Category</th>
                                <th>Subcategory</th>
                                <th>Account</th>
                                <th>Sum</th>
                                <th>Currency</th>
                                <c:if test="${path=='/administration/all_operations'}">
                                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                                        <th>User_id</th>
                                    </sec:authorize>
                                </c:if>
                                </thead>
                                <tbody>
                                <c:forEach var="oper" items="${operations}">
                                    <tr id="row${oper.id}" onmouseover="showMenu(${oper.id})"
                                        onmouseout="hideMenu(${oper.id})">
                                        <c:if test="${path=='/administration/all_operations'}">
                                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                <td>${oper.id}</td>
                                            </sec:authorize>
                                        </c:if>
                                        <td>
                                            <div id="menuOperation${oper.id}" class="rc dopMenu box_shadow popup"
                                                 style="z-index: 10; display: none">
                                                <div style="padding: 0px 8px 5px 8px">
                                                    <a id="editOperationLink${oper.id}"
                                                       href="#" onclick="editOperation(${oper.id})">
                                                        <span>Редактировать</span>
                                                        <img border="0" src="/resources/img/edit.png" width="15"
                                                             height="15">
                                                    </a>
                                                </div>
                                                <div style="padding: 5px 8px; ">
                                                    <a id="deleteOperationLink${oper.id}" class="deleteOperationLink"
                                                        <%--href="<c:url value='/account/${account.id}/delete.html' />"--%>
                                                       href='#' onclick="deleteOperation(${oper.id})"/>
                                                    <span>Удалить</span>
                                                    <img border="0"
                                                         src="/resources/img/del.gif"
                                                         width="15"
                                                         height="15"></a>
                                                </div>
                                            </div>
                                            <fmt:formatDate var="fmtDate" pattern="dd.MM.yyyy HH:mm:ss"
                                                            value="${oper.date}"/>
                                                ${fmtDate}</td>

                                        <td>${oper.subcategory.category.name}</td>
                                        <td>${oper.subcategory.name}</td>
                                        <td>${oper.account.name}</td>
                                        <td>${oper.sum}</td>
                                        <td>${oper.currency}</td>

                                        <c:if test="${path=='/administration/all_operations'}">
                                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                <td>${oper.user.id}</td>
                                            </sec:authorize>
                                        </c:if>

                                    </tr>
                                </c:forEach>
                                </tbody>

                            </table>
                        </div>

                    </div>


                </div>


                <div id="pagination">

                    <c:if test="${numberOfPages>1}">

                        <%--Previous page button--%>
                        <c:if test="${currentPage!=1}">
                            <span id="prevPage" class="inactivePage">Prev</span>
                        </c:if>
                        <c:if test="${currentPage==1}">
                            <span id="prevPage" class="inactivePage" style="display: none;">Prev</span>
                        </c:if>

                        <%--number of pages is more--%>
                        <c:if test="${fullPage==0}">

                            <%--if page count is more than 10--%>
                            <c:if test="${numberOfPages>10}">

                                <%--14--%>
                                <c:forEach var="i" begin="1" end="${numberOfPages}" step="1">

                                    <c:if test="${i<4}">

                                        <c:if test="${currentPage==i}">
                                            <span id="page${i}" class="paginationSpan activePage"
                                                  onclick="loadPage(${i})">${i}</span>
                                        </c:if>
                                        <c:if test="${currentPage!=i}">
                                            <span id="page${i}" class="paginationSpan inactivePage"
                                                  onclick="loadPage(${i})">${i}</span>
                                        </c:if>

                                    </c:if>
                                    <c:if test="${i==4}">
                                        <span id="pageDivider" class="paginationSpan inactivePage"> ... </span>
                                    </c:if>


                                    <c:if test="${i>numberOfPages-3}">

                                        <c:if test="${currentPage==numberOfPages-3&&currentPage==i}">
                                            <span id="page${i}" class="paginationSpan inactivePage"
                                                  onclick="loadPage(${i})">... ${i}</span>
                                        </c:if>

                                        <c:if test="${numberOfPages-i<3}">
                                            <span id="page${i}" class="paginationSpan inactivePage"
                                                  onclick="loadPage(${i})">${i}</span>
                                        </c:if>

                                    </c:if>

                                </c:forEach>
                                1===============================
                            </c:if>

                            <%--if page count is less than 10--%>
                            <c:if test="${numberOfPages<=10}">
                                <c:forEach begin="1" end="${numberOfPages}" step="1" var="i">
                                    <c:if test="${i==currentPage}">
                                        <span id="page${i}" class="paginationSpan activePage"
                                              onclick="loadPage(${i})">${i}</span>
                                    </c:if>
                                    <c:if test="${i!=currentPage}">
                                        <span id="page${i}" class="paginationSpan" onclick="loadPage(${i})">${i}</span>
                                    </c:if>

                                </c:forEach>
                                2===============================
                            </c:if>
                        </c:if>


                        <c:if test="${fullPage==1}">
                            <%--if page count is more than 10--%>
                            <c:if test="${numberOfPages>10}">


                                <c:forEach var="i" begin="1" end="${numberOfPages-1}" step="1">

                                    <c:if test="${i<4}">
                                        <c:if test="${currentPage==i}">
                                            <span id="page${i}" class="paginationSpan activePage"
                                                  onclick="loadPage(${i})">${i}</span>
                                        </c:if>
                                        <c:if test="${currentPage!=i}">
                                            <span id="page${i}" class="paginationSpan inactivePage"
                                                  onclick="loadPage(${i})">${i}</span>
                                        </c:if>

                                        <c:if test="${i+1==currentPage&&i>=4}">
                                            <span id="page${i+1}" class="paginationSpan inactivePage"
                                                  onclick="loadPage(${i+1})">${i+1}</span>
                                        </c:if>
                                    </c:if>

                                    <c:if test="${i==4}">
                                        <span id="pageDivider" class="paginationSpan inactivePage"> ... </span>
                                    </c:if>

                                    <c:if test="${i>numberOfPages-4}">

                                        <c:if test="${currentPage==numberOfPages-4&&currentPage==i}">
                                            <span id="page${i}" class="paginationSpan inactivePage"
                                                  onclick="loadPage(${i})">... ${i}</span>
                                        </c:if>

                                        <c:if test="${numberOfPages-i<4}">
                                            <span id="page${i}" class="paginationSpan inactivePage"
                                                  onclick="loadPage(${i})">${i}</span>
                                        </c:if>

                                    </c:if>

                                </c:forEach>
                                3===============================

                            </c:if>
                            <%--if page count is less than 10--%>
                            <c:if test="${numberOfPages<=10}">
                                <c:forEach begin="1" end="${numberOfPages}" step="1" var="i">
                                    <c:if test="${i==currentPage}">
                                        <span id="page${i}" class="paginationSpan activePage"
                                              onclick="loadPage(${i})">${i}</span>
                                    </c:if>
                                    <c:if test="${i!=currentPage}">
                                        <span id="page${i}" class="paginationSpan" onclick="loadPage(${i})">${i}</span>
                                    </c:if>

                                </c:forEach>
                                4===============================
                            </c:if>
                        </c:if>

                        <%--Next page button--%>
                        <c:if test="${numberOfPages!=currentPage}">
                            <span id="nextPage" class="inactivePage">Next</span>
                        </c:if>
                        <c:if test="${numberOfPages==currentPage}">
                            <span id="nextPage" class="inactivePage" style="display: none;">Next</span>
                        </c:if>

                        <br/>

                        <div id="go_to_page_div">
                            <input id="page_to_go" type="number" size="4"> <input type="button" id="go_to_page_button"
                                                                                  value="Go"/>
                        </div>

                        <br/>
                        <div id="load_more_div" class="text-center">
                            <img src="/resources/img/load+more.ico" alt="" width="32" height="32"> Load More
                        </div>

                    </c:if>
                </div>


                <br/>
                <div class="text-center">
                    <br>
                    <img src="/resources/img/finance/PNG/png32/pie_chart.png" alt="" width="32" height="32">

                    <a href="/operationsStat.html">Statistics of operations</a><br>
                    <br>
                </div>

            </c:if>
            <img src="/resources/img/button_green_add.png" alt="" width="32" height="32">
            <a href="/addOperation.html">Add new operation</a><br>
        </div>
        <div id="rightBlock" class="col-sm-4 well text-center">

            <span id="createNewOperationTitle" class="text-center"><span id="createNewOperationSpan">Create new operation</span></span>
            <fmt:formatDate var="fmtDate" pattern="dd.MM.yyyy HH:mm:ss" value="<%=new java.util.Date()%>"/>
            <%--<%java.text.DateFormat df=new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm:ss");%>--%>

            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Date</span>
                </div>
                <div class="col-sm-9">
                    <input class="form-control" id="date" type="text" value="${fmtDate}">
                    <%--<input class="form-control" id="date" type="text" value="<%df.format(new java.util.Date());%>">--%>
                </div>
            </div>

            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Category</span>
                </div>
                <div class="col-sm-9">
                    <select id="category" style="width: 100%">

                    </select>

                </div>
            </div>


            <div id="subcatDiv" class="row" style="margin-bottom: 3px">
                <div class="col-sm-3 text-right">
                    <span>Subcategory</span>
                </div>
                <div id="subcatSelect" class="col-sm-9">
                    <select id="subcategory" style="width: 100%">

                    </select>


                </div>
            </div>


            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Account</span>
                </div>
                <div class="col-sm-9">
                    <select id="accountSelect">
                        <option value="">--Select--</option>
                        <c:forEach items="${accounts}" var="acc">
                            <option value="${acc.name}">${acc.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div id="accountBalance">

            </div>

            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Amount</span>
                </div>
                <div class="col-sm-9">
                    <input class="form-control" id="amount" type="text" value="">
                </div>
            </div>
            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Currency</span>
                </div>
                <div class="col-sm-9">

                    <select id="currencyOption" style="width: 100%" onchange="change()">
                        <c:forEach items="${currencies}" var="curr">
                            <option value="${curr}">${curr}</option>
                        </c:forEach>
                    </select>
                    <span style="font-size: 12px"><a href="/settings">Change default currencies</a></span>
                </div>
            </div>

            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                </div>
                <div class="col-sm-9">
                    <span id="error" type="text" value=""></span>
                </div>
            </div>


            <div id="buttonsRow" class="row text-right">

                <input id="addNewOperationButton" <%--disabled="true"--%> type="button" onclick="addOperation()" value=
                <spring:message code="user.body.buttonlabel"/>>

            </div>

            <div id="output">

            </div>
        </div>
    </div>
</div>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>


</body>
</html>