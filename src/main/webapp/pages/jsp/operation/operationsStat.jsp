<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Operations Statistics</title>
    <sec:csrfMetaTags/>

    <%--JQUERY--%>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>
    <link type="text/css" href="<c:url value="/resources/sorttable/style.css"/>" rel="stylesheet"/>



    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-1.8.3.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/datepicker/jquery-ui-1.12.1.js"/>"></script>
    <script type="text/javascript"
            src="<c:url value="/resources/js/datepicker/jquery-ui-timepicker-addon.js"/>"></script>

    <link type="text/css" href="<c:url value="/resources/js/datepicker/jquery-ui-1.12.1.css"/>" rel="stylesheet"/>
    <link type="text/css" href="<c:url value="/resources/js/datepicker/jquery-ui-timepicker-addon.css"/>"
          rel="stylesheet"/>

    <link type="text/css" href="<c:url value="/resources/multiple-select/multiple-select.css"/>" rel="stylesheet"/>
    <script type="text/javascript" src="<c:url value="/resources/multiple-select/multiple-select.js"/>"></script>
    <link type="text/css" href="<c:url value="/resources/sorttable/style.css"/>" rel="stylesheet"/>

    <link type="text/css" src="<c:url value="/resources/jquery-ui/jquery-ui-1.8.10.custom.css"/>" rel="stylesheet"/>

    <%--Add functionality of charts--%>
    <script type="text/javascript" src="<c:url value="/resources/js/amcharts/amcharts.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/amcharts/pie.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/amcharts/themes/light.js"/>"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all"/>

    <%--BOOTSTRAP--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

    <script type="text/javascript" src="<c:url value="/resources/tree/jstree.min.js"/>"></script>
    <%--<script type="text/javascript" src="<c:url value="/resources/tree/customCategoryTree.js"/>"></script>--%>

    <link type="text/css" href="<c:url value="/resources/tree/themes/default/style.min.css"/>" rel="stylesheet"/>
    <script type="text/javascript" src="<c:url value="/resources/js/operationsStat.js"/>"></script>


    <c:url var="operationStatsByCreaterias" value="/balanceOfOperations"/>
    <%--<c:url var="findBalanceOfOperations" value="/balanceOfOperations"/>--%>
    <c:url var="findBalanceOfOperationsForSelectedUsers" value="/balanceOfOperationsForSelectedUsers"/>
    <c:url var="findOperationsForUsers" value="/findOperationsForUsers"/>



    <style type="text/css">
        #chartdiv {
            width: 100%;
            height: 300px;
            font-size: 13px;
        }
        .myScrollableBlock {
            display: block;
            height: 170px;
            overflow: auto;
        }
    </style>


</head>


<body>
<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>


<div class="container">
    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>
        <div id="main" class="col-sm-6 well text-center">
            <%--<div style="text-align:center">--%>
                <%--categories <span style="font-size: 10px">(* without operations that have negative amount)</span>--%>
            <%--</div>--%>
                <div class="row">
            <a class="btn" data-toggle="collapse" data-target="#chartdiv">View graphic&raquo;</a>
            <div id="chartdiv" class="collapse"></div>
                </div>
                <div class="row">

                <a class="btn" data-toggle="collapse" data-target="#tableDiv">By details&raquo;</a>
            <div id="tableDiv" class="collapse"></div>
                </div>
        </div>
        <div id="rightBlock" class="col-sm-4 well text-left">
            <div id="whatToShowSelect" class="row">
                <div class="col-sm-4  text-left">
                    What to show
                </div>
                <div class="col-sm-8 ">
                    <select id="whatToShowOption" style="width: 100%" path="whatToShow">
                        <option value="1" >All operations</option>
                        <option value="2" selected>Costs</option>
                        <option value="3">Income</option>
                        <option value="4">Transactions</option>
                        <option value="5">Exchange</option>

                    </select>
                </div>

            </div>

            <div id="howToShowSelect" class="row" >
                <div class="col-sm-4">
                    How to show
                </div>
                <div class="col-sm-8">
                    <select id="howToShowOption" style="width: 100%; ">
                        <option value="details">Details</option>
                        <option value="cat" selected>By categories</option>
                        <option value="sources" style="display: none">By sources</option>
                        <option value="subcat">By subcategories</option>
                        <%--<option value="exchange">Exchange</option>--%>
                    </select>
                </div>


            </div>

            <div id="showAmountAfterEveryOperation" class="row">
                <div class="col-sm-4 " style="word-break: keep-all" >
                    Show amount after every operation
                </div>
                <div class="col-sm-8" style="text-align: center">
                    <input type="checkbox" path="showAmount" id="showAmount"/>
                </div>

            </div>

            <div id="periodSelect" class="row">

                <div class="col-sm-4">
                    For period
                </div>
                <div class="col-sm-8">
                    <select id="periodOption" style="width: 100%" path="periodToShow"
                            data-placeholder="Other period">
                        <option value="1">This day</option>
                        <option value="2">This week</option>
                        <option value="3">This month</option>
                        <option value="4">This year</option>
                        <option value="5">Last 20 operations</option>
                        <option value="6">Last 7 days</option>
                        <option value="7">Last 6 months</option>
                        <option value="8">Last month</option>
                        <option value="9">Last year</option>
                        <option value="10">All time</option>
                        <option value="11" selected>Other period</option>
                    </select>
                </div>

            </div>

            <div id="periodChooser">

                <div id="startPeriodSelect" class="row">
                    <div class="col-sm-4 ">

                    From:
                    </div>
                        <div class="col-sm-4">

                        <input id="datepickerStart" type="text"/>
                        </div>
                </div>

                <div id="endPeriodSelect" class="row">
                    <div class="col-sm-4">

                    To:
                    </div>
                    <div class="col-sm-4">

                    <input id="datepickerEnd" type="text"/>
                    </div>

                </div>

            </div>

            <div id="currencySelect" class="row">
                <div class="col-sm-4">
                    Currency
                </div>
                <div class="col-sm-8">
                    <form:select id="currencyOption" style="width:100%" onchange="change()" path="currencies">
                        <c:forEach items="${currencies}" var="curr">
                            <form:option value="${curr}">${curr}</form:option>
                        </c:forEach>
                    </form:select>
                </div>

            </div>

            <div id="accountSelect" class="row">
                <div class="col-sm-4">
                    Accounts
                </div>
                <div class="col-sm-4">
                    <form:select id="accountsOption" multiple="multiple" style="width: 100%" onchange="change()" path="accounts">
                        <c:forEach items="${accounts}" var="account">
                            <form:option value="${account.id}" selected="true">${account.name}</form:option>
                        </c:forEach>
                    </form:select>
                </div>


            </div>

            <div id="selectDiv" class="row" >
                <div class="col-sm-4">
                    Category
                </div>
                <div class="<%--myScrollableBlock--%> col-sm-8">
                <div id="catTree" style="width:100%">
                    <%--<ul>--%>
                        <%--<li>Root node 1</li>--%>
                        <%--<li>Root node 2</li>--%>
                    <%--</ul>--%>
                </div>
                </div>
                <%--Category <form:select class="select" style="width:250px" multiple="multiple" onchange="change()"--%>
                                      <%--data-placeholder="Choose category"--%>
                                      <%--id="categoriesSelect" path="categories">--%>

                <%--<form:options items="${categories}" itemLabel="name"--%>
                              <%--itemValue="id" selected="true"/>--%>

            <%--</form:select>--%>


            </div>

            <c:if test="${sources}!=null">

            <div id="sourcesSelectDiv" class="row" style="display: none">
                <div class="col-sm-4">
                    Sources
                </div>
                <div class="col-sm-8">
                    <form:select class="select" style="width:100%" multiple="multiple" onchange="change()"
                                 data-placeholder="Choose sources"
                                 id="sourcesSelect" path="sources">

                        <form:options items="${sources}" itemLabel="name"
                                      itemValue="id"/>

                    </form:select>
                </div>

            </div>
            </c:if>

            <div id="amountSelect" class="row" style="display: none;">
                <div class="col-sm-4">
                    Amount
                </div>
                <div class="col-sm-8">
                    <select id="amountOption" style="width: 100%" path="amountSelect"
                            data-placeholder="Any">
                        <option value="1" selected>Any</option>
                        <option value="2">More than</option>
                        <option value="3">Less than</option>
                        <option value="4">Equal</option>
                        <option value="5">Among</option>
                    </select>
                </div>

            </div>

            <div id="amountMerger" class="row" style="display: none;">
                from <input id="fromAmount" type="text" ><br>
                to <input id="toAmount" type="text"     >
            </div>

            <div id="amountValue" class="row" style="display: none;">
                value <input id="value" type="text">
            </div>

            <div id="roundModSelect" class="row">
                <div class="col-sm-4">

                    Average

                </div>
                <div class="col-sm-8">

                <select id="roundOption" style="width: 100%" path="howToRound">
                <option value="5" selected>No need</option>
                <option value="1">By day</option>
                <option value="2">By week</option>
                <option value="3">By month</option>
                <option value="4">By year</option>

            </select>

            </div>
            </div>

            <div id="buttonsRow" class="row text-right">

                <input id="refreshButton" <%--disabled="true"--%> type="button" onclick="refreshDiagram()" value=
                <spring:message code="user.body.buttonR"/>>

            </div>


        </div>

    </div>
</div>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>


</body>
</html>