<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Add Operation</title>
    <script src="/resources/jquery-ui/jquery-1.8.3.js" type="text/javascript"></script>

    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

    <c:url var="findCategoriesURL" value="/findcategories"/>
    <c:url var="findSubcategoriesURL" value="/findsubcategories"/>
    <c:url var="findAccountSumURL" value="/findAccountSum"/>

    <script language="javascript" type="text/javascript">
        $(document).ready(
            function () {
                $.getJSON('${findCategoriesURL}', {
                    ajax: 'true'
                }, function (data) {
                    var html = '<option value="">Category</option>';
                    var len = data.length;

                    for (var i = 0; i < len; i++) {
                        html += '<option value="' + data[i].name + '">'
                            + data[i].name + '</option>';
                    }
                    html += '</option>';
                    $('#category').html(html);
                });
            });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#category').change(
                function () {
                    $.getJSON('${findSubcategoriesURL}', {
                        category: $(this).val(),
                        ajax: 'true'
                    }, function (data) {
                        var html = '<option value="">Subcategory</option>';
                        var len = data.length;
                        for (var i = 0; i < len; i++) {
                            html += '<option value="' + data[i].name + '">'
                                + data[i].name + '</option>';
                        }
                        html += '</option>';

                        $('#subcategory').html(html);
                    });
                });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#subcategory").change(onSelectChange);
        });

        function onSelectChange() {
            var selected = $("#subcategory option:selected");
            var output = "";
            if (selected.val() != 0) {
                output = "You selected Subcategory " + selected.text();
            }
            $("#output").html(output);
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#accountSelect').change(
                function () {
                    $.getJSON('${findAccountSumURL}', {
                        accountName: $(this).val(),
                        ajax: 'true'
                    }, function (data) {
                        var html = "Available amount of money at this account= " + data;

                        $('#accountBalance').html(html);
                    });
                });
        });

    </script>

</head>
<body>
<h1>Enter new operation</h1>
Language: <a href="?lang=en">English</a>|<a href="?lang=es">Espanol</a>


<form:form modelAttribute="operation" method="post" enctype="utf-8">
    <form:errors path="*" cssClass="errorblock" element="div"/>
    <table cellpadding="2" cellspacing="2" width="100%">
        <fmt:formatDate var="fmtDate" value="<%=new java.util.Date()%>" pattern="dd.MM.yyyy HH:mm:ss"/>
        <tr>
            <td>Date</td>
            <td>
                <form:input path="date" value="${fmtDate}" type="text"/>
            </td>
            <td>
                <form:errors path="date" element="div"/>
            </td>

        </tr>


        <tr>
            <td>Category</td>
            <td>
                <form:select id="category" path="category">
                </form:select>
            </td>
        </tr>
        <tr>
            <td>Subcategory</td>
            <td>
                <form:select id="subcategory" path="subcategory">
                    <form:option value="">Subcategory</form:option>
                </form:select>
            </td>
        </tr>


        <tr>
            <td>Account</td>
            <td>
                <form:select id="accountSelect" path="account" multiple="false">

                    <form:option value="">--Select--</form:option>
                    <form:options items="${accounts}" itemLabel="name"
                                  itemValue="name"/>
                </form:select>
            </td>
            <td>
                <form:errors path="account" element="div"/>
            </td>

        </tr>
        <tr>
            <td></td>
            <td id="accountBalance"></td>
            <td></td>
        </tr>
        <tr>
            <td>Amount</td>
            <td>
                <form:input path="sum" type="text"/>

            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="submit" value="Add"/>
            </td>
        </tr>
    </table>
</form:form>
<div id="output">

</div>


<a href="/">Start page</a><br>

</body>
</html>