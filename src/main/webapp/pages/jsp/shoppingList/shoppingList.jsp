<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="func" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <sec:csrfMetaTags/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <%--JQUERY--%>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
    <%--<script type="text/javascript" src="<c:url value="/resources/multiple-select/multiple-select.js"/>"></script>--%>
    <script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>

    <link type="text/css" href="<c:url value="/resources/myStyle.css"/>" rel="stylesheet"/>

    <%--BOOTSTRAP--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

    <script type="text/javascript" src="<c:url value="/resources/js/shoppingList.js"/>"></script>

    <script type="application/javascript">
        var currencies = new Array();
        <c:forEach items="${fav_curr}" var="curr">
        currencies.push("${curr}");
        </c:forEach>
    </script>


    <link type="text/css" href="<c:url value="/resources/css/shoppingList.css"/>" rel="stylesheet"/>
    <title>Shopping List</title>
</head>
<body <%--onload="hideItems()"--%>>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>
<div class="container">
    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>
        <div id="main" class="col-sm-6 well text-center">
            <c:if test="${fn:length(shoppingListQueries)==0}">
                <br><br><br>
                You haven`t had shopping list query yet.
                <br>
                You can add it simply by click the button below
                <br>
            </c:if>


            <c:if test="${item!=null}">
                <h5>You added new item ${item.name}</h5>
            </c:if>

            <c:if test="${fn:length(shoppingListQueries)!=0}">

                <div style="text-align: center">
                    <h4>Shopping Lists</h4>
                </div>


                <div id="shopping_queries" align="center">

                    <c:forEach var="query" items="${shoppingListQueries}">


                        <div id="query_${query.key.id}" class="row no-gutter listQuery well"
                             style="margin-top:0px; margin-bottom: 0px; background-color: #00dd00"
                             onclick="showItems(this.id, ${query.key.id})"
                             onmouseover="showMenu(${query.key.id})" onmouseout="hideMenu(${query.key.id})" class="btn"
                             data-toggle="collapse" data-target="#query_list_${query.key.id}">


                            <div id="menuShopping${query.key.id}" class="rc dopMenu box_shadow popup"
                                 style="z-index: 10; display: none; font-size: 10px">

                                <div style="padding: 0px 8px 5px 8px">
                                    <a id="editShoppingListLink${query.key.id}" href="#"
                                       onclick="editShoppingList(${query.key.id})">
                                        <span>Edit</span> <img border="0" src="/resources/img/edit.png"
                                                                        width="15" height="15">
                                    </a>
                                </div>

                                <div style="padding: 5px 8px; ">
                                    <a id="deleteShoppingListLink${query.key.id}" class="deleteShoppingListLink"
                                       href='#' onclick="deleteShoppingList(${query.key.id})"/>
                                    <span>Удалить</span> <img border="0" src="/resources/img/del.gif" width="15"
                                                              height="15"></a>
                                </div>
                            </div>

                            <div id="query_info_${query.key.id}" class="col-md-10 text-left">

                                        <span id="query_name_span_${query.key.id}" class="listName">
                                                ${query.key.name}
                                        </span>

                                <span id="query_info_span_${query.key.id}" style="font-size: 10px">
                                        <c:choose>
                                            <c:when test="${fn:length(query.key.items)!=0}">(Contains ${fn:length(query.key.items)} items. Total price is ${func:getSummary(query.key.items, user)} ${def_curr} )

                                            </c:when>
                                            <c:otherwise>(No contains items)
                                            </c:otherwise>
                                        </c:choose>
                                        </span>
                                <br/>
                                <span id="query_desc_span_${query.key.id}">
                                        ${query.key.desc}
                                </span>

                            </div>

                            <div class="col-md-2" <%--style="border: medium solid"--%>>


                    <span id="img_expand_query_${query.key.id}" style="cursor:pointer;display:block"
                        <%--  onclick="showItems(this.id, ${query.key.id})"--%> title="Expand">
                        <img src="/resources/img/v3_expand.png" style="" border="0" vspace="8" width="15" class="btn"
                             data-toggle="collapse" data-target="#query_list_${query.key.id}"/>
                    </span>

                                <span id="img_min_query_${query.key.id}" style="cursor:pointer;display:none"
                                      <%--onclick="hideItems(this.id, ${query.key.id})" --%>title="Minimize">
                        <img src="/resources/img/v3_minim.png" style="" border="0" vspace="8" width="15" class="btn"
                             data-toggle="collapse" data-target="#query_list_${query.key.id}"/>
                    </span>


                            </div>


                        </div>
                        <div id="query_list_${query.key.id}" class="bBody collapse well query"
                             style="margin-top:0px; margin-bottom: 0px; background-color: #9acfea;padding:0px;/*display:block;*/">

                            <c:choose>
                                <c:when test="${fn:length(query.value)!=0}">
                                    <div id="items${query.key.id}" class="items">
                                        <c:forEach items="${query.value}" var="item">

                                            <div id="item${item.id}" class="row itemOfList"
                                                 style="word-wrap: break-word" data-sort="${item.priority}">
                                                <div <%--id="menuShopping${query.key.id}" --%>class="col-md-6 text-left">
                                                    <span id="item_name_${item.id}" style="margin-left: 10px">${item.name} </span>
                                                    <br>

                                                    <div style="margin-left: 10px" class="tMini tGray"
                                                         style="word-break: break-all">
                                                        Description: <span id="item_desc_${item.id}"> ${item.desc}</span>
                                                    </div>

                                                </div>
                                                <div <%--id="menuShopping${query.key.id}" --%>class="col-md-2 text-left">
                                                    <span onclick="deleteItem(${item.id})">Удалить</span>
                                                    <span onclick="editShoppingItem(${item.id})">Edit</span>
                                                    <span onclick="itemUp(${item.id})">Поднять</span>
                                                    <span onclick="itemDown(${item.id})">Опустить</span>

                                                </div>
                                                <div class="col-md-4 text-right">
                                                    <div style="margin-right: 5px">
                                                <span title="Price">
                                                        <div class="sum">
                                                                <span id="item_price_${item.id}" class="green">${item.price}
                                                                </span>
                                                            <span id="item_currency_${item.id}" class="tMini tGray">
                                                                    ${item.currencyType}
                                                            </span>
                                                        </div>
                                                  </span>
                                                    </div>

                                                </div>
                                            </div>

                                        </c:forEach>
                                    </div>
                                </c:when>
                                <c:otherwise>

                                    <div id="query_list_${query.key.id}" class="bBody collapse well "></div>
                                    <div id="items${query.key.id}" class="items"></div>
                                    <div id="absent_items_${query.key.id}" style="display: none">
                                        <c:if test="${fn:length(shoppingList)==0}">
                                            You haven`t had items in shopping list yet.<br>
                                            You can add it simply by click the button below<br>
                                        </c:if>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                            <div id="addItem_${query.key.id}" class="btn-success" style="display:none;">
                                <span style="cursor: pointer;" onclick="showAddNewItemForm(${query.key.id})">Add new item</span>
                            </div>

                        </div>

                        <br>
                    </c:forEach>


                </div>


            </c:if>
            <div class="btn">
                <a href="/addShoppingList.html">Add new list</a>
            </div>


        </div>


        <div id="rightBlock" class="col-sm-4 well text-center">
            <div id="nodeInfo">
                <div id="createOffer">
                    <c:choose>
                        <c:when test="${fn:length(shoppingListQueries)!=0}">
                            <div class="row">
                                Select list or item for edit
                            </div>
                            <div class="row">
                                or create new:
                                <br>
                                <a id="createList" href="#">List</a>
                                <br>
                                <a id="createItem" href="#">Item</a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="row">
                                Create new <a id="createList" href="#">List</a>

                            </div>
                        </c:otherwise>
                    </c:choose>

                </div>

                <div id="errorField">

                </div>
            </div>
        </div>
    </div>
</div>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>


</body>
</html>
