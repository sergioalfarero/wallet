<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <script type="application/javascript">
        $("#btnAddProfile").attr('value', 'Save');
        }
    </script>
    <script src="<c:url value="/resources/js/jquery.1.9.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/vendor/jquery.ui.widget.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.iframe-transport.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.fileupload.js"/>"></script>
    <!-- we code these -->
    <link href="<c:url value="/resources/css/dropzone.css"/>" type="text/css" rel="stylesheet"/>
    <%--<link href="<c:url value="/resources/css/tinylife.css"/>" type="text/css" rel="stylesheet"/>--%>
    <script c src="<c:url value="/resources/js/myuploadfunction.js" />"></script>
    <%--<script src="<c:url value="/resources/js/myuploadfunction1.js"/>"></script>--%>


    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title><spring:message code="account.addnew"/></title>
    <style type="text/css">
        .error {
            color: #ff0000;
        }

        .icons {
            color: #ff0000;
            border: dashed grey;
            background-color: gainsboro;
            margin: 15px 0px;
            width: 375px;
        }

        .standard_icons {

            display: inline-block;
            vertical-align: middle;
        }

        .errorblock {
            color: #000;
            background-color: #ffEEEE;
            border: 3px solid #ffEEEE;
            padding: 8px;
            margin: 16px;
        }

    </style>

</head>
<body>

<h1>Change shopping item</h1>
Language: <a href="?lang=en">English</a>|<a href="?lang=es">Espanol</a>


<form:form modelAttribute="shoppingItem" method="post" enctype="utf-8">

    <form:errors path="*" cssClass="errorblock" element="div"/>
    <table>

        <tr>
            <td>Items's Name</td>
            <td>
                <form:input path="name"/>
            </td>
            <td>
                <form:errors path="name" element="div"/>
            </td>
        </tr>
        <tr>
            <td>Item's price</td>
            <td><form:input path="price" pattern='^\d+(?:\.\d{0,2})$' title="Format example: 1234.56"/></td>
            <td><form:errors path="price" cssClass="error"/></td>
        </tr>
        <tr>
            <td>Currency</td>
            <td>
                <form:select path="currencyType" multiple="false">
                    <option value="select" label="-Select-" disabled="disabled">
                        <form:options items="${currencies}"/>
                    </option>
                </form:select>
            </td>
        </tr>
        <tr>
            <td>Description</td>
            <td>
                <form:input path="desc"/>
            </td>
            <td>
                <form:errors path="desc" element="div"/>
            </td>
        </tr>
        <tr>
                <%--//:todo--%>
            <form:hidden path="user" value="${pageContext.request.userPrincipal.name}"/>
        </tr>
        <tr>
            <td colspan="1">
                <input type="submit" value="Apply"/>
            </td>
        </tr>
    </table>
</form:form>
<a href="${prevPage}">Cancel</a><br><br><br><br>

<%--<sec:authorize access="!hasRole('ROLE_ADMIN')">--%>
<%--<a href="/accounts.html">Cancel</a><br><br><br><br>--%>
<%--</sec:authorize>--%>
<%--<sec:authorize access="hasRole('ROLE_ADMIN')">--%>
<%--<a href="/administration/all_accounts.html">Cancel</a><br><br><br><br>--%>
<%--</sec:authorize>--%>

<a href="/">Start page</a><br>

</body>
</html>