<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ page session="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>Update Password</title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>
<%--<sec:authorize access="hasAnyRole">--%>
<div class="container">
    <div class="span12">
        <h1>
            Reser Password
        </h1>
        <div>
            <br>

            <tr>
                <td><label>
                    Password
                </label></td>
                <td><input id="pass" name="password" type="password" value=""/></td>
            </tr>
            <tr>
                <td><label>
                    Confirm
                </label></td>
                <td>
                    <input id="passConfirm" type="password" value=""/>
                    <span id="error" class="alert alert-error" style="display:none">
                            PasswordMatches.user
                        </span>
                </td>
            </tr>
            <br><br>
            <button type="submit" onclick="savePass()">
                Update Password
            </button>
        </div>

    </div>
</div>

<script src="/resources/jquery-ui/jquery-1.8.3.js" type="text/javascript"></script>

<script type="text/javascript">
    function savePass() {
        var pass = $("#pass").val();
        alert(pass);
        var valid = pass == $("#passConfirm").val();
        if (!valid) {
            $("#error").show();
            alert("error");
            return;
        }
        $.get("<c:url value="/user/savePassword"></c:url>", {password: pass}, function (data) {
            alert("Ok");
            window.location.href = "<c:url value="/login"></c:url>" + "?message=" + data.message;
        })
            .fail(function (data) {
                alert(data.responseJSON.message);
                window.location.href =
                    "<c:url value="/login"></c:url>" + "?message=" + data.responseJSON.message;
            });
    }
</script>
<%--</sec:authorize>--%>
</body>

</html>