<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title></title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>


</head>
<body>
<c:url value="/logout" var="logoutUrl"/>

<sec:authorize access="isAnonymous()">
    <a href="/user/registration.html">Registration</a><br>
</sec:authorize>

<sec:authorize access="!isAnonymous()">
    <a href="${logoutUrl}">Log Out</a><br>
</sec:authorize>

</body>
</html>
