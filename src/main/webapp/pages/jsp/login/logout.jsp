<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Logout</title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>
    <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
        <h1 id="error" class="alert alert-danger">
            <spring:message code="messages.logoutError"></spring:message>
        </h1>
    </c:if>
</head>
<body>

<div class="container">

    <c:if test="${param.logSucc==true}">
        <h1 id="success" class="alert alert-info">
            <spring:message code="messages.logoutSucc"></spring:message>
        </h1>
    </c:if>
    <a class="btn btn-primary" href="<c:url value="" />">
        <spring:message code="label.form.loginLink"></spring:message>
    </a>
</div>


<a href="/" style="align-self:center">Start page</a><br>

</body>
</html>