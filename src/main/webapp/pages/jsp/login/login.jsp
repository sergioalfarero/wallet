<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Autorization</title>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
    <%--BOOTSTRAP--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body onload="document.loginForm.login.focus()">
<jsp:include page="/pages/jsp/menuHeader1.jsp"/>


<div class="container">
    This application is created for those people who calculate their money and want to optimize their spents for permite
    themself more
</div>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4 well text-center">
            <img src="/resources/img/default/mainPhoto.png" height="300" width="300">
        </div>
        <div class="col-sm-3">
            <div <%--class="container"--%> <%--style="width:300px;"--%>>
                <c:url value="/pages/jsp/login/login.jsp" var="loginUrl"/>

                <form name="loginForm" action="/j_spring_security_check" method="post">
                    <c:if test="${param.logout != null}">
                        <div class="alert alert-success">
                            <p>You have been logged out successfully.</p>
                        </div>
                    </c:if>
                    <table>

                        <tr>
                            <h2 class="form-signin-heading">Please sign in</h2>
                        </tr>

                        <tr>
                            <td>Login</td>
                            <td><input type="text" id="username" name="username" class="form-control" required
                                       autofocus>
                            </td>
                        </tr>

                        <tr>
                            <td>Password</td>
                            <td><input type="password" id="password" name="password" class="form-control" required></td>
                        </tr>

                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <c:if test="${param.error=='true'}">
                            Invalid login or password
                        </c:if>
                        <tr>
                            <td>Remember Me:</td>
                            <td><input type="checkbox" name="remember-me"/></td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-lg btn-primary btn-block" type="submit" value="submit">Войти
                                </button>
                            </td>
                        </tr>
                    </table>

                </form>
                <a href="<c:url value="/forgot" />">Forget Password</a><br>
                <a href="<c:url value="/user/registration"/>" style="align-self:center">Registration</a><br>
            </div>

        </div>

    </div>

</div>

<script type="text/javascript">
    document.getElementById('loginForm:login').onfocus();
</script>

<c:if test="${param.message != null}">
    <div class="alert alert-info">
            ${param.message}
    </div>
</c:if>
<%--<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>--%>


<c:if test="${param.error != null}">
    <div class="alert alert-danger">
            ${SPRING_SECURITY_LAST_EXCEPTION}
    </div>
</c:if>
<script type="text/javascript">
    function validate() {
        if (document.loginForm.username.value == "" && document.loginForm.password.value == "") {
            alert("Login and password are required");
            document.loginForm.username.focus();
            return false;
        }
        if (document.loginForm.username.value == "") {
            alert("Login is required");
            document.loginForm.username.focus();
            return false;
        }
        if (document.loginForm.password.value == "") {
            alert("Password is required");
            document.loginForm.password.focus();
            return false;
        }
    }
</script>
<br>


</body>
</html>