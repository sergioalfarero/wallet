<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
<head>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>

<script type="text/javascript">
    function validate() {
        if (document.f.username.value == "" && document.f.password.value == "") {
            alert("${noUser} and ${noPass}");
            document.f.username.focus();
            return false;
        }
        if (document.f.username.value == "") {
            alert("${noUser}");
            document.f.username.focus();
            return false;
        }
        if (document.f.password.value == "") {
            alert("${noPass}");
            document.f.password.focus();
            return false;
        }
    }
</script>

<h1>Login</h1>
<fmt:setBundle basename="messages"/>
<fmt:message key="message.password" var="noPass"/>
<fmt:message key="message.username" var="noUser"/>
<form name='f' action="" method='POST'>
    <table>
        <tr>
            <td>User:</td>
            <td><input type='text' name='username' value=''></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='password'/></td>
        </tr>
        <tr>
            <td><input name="submit" type="submit" value="submit"/></td>
        </tr>
    </table>
</form>
<a href="?lang=en">English</a> |
<a href="?lang=es_ES">Spanish</a>

<c:if test="${param.regSucc == true}">
    <div id="status">
        <spring:message code="message.regSucc">
        </spring:message>
    </div>
</c:if>
<c:if test="${param.regError == true}">
    <div id="error">
        <spring:message code="message.regError">
        </spring:message>
    </div>
</c:if>

</body>
</html>