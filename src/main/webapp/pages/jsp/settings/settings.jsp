<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="func" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <sec:csrfMetaTags/>

    <title>Settings</title>
    <%--<link type="text/css" href="<c:url value="/resources/multiple-select/multiple-select.css"/>" rel="stylesheet"/>--%>
    <link type="text/css" href="<c:url value="/resources/sorttable/style.css"/>" rel="stylesheet"/>
    <link type="text/css" href="<c:url value="/resources/myStyle.css"/>" rel="stylesheet"/>

    <%--JQUERY--%>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
    <%--<script type="text/javascript" src="<c:url value="/resources/multiple-select/multiple-select.js"/>"></script>--%>
    <script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>

    <%--BOOTSTRAP--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/bootstrap-select-1.12.2/dist/css/bootstrap-select.css" />"
          rel="stylesheet"/>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap-select-1.12.2/dist/js/bootstrap-select.js"/>"></script>




    <script type="text/javascript">
        $(document).ready(function () {
//            $('#currenciesSelect').multipleSelect({
//                isOpen: false,
//                keepOpen: false,
//                filter: true,
//                multipleWidth: 250
//            });
            $('.selectpicker').selectpicker({
                style: 'btn-info',
                size: 4
            });
        });

    </script>

    <script type="text/javascript">
        function setSettings() {
            var csrfParameter = jQuery("meta[name='_csrf_parameter']").attr("content");
            var csrfToken = jQuery("meta[name='_csrf']").attr("content");
            var csrfHeader = jQuery("meta[name='_csrf_header']").attr("content");

            var bank= jQuery("#bank :selected").val();

            var isOfficialRates = jQuery('#useOfficialRates').prop("checked");

            var currency = jQuery("#defaultCurr :selected").val();


            var currencies = [];
            jQuery("#currenciesSelect option:selected").each(function () {
                currencies.push(this.value);
            });

            alert("1. "+bank+" "+
                +" 2. "+isOfficialRates+" 3."+ currency+" 4."+currencies);

            var settings= {
                bank: bank,
//                isOfficialRates: isOfficialRates,
                defaultCurrency: currency,
                favouriteCurrenciesSet: currencies,
                defaultRates: isOfficialRates
            }

            jQuery(document).ajaxSend(function (e, xhr, options) {
                xhr.setRequestHeader(csrfHeader, csrfToken);
                xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

            });
            jQuery.ajax({
                type: 'POST',
                url: 'settings/change/',
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                async: true,
                data: JSON.stringify(settings),
                success: function (data) {
                    console.log("SUCCESS: ", data);
                    jQuery("#error").text("");
                },
                error: function (e) {
                    console.log("ERROR: ", e);
                    console.log("ERROR: ", "");
                    jQuery("#error").text("e.responseJSON.message");
                    jQuery("#error").style("color:red");

                },
                done: function (e) {
                    console.log("DONE");
                }

            })

        }
    </script>

</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>

<div class="container">
    <div class="row">

        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>

        <div id="main" class="col-sm-6 well">
<div class="row text-center" >
    <span class="text-center;" style="font-size: 16px">Settings</span>

</div>

            <div class="row">
                <div class="col-sm-4">
                    Default Bank
                </div>
                <div class="col-sm-8">
                    <select id="bank" class="selectpicker">
                        <c:choose>
                            <c:when test="${fn:length(banks)==0}">
                                value="${banks}">${banks}</option>
                            </c:when>
                            <c:otherwise>
                                <c:forEach items="${banks}" var="bank">
                                    <c:choose>
                                        <c:when test="${func:containsList(banks, settingsDto.bank)}">
                                            <option value="${bank}" selected="true">${bank}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${bank}">${bank}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    Use official rates
                </div>
                <div class="col-sm-8">
                    <c:choose>
                        <c:when test="${settingsDto.defaultRates==true}">
                            <input type="checkbox" path="defaultRates" id="useOfficialRates" checked/>
                        </c:when>
                        <c:otherwise>
                            <input type="checkbox" path="defaultRates" id="useOfficialRates"/>
                        </c:otherwise>
                    </c:choose>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    Default currency
                </div>
                <div class="col-sm-8">
                    <select id="defaultCurr" class="selectpicker"  path="defaultCurrency">
                        <c:choose>
                            <c:when test="${fn:length(currencies)==0}">
                             <option value="${currencies}">${currencies}</option>
                            </c:when>
                            <c:otherwise>
                                <c:forEach items="${currencies}" var="curr">
                                    <c:choose>
                                        <c:when test="${curr==settingsDto.defaultCurrency}">
                                            <option value="${curr}" selected="true">${curr}
                                            </option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${curr}">${curr}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>

                            </c:otherwise>

                        </c:choose>

                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    The most used currencies
                </div>
                <div class="col-sm-8">
                    <div id="selectDiv" style="width:100%; margin: 0 auto">


                        <select class="selectpicker" style="width: 100%;"
                                data-placeholder="Choose favorite currencies"
                                id="currenciesSelect" path="favouriteCurrenciesSet" multiple>
                            <c:choose>
                                <c:when test="${fn:length(favouriteCurrencies)==0}">
                                    value="${currencies}">${currencies}</option>
                                </c:when>
                                <c:otherwise>
                                    <c:forEach items="${currencies}" var="curr">
                                        <c:choose>
                                            <c:when test="${func:contains(favouriteCurrencies, curr)}">
                                                <option value="${curr}" selected="true">${curr}
                                                </option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${curr}">${curr}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>

                                </c:otherwise>

                            </c:choose>


                        </select>
                    </div>

                </div>
            </div>


            <input type="button" onclick="setSettings()" value=<spring:message code="user.body.buttonChangeLabel"/>>


        </div>

        <div id="rightBlock" class="col-sm-4 well text-center"></div>

    </div>

</div>

<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>

</body>
</html>