<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<html>
<head>
    <script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>CurrenciesRates</title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>

<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>


<div class="container">
    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>
        <div id="main" class="col-sm-6 well text-center">
            <c:choose>
                <c:when test="${rateList==null}">
                    There is not downloaded rates in server. <br>
                    Otherwise, there can be Internet connecton problems on the server or bank site
                </c:when>
                <c:otherwise>
                    Course of ${rateList.baseRate} ${rateList.baseType} <img src="/resources/img/flags/${rateList.baseType}.ico" align="absmiddle"
                                                                             width="24"
                                                                             height="24"> on ${rateList.date}
                    <table class="tablesorter" id="currencyTable" border="1" cellpadding="5" cellspacing="1"
                           align="center"
                           summary="currenciesRates">
                        <tr>
                                <%--<td>Count</td>--%>
                                <%--<td>Output Currency</td>--%>
                                <%--<td></td>--%>
                            <td>Currency</td>
                            <td>Rate</td>
                        </tr>


                        <c:choose>
                            <c:when test="${fn:length(rateList.rates)!=0}">
                                <c:forEach items="${rateList.rates}" var="rate" varStatus="status">


                                    <%--<c:forEach var="rate" items="${rateList}">--%>
                                    <tr>

                                            <%--<td>${rate.rate1}</td>--%>
                                            <%--<td>${rate.currencyType1}</td>--%>
                                            <%--<td>=</td>--%>
                                        <td><img src="/resources/img/flags/${rate.currencyType}.ico" align="absmiddle"
                                                 width="24"
                                                 height="24"> ${rate.currencyType}</td>
                                        <td>${rate.rate}</td>

                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                No official currencies found!

                            </c:otherwise>
                        </c:choose>
                    </table>

                </c:otherwise>
            </c:choose>


        </div>
        <div id="rightBlock" class="col-sm-4 well text-center"></div>
    </div>
</div>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>


</body>
</html>
