<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>User`s information</title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>


<table>
    <tr>
        <th>id</th>
        <td>${userinfo.id}</td>
    </tr>
    <tr>
        <th>sur</th>
        <td>${userinfo.surname}</td>
    </tr>
    <tr>
        <th>name</th>
        <td>${userinfo.firstname}</td>
    </tr>
    <tr>
        <th>org</th>
        <td>${userinfo.organization}</td>
    </tr>
    <tr>
        <th>gender</th>
        <td>${userinfo.gender}</td>
    </tr>
    <tr>
        <th>country</th>
        <td>${userinfo.country}</td>
    </tr>
    <tr>
        <th>city</th>
        <td>${userinfo.city}</td>
    </tr>


</table>
<a href="/">Change information</a><br>
<a href="/">Start page</a><br>

<a href="/">Start page</a><br>
</body>
</html>