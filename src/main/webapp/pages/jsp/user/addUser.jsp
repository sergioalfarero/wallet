<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="user.title.add"/></title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>

<h1><spring:message code="user.body.intro"/></h1>

Language: <a href="?lang=en">English</a>|<a href="?lang=es">Espanol</a>

<form:form commandName="user" method="post" modelAttribute="user">
    <table>

        <tr>
            <td><spring:message code="user.body.login"/></td>
            <td>
                <form:input path="login"/>
            </td>
        </tr>

        <tr>
            <td>E-mail</td>
            <td>
                <form:input path="email"/>
            </td>
        </tr>


        <tr>
            <td><spring:message code="user.body.password"/></td>
            <td>
                <form:password showPassword="false" tabindex="*" path="password"/>
            </td>
        </tr>


        <tr>
            <td><spring:message code="user.body.password"/></td>
            <td>
                <form:password showPassword="false" tabindex="*" path="matchingPassword"/>
            </td>
        </tr>

        <tr>
            <td>UserRole</td>
            <td>
                <form:select path="roleType" multiple="false">
                    <option value="select" label="-Select-">
                        <form:options items="${roleTypes}"/>
                    </option>
                </form:select>
            </td>
        </tr>


        <tr>
            <td colspan="2">
                <input type="submit" value=<spring:message code="user.body.buttonlabel"/>/>
            </td>
        </tr>
    </table>
</form:form>

<a href="/">Start page</a><br>

</body>
</html>