<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Categories</title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>
    <%--JQUERY--%>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>
    <link type="text/css" href="<c:url value="/resources/sorttable/style.css"/>" rel="stylesheet"/>

    <%--BOOTSTRAP--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>

<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>

<div class="container">
    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>
        <div id="main" class="col-sm-6 well text-center">
            <h1>Registered users</h1>
            <div class="row">
                <table>
                    <tr>
                        <th>id</th>
                        <th>login</th>
                        <th>pass</th>
                        <th>name</th>
                        <th>sur</th>
                        <th>org</th>
                        <th>sex</th>
                    </tr>
                    <c:forEach var="user" items="${users}">
                        <tr>
                            <td>${user.id}</td>
                            <td>${user.login}</td>
                            <td>${user.password}</td>
                            <td>${user.firstName}</td>
                            <td>${user.surname}</td>
                            <td>${user.organization}</td>
                            <td>${user.gender}</td>
                        </tr>
                    </c:forEach>
                </table>

            </div>
            <a href="/addUser.html">Add new user</a><br>

        </div>
        <div id="rightBlock" class="col-sm-4 well text-center"></div>
    </div>
</div>
<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>


</body>
</html>