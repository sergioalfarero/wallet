<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <link type="text/css" href="<c:url value="/resources/css/menu.css"/>" rel="stylesheet"/>
    <script type="text/javascript" src="<c:url value="/resources/js/Menu.js"/>"></script>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>

<div class="rc box_shadow blk" id="m_plan_block" style="">
    <div class="bTitle" id="c_plan_block">
        <div id="s_plan_block">Контроль расходов</div>
        <div class="tMiddle tGray" id="w_plan_block" style="display: none;">
            <img src="img/indicator.gif" align="absmiddle">
            Секунду
        </div>
        <div class="cls">
            <div style="padding-top:8px;display:none" class="dash tGray tMini" onclick="toggleRoundDiv('plan_block');"
                 id="div_open_plan_block">Показать
            </div>
            <span style="cursor: pointer;display:show" onmouseover="hltMinClose(this);" onmouseout="hltMinClose(this);"
                  onclick="return toggleRoundDiv('plan_block');" title="Свернуть" id="a_min_plan_block">
		<img vspace="8" width="15" src="img/v3_min.png" border="0">
		<img vspace="8" width="15" src="img/v3_min_h.png" border="0" style="display:none;">
	</span>
            <span style="cursor: pointer;display:show" onmouseover="hltMinClose(this);" onmouseout="hltMinClose(this);"
                  onclick="return hideRoundBlock('plan_block');" title="Скрыть" id="a_cls_plan_block">
			<img vspace="8" width="15" src="img/v3_cls.png" border="0">
			<img vspace="8" width="15" src="img/v3_cls_h.png" border="0" style="display:none;">
		</span>
        </div>
    </div>
    <div class="bBody" id="plan_block" style="display:block;">
        <table cellpadding="0" cellspacing="0" class="restTbl">
            <tr>
                <td class="place" colspan="2">
                    <div class="limited plname" style="padding-left:0px;">
                        <a href="?module=v2_homeBuhPrivatePlanList" title="Перейти в раздел планирования">План на Июль
                            2016</a>
                    </div>
                </td>
            <tr>
            <tr>
                <td class="place">
                    <div class="limited plname" style="padding-left:0px;">
                        Лимит по плану
                    </div>
                </td>
                <td class="amount">
                    <div style="position:relative;z-index:2;">
                        <div class="sum">
                            <div class="s"><span class="green">7&nbsp;900<span style="font-size:9px">.00</span></span>
                            </div>
                            <div class="c tMini tGray" style="">руб</div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr style="display:show">
                <td class="place" colspan="2" style="padding-bottom:5px;">
                    <div class="rc" style="height:17px;position:relative;">
                        <div style="margin:-1px 0px 0px -1px; background: url(img/v3_prGreen.png); padding-top:2px; border-top-left-radius: 4px; border-bottom-left-radius: 4px; width:51%;text-align:center;color:white;height:17px; font-size:12px;position:relative;">
                            <span style="">51%</span>

                        </div>
                        <div style="background: url(img/v3_prRed.png); text-align:center; color: white; height:17px; font-size:12px; padding-top:2px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; width:0%; position: absolute; right: -1px; top:-1px;display: none"
                             title="Перерасход">
                            <span style="position:absolute; left: -30px; color: red;">0%</span>
                        </div>
                    </div>
                </td>
            <tr>

            <tr>
                <td class="place">
                    <div class="limited plname" style="padding-left:0px;">
                        Осталось потратить
                    </div>
                </td>
                <td class="amount">
                    <div style="position:relative;z-index:2;" title="Осталось потратить в среднем, 132.7 руб / день">
                        <div class="sum">
                            <div class="s"><span class="green">3&nbsp;848<span style="font-size:9px">.36</span></span>
                            </div>
                            <div class="c tMini tGray" style="">руб</div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>

                <td class="place">
                    <div class="limited plname" style="padding-left:0px;">
                        Потрачено
                    </div>
                </td>
                <td class="amount">
                    <div style="position:relative;z-index:2;">
                        <div class="sum">
                            <div class="s"><a href="?module=v2_homeBuhPrivateTextReportMain&report_plan_id=2821884"
                                              title="Показать отчёт" class="green">4&nbsp;051<span
                                    style="font-size:9px">.64</span></a></div>
                            <div class="c tMini tGray" style="">руб</div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" height="10"></td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top:10px;border-top:1px solid #F7F7F7;">
                    <div class="limited" style="height:20px; display:none;" id="div_tr_quick_rest">
                        Итоги июля <span class="tMini tGray dash" onclick="return toggleRoundDiv('tr_quick_rest');">
						показать					</span>
                    </div>
                    <table cellpadding="0" cellspacing="0" class="restTbl" style="display:show;" id="tr_quick_rest">
                        <tr>
                            <td class="place">
                                <div class="limited" style="height:20px;">
                                    Итоги июля <span class="tMini tGray dash"
                                                     onclick="return toggleRoundDiv('tr_quick_rest');">скрыть</span>
                                </div>
                            </td>
                            <td class="amount">
                                <div style="position:relative;z-index:2;">
                                    <div class="sum">
                                        <div class="s"><span class="green">45&nbsp;409<span
                                                style="font-size:9px">.37</span></span></div>
                                        <div class="c tMini tGray" style="">руб</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="place tMini tGray">
                                <div style="padding-left:5px;">
                                    Доходы
                                </div>
                            </td>
                            <td class="amount">
                                <div style="position:relative;z-index:2;">
                                    <div class="sum">
                                        <div class="s"><span class="green">49&nbsp;461<span
                                                style="font-size:9px">.00</span></span></div>
                                        <div class="c tMini tGray" style="">руб</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="place tMini tGray">
                                <div style="padding-left:5px;color:gray">Расходы</div>
                            </td>
                            <td class="amount">
                                <div style="position:relative;z-index:2;">
                                    <div class="sum">
                                        <div class="s"><span class="red">-4&nbsp;051<span
                                                style="font-size:9px">.63</span></span></div>
                                        <div class="c tMini tGray" style="">руб</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
