<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title>
    <link type="text/css" href="<c:url value="/resources/css/menu.css"/>" rel="stylesheet"/>
    <link type="text/css" href="<c:url value="/resources/css/calendar-brown.css"/>" rel="stylesheet"/>
    <script type="text/javascript" src="<c:url value="/resources/js/Menu.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/calendar.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/calendar-ru.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/calendar-setup.js"/>"></script>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>

<div class="rc box_shadow blk" id="m_rest_block" style="">
    <div class="bTitle" id="c_rest_block">

        <div id="s_rest_block">Остатки на <span class="dash" id="rest_menu_text"
                                                title="Остатки на другую дату, в другой валюте и т.д."
                                                onclick="toggleDashList('rest_menu');">сегодня</span>
            <img src="img/v3_arr_d.png" hspace="5" border="0" align="absmiddle" onclick="toggleDashList('rest_menu');">
        </div>

        <div class="tMiddle tGray" id="w_rest_block" style="display: none;">
            <img src="img/indicator.gif" align="absmiddle">
            Секунду
        </div>

        <div class="cls">
            <div style="padding-top:8px;display:none" class="dash tGray tMini" onclick="toggleRoundDiv('rest_block');"
                 id="div_open_rest_block">Показать
            </div>

            <span style="cursor: pointer;display:show" onmouseover="hltMinClose(this);" onmouseout="hltMinClose(this);"
                  onclick="return toggleRoundDiv('rest_block');" title="Свернуть" id="a_min_rest_block">
		<img vspace="8" width="15" src="img/v3_min.png" border="0">
		<img vspace="8" width="15" src="img/v3_min_h.png" border="0" style="display:none;">
	</span>
            <span style="cursor: pointer;display:show" onmouseover="hltMinClose(this);" onmouseout="hltMinClose(this);"
                  onclick="return hideRoundBlock('rest_block');" title="Скрыть" id="a_cls_rest_block">
			<img vspace="8" width="15" src="img/v3_cls.png" border="0">
			<img vspace="8" width="15" src="img/v3_cls_h.png" border="0" style="display:none;">
		</span>

        </div>
    </div>


    <div class="bBody" id="rest_block" style="display:block;">
        <div id="rest_menu_div" class="rc box_shadow popup tMiddle" style="display:none">

            <div class="tBig">
                Остатки на <span class="dash" id="rest_menu_sp" title="Остатки на другую дату, в другой валюте и т.д."
                                 onclick="toggleDashList('rest_menu');">сегодня</span><img src="img/v3_arr_d.png"
                                                                                           border="0" hspace="5"
                                                                                           align="absmiddle"
                                                                                           onclick="toggleDashList('rest_menu');">
            </div>

            <div id="calendar_rest" style="padding:10px 0px;width:228px;line-height: normal !important;"></div>
            <input type="text" size="1" id="rest_date" name="rest_date" style="display:none">
            <div class="tBig" style="padding-bottom:0px">Дополнительно</div>
            <table cellspacing="0">
                <tr>
                    <td style="padding:7px 0px;">
                        <select id="sub_currency" style="width:190px;" onchange="recountCurrency();" class="chosen">
                            <option value="0">В исходной валюте
                            <OPTION value="17" selected>Пересчитать в руб
                            <OPTION value="18">Пересчитать в USD
                            <OPTION value="19">Пересчитать в EUR
                            <OPTION value="20">Пересчитать в грн
                        </select>
                        <span class="help_sp" onclick="return help('course');" title="Пояснение"><img src="img/v3_i.png"
                                                                                                      align="absmiddle"
                                                                                                      border="0"></span>
                        <br>1 руб =1 руб
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom:7px;">
                        <input type="checkbox" id="is_with_duty" onclick="recountCurrency();" checked>
                        <label for="is_with_duty"><span class="ch"></span>Если вернуть все долги</label>
                        <span class="help_sp" onclick="return help('restDuty');" title="Пояснение"><img
                                src="img/v3_i.png" align="absmiddle" border="0"></span>
                    </td>
                </tr>

                <tr>
                    <td style="padding-bottom:7px;">
                        <input type="checkbox" id="is_with_accum" onclick="recountCurrency();">
                        <label for="is_with_accum"><span class="ch"></span>Вычесть накопления</label>
                        <span class="help_sp" onclick="return help('restAccum');" title="Пояснение"><img
                                src="img/v3_i.png" align="absmiddle" border="0"></span>
                    </td>
                </tr>

                <tr>
                    <td style="padding-bottom:7px;">
                        <input type="checkbox" id="is_with_null" onclick="recountCurrency();" checked>
                        <label for="is_with_null"><span class="ch"></span>Отображать нулевые остатки</label>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom:7px;">
                        <input type="checkbox" id="is_with_planned" onclick="recountCurrency();" checked>
                        <label for="is_with_planned"><span class="ch"></span>Учесть планируемые траты</label>
                    </td>
                </tr>
            </table>
        </div>

        <form style="left:-1px; z-index:10; min-width: 200px; display:none; padding:10px 15px;"
              class="rc box_shadow popup" id="corr_rest">
            <div class="cls">
                <a href="#" title="Скрыть" onclick="$('corr_rest_sum').value = ''; return correctRest();">
                    <img vspace="4" width="6" src="img/close-mini.gif" border="0">
                </a>
            </div>
            <div class="limited plname" style="margin-left:-5px;" id="corr_rest_pln"></div>
            <input type="text" tabindex="2" id="corr_rest_sum" maxlength="32" vldType="float" vldMax="9223372036854"
                   vldMin="0" value="" autocomplete="off" style="width:150px;margin-bottom:5px;">

            <div style="margin-left:-35px;margin-top:3px;position:absolute;background:white;display: inline-block;">
                <span title="Выбрать другую валюту" onclick="return toggleDashList('corr_rest_currency');"
                      class="dash tMini tGray" id="corr_rest_currency_text">руб</span>

                <div id="corr_rest_currency_div" class="rc box_shadow popup" style="display:none;width:130px;">
                    <div style="padding-bottom:7px;">
                        <span onclick="return toggleDashList('corr_rest_currency');" class="dash tMini tGray"
                              id="corr_rest_currency_sp">руб</span>
                    </div>
                    <a href="#" onclick="return selDashItem('corr_rest_currency', '17');" id="corr_rest_currency_a_17"
                       style="line-height:20px;">руб</a><br>
                    <a href="#" onclick="return selDashItem('corr_rest_currency', '18');" id="corr_rest_currency_a_18"
                       style="line-height:20px;">USD</a><br>
                    <a href="#" onclick="return selDashItem('corr_rest_currency', '19');" id="corr_rest_currency_a_19"
                       style="line-height:20px;">EUR</a><br>
                    <a href="#" onclick="return selDashItem('corr_rest_currency', '20');" id="corr_rest_currency_a_20"
                       style="line-height:20px;">грн</a><br>

                    <div style="padding-top:5px;margin:10px -5px 0px -12px;border-top:1px solid #ccc">
                        +&nbsp;<a href="?module=v2_homeBuhPrivateSettings&tab=currency">Добавить&nbsp;валюту</a>&nbsp;
                    </div>
                </div>

            </div>
            <input type="submit" onclick="return correctRest();"
                   style="position: absolute; top: 37px; left: 180px; padding: 0px 7px 2px 7px;" id="corr_rest_but"
                   value="Ок"><br>
            <span class="tMini tGray">Введите реальный остаток</span>
            <input type="hidden" id="corr_rest_currency" value="17">
        </form>

        <script type="text/javascript">
            dutyRestList = {
                "17": {
                    "40030": 5000000,
                    "40032": 65200,
                    "40034": 14091400,
                    "40040": 4197400,
                    "41439": 928100,
                    "3628000": 40000000
                }
            };
            restList = {"17": 76712384};
        </script>


        <%--========================================--%>
        <table cellpadding="0" cellspacing="0" class="restTbl">
            <tbody>

            <tr id="pl_tr_40030">
                <td class="place">

                    <div class="rc box_shadow popup dopMenu" style="display:none; z-index:10;" id="pl40030">

                        <div style="padding:0px 8px 5px 8px;">
                            <a href="?module=v2_homeBuhPrivateTextReportMain&place_id=40030"
                               title="Все операции по счёту за месяц">Отчёт за месяц</a>
                        </div>

                        <div style="padding:5px 8px;" id="dyn40030">
                            <a href="#" onclick="return toGroup('40030');"
                               title="Добавить счёт в группу счетов, или исключить из группы">В группу</a>
                        </div>

                        <div style="padding:5px 8px;"><a href="#" onclick="return correctRestMenu('40030', '-1');"
                                                         title="Скорректировать баланс, указав реальный текущий остаток">Корректировка</a>
                        </div>

                        <div style="padding:5px 8px;"><a href="#" onclick="return help('creditCard');">Это кредитка</a>
                        </div>

                        <div style="padding:10px 8px 10px 8px;"><span onclick="return addDescription('40030');"
                                                                      class="dash tGray tMini">Описание счёта</span>
                        </div>

                        <div style="position: absolute; top: 8px; right: -3px; height: 25px; width: 3px; background: #fafced;border-left-width:0px; border-right-width:0px;box-shadow: 0px 1px 3px -4px #ddd;"
                             class="box_shadow">

                        </div>
                    </div>


                    <div class="limited plname" id="pln40030">
                        <img width="20" height="20" src="img/pl16.png" style="position: absolute; top:3px; left: 0px;">
                        <span>Сбербанк.</span>
                    </div>

                </td>

                <td class="amount">
                    <div style="position:relative;z-index:2;" id="amount_40030">
                        <div class="sum">
                            <div class="s"><span class="green">50&nbsp;071<span style="font-size:9px">.10</span></span>
                            </div>
                            <div class="c tMini tGray" style="">руб</div>
                        </div>
                    </div>
                </td>

            </tr>


            <tr id="pl_tr_40032">

                <td class="place">

                    <div class="rc box_shadow popup dopMenu" style="display:none;z-index:10;" id="pl40032">

                        <div style="padding:0px 8px 5px 8px;">
                            <a href="?module=v2_homeBuhPrivateTextReportMain&place_id=40032"
                               title="Все операции по счёту за месяц">Отчёт за месяц</a>
                        </div>

                        <div style="padding:5px 8px;" id="dyn40032"><a href="#" onclick="return toGroup('40032');"
                                                                       title="Добавить счёт в группу счетов, или исключить из группы">В
                            группу</a>
                        </div>

                        <div style="padding:5px 8px;"><a href="#" onclick="return correctRestMenu('40032', '-1');"
                                                         title="Скорректировать баланс, указав реальный текущий остаток">Корректировка</a>
                        </div>

                        <div style="padding:5px 8px;"><a href="#" onclick="return help('creditCard');">Это кредитка</a>
                        </div>

                        <div style="padding:10px 8px 10px 8px;"><span onclick="return addDescription('40032');"
                                                                      class="dash tGray tMini">Описание счёта</span>
                        </div>

                        <div style="position: absolute; top: 8px; right: -3px; height: 25px; width: 3px; background: #fafced;border-left-width:0px; border-right-width:0px;box-shadow: 0px 1px 3px -4px #ddd;"
                             class="box_shadow">

                        </div>
                    </div>

                    <div class="limited plname" id="pln40032">

                        <img width="20" height="20" src="img/pl1.png" style="position: absolute; top:3px; left: 0px;">

                        <span>Кошелёк папы</span>
                    </div>

                </td>


                <td class="amount">

                    <div style="position:relative;z-index:2;" id="amount_40032">

                        <div class="sum">
                            <div class="s"><span class="red">-1&nbsp;757<span style="font-size:9px">.63</span></span>
                            </div>
                            <div class="c tMini tGray" style="">руб</div>
                        </div>
                    </div>
                </td>
            </tr>


            <tr id="pl_tr_40034">

                <td class="place">
                    <div class="rc box_shadow popup dopMenu" style="display:none;z-index:10;" id="pl40034">
                        <div style="padding:0px 8px 5px 8px;"><a
                                href="?module=v2_homeBuhPrivateTextReportMain&place_id=40034"
                                title="Все операции по счёту за месяц">Отчёт за месяц</a></div>
                        <div style="padding:5px 8px;" id="dyn40034"><a href="#" onclick="return toGroup('40034');"
                                                                       title="Добавить счёт в группу счетов, или исключить из группы">В
                            группу</a></div>
                        <div style="padding:5px 8px;"><a href="#" onclick="return correctRestMenu('40034', '-1');"
                                                         title="Скорректировать баланс, указав реальный текущий остаток">Корректировка</a>
                        </div>
                        <div style="padding:5px 8px;"><a href="#" onclick="return help('creditCard');">Это кредитка</a>
                        </div>
                        <div style="padding:10px 8px 10px 8px;"><span onclick="return addDescription('40034');"
                                                                      class="dash tGray tMini">Описание счёта</span>
                        </div>
                        <div style="position: absolute; top: 8px; right: -3px; height: 25px; width: 3px; background: #fafced;border-left-width:0px; border-right-width:0px;box-shadow: 0px 1px 3px -4px #ddd;"
                             class="box_shadow"></div>
                    </div>

                    <div class="limited plname" id="pln40034">
                        <img width="20" height="20" src="img/pl6.png" style="position: absolute; top:3px; left: 0px;">
                        <span>Сейф</span>
                    </div>

                </td>
                <td class="amount">
                    <div style="position:relative;z-index:2;" id="amount_40034">
                        <div class="sum">
                            <div class="s"><span class="green">140&nbsp;914<span style="font-size:9px">.00</span></span>
                            </div>
                            <div class="c tMini tGray" style="">руб</div>
                        </div>
                    </div>
                </td>
            </tr>


            <tr id="pl_tr_40040">
                <td class="place">
                    <div class="rc box_shadow popup dopMenu" style="display:none;z-index:10;" id="pl40040">
                        <div style="padding:0px 8px 5px 8px;"><a
                                href="?module=v2_homeBuhPrivateTextReportMain&place_id=40040"
                                title="Все операции по счёту за месяц">Отчёт за месяц</a></div>
                        <div style="padding:5px 8px;" id="dyn40040"><a href="#" onclick="return toGroup('40040');"
                                                                       title="Добавить счёт в группу счетов, или исключить из группы">В
                            группу</a></div>
                        <div style="padding:5px 8px;"><a href="#" onclick="return correctRestMenu('40040', '-1');"
                                                         title="Скорректировать баланс, указав реальный текущий остаток">Корректировка</a>
                        </div>
                        <div style="padding:5px 8px;"><a href="#" onclick="return help('creditCard');">Это кредитка</a>
                        </div>
                        <div style="padding:10px 8px 10px 8px;"><span onclick="return addDescription('40040');"
                                                                      class="dash tGray tMini">Описание счёта</span>
                        </div>
                        <div style="position: absolute; top: 8px; right: -3px; height: 25px; width: 3px; background: #fafced;border-left-width:0px; border-right-width:0px;box-shadow: 0px 1px 3px -4px #ddd;"
                             class="box_shadow"></div>
                    </div>

                    <div class="limited plname" id="pln40040">
                        <img width="20" height="20" src="img/pl17.png" style="position: absolute; top:3px; left: 0px;">
                        <span>Газпромбанк</span>
                    </div>

                </td>
                <td class="amount">
                    <div style="position:relative;z-index:2;" id="amount_40040">
                        <div class="sum">
                            <div class="s"><span class="green">168&nbsp;615<span style="font-size:9px">.37</span></span>
                            </div>
                            <div class="c tMini tGray" style="">руб</div>
                        </div>
                    </div>
                </td>
            </tr>


            <tr id="pl_tr_41439">
                <td class="place">
                    <div class="rc box_shadow popup dopMenu" style="display:none;z-index:10;" id="pl41439">
                        <div style="padding:0px 8px 5px 8px;"><a
                                href="?module=v2_homeBuhPrivateTextReportMain&place_id=41439"
                                title="Все операции по счёту за месяц">Отчёт за месяц</a></div>
                        <div style="padding:5px 8px;" id="dyn41439"><a href="#" onclick="return toGroup('41439');"
                                                                       title="Добавить счёт в группу счетов, или исключить из группы">В
                            группу</a></div>
                        <div style="padding:5px 8px;"><a href="#" onclick="return correctRestMenu('41439', '-1');"
                                                         title="Скорректировать баланс, указав реальный текущий остаток">Корректировка</a>
                        </div>
                        <div style="padding:5px 8px;"><a href="#" onclick="return help('creditCard');">Это кредитка</a>
                        </div>
                        <div style="padding:10px 8px 10px 8px;"><span onclick="return addDescription('41439');"
                                                                      class="dash tGray tMini">Описание счёта</span>
                        </div>
                        <div style="position: absolute; top: 8px; right: -3px; height: 25px; width: 3px; background: #fafced;border-left-width:0px; border-right-width:0px;box-shadow: 0px 1px 3px -4px #ddd;"
                             class="box_shadow"></div>
                    </div>

                    <div class="limited plname" id="pln41439">
                        <img width="20" height="20" src="img/pl20.png" style="position: absolute; top:3px; left: 0px;">
                        <span>З.п. карта</span>
                    </div>

                </td>

                <td class="amount">
                    <div style="position:relative;z-index:2;" id="amount_41439">
                        <div class="sum">
                            <div class="s"><span class="green">9&nbsp;281<span style="font-size:9px">.00</span></span>
                            </div>
                            <div class="c tMini tGray" style="">руб</div>
                        </div>
                    </div>
                </td>


            </tr>


            <tr id="pl_tr_3628000">
                <td class="place">
                    <div class="rc box_shadow popup dopMenu" style="display:none;z-index:10;" id="pl3628000">
                        <div style="padding:0px 8px 5px 8px;"><a
                                href="?module=v2_homeBuhPrivateTextReportMain&place_id=3628000"
                                title="Все операции по счёту за месяц">Отчёт за месяц</a></div>
                        <div style="padding:5px 8px;" id="dyn3628000"><a href="#" onclick="return toGroup('3628000');"
                                                                         title="Добавить счёт в группу счетов, или исключить из группы">В
                            группу</a></div>
                        <div style="padding:5px 8px;"><a href="#" onclick="return correctRestMenu('3628000', '-1');"
                                                         title="Скорректировать баланс, указав реальный текущий остаток">Корректировка</a>
                        </div>
                        <div style="padding:5px 8px;"><a href="#" onclick="return help('creditCard');">Это кредитка</a>
                        </div>
                        <div style="padding:10px 8px 10px 8px;"><span onclick="return addDescription('3628000');"
                                                                      class="dash tGray tMini">Описание счёта</span>
                        </div>
                        <div style="position: absolute; top: 8px; right: -3px; height: 25px; width: 3px; background: #fafced;border-left-width:0px; border-right-width:0px;box-shadow: 0px 1px 3px -4px #ddd;"
                             class="box_shadow"></div>
                    </div>

                    <div class="limited plname" id="pln3628000">
                        <img width="20" height="20" src="img/pl.png" style="position: absolute; top:3px; left: 0px;">
                        <span>На квартиру</span>
                    </div>

                </td>
                <td class="amount">
                    <div style="position:relative;z-index:2;" id="amount_3628000">
                        <div class="sum">
                            <div class="s"><span class="green">400&nbsp;000<span style="font-size:9px">.00</span></span>
                            </div>
                            <div class="c tMini tGray" style="">руб</div>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="2" height="15" valign="top">
                    <div style="height:10px;border-bottom:1px solid #F7F7F7"></div>
                </td>
            </tr>
            <tr id="tr_rdjs_17" style="display:show">
                <td class="place">
                    <div class="plname" style="padding: 3px 0px 0px 28px;">
                        Итого
                    </div>
                </td>

                <td class="amount" style="font-weight:bold;">
                    <div class="sum">
                        <div id="rdjs17" class="s"><span class="green">767&nbsp;123<span
                                style="font-size:9px">.84</span></span></div>
                        <div class="c tMini tGray" style="">руб</div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
