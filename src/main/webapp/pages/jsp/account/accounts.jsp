<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="http://www.springframework.org/tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Accounts</title>


    <sec:csrfMetaTags/>
    <%--<link type="text/css" href="<c:url value="/resources/multiple-select/multiple-select.css"/>" rel="stylesheet"/>--%>
    <link type="text/css" href="<c:url value="/resources/sorttable/style.css"/>" rel="stylesheet"/>
    <link type="text/css" href="<c:url value="/resources/myStyle.css"/>" rel="stylesheet"/>

    <%--JQUERY--%>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
    <%--<script type="text/javascript" src="<c:url value="/resources/multiple-select/multiple-select.js"/>"></script>--%>
    <script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>

    <%--BOOTSTRAP--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>
    <script src="<c:url value="/resources/js/myuploadfunction.js" />"></script>
    <script src="<c:url value="/resources/js/jquery.alphanumeric.pack.js" />"></script>
    <c:url var="findAccountsForUsers" value="/findAccountsForUsers"/>
    <c:url var="findBalanceOfAccounts" value="/accounts/"/>
    <%--<c:url var="deleteAccounts" value="/accounts/delete/"/>--%>

    <script type="text/javascript" src="<c:url value="/resources/accounts/accounts.js"/>"></script>

</head>
<body>

<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>

<div class="container">

    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>

        <div id="main" class="col-sm-6 well text-center">
            <div class="text-success">
                <c:if test="${fn:length(accounts)==0}">
                You haven`t accounts yet.
                <br>
                You can add it simply by click the button below
                <br>
                </c:if>
                <c:if test="${accountDto!=null}">
            <h5>You added new account ${account.name}</h5>
            </c:if>
            </div>


            <c:if test="${fn:length(accounts)!=0}">
                <c:if test="${path=='/administration/all_accounts'}">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <div id="selectDiv" style="width:300px; margin: 0 auto">

                            <form:select style="width:250px" multiple="multiple" data-placeholder="Choose user login"
                                         id="userSelect" path="users">

                                <form:options items="${users}" itemLabel="login"
                                              itemValue="login"/>

                            </form:select>

                        </div>

                    </sec:authorize>
                </c:if>
                Accounts
                <div id="tableDiv">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="tablesorter" style="width: 100%;" id="accountTable" border="1" cellpadding="5"
                                   cellspacing="1"
                                   width="100%" align="center"
                                   summary="accounts">
                                <thead class="text-center">

                                <tr>
                                    <c:if test="${path=='/administration/all_accounts'}">
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <th>Account_id</th>
                                        </sec:authorize>
                                    </c:if>
                                    <th>Name</th>
                                    <th>Start Balance</th>
                                    <th>Current Balance</th>
                                    <th>Balance in ${def_curr}</th>
                                    <c:if test="${path=='/administration/all_accounts'}">
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <th>User_id</th>
                                        </sec:authorize>
                                    </c:if>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="account" items="${accounts}">
                                    <c:choose>
                                        <c:when test="${account.hidden==true}">
                                            <tr id="row${account.id}" onmouseover="showMenu(${account.id})"
                                                onmouseout="hideMenu(${account.id})" style="color:lightgray">

                                                <c:if test="${path=='/administration/all_accounts'}">
                                                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                        <td style="color:lightgray">${account.id}</td>
                                                    </sec:authorize>
                                                </c:if>

                                                <td style="color:lightgray">
                                                    <div id="menuAccount${account.id}" class="rc dopMenu box_shadow popup"
                                                         style="z-index: 10; display: none">
                                                        <div style="padding: 0px 8px 5px 8px">
                                                            <a id="editAccountLink${account.id}"
                                                               href="#" onclick="editAccount(${account.id})">
                                                                <span>Редактировать</span>
                                                                <img border="0" src="/resources/img/edit.png" width="15"
                                                                     height="15">
                                                            </a>
                                                        </div>
                                                        <div style="padding: 5px 8px; ">
                                                            <a id="deleteAccountLink${account.id}" class="deleteAccountLink"
                                                                <%--href="<c:url value='/account/${account.id}/delete.html' />"--%>
                                                               href='#' onclick="deleteAcc(${account.id})"/>
                                                            <span>Удалить</span>
                                                            <img border="0"
                                                                 src="/resources/img/del.gif"
                                                                 width="15"
                                                                 height="15"></a>
                                                        </div>
                                                    </div>
                                                    <img width="20" height="20" src="${account.icon}"/> ${account.name}</td>
                                                <td align="right" style="color:lightgray">${account.startBalance}
                                                    <span style="color:lightgray">${account.currencyType}</span>
                                                </td>
                                                <td align="right" style="color:lightgray">${account.balance}
                                                    <span style="color:lightgray">${account.currencyType}</span>
                                                </td>
                                                <td style="color:lightgray">
                                                        <%--${account.startBalanceConverted}--%>
                                                        ${account.actualBalanceConverted}

                                                </td>
                                                <c:if test="${path=='/administration/all_accounts'}">
                                                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                        <td style="color:lightgray">${account.user.id}</td>
                                                    </sec:authorize>
                                                </c:if>


                                            </tr>
                                        </c:when>
                                        <c:otherwise>
                                            <tr id="row${account.id}" onmouseover="showMenu(${account.id})"
                                                onmouseout="hideMenu(${account.id})" style="font:#0f0f0f;">

                                                <c:if test="${path=='/administration/all_accounts'}">
                                                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                        <td>${account.id}</td>
                                                    </sec:authorize>
                                                </c:if>

                                                <td>
                                                    <div id="menuAccount${account.id}" class="rc dopMenu box_shadow popup"
                                                         style="z-index: 10; display: none">
                                                        <div style="padding: 0px 8px 5px 8px">
                                                            <a id="editAccountLink${account.id}"
                                                               href="#" onclick="editAccount(${account.id})">
                                                                <span>Редактировать</span>
                                                                <img border="0" src="/resources/img/edit.png" width="15"
                                                                     height="15">
                                                            </a>
                                                        </div>
                                                        <div style="padding: 5px 8px; ">
                                                            <a id="deleteAccountLink${account.id}" class="deleteAccountLink"
                                                                <%--href="<c:url value='/account/${account.id}/delete.html' />"--%>
                                                               href='#' onclick="deleteAcc(${account.id})"/>
                                                            <span>Удалить</span>
                                                            <img border="0"
                                                                 src="/resources/img/del.gif"
                                                                 width="15"
                                                                 height="15"></a>
                                                        </div>
                                                    </div>
                                                    <img width="20" height="20" src="${account.icon}"/> ${account.name}</td>
                                                <td align="right">${account.startBalance}
                                                    <span style="color:green">${account.currencyType}</span>
                                                </td>
                                                <td align="right">${account.balance}
                                                    <span style="color:green">${account.currencyType}</span>
                                                </td>
                                                <td>
                                                        <%--${account.startBalanceConverted}--%>
                                                        ${account.actualBalanceConverted}

                                                </td>
                                                <c:if test="${path=='/administration/all_accounts'}">
                                                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                        <td>${account.user.id}</td>
                                                    </sec:authorize>
                                                </c:if>
                                            </tr>
                                        </c:otherwise>
                                    </c:choose>

                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </c:if>

            <div class="text-center">
                <br>
                <img src="/resources/img/finance/PNG/png32/pie_chart.png" alt="" width="32" height="32">

                <a href="/accountsStat.html">Statistics of accounts</a><br>
                <br>
            </div>


        </div>

        <div id="editAccountBlock" class="col-sm-4 well text-center">
            <span id="createNewAccountTitle" class="text-center">Create new account</span>

            <div class="row" id="nameRow" style="margin-bottom: 3px;" data-toggle="max lenght 64 symbols">
                <div class="col-sm-3 text-right">
                    <span>Name</span>
                </div>
                <div class="col-sm-9">
                    <input class="form-control " id="name" type="text" value="">
                </div>
            </div>
            <div class="row" id="nameRowError" style="margin-bottom: 3px;">
            </div>
            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Start balance</span>
                </div>
                <div class="col-sm-9">
                    <input class="form-control" id="startBalance" type="text" step="0.01" pattern='^[-+]?\d+(\.\d+)?'
                           title="Format example: 1234.56 or -1234.56 " value="">
                </div>
            </div>
            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Currency</span>
                </div>
                <div class="col-sm-9">

                    <select id="currencyOption" style="width: 100%" onchange="change()">
                        <%--<c:forEach items="${currencies}" var="curr">--%>
                            <%--<option value="${curr}">${curr}</option>--%>
                        <%--</c:forEach>--%>

                        <%--<c:choose>--%>
                        <c:choose>
                            <c:when test="${fn:length(currencies)==0}">
                                <option value="${currencies}">${currencies}</option>
                            </c:when>
                            <c:otherwise>
                                <c:forEach items="${currencies}" var="curr">
                                    <c:choose>
                                        <c:when test="${curr==def_curr}">
                                            <option value="${curr}" selected="true">${curr}
                                            </option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${curr}">${curr}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>

                            </c:otherwise>

                        </c:choose>

                    </select>

                    <span style="font-size: 12px"><a href="/settings">Change default currencies</a></span>
                </div>
            </div>
            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Description</span>
                </div>
                <div class="col-sm-9 ">
                    <input class="form-control" id="description" type="text" value="">
                </div>
            </div>
            <div class="row" id="descriptionRowError" style="margin-bottom: 3px;">
            </div>
            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Is credit</span>
                </div>
                <div class="col-sm-9 text-left">
                    <input type="checkbox" path="isCredit" id="isCredit"/>
                </div>
            </div>
            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Is hidden</span>
                </div>
                <div class="col-sm-9 text-left">
                    <input type="checkbox" path="isHidden" id="isHidden"/>
                </div>
            </div>
            <div class="row" style="margin-bottom: 9px;">
                <div class="col-sm-3 text-right">
                    <span>Is cash</span>
                </div>
                <div class="col-sm-9 text-left">
                    <input type="checkbox" path="isCash" id="isCash"/>
                </div>
            </div>
            <div class="row">
                <input type="hidden" id="username" path="username" value="${pageContext.request.userPrincipal.name}"/>
            </div>

            <div class="row">
                <a class="btn" data-toggle="collapse" data-target="#showIconPanel">Choose icon &raquo;</a>
            </div>

            <div class="collapse" id="showIconPanel">
                <div class="row" style="margin-bottom: 6px;">
                    <div class="col-sm-12">
                        <div class="icons">
                            <div class="standard_icons">
                                <c:forEach items="${defaultIcons}" var="icon" varStatus="loop">
                        <c:choose>
                                    <c:when test="${loop.index==0}">
                                    <span style="float:left;padding-right:25px;">
                             <input type="radio" name="icon" id="${icon}" value="${icon}" checked="checked">
                             <label for="${icon}"><span class="ch"></span><img src="${icon}" align="absmiddle"
                                                                               width="24"
                                                                               height="24"></label>
                         </span>
                                    </c:when>
                                    <c:otherwise>
                                    <span style="float:left;padding-right:25px;">
                             <input type="radio" name="icon" id="${icon}" value="${icon}">
                             <label for="${icon}"><span class="ch"></span><img src="${icon}" align="absmiddle"
                                                                               width="24"
                                                                               height="24"></label>
                         </span>
                                    </c:otherwise>
                                </c:choose>
                                </c:forEach>
                            </div>

                            <br>
                            <span style="padding-right:20px">
    <input type="radio" name="icon" id="withoutIcon" value="icon"><label for="withoutIcon">Without icon</label>
    </span>


                            <div style="text-align: center">Own icons <br>

                                <div id="result" class="res">
                                    <c:forEach items="${userIcons}" var="userIcon">

                                        <div id="${userIcon}">
                                    <span style="float:left;padding-right:25px;">
                             <input type="radio" name="icon" value="${userIcon}">
                             <label for="${userIcon}"><span class="ch"></span><img src="${userIcon}" align="absmiddle"
                                                                                   width="24"
                                                                                   height="24"></label>
                         <span style="color: black; border-bottom:1px dashed gray; cursor:pointer; " class="tMini "
                               onclick="myiconDelete('${userIcon}');">Удалить</span>
                         </span>
                                        </div>
                                    </c:forEach>


                                </div>
                            </div>

                            <br>


                            <div style=" clear:left;overflow: auto">
                                <div style="text-align: center">Load new icon</div>

                                <input style="padding:0px 0px 10px 50px;/*background-color: gainsboro;*/"
                                       id="fileupload"
                                       type="file" name="files[]"
                                       onchange="myiconLoad2(this.files)"
                                       enctype="multipart/form-data" accept="image/jpeg,image/png,image/gif">
                                <input type="hidden"
                                       name="${_csrf.parameterName}"
                                       value="${_csrf.token}"/>
                            </div>


                        </div>
                    </div>


                </div>
            </div>

            <div class="row text-right">
                <input id="cancelNewAccountForm" type="button" onclick="cancel()" value=
                <spring:message code="user.body.cancelButtonLabel"/>>

                <input id="addNewAccountButton" disabled type="button" onclick="addAccount()" value=
                <spring:message code="user.body.buttonlabel"/>>

            </div>

            <div class="row text-right has-error text-danger">
                <span class="text-danger" id="accountAddingError"></span>

            </div>

        </div>

    </div>
</div>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>

</body>
</html>