<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>List of Accounts</title>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-1.8.3.js"/>"></script>
    <link type="text/css" href="<c:url value="/resources/multiple-select/multiple-select.css"/>" rel="stylesheet"/>
    <script type="text/javascript" src="<c:url value="/resources/multiple-select/multiple-select.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>
    <link type="text/css" href="<c:url value="/resources/sorttable/style.css"/>" rel="stylesheet"/>

    <%--Add functionality of charts--%>
    <script type="text/javascript" src="<c:url value="/resources/js/amcharts/amcharts.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/amcharts/pie.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/amcharts/themes/light.js"/>"></script>

    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>


    <c:url var="findBalanceOfAccounts" value="/balanceOfAccounts"/>
    <c:url var="findBalanceOfAccountsForSelectedUsers" value="/balanceOfAccountsForSelectedUsers"/>
    <c:url var="findAccountsForUsers" value="/findAccountsForUsers"/>


    <script type="application/javascript">
        var chart;
        var chartData = [];


        chart = AmCharts.makeChart("chartdiv",
            {
                "type": "pie",
                "theme": "light",
                "dataProvider": [],
                "valueField": "balance",
                "titleField": "name",
                "balloon": {
                    "fixedPosition": true
                },

                "export": {
                    "enabled": true
                }
            });

        <%--$(document).ready(function () {--%>
        <%--$('.select').change(function () {--%>
        <%--var accountsSelected = '';--%>
        <%--var userSelected='';--%>
        <%--$('#accountSelect option:selected').each(function () {--%>

        <%--accountsSelected+= $(this).text() + ',';--%>
        <%--});--%>
        <%--$('#userSelect option:selected').each(function () {--%>

        <%--userSelected+= $(this).text() + ',';--%>
        <%--});--%>



        <%--$.getJSON('${findBalanceOfAccountsForSelectedUsers}', {--%>
        <%--accounts: accountsSelected,--%>
        <%--users: userSelected,--%>
        <%--ajax: 'true'--%>
        <%--}, function (data) {--%>

        <%--if (accountsSelected != ''&&userSelected!='') {--%>

        <%--var len = data.length;--%>
        <%--if (len == 0) {--%>
        <%--$('#tableDiv').html('chosen users havent had accounts yet');--%>
        <%--} else {--%>
        <%--chartData = [];--%>

        <%--for (var i = 0; i < len; i++) {--%>

        <%--chartData.push({--%>
        <%--name: data[i].name,--%>
        <%--balance: data[i].balance--%>
        <%--});--%>
        <%--}--%>

        <%--chart.dataProvider=(chartData);--%>
        <%--chart.validateData();--%>
        <%--}--%>
        <%--} else {--%>
        <%--chartData = [];--%>

        <%--chart.dataProvider=(chartData);--%>
        <%--chart.validateData();--%>

        <%--}--%>

        <%--});--%>
        <%--}--%>
        <%--);--%>
        <%--});--%>
        $(document).ready(function () {
            $('.select').change(function () {
                    var accountsSelected = '';
//                        var userSelected='';
                    $('#accountSelect option:selected').each(function () {

                        accountsSelected += $(this).text() + ',';
                    });
//                        $('#userSelect option:selected').each(function () {
//
//                            userSelected+= $(this).text() + ',';
//                        });

                    $.getJSON('${findBalanceOfAccounts}', {
                        accounts: accountsSelected,
//                            users: userSelected,
                        ajax: 'true'
                    }, function (data) {

                        if (accountsSelected != '') {

                            var len = data.length;
                            if (len == 0) {
                                $('#tableDiv').html('chosen users havent had accounts yet');
                            } else {
                                chartData = [];

                                for (var i = 0; i < len; i++) {

                                    chartData.push({
                                        name: data[i].name,
                                        balance: data[i].balance
                                    });
                                }

                                chart.dataProvider = (chartData);
                                chart.validateData();
                            }
                        } else {
                            chartData = [];

                            chart.dataProvider = (chartData);
                            chart.validateData();

                        }

                    });
                }
            );
        });


    </script>


    <style type="text/css">
        #chartdiv {
            width: 100%;
            height: 300px;
            font-size: 13px;
        }

    </style>


    <script type="text/javascript">
        $(document).ready(function () {
                $("#accountTable").tablesorter();
            }
        );
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('select').multipleSelect({
                isOpen: false,
                keepOpen: false,
                filter: true,
                multipleWidth: 250
            });
        });
    </script>
</head>

<body>
<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>


<div class="container">
    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>
        <div id="main" class="col-sm-6 well text-center">
            <c:if test="${fn:length(accounts)==0}">
                <br><br><br>
                You haven`t had accounts yet.
                <br>
                You can add it simply by click the button below
                <br>
            </c:if>
            <c:if test="${accountDto!=null}">
                <h5>You added new account ${account.name}</h5>
            </c:if>

            <c:if test="${fn:length(accounts)!=0}">
                <div style="text-align:center">
                    Accounts <span style="font-size: 10px">(* without accounts that have negative amount)</span>
                </div>

                <div id="chartdiv"></div>


            </c:if>

        </div>
        <div id="rightBlock" class="col-sm-4 well text-left">
            <div class="row">

                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <form:select class="select" style="width:250px" multiple="multiple" data-placeholder="Choose users"
                                 id="userSelect" path="users">

                        <form:options items="${users}" itemLabel="login"
                                      itemValue="login"/>

                    </form:select>
                </sec:authorize>
            </div>
            <div id="selectDiv" class="row">

                <form:select class="select" style="width:250px" multiple="multiple" data-placeholder="Choose account"
                             id="accountSelect" path="accounts">

                    <form:options items="${accounts}" itemLabel="name"
                                  itemValue="name"/>

                </form:select>

            </div>


        </div>
    </div>
</div>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>


</body>
</html>