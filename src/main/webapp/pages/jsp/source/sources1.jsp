<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>

    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/tree/jstree.min.js"/>"></script>
    <link type="text/css" href="<c:url value="/resources/tree/themes/default/style.min.css"/>" rel="stylesheet"/>

    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Categories</title>


    <script type="text/javascript">

        function demo_create() {
            var ref = $('#jstree_demo').jstree(true),
                sel = ref.get_selected();
            if (!sel.length) {
                return false;
            }
            sel = sel[0];
            sel = ref.create_node(sel, {"type": "file"});
            if (sel) {
                ref.edit(sel);
            }
        }
        ;
        function demo_rename() {
            var ref = $('#jstree_demo').jstree(true),
                sel = ref.get_selected();
            if (!sel.length) {
                return false;
            }
            sel = sel[0];
            ref.edit(sel);
        }
        ;
        function demo_delete() {
            var ref = $('#jstree_demo').jstree(true),
                sel = ref.get_selected();
            if (!sel.length) {
                return false;
            }
            ref.delete_node(sel);
        }
        ;
        $(function () {
            var to = false;
            $('#demo_q').keyup(function () {
                if (to) {
                    clearTimeout(to);
                }
                to = setTimeout(function () {
                    var v = $('#demo_q').val();
                    $('#jstree_demo').jstree(true).search(v);
                }, 250);
            });

            $('#jstree_demo')
                .jstree({
                    "core": {
                        "animation": 0,
                        "check_callback": true,
                        'force_text': true,
                        "themes": {"stripes": true},
                        'data': {
                            'url': function (node) {
                                return node.id === '#' ? '/static/3.3.1/assets/ajax_demo_roots.json' : '/static/3.3.1/assets/ajax_demo_children.json';
                            },
                            'data': function (node) {
                                return {'id': node.id};
                            }
                        }
                    },
                    "types": {
                        "#": {"max_children": 1, "max_depth": 4, "valid_children": ["root"]},
                        "root": {"icon": "/static/3.3.1/assets/images/tree_icon.png", "valid_children": ["default"]},
                        "default": {"valid_children": ["default", "file"]},
                        "file": {"icon": "glyphicon glyphicon-file", "valid_children": []}
                    },
                    "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow"]
                });
        });

    </script>
</head>
<body>


<div id="tree">
</div>


<a href="/add_category.html">Add new category</a><br>
<a href="/add_subcategory.html">Add new subcategory</a><br>
<a href="/">Start page</a><br>

</body>
</html>