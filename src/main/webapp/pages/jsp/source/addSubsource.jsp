<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<c:if test="${sessionScope.user!=null}">
    Hello, ${sessionScope.user.login}<br>
    <a href="/logout.html">Logout</a><br>
</c:if>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WalletEE</title>

<script type="text/javascript" src="../../../resources/jquery-ui/jquery-1.8.3.js"></script>
<link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
      rel="stylesheet"/>
<link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
      rel="stylesheet"/>

<script type="text/javascript">
    $(document).ready(
        function () {
            $.getJSON('categories.json', {
                ajax: 'true'
            }, function (data) {
                var html = '<option value="">--Please select one--</option>';
                var len = data.length;
                for (var i = 0; i < len; i++) {
                    html += '<option value="' + data[i].sourceName + '">'
                        + data[i].sourceName + '</option>';
                }
                html += '</option>';

                $('#categories').html(html);
            });

        });

</script>


</head>
<body>
<h1> add subsource </h1>
Language: <a href="?lang=en"> English </a>|<a href="?lang=es">Espanol</a>
<form:form commandName="subsource">
    <form:errors path="*" cssClass="errorblock" element="div"/>
    <table>

        <tr>
            <td> Source Name</td>
            <td>
                <form:select id="categories" path="source"/>
            </td>
        </tr>

        <tr>
            <td> Subsource name</td>
            <td>
                <form:input path="subsource"/>
            </td>
        </tr>

        <tr>
            <td
                    colspan="3">
                <input
                        type="submit"
                        value="Add new subsource">
            </td>
        </tr>
    </table>
</form:form>
<a href="/">Start page</a><br>

</body>
</html>