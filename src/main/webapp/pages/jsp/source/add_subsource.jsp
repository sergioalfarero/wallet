<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Add subsource</title>

    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>" rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />" rel="stylesheet"/>
</head>
<body>

<h4>Enter new subsource</h4>
<form:form modelAttribute="subsourceDto" method="post" enctype="utf-8">
    <table>
        <tr>
            <td>Source</td>
            <td>
                <form:select path="sourceName" multiple="false">
                    <option value="select" label="-Select-">
                        <form:options itemLabel="name" itemValue="name" items="${sources}"/>
                    </option>
                </form:select>
            </td>
        </tr>

        <tr>
            <td>Name</td>
            <td><form:input path="name"/></td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="submit" value="Add"/>
            </td>
        </tr>
    </table>
</form:form>

<a href="/">Start page</a><br>

</body>
</html>