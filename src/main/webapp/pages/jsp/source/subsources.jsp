<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>WalletEE</title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>
<c:if test="${sessionScope.user!=null}">
    Hello, ${sessionScope.user.login}<br>
    <a href="/logout.html">Logout</a><br>
</c:if>
<h1>YES</h1>
<a href="/addSubsource.html">Add new subcategory</a><br>
<a href="/">Start page</a><br>

</body>
</html>