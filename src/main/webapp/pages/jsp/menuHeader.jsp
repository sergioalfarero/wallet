<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <title></title>
    <link type="text/css" href="<c:url value="/resources/css/menu.css"/>" rel="stylesheet"/>
    <script type="text/javascript" src="<c:url value="/resources/js/Menu.js"/>"></script>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>


<body>
<div class="rc topMinMenu" title="Показать верхнее Меню" onclick="toggleTopMenu();" style="display:show"
     id="topMenuMin">Меню
</div>
<div class="tMini tGray topMinExit" style="margin-top:2px; display:show" id="topMinExit">
</div>
<div style="cursor: pointer; position: absolute; left: 5px; top: 0px; width: 10px; height: 17px; display:none;"
     title="Скрывать автоматически" onmouseover="hltMinClose(this);" onmouseout="hltMinClose(this);"
     onclick="topMenuLock();" id="topMenuLck">
    <img vspace="8" width="15" src="img/v3_unlock.png" border="0" style="" id="v3_lock">
    <img vspace="8" width="15" src="img/v3_unlock_h.png" border="0" style="display: none;" id="v3_lock_h">

    <div style="display:none;">
        <img vspace="8" width="15" src="img/v3_lock.png" border="0">
        <img vspace="8" width="15" src="img/v3_lock_h.png" border="0">
    </div>
</div>

<table cellpadding="0" width="100%" cellspacing="0" class="topBg" id="topMenu" style="display:none">
    <tr>
        <td style="padding-left:57px">&nbsp;</td>
        <td style="padding-right:90px">
            <a href="?module=v2_start" title="Главная">
                <img src="img/v3_logo.png" border="0" width="76" height="37" align="absmiddle">
            </a>
        </td>
        <td nowrap style="padding-left:10px;">
            <div id="1_tmd" onmouseover="grayElem(this.id, false, '#ddd');" onmouseout="grayElem(this.id, true);"
                 class="tbgItem tMiddle">
                <a href="?module=v2_news" title="Что нового">Новости</a>
            </div>
        </td>
        <td nowrap style="padding-left:10px;">
            <div id="2_tmd" onmouseover="grayElem(this.id, false, '#ddd');" onmouseout="grayElem(this.id, true);"
                 class="tbgItem tMiddle">
                <a href="?module=forum" title="Коллективный разум">Сообщество</a>
            </div>
        </td>
        <td nowrap style="padding-left:10px;">
            <div id="3_tmd" onmouseover="grayElem(this.id, false, '#ddd');" onmouseout="grayElem(this.id, true);"
                 class="tbgItem tMiddle">
                <a href="?module=v2_faq" title="Часто задаваемые вопросы">FAQ</a>
            </div>
        </td>
        <td nowrap style="padding-left:10px;">
            <div id="4_tmd" onmouseover="grayElem(this.id, false, '#ddd');" onmouseout="grayElem(this.id, true);"
                 class="tbgItem tMiddle">
                <a href="?module=v2_aboutPrice" title="Отличия платной версии от бесплатной">Цены</a>
            </div>
        </td>
        <td nowrap style="padding-left:10px;">
            <div id="5_tmd" class="tbgItem tbgaItem tMiddle rc"
                 style="background: #fafced url(img/v3_lkicon.png) 10px 50% no-repeat; padding-left: 33px;">
                Личный кабинет
            </div>
        </td>
        <td nowrap align="right" width="90%" style="padding-right:38px" class="tMiddle">


            <sec:authorize access="!isAuthenticated()">
                <p><a class="btn btn-lg btn-success" href="<c:url value="/login" />"
                      role="button">Войти</a>
                </p>
            </sec:authorize>

            <sec:authorize access="isAuthenticated()">
                Hello, <sec:authentication property="principal.username"/> <br>
                <a href="<c:url value="/userInfo" />" role="button">Information</a><br>
                <a href="<c:url value="/logout" />" role="button">Выйти</a>
            </sec:authorize>

        </td>
    </tr>
</table>

</body>
</html>
