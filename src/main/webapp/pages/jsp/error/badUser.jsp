<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setBundle basename="messages"/>
<%@ page session="true" %>
<html>
<head>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

    <title>Invalid Link</title>
</head>
<body>
<div class="container">
    <h1 class="alert alert-danger">
        ${param.message}
    </h1>
    <br>
    <a class="btn btn-default" href="<c:url value="/user/registration" />">Sign up</a>

    <c:if test="${param.expired}">
        <br>

        <h1>${label.form.resendRegistrationToken}</h1>
        <button onclick="resendToken()">Resend Token</button>

        <script src="/resources/jquery-ui/jquery-1.8.3.js"></script>
        <script type="text/javascript">
            function resendToken() {
                $.get("<c:url value="/resendRegistrationToken"><c:param name="token" value="${param.token}"/></c:url>",
                    function (data) {
                        window.location.href = "<c:url value="/login.html"></c:url>" + "?message=" + data.message;
                    })
                    .fail(function (data) {

                        if (data.responseJSON.error.indexOf("MailError") > -1) {
                            alert(data.toString);

                            window.location.href = "<c:url value="loginError.jsp"></c:url>";
                        }
                        else {
                            window.location.href = "<c:url value="/login.html"></c:url>" + "?message=" + data.responseJSON.message;
                        }
                    });
            }

            $(document).ajaxStart(function () {
                $("title").html("LOADING ...");
            });
        </script>
    </c:if>
</div>
</body>
</html>
