<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
</head>
<body>

<%--<p><a href="/accounts">Accounts</a></p>--%>

<p><a href="/accounts">Accounts</a></p>

<p><a href="/categories">Categories</a></p>

<p><a href="/sources">Sources</a></p>

<p><a href="/operations">Operations</a></p>

<p><a href="/transactions">Transactions</a></p>

<p><a href="/currenciesRates">Currencies Rates</a></p>

<p><a href="/currencies/user">User currencies Rates</a></p>

<p><a href="/shoppingList">Shopping List</a></p>

<p><a href="/settings">Settings</a></p>


<sec:authorize access="hasRole('ROLE_ADMIN')">
    <a class="btn" data-toggle="collapse" data-target="#demo">Administration &raquo;</a>

    <div class="collapse" id="demo">
        <p><a href="/administration/all_accounts">All accounts</a></p>

        <p><a href="/administration/users">All users</a></p>

    </div>


</sec:authorize>


</body>
</html>
