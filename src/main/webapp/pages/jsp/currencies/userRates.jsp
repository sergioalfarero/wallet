<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>User currencies rates</title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>

<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>

<div class="container">
    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>
        <div id="main" class="col-sm-6 well text-center">
            Course of 1.00 ${user1.settings.defaultCurrency} set by user ${user1.login}
            <table>
                <c:choose>
                    <c:when test="${fn:length(userRates.rates)!=0}">
                        <c:forEach items="${userRates.rates}" var="rate" varStatus="status">

                            <tr>
                                <td align="center">${status.count}</td>
                                <td>${rate.rate}
                                </td>
                                <td>${rate.currencyType}</td>


                                    <%--<td>=</td>--%>
                                    <%--<td>${rate.rate1}--%>
                                    <%--</td>--%>
                                    <%--<td>${rate.currencyType1}</td>--%>


                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        Add you currency rates
                    </c:otherwise>
                </c:choose>
            </table>

            <a href="/currencies/user/change.html">Change rates</a> <br>
            <%--<a href="/currencies/user/add.html">Add rates</a> <br>--%>

        </div>
        <div id="rightBlock" class="col-sm-4 well text-center"></div>
    </div>
</div>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>

</body>
</html>