<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>User currencies rates</title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>
<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>
<div class="container">
    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>
        <div id="main" class="col-sm-6 well text-center">
            <h3>Change your currencies rates</h3>
            Language: <a href="?lang=en">English</a>|<a href="?lang=es">Espanol</a>

            <form:form modelAttribute="userRates" method="post" enctype="utf-8">
            <table>
                <c:if test="${fn:length(userRates.rates)!=0}">
                    <c:forEach items="${userRates.rates}" var="rate" varStatus="status">
                        <tr>
                            <td align="center">${status.count}</td>
                            <td><input name="rates[${status.index}].rate" value="${rate.rate}"
                                       pattern='^[-+]?\d+(\.\d+)?'
                                       title="Format example: 1234.56 or -1234.56 "/>
                            </td>
                            <td><input name="rates[${status.index}].currencyType" value="${rate.currencyType}"/></td>

                        </tr>
                    </c:forEach>

                </c:if>

            </table>
            <input type="submit" value=
                <spring:message code="user.body.buttonChangeLabel"/>
            </form:form>

            <div id="rightBlock" class="col-sm-4 well text-center"></div>
        </div>
    </div>
</div>
<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>

</body>
</html>