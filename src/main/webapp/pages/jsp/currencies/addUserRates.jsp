<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

    <title>User currencies rates</title>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

</head>
<body>

<h1>Change your currencies rates</h1>
Language: <a href="?lang=en">English</a>|<a href="?lang=es">Espanol</a>


<form:form modelAttribute="userRate" method="post" enctype="utf-8">
<table>
    <tr>
        <td align="center">${status.count}</td>
        <td><input name="rate1" value="${rate.rate1}" pattern='^[-+]?\d+(\.\d+)?'
                   title="Format example: 1234.56 or -1234.56 "/>
        </td>
        <td><input name="currencyType1" value="${rate.currencyType1}"/></td>
        <td>=</td>
        <td><input name="rate2" value="${rate.rate2}" pattern='^[-+]?\d+(\.\d+)?'
                   title="Format example: 1234.56 or -1234.56 "/>
        </td>
        <td><input name="currencyType2" value="${rate.currencyType2}"/></td>
    </tr>

</table>
<input type="submit" value=
    <spring:message code="user.body.buttonChangeLabel"/>
</form:form>


        <a href="${prevPage}">Cancel</a><br>
<a href="/">Start page</a><br>

</body>
</html>