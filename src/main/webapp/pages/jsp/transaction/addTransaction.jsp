<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Create new transaction</title>
    <script src="/resources/jquery-ui/jquery-1.8.3.js" type="text/javascript"></script>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>

    <c:url var="findAccountsUrl" value="/findAccounts"/>


    <script language="javascript" type="text/javascript">
        $(document).ready(
            function () {
                $("select").change(function (e) {
                    $("select option").removeAttr('disabled');
                    $("select").each(function (i, s) {
                        $("select").not(s).find("option[value=" + $(s).val() + "]").attr('disabled', 'disabled');
                    });
                });
            });
    </script>


</head>
<body>

<form:form modelAttribute="transaction" method="post" enctype="utf-8">
    <form:errors path="*" cssClass="errorblock" element="div"/>
    <table cellpadding="2" cellspacing="2" width="100%">
        <fmt:formatDate var="fmtDate" value="<%=new java.util.Date()%>" pattern="dd.MM.yyyy HH:mm:ss"/>
        <tr>
            <td>Date</td>
            <td>
                <form:input path="date" value="${fmtDate}" type="text"/>
            </td>
            <td>
                <form:errors path="date" element="div"/>
            </td>

        </tr>

        <tr>
            <td>Outcoming Account</td>
            <td>
                <form:select id="outcomingAccount" path="outcomingAccount">
                    <option value="select" label="Outcoming Account">
                        <form:options itemValue="name" itemLabel="name" items="${accounts}"/>
                    </option>
                </form:select>
            </td>
        </tr>
        <tr>
            <td>IncomingAccount</td>
            <td>
                <form:select id="incomingAccount" path="incomingAccount">
                    <option value="select" label="Incoming Account">
                        <form:options itemValue="name" itemLabel="name" items="${accounts}"/>
                    </option>
                </form:select>
            </td>
        </tr>

        <tr>
            <td>Amount</td>
            <td>
                <form:input path="sum" type="text"/>

            </td>
        </tr>
        <tr>
            <td colspan="1">
                <input type="submit" value="Add"/>
            </td>
        </tr>
    </table>
</form:form>

<div id="output">

</div>
<a href="/">Start page</a><br>

</body>
</html>