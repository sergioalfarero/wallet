<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Transactions</title>

<link type="text/css" href="<c:url value="/resources/sorttable/style.css"/>" rel="stylesheet"/>
<link type="text/css" href="<c:url value="/resources/myStyle.css"/>" rel="stylesheet"/>

<%--JQUERY--%>
<script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
<%--<script type="text/javascript" src="<c:url value="/resources/multiple-select/multiple-select.js"/>"></script>--%>
<script type="text/javascript" src="<c:url value="/resources/sorttable/jquery.tablesorter.js"/>"></script>
<%--<script type="text/javascript" src="<c:url value="/resources/js/getCategories.js"/>"></script>--%>

<%--BOOTSTRAP--%>
<link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
      rel="stylesheet"/>
<link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
      rel="stylesheet"/>

<sec:csrfMetaTags/>


<c:url var="findAccountsUrl" value="/findAccounts"/>

<c:url var="deleteTransaction" value="/transactions/delete/"/>


<script language="javascript" type="text/javascript">
    jQuery(document).ready(
        function () {
            jQuery("#transactionTable").tablesorter();
            jQuery("#addNewTransactionButton").attr("disabled", true);
            jQuery("#date").on("input", function () {

                checkFieldValues();
            });

            jQuery("#outcomingAccount").on("change", function () {
                checkFieldValues();
            });

            jQuery("#incomingAccount").on("change", function () {
                checkFieldValues();
            });

            jQuery("#amount").on("input", function () {
                checkFieldValues();
            });


            function checkFieldValues() {

                var date = jQuery("#date").val().trim();
                var outcomingAcc = jQuery("#outcomingAccount :selected").text().trim();
                var incomingAcc = jQuery("#incomingAccount :selected").text().trim();
                var amount = jQuery("#amount").val().trim();

                var outAccBoolean = outcomingAcc != "--Select--";
                var inAccBoolean = incomingAcc != "--Select--";
                var dateBoolean = date != "";
                var amountBoolean = amount != "";

                var b = outAccBoolean && dateBoolean && inAccBoolean && amountBoolean;
                if (b) {
                    jQuery("#addNewTransactionButton").attr("disabled", false);
                } else {

                    jQuery("#addNewTransactionButton").attr("disabled", true);
                }
            };
        });




</script>
<script language="javascript" type="text/javascript">

    function changeOutcome() {
        var x = jQuery("#incomingAccount");
        x.find("option").removeAttr("disabled");
//                jQuery("select option").removeAttr('disabled');
        var chosenOutcomingAccount = jQuery("#outcomingAccount").val();
        if (chosenOutcomingAccount == "") {
            $("#incomingAccountRow").hide();
        } else {
            var inAcc = jQuery("#incomingAccount");
            inAcc.find("option[value=" + chosenOutcomingAccount + "]").attr("disabled", "disabled");
            jQuery("#incomingAccountRow").show();

        }
    }
    ;



    function cancelCreateTransaction() {
        jQuery("#outcomingAccount").find("option").removeAttr("disabled");
        jQuery("#outcomingAccount").find("option[value='']").attr("selected", true);
       jQuery("#incomingAccountRow").hide();
        jQuery("#amount").val("");

    }
    ;

    function addTransaction() {
        var csrfParameter = jQuery("meta[name='_csrf_parameter']").attr("content");
        var csrfToken = jQuery("meta[name='_csrf']").attr("content");
        var csrfHeader = jQuery("meta[name='_csrf_header']").attr("content");

        var date = document.getElementById('date').value;

        var outAccountElement = document.getElementById("outcomingAccount");
        var outAccount = outAccountElement.options[outAccountElement.selectedIndex].value;

        var inAccountElement = document.getElementById("incomingAccount");
        var inAccount = inAccountElement.options[inAccountElement.selectedIndex].value;

        var amount = document.getElementById("amount").value;

        var transaction = {
            date: date,
            incomingAccount: inAccount,
            outcomingAccount: outAccount,
            sum: amount
        }
        var headers = {};
        var data = {};
        data[csrfParameter] = csrfToken;
        headers[csrfHeader] = csrfToken;
        jQuery(document).ajaxSend(function (e, xhr, options) {
            xhr.setRequestHeader(csrfHeader, csrfToken);
            xhr.setRequestHeader('Access-Control-Allow-Origin', "*");

        });

        jQuery.ajax({
            url: 'transaction/save',
            type: "post",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(transaction),

            success: function (data) {
                if (data.error == "200") {
                    var result = JSON.parse(data.message);
                    alert(result.id);
                    var divTitle = jQuery("#absentTransactions");
                    var divTitleStyle = jQuery("#absentTransactions").css("display");
                    if (divTitleStyle != "none") {
                        var x = '<div id="tableDiv">' +
                            '<div class="row">' +
                            '<div class="col-sm-12">' +
                            '<table class="tablesorter" style="width: 100%;" id="transactionTable" border="1" cellpadding="5" cellspacing="1" width="100%" align="center" summary="transaction">' +
                            '<thead class="text-center">' +
                            '<tr>' +
                            '<td>Date</td>' +
                            '<td>Outcoming account</td>' +
                            '<td>Incomming account</td>' +
                            '<td>Sum</td>' +
                            '</tr>' +
                            '</thead><tbody></tbody>';
                        jQuery(x).insertAfter(divTitle);
                        jQuery('<h1 id="transactionHeader">Transactions</h1>').insertAfter(divTitle);

                        var html = '<tr id="row' + result.id + '" onmouseover="showMenu(' + result.id + ')" onmouseout="hideMenu(' + result.id + ')">' +
                            '<td>' +
                            '<div id="menuTransaction' + result.id + '" class="rc dopMenu box_shadow popup"' +
                            'style="z-index: 10; display: none">' +
                            '<div style="padding: 0px 8px 5px 8px">' +
                            '<a id="editTransactionLink' + result.id + '" href="#" onclick="editTransaction(' + result.id + ')">' +
                            '<span>Редактировать</span>' +
                            '<img border="0" src="/resources/img/edit.png" width="15" height="15">' +
                            '</a> </div><div style="padding: 5px 8px; "> <a id="deleteTransactionLink' + result.id + '" class="deleteTranasctionLink"' +
                            'href="#" onclick="deleteTransaction(' + result.id + ')"><span>Удалить</span><img border="0"src="/resources/img/del.gif" width="15" height="15"></a></div>' +
                            '</div>' + result.date + '</td>' +
                            '<td>' + result.outcomingAccount + '</td> <td>' + result.incomingAccount + '</td><td>' + parseFloat(result.sum).toFixed(2) + '</td></tr>';
                        jQuery('#transactionTable > tbody:last-child').append(html);

                        jQuery(divTitle).hide();

                    } else {
                        var html = '<tr id="row' + result.id + '" onmouseover="showMenu(' + result.id + ')" onmouseout="hideMenu(' + result.id + ')">' +
                            '<td>' +
                            '<div id="menuTransaction' + result.id + '" class="rc dopMenu box_shadow popup"' +
                            'style="z-index: 10; display: none">' +
                            '<div style="padding: 0px 8px 5px 8px">' +
                            '<a id="editTransactionLink' + result.id + '" href="#" onclick="editTransaction(' + result.id + ')">' +
                            '<span>Редактировать</span>' +
                            '<img border="0" src="/resources/img/edit.png" width="15" height="15">' +
                            '</a> </div><div style="padding: 5px 8px; "> <a id="deleteTransactionLink' + result.id + '" class="deleteTranasctionLink"' +
                            'href="#" onclick="deleteTransaction(' + result.id + ')"><span>Удалить</span><img border="0"src="/resources/img/del.gif" width="15" height="15"></a></div>' +
                            '</div>' + result.date + '</td>' +
                            '<td>' + result.outcomingAccount + '</td> <td>' + result.incomingAccount + '</td><td>' + parseFloat(result.sum).toFixed(2) + '</td></tr>';

                        jQuery('#transactionTable > tbody:last-child').append(html);

                    }

                } else if (data.error == "204") {
                    alert("wrong transaction");
                } else if (data.error == "400") {

                    alert("no enough balance on your account");

                    var amount = jQuery("#amount");
                    amount.addClass("has-error")
                }
                alert("123");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(' произошла ошибка: ' + textStatus + '. сode=' + jqXHR.status);

            }
        });


    }

    function showMenu(id) {
        var a = document.getElementById("menuTransaction" + id);
        a.style.display = "";

    }
    function hideMenu(id) {
        var a = document.getElementById("menuTransaction" + id);
        a.style.display = "none";


    }

    function deleteTransaction(id) {
        $.getJSON('${deleteTransaction}' + id, {
            ajax: 'true'
        }, function (data) {
            var result = JSON.parse(data.message);
            alert(result);
            jQuery("#row" + result).remove();
            var tableContent = jQuery("#transactionTable > tbody").children();
            if (tableContent.length == 0) {
                jQuery('#absentTransactions').show();
                jQuery('#transactionHeader').hide();
                jQuery('#tableDiv').remove();

            }
            ;
        });


    }

</script>

</head>
<body>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>


<div class="container">
    <div class="row">
        <div id="sideMenu" class="col-sm-2 side-nave well">
            <jsp:include page="${pageContext.request.contextPath}/pages/jsp/sideMenu.jsp"/>
        </div>

        <div id="main" class="col-sm-6 well text-center">
            <p class="text-success">

                <c:choose>
                <c:when test="${fn:length(transactions)==0}">
            <div id="absentTransactions">
                You haven`t transactions yet.
                <br>
                You can add it simply by click the button below
                <br>
            </div>
            </c:when>
            <c:otherwise>
                <div id="absentTransactions" style="display: none">
                    You haven`t transactions yet.
                    <br>
                    You can add it simply by click the button below
                    <br>
                </div>
            </c:otherwise>
            </c:choose>

            <c:if test="${transactionDto!=null}">
                <h5>You added new transaction </h5>
            </c:if>

            </p>

            <c:if test="${fn:length(transactions)!=0}">
                <h1 id="transactionHeader">Transactions</h1>
                <c:if test="${path=='/administration/all_accounts'}">
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <div id="selectDiv" style="width:300px; margin: 0 auto">

                            <form:select style="width:250px" multiple="multiple" data-placeholder="Choose user login"
                                         id="userSelect" path="users">

                                <form:options items="${users}" itemLabel="login"
                                              itemValue="login"/>

                            </form:select>

                        </div>

                    </sec:authorize>
                </c:if>

                <div id="tableDiv">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="tablesorter" style="width: 100%;" id="transactionTable" border="1"
                                   cellpadding="5"
                                   cellspacing="1"
                                   width="100%" align="center"
                                   summary="transaction">
                                <thead class="text-center">
                                <tr>
                                    <c:if test="${path=='/administration/all_transactions'}">
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <th>Transaction_id</th>
                                        </sec:authorize>
                                    </c:if>
                                    <td>Date</td>
                                    <td>Outcoming account</td>
                                    <td>Incomming account</td>
                                    <td>Sum</td>
                                        <%--<td>Sum</td>--%>
                                    <c:if test="${path=='/administration/all_transactions'}">
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <th>User_id</th>
                                        </sec:authorize>
                                    </c:if>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="tran" items="${transactions}">
                                    <tr id="row${tran.id}" onmouseover="showMenu(${tran.id})"
                                        onmouseout="hideMenu(${tran.id})">
                                        <c:if test="${path=='/administration/all_transactions'}">
                                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                <td>${tran.id}</td>
                                            </sec:authorize>
                                        </c:if>
                                        <td>
                                            <div id="menuTransaction${tran.id}" class="rc dopMenu box_shadow popup"
                                                 style="z-index: 10; display: none">
                                                <div style="padding: 0px 8px 5px 8px">
                                                    <a id="editTransactionLink${tran.id}"
                                                       href="#" onclick="editTransaction(${tran.id})">
                                                        <span>Редактировать</span>
                                                        <img border="0" src="/resources/img/edit.png" width="15"
                                                             height="15">
                                                    </a>
                                                </div>
                                                <div style="padding: 5px 8px; ">
                                                    <a id="deleteTransactionLink${tran.id}"
                                                       class="deleteTransactionLink"
                                                        <%--href="<c:url value='/account/${account.id}/delete.html' />"--%>
                                                       href='#' onclick="deleteTransaction(${tran.id})"/>
                                                    <span>Удалить</span>
                                                    <img border="0"
                                                         src="/resources/img/del.gif"
                                                         width="15"
                                                         height="15"></a>
                                                </div>
                                            </div>
                                            <fmt:formatDate var="fmtDate" pattern="dd.MM.yyyy HH:mm:ss"
                                                            value="${tran.date}"/>
                                                ${fmtDate}</td>

                                        <td>${tran.outcomingAccount.name}</td>
                                        <td>${tran.incomingAccount.name}</td>
                                        <td>${tran.sum}</td>

                                        <c:if test="${path=='/administration/all_transactions'}">
                                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                                <td>${tran.user.id}</td>
                                            </sec:authorize>
                                        </c:if>

                                    </tr>
                                </c:forEach>
                                </tbody>

                            </table>
                        </div>

                    </div>


                </div>


            </c:if>
            <img src="/resources/img/button_green_add.png" alt="" width="32" height="32">
            <a href="/addTransaction.html">Add new transaction</a><br>

        </div>
        <div id="rightBlock" class="col-sm-4 well text-center">
            <span id="createNewTransactionTitle" class="text-center">Create new transaction</span>

            <fmt:formatDate var="fmtDate" value="<%=new java.util.Date()%>" pattern="dd.MM.yyyy HH:mm:ss"/>

            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Date</span>
                </div>
                <div class="col-sm-9">
                    <input class="form-control" id="date" type="text" value="${fmtDate}">
                </div>
            </div>


            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>OutcomingAccount</span>
                </div>
                <div class="col-sm-9">
                    <select id="outcomingAccount" onchange="changeOutcome()">
                        <option value="">--Select--</option>
                        <c:forEach items="${accounts}" var="acc">
                            <option value="${acc.id}">${acc.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row" id="incomingAccountRow" style="margin-bottom: 3px; display:none">
                <div class="col-sm-3 text-right">
                    <span>IncomingAccount</span>
                </div>
                <div class="col-sm-9">
                    <select id="incomingAccount">
                        <option value="">--Select--</option>
                        <c:forEach items="${accounts}" var="acc">
                            <option value="${acc.id}">${acc.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div id="accountBalance">

            </div>

            <div class="row" style="margin-bottom: 3px;">
                <div class="col-sm-3 text-right">
                    <span>Ammount</span>
                </div>
                <div class="col-sm-9">
                    <input id="amount" class="form-control" type="text" value="">
                </div>
            </div>


            <div class="row text-right">
                <input type="button" value="Cancel" onclick="cancelCreateTransaction()"/>

                <input id="addNewTransactionButton" <%--disabled="true"--%> type="button" onclick="addTransaction()"
                       value=
                <spring:message code="user.body.buttonlabel"/>>

            </div>

            <div id="output">

            </div>

        </div>
    </div>
</div>


<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>

</body>
</html>