<%--<!DOCTYPE html PUBLIC "-//W3C//DTD X HTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%--<%@ page contentType="text/html; ISO-8859-1;charset=UTF-8" language="java" %>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false" %>

<html>
<head>
    <script src="/resources/jquery-ui/jquery-1.8.3.js" type="text/javascript"></script>
    <%--<meta http-equiv="X-UA-Compatible" content="text/html; charset=US-ASCII">--%>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">

    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>


    <title>Reset Password</title>
</head>
<body>
<div class="container">
    <div class="span12">
        <h1>
            Reset Password
        </h1>

        <div>
            <br>

            <label>E-mail</label>
            <br>
            <input id="email" name="email" type="text" value=""/>

            <button type="submit" onclick="resetPass()">
                Reset
            </button>
        </div>

        <br>
        <a href="<c:url value="/user/registration" />">
            Register
        </a>
        <br>
        <a href="<c:url value="/login" />">
            Login
        </a>

    </div>
</div>
<script type="text/javascript">

    function resetPass() {
        var email = $("#email").val();
        $.get("<c:url value="/forgot1"></c:url>", {email: email}, function (data) {
            window.location.href = "<c:url value="/login"></c:url>" + "?message=" + data.message;
        })
            .fail(function (data) {

                if (data.responseJSON.error.indexOf("MailError") > -1) {
                    window.location.href = "<c:url value="/403"></c:url>";
                }
                else {
                    window.location.href = "<c:url value="/login"></c:url>" + "?message=" + data.responseJSON.message;
                }
            });
    }

</script>
<script type="text/javascript">
    $(document).ajaxStart(function () {
        $("title").html("LOADING ...");
    });
</script>


</body>

</html>