<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="registration.title"/></title>
    <script type="text/javascript" src="<c:url value="/resources/jquery-ui/jquery-3.1.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
    <%--BOOTSTRAP--%>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/bootstrap.css" />"
          rel="stylesheet"/>
    <link type="text/css" href="<c:url value="${pageContext.request.contextPath}/resources/css/signin.css" />"
          rel="stylesheet"/>
</head>


<body>

<jsp:include page="${pageContext.request.contextPath}/pages/jsp/menuHeader1.jsp"/>
<div class="container">
    <div class="row">
        <div id="left" class="col-sm-2 well text-center"></div>
        <div id="main" class="col-sm-8 well text-center">
            <h1><spring:message code="user.body.intro"/></h1>
            Language: <a href="?lang=en">English</a>|<a href="?lang=es">Espanol</a>


            <form:form modelAttribute="user" method="post" enctype="utf8">
                <table>

                    <tr>
                        <td>
                            <spring:message code="user.body.login"/>
                        </td>
                        <td>
                            <form:input path="login" value=""/>
                        </td>
                        <td>
                            <form:errors path="login" element="div"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>
                                <spring:message code="label.user.email"></spring:message>
                            </label>
                        </td>

                        <td>
                            <form:input path="email" value=""/>
                        </td>
                        <td>
                            <form:errors path="email" element="div"/>
                        </td>
                    </tr>

                    <tr>

                        <td>
                            <spring:message code="user.body.password"/>
                        </td>

                        <td>
                            <form:password
                                    showPassword="false"
                                    value=""
                                    tabindex="*"
                                    path="password"/>
                        </td>

                        <td>
                            <form:errors path="password" element="div"/>
                        </td>

                    </tr>

                    <tr>

                        <td>
                            <spring:message code="user.body.confirm_password"/>
                        </td>
                        <td>
                            <form:password showPassword="false" tabindex="*" path="matchingPassword"/>
                        </td>
                        <td>
                            <form:errors element="div"/>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="1">
                            <input type="submit" value=<spring:message code="user.body.buttonlabel"/>/>
                        </td>
                    </tr>

                </table>
            </form:form>


        </div>
        <div id="right" class="col-sm-2 well text-center"></div>

    </div>
</div>

<%--<a href="/">Start page</a><br>--%>

<jsp:include page="${pageContext.request.contextPath}/pages/jsp/footer.jsp"/>

</body>
</html>
<%--<tr>
    <td><spring:message code="user.body.surname"/></td>
    <td>
        <form:input path="surname"/>
    </td>
</tr>
<tr>
    <td><spring:message code="user.body.firstName"/></td>
    <td>
        <form:input path="firstName"/>
    </td>
</tr>
<tr>
    <td><spring:message code="user.body.organization"/></td>
    <td>
        <form:input path="organization"/>
    </td>
</tr>
<tr>
    <td><spring:message code="user.body.sex"/></td>
    <td>
        <form:input path="sex"/>
    </td>
</tr>--%>
