package ua.kiev.sa.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = {"ua.kiev.sa", "ua.kiev.sa.it.helper"})
@PropertySource(value = {"classpath:application_test.properties"})
public class ApplicationConfigTest {


}
