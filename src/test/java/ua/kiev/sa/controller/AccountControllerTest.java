package ua.kiev.sa.controller;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
//import org.powermock.api.mockito.PowerMockito;
//import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
//import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import ua.kiev.sa.constants.Constants;
import ua.kiev.sa.exceptions.AccountAlreadyExistsException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.builders.AccountBuilder;
import ua.kiev.sa.model.builders.AccountStatBuilder;
import ua.kiev.sa.model.builders.UserBuilder;
import ua.kiev.sa.model.builders.UserSettingsBuilder;
import ua.kiev.sa.model.dto.AccountDto;
import ua.kiev.sa.model.dto.AccountWithRatesDto;
import ua.kiev.sa.service.*;
import ua.kiev.sa.utils.ConverterJSON;
import ua.kiev.sa.utils.GenericResponse;


import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringJUnit4ClassRunner.class)
@PrepareForTest(AccountController.class)
@ContextConfiguration({"classpath*:ua/kiev/sa/spring-test.xml"})
public class AccountControllerTest {

    private MockMvc mockMvc;

    @Mock
    private
    AccountService accountService;

    @Mock
    private
    UserService userService;

    @Mock
    MockHttpServletRequest request;

    @Mock
    MockHttpServletResponse response;

    @Mock
    MockHttpSession session;

    @InjectMocks
    private AccountController accountController;

    private User user;

    private final String EMPTY_LIST_IN_JSON="[]";


    private AccountDto newCorrectAccount;
    private AccountDto newWrongAccount;

    @Before
    public void setup() throws Exception, AccountAlreadyExistsException {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
        user = new UserBuilder().setId(1L).
                setCredentials("222", "222", null).
                setEnabled(true).build();

        Account account1 = new AccountBuilder().
                setId(1L).setName("firstAcc").setDesc("first").
                setSum(new BigDecimal(1), CurrencyType.AUD).
                setOptions(false, false, false).build();
        account1.setUser(user);

        Account account2 = new AccountBuilder().
                setId(2L).setName("secondAcc").setDesc("2nd").
                setSum(new BigDecimal(2), CurrencyType.AUD).
                setOptions(false, false, false).build();
        account2.setUser(user);

        UserSettings settings = new UserSettingsBuilder().
                setBank(BankType.NBU).
                setDefaultCurrency(CurrencyType.USD).
                setFavouriteCurrencySet(new HashSet(Arrays.asList(CurrencyType.USD, CurrencyType.UAH))).
                build();

        user.setAccounts(Arrays.asList(account1, account2));
        user.setSettings(settings);
//todo: icons

        AccountWithRatesDto accountWithRatesDto1 = new AccountWithRatesDto(account1);
        accountWithRatesDto1.setStartBalanceConverted
                (accountWithRatesDto1.getStartBalance().multiply(BigDecimal.TEN));
        accountWithRatesDto1.setActualBalanceConverted(
                (accountWithRatesDto1.getBalance().multiply(new BigDecimal(2))));
        AccountWithRatesDto accountWithRatesDto2 = new AccountWithRatesDto(account2);
        accountWithRatesDto2.setStartBalanceConverted
                (accountWithRatesDto1.getStartBalance().multiply(new BigDecimal(4)));
        accountWithRatesDto2.setActualBalanceConverted(
                (accountWithRatesDto1.getBalance().multiply(new BigDecimal(8))));
        List<AccountWithRatesDto> accountWithRatesDtoList = Arrays.asList(accountWithRatesDto1, accountWithRatesDto2);

        User user2 = new UserBuilder().setId(2L).
                setCredentials("333", "333", null).
                setEnabled(true).build();

        Account account2_1 = new AccountBuilder().
                setId(3L).setName("firstAccOfSecond").setDesc("first").
                setSum(new BigDecimal(12), CurrencyType.RUB).
                setOptions(false, false, false).build();

        Account account2_2 = new AccountBuilder().
                setId(4L).setName("secondAccOfSecond").setDesc("2nd").
                setSum(new BigDecimal(22), CurrencyType.RUB).
                setOptions(false, false, false).build();

        Account account2_3 = new AccountBuilder().
                setId(5L).setName("thirdAccOfSecond").setDesc("3rd").
                setSum(new BigDecimal(32), CurrencyType.RUB).
                setOptions(false, false, false).build();

        user2.setAccounts(Arrays.asList(account2_1, account2_2, account2_3));

        newCorrectAccount=new AccountDto(
                new AccountBuilder().setName("correctAcc").setDesc("correctAcc").
                        setSum(new BigDecimal(10), CurrencyType.UAH).
                        setOptions(false, false, false).build());

        newWrongAccount=new AccountDto(
                new AccountBuilder().setName("wrongAcc").setDesc("wrongAcc").
                        setSum(new BigDecimal(0), CurrencyType.UAH).
                        setOptions(false, false, false).build());


        List<User> users = Arrays.asList(user, user2);
        List<Account> allAccounts = Arrays.asList(account1, account2,
                account2_1, account2_2, account2_3);

        when(accountService.findAllForLogin("222")).thenReturn(Arrays.asList(account1, account2));
        when(accountService.findByName("firstAcc", "222")).thenReturn(account1);
        when(accountService.findByName("firstAccWrong", "222")).thenReturn(null);
        when(accountService.findByName("firstAcc", "wrongUser")).thenReturn(null);
        when(accountService.findById(1)).thenReturn(account1);
        when(accountService.findById(10)).thenReturn(null);
        when(accountService.getAccountsWithRates(Matchers.<User>any())).thenReturn(accountWithRatesDtoList);
        when(accountService.findAll()).thenReturn(allAccounts);

        ArrayList<Account> allAccountsForTest = new ArrayList<>();
        allAccountsForTest.addAll(user.getAccounts());
        allAccountsForTest.addAll(user2.getAccounts());
        when(accountService.findAllForSomeUsers(Arrays.<String>asList("1", "2"))).thenReturn(allAccountsForTest);
        when(accountService.findAllForSomeUsers(Arrays.<String>asList("1"))).thenReturn(user.getAccounts());
        when(accountService.findAllForSomeUsers(Arrays.<String>asList("3", "4"))).thenReturn(new ArrayList<Account>());

        when(userService.getByUserName("222")).thenReturn(user);
        when(userService.findAll()).thenReturn(users);







       when( accountService.getBalanceConverted(newCorrectAccount.getStartBalance(), newCorrectAccount.getCurrencyType())).
               thenReturn(new BigDecimal(20));
//        when(request.getSession().getServletContext().getRealPath(anyString(.getServletContext().getRealPath(anyString())).thenReturn("/icon");

//        when(request.getSession()).thenReturn(session);
//        when(session.getServletContext().getRealPath(anyString())).thenReturn("/icon");
//        AccountController spy=PowerMockito.spy(new AccountController());
//        PowerMockito.doReturn(Arrays.asList("/default/1","/default/2","/default/3")).
//        when(spy, "getDefaultIconsUrl",  any());
//        PowerMockito.doReturn(Arrays.asList("/222/1","/222/2","/222/3")).
//        when(spy, "getUserIconsUrl", anyString(), any());
//        PowerMockito.when(spy, method(AccountController.class, "getUserIconsUrl", String.class, HttpServletRequest.class)).withArguments(anyString(),any()).thenReturn(Arrays.asList("/222/1","/222/2","/222/3"));
//
    }

    @Test
    public void givenRightPrincipal_whenAllAccountsOfCustomer_ReturnCorrectInformation() throws Exception {
        Principal mockPrincipal = mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("222");

        String realPath = "file:/1111";

        MockServletContext context = new MockServletContext(realPath);
        MockHttpServletRequest request = new MockHttpServletRequest(context, "getAllAccountsOfCustomer", "/accounts");

        ModelAndView modelTest = accountController.getAllAccountsOfCustomer(mockPrincipal, request);

        System.out.println(modelTest);
        assertTrue("reference to view", modelTest.getViewName().equals("account/accounts"));
        assertTrue("favourite currencies", modelTest.getModel().get("currencies").equals(new HashSet<CurrencyType>(Arrays.asList(CurrencyType.USD, CurrencyType.UAH))));
        assertTrue("users", modelTest.getModel().get("users").equals("222"));

        List<AccountWithRatesDto> list = (List<AccountWithRatesDto>) modelTest.getModel().get("accounts");

        assertTrue("start balance converted",
                list.get(0).getStartBalance().compareTo(new BigDecimal(1))==0);
        assertTrue("start balance currency", list.get(0).getCurrencyType().equals(CurrencyType.AUD));
        assertFalse("start balance currency", list.get(0).getCurrencyType().equals(CurrencyType.UAH));
        assertTrue("actual balance converted",
                list.get(0).getActualBalanceConverted().compareTo(new BigDecimal(2))==0);

        assertTrue("actual balance ",
                list.get(0).getBalance().compareTo(new BigDecimal(1))==0);
        assertTrue("start balance ",
                list.get(0).getStartBalance().compareTo(new BigDecimal(1))==0);
        assertTrue("default currency", modelTest.getModel().get("def_curr").equals(CurrencyType.USD));

        verify(accountService, times(1)).getAccountsWithRates(user);
        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenWrongPrincipal_whenAllAccountsOfCustomer_ReturnEmptyModel() throws Exception {
        Principal mockPrincipal = mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("Wrong_user");

        ModelAndView modelTest = accountController.getAllAccountsOfCustomer(mockPrincipal, request);

        System.out.println(modelTest);

        assertTrue("reference to view", modelTest.getViewName().equals("account/accounts"));
        assertTrue("model isn't initialize", (modelTest.getModel().isEmpty()));

        verify(accountService, times(0)).getAccountsWithRates(user);

    }

    @Test
    public void whenAllAccountsOfAllUser_ReturnCorrectInformation() throws Exception {

        ModelAndView modelTest = accountController.getAccountsOfAllUsers(request);

        assertTrue("reference to view", modelTest.getViewName().equals("account/accounts"));
        assertTrue("accounts", ((List<Account>) modelTest.getModel().get("accounts")).size() == 5);
        assertTrue("users", ((List<Account>) modelTest.getModel().get("users")).size() == 2);

        verify(accountService, times(1)).findAll();
        verify(userService, times(1)).findAll();
        verifyNoMoreInteractions(accountService);
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void givenRightAccountId_whenGetAccountInfo_ReturnCorrectAccountInfo() throws Exception {

        Long givenAccountId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.
                get("/accounts/" + givenAccountId)).
                andDo(MockMvcResultHandlers.print()).
                andExpect(status().isOk()).
                andExpect(jsonPath("$.error", is("200"))).
                andExpect(jsonPath("$.message", containsString("\"id\":1")));
//                andExpect(content().string(containsString("firstAcc"))).
//                andExpect(content().string(containsString("1"))).
//                andExpect(content().string(containsString("AUD")));

        verify(accountService, times(1)).findById(givenAccountId);

    }

    @Test
    public void givenWrongAccountId_whenGetAccountInfo_ReturnResponseWith204Error() throws Exception {

        Long givenWrongAccountId = 10L;

        mockMvc.perform(MockMvcRequestBuilders.
                get("/accounts/" + givenWrongAccountId)).
                andDo(MockMvcResultHandlers.print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(content().contentType("application/json")).
                andExpect(jsonPath("error", is("204"))).
                andExpect(jsonPath("message", is("Can't return account with this id or it doesn't exists!")));

        verify(accountService, times(1)).findById(givenWrongAccountId);

    }


    @Test
    public void givenRightAccountName_whenFindAccountByName_ReturnAccountStat() throws Exception {
        Principal mockPrincipal = mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("222");

        String givenAccountName = "firstAcc";

        mockMvc.perform(MockMvcRequestBuilders.
                get("/findAccountSum").
                param("accountName", givenAccountName).
                principal(mockPrincipal)).
                andDo(MockMvcResultHandlers.print()).
                andExpect(status().isOk()).
                andExpect(content().string(containsString("firstAcc"))).
                andExpect(content().string(containsString("1"))).
                andExpect(content().string(containsString("AUD")));

        verify(accountService, times(1)).findByName(givenAccountName, "222");

    }

    @Test
    public void givenWrongAccountName_whenFindAccountByName_ReturnNull() throws Exception {
        Principal mockPrincipal = mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("222");

        String givenWrongAccountName = "firstAccWrong";

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.
                get("/findAccountSum").
                param("accountName", givenWrongAccountName).
                principal(mockPrincipal)).
                andDo(MockMvcResultHandlers.print()).
                andExpect(status().isOk()).
                andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        assertTrue(contentAsString.isEmpty());
        verify(accountService, times(1)).findByName(givenWrongAccountName, "222");
        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenWrongPrincipal_whenFindAccountByName_ReturnNull() throws Exception {
        Principal mockPrincipal = mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("wrongUser");

        String givenAccountName = "firstAcc";

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.
                get("/findAccountSum").
                param("accountName", givenAccountName).
                principal(mockPrincipal)).
                andDo(MockMvcResultHandlers.print()).
                andExpect(status().isOk()).
                andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        assertTrue(contentAsString.isEmpty());
        verify(accountService, times(1)).findByName(givenAccountName, "wrongUser");
        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenCorrectAccountDto_whenCreateNewAccount_returnCorrectModel() {
        Principal mockPrincipal = mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("222");
//:todo
//        AccountDto givenCorrectAccountDto=new AccountDto(user.getAccounts().get(0));

    }


    @Test
    public void givenCorrectUser_whenShowAccountsStats_returnCorrectModel() throws Exception {
        Principal mockPrincipal = mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("222");
//        ModelAndView model=accountController.showAccountsStats(mockPrincipal, request);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.
                get("/accountsStat").
                principal(mockPrincipal)).
                andDo(MockMvcResultHandlers.print()).
                andExpect(status().isOk()).
                andExpect(model().attribute("accounts", hasSize(2))).
                andExpect(model().attribute("users", hasSize(2))).
                andReturn();
//        assertTrue(((List<Account>)model.getModel().get("accounts")).size()==2);
//        assertTrue(((List<Account>)model.getModel().get("accounts")).get(0).==2);
        verify(accountService, times(1)).findAllForLogin(mockPrincipal.getName());
        verify(userService, times(1)).findAll();

        verifyNoMoreInteractions(accountService);
        verifyNoMoreInteractions(userService);

    }

    @Test
    public void givenCorrectStringOfUsers_whenfindAccountsForUser_returnCorrectModel() throws Exception {
        String usersString = "1,2";

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.
                get("/findAccountsForUsers").
                param("users", usersString)).
                andDo(MockMvcResultHandlers.print()).
                andExpect(status().isOk()).
                andExpect(content().contentType("application/json;charset=UTF-8")).
                andExpect(content().string(containsString("{\"id\":1,\"name\":\"firstAcc\",\"balance\":1,\"startBalance\":1,\"currencyType\":\"AUD\""))).
                andExpect(content().string(containsString("{\"id\":2,\"name\":\"secondAcc\",\"balance\":2,\"startBalance\":2,\"currencyType\":\"AUD\""))).
                andExpect(content().string(containsString("{\"id\":3,\"name\":\"firstAccOfSecond\",\"balance\":12,\"startBalance\":12,\"currencyType\":\"RUB\""))).
                andExpect(content().string(containsString("{\"id\":4,\"name\":\"secondAccOfSecond\",\"balance\":22,\"startBalance\":22,\"currencyType\":\"RUB\""))).
                andExpect(content().string(containsString("{\"id\":5,\"name\":\"thirdAccOfSecond\",\"balance\":32,\"startBalance\":32,\"currencyType\":\"RUB\""))).
                andReturn();

        verify(accountService, times(1)).findAllForSomeUsers(anyList());
        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenStringOfUsersWithOneId_whenfindAccountsForUser_returnCorrectModel() throws Exception {
        String usersString = "1,";

        MvcResult result = mockMvc.
                perform(MockMvcRequestBuilders.
                get("/findAccountsForUsers").
                param("users", usersString)).

                andDo(MockMvcResultHandlers.print()).

                andExpect(status().isOk()).
                andExpect(content().contentType("application/json;charset=UTF-8")).
                andExpect(content().string(containsString("{\"id\":1,\"name\":\"firstAcc\",\"balance\":1,\"startBalance\":1,\"currencyType\":\"AUD\""))).
                andExpect(content().string(containsString("{\"id\":2,\"name\":\"secondAcc\",\"balance\":2,\"startBalance\":2,\"currencyType\":\"AUD\""))).

                andReturn();

        verify(accountService, times(1)).findAllForSomeUsers(anyList());
        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenStringOfNotExistingUsers_whenfindAccountsForUser_returnNull() throws Exception {
        String usersString = "3,4";

        MvcResult result = mockMvc.
                perform(MockMvcRequestBuilders.
                        get("/findAccountsForUsers").
                        param("users", usersString)).

                andDo(MockMvcResultHandlers.print()).

                andExpect(status().isOk()).
                andExpect(content().string("")).

                andReturn();

        verify(accountService, times(1)).findAllForSomeUsers(anyList());
        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenCorrectPrincipal_whenFindAllAccountsForUser_returnCorrectJson() throws Exception {
        Principal mockPrincipal = mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("222");


        mockMvc.
                perform(MockMvcRequestBuilders.
                        get("/findAccounts").
                        principal(mockPrincipal)).

                andDo(MockMvcResultHandlers.print()).

                andExpect(status().isOk()).
                andExpect(content().string(containsString("{\"id\":1,\"name\":\"firstAcc\",\"balance\":1,\"startBalance\":1,\"currencyType\":\"AUD\""))).
                andExpect(content().string(containsString("{\"id\":2,\"name\":\"secondAcc\",\"balance\":2,\"startBalance\":2,\"currencyType\":\"AUD\""))).

                andReturn();

        verify(accountService, times(1)).findAllForLogin("222");
        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenWrongPrincipal_whenFindAllAccountsForUser_returnEmptyList() throws Exception {
        Principal mockPrincipal = mock(Principal.class);
        Mockito.when(mockPrincipal.getName()).thenReturn("WrongUser");


        mockMvc.
                perform(MockMvcRequestBuilders.
                        get("/findAccounts").
                        principal(mockPrincipal)).

                andDo(MockMvcResultHandlers.print()).

                andExpect(status().isOk()).
                andExpect(content().string(EMPTY_LIST_IN_JSON)).

                andReturn();

        verify(accountService, times(1)).findAllForLogin("WrongUser");
        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenCorrectAccountDto_whenCreateNewAccount_returnCorrectResponse() throws Exception, AccountAlreadyExistsException {
        when(accountService.createAccount((AccountDto) anyObject())).
                thenReturn(new AccountBuilder(newCorrectAccount).setId(10L).build());

        mockMvc.
                perform(MockMvcRequestBuilders.
                        post("/account/save").
                        contentType(
                                MediaType.APPLICATION_JSON).content(ConverterJSON.toJSON_String(newCorrectAccount))).

                andDo(MockMvcResultHandlers.print()).

                andExpect(status().isOk()).
                andExpect(content().contentType("application/json")).
                andExpect(content().string(containsString("10"))).
                andExpect(content().string(containsString("\"{\\\"id\\\":10," +
                        "\\\"name\\\":\\\"correctAcc\\\"," +
                        "\\\"description\\\":\\\"correctAcc\\\"," +
                        "\\\"hidden\\\":false,\\\"icon\\\":null," +
                        "\\\"currencyType\\\":\\\"UAH\\\"," +
                        "\\\"username\\\":null," +
                        "\\\"startBalance\\\":10.00," +
                        "\\\"startBalanceConverted\\\":null," +
                        "\\\"balance\\\":10.00," +
                        "\\\"actualBalanceConverted\\\":null," +
                        "\\\"u\\\":null}"))).
                andExpect(content().string(containsString("error\":\"200\"}"))).

                andReturn();

        verify(accountService, times(1)).createAccount((AccountDto) anyObject());
        verify(accountService, times(1)).getBalanceConverted((BigDecimal) anyObject(),(CurrencyType) anyObject());

    }


    @Test
    public void givenExistingAccountDto_whenCreateNewAccount_throwsAccountAlreadyExistsException() throws Exception, AccountAlreadyExistsException {

        when(accountService.createAccount((AccountDto) anyObject())).thenThrow(new AccountAlreadyExistsException(newWrongAccount.getName()));

        mockMvc.
                perform(MockMvcRequestBuilders.
                        post("/account/save").
                        contentType(
                                MediaType.APPLICATION_JSON).content(ConverterJSON.toJSON_String(newWrongAccount))).

                andDo(MockMvcResultHandlers.print()).

                andExpect(status().isBadRequest()).
                andExpect(content().contentType("application/json")).
                andExpect(content().string(containsString("Account with this name already exists"))).
                andExpect(content().string(containsString("error\":\"400\"}"))).

                andReturn();

        verify(accountService, times(1)).createAccount((AccountDto) anyObject());
        verify(accountService, times(0)).getBalanceConverted((BigDecimal) anyObject(),(CurrencyType) anyObject());
        //        verify(accountService, times(1)).getBalanceConverted((AccountDto) anyObject());

//        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenInvalidAccountDto_whenCreateNewAccount_returnModelWithAppropriateMessage() throws Exception, AccountAlreadyExistsException {
AccountDto emptyAccount=new AccountDto(
        new AccountBuilder().setName("").setDesc("emptyAcc").
                setSum(new BigDecimal(0), CurrencyType.UAH).
                setOptions(false, false, false).build());

        when(accountService.createAccount((AccountDto) anyObject())).thenReturn(new AccountBuilder(emptyAccount).build());

        mockMvc.
                perform(MockMvcRequestBuilders.
                        post("/account/save").
                        contentType(
                                MediaType.APPLICATION_JSON).content(ConverterJSON.toJSON_String(emptyAccount))).

                andDo(MockMvcResultHandlers.print()).

                andExpect(status().isBadRequest()).
                andExpect(content().contentType("application/json")).
                andExpect(content().string(containsString("Some fields are empty or not valid"))).
                andExpect(content().string(containsString("error\":\"400\"}"))).

                andReturn();

        verify(accountService, times(0)).createAccount((AccountDto) anyObject());
        verify(accountService, times(0)).getBalanceConverted((BigDecimal) anyObject(),(CurrencyType) anyObject());
        //        verify(accountService, times(1)).getBalanceConverted((AccountDto) anyObject());

//        verifyNoMoreInteractions(accountService);
    }

    @Test
    public void givenNullAccountDto_whenCreateNewAccount_returnModelWithAppropriateMessage() throws Exception, AccountAlreadyExistsException {
        AccountDto emptyAccount=null;

        GenericResponse genericResponse= accountController.createNewAccount(emptyAccount, response);

        assertTrue(genericResponse.getMessage().equals("Some fields are empty or not valid"));
        assertTrue(genericResponse.getError().equals("400"));

        verify(accountService, times(0)).createAccount((AccountDto) anyObject());
        verify(accountService, times(0)).getBalanceConverted((BigDecimal) anyObject(),(CurrencyType) anyObject());
    }
}
