package ua.kiev.sa.service;


import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import sun.plugin.liveconnect.SecurityContextHelper;
import ua.kiev.sa.exceptions.NoEnoughMoneyAtAccountException;
import ua.kiev.sa.model.*;
import ua.kiev.sa.model.dto.OperationDto;
import ua.kiev.sa.repository.*;
import ua.kiev.sa.utils.CurrenciesConverter;
import ua.kiev.sa.utils.CurrencyRatesDownloader;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.when;
import static ua.kiev.sa.model.QUser.user;


@RunWith(PowerMockRunner.class)
@ContextConfiguration
@PrepareForTest({CurrencyRatesDownloader.class, OperationServiceImpl.class, CurrenciesConverter.class})
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PowerMockIgnore("javax.security.*")
public class OperationServiceTest {
    final BankType BANK = BankType.NBU;

    private Account account=getAccount();
    private OperationDto operationDto=getOperationDto();

    @Autowired
    private RateService rateService;
    @Autowired
    private UserRepository userRepository;
    @Qualifier("subcategoryRepository")
    @Autowired
    private SubcategoryRepository subcategoryRepository;
    @Qualifier("categoryRepository")
    @Autowired
    private CategoryRepository categoryRepository;
    @Qualifier("accountRepository")
    @Autowired
    private AccountRepository accountRepository;
    @Qualifier("operationRepository")
    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    @Qualifier("reportRepository")
    private ReportRepository reportRepository;
    @Autowired
    private OperationService operationService;

    CurrenciesConverter currenciesConverter;
    //    CurrencyRatesDownloader downloader;
//    @Mock
//    private CurrencyRatesDownloader downloader1;
//    List<CurrencyRates> rates;

    //@Rule
//public PowerMockRule rule=new PowerMockRule();
    @Before
    public void setup() throws Exception {
        Mockito.when(userRepository.getByUserName(anyString())).thenReturn(getUser());
        Mockito.when(categoryRepository.findOne(anyLong())).thenReturn(getCategory());
        Mockito.when(subcategoryRepository.findSubcategoryByNameAndCategoryId(anyLong(), anyLong(), any(User.class))).thenReturn(getSubcategory());
        Mockito.when(accountRepository.findAccountByName(anyString(), any(User.class))).thenReturn(getAccount());

        currenciesConverter = PowerMockito.mock(CurrenciesConverter.class);
        PowerMockito.whenNew(CurrenciesConverter.class).withArguments(Matchers.any(CurrencyRates.class), any(User.class)).thenReturn(currenciesConverter);
        PowerMockito.when(currenciesConverter.convert(any(BigDecimal.class), any(CurrencyType.class), any(CurrencyType.class))).thenReturn(new BigDecimal(1));


        Authentication a= SecurityContextHolder.getContext().getAuthentication();

        Authentication authentication=Mockito.mock(Authentication.class);
        SecurityContext context= Mockito.mock(SecurityContext.class);
        Mockito.when(context.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(context);
        Mockito.when(authentication.getName()).thenReturn("1");

//        PowerMockito.when(downloader.getDaylyCurrencyRates()).thenReturn(rates.get(0).getRates());
//        when(rateService.updateCurrenciesRates(any(Set.class))).thenReturn(rates.get(0));
    }

    @Test
//    @WithMockUser(username)
    public void createOperationTest() throws NoEnoughMoneyAtAccountException {
        OperationDto operationDto = getOperationDto();
        assertThat(account.getBalance()).isEqualTo(new BigDecimal(20001));
        Operation operation=operationService.createOperation(operationDto);
//        operationService.createOperation(operationDto);
        assertThat(operation.getSum()).isEqualTo(new BigDecimal(20000));
        assertThat(account.getBalance()).isEqualTo(BigDecimal.ONE);

    }

    private Account getAccount() {
        Account account = new Account();
        account.setId(1l);
        account.setName("1");
        account.setCurrencyType(CurrencyType.USD);
        account.setUser(getUser());
        account.setDescription("111");
        account.setStartBalance(new BigDecimal(1000));
        account.setCash(true);
        account.setHidden(false);
        account.setBalance(new BigDecimal(20001));
        return account;

    }


    private Subcategory getSubcategory() {
        Subcategory subcategory = new Subcategory();
        subcategory.setId(1l);
        subcategory.setName("1");
        subcategory.setCategory(getCategory());
        return subcategory;
    }

    private Category getCategory() {
        Category cat = new Category();
        cat.setId(1l);
        cat.setName("1");
        return cat;
    }

    private User getUser() {
        User u = new User();
        u.setId(1l);
        return u;
    }


    private OperationDto getOperationDto() {
        OperationDto operationDto = new OperationDto();
        operationDto.setCurrencyType(CurrencyType.AUD);
        operationDto.setSum("20000");
        operationDto.setAccount("1");
        operationDto.setCategory("1");
        operationDto.setSubcategory("1");
        operationDto.setId("1");
//        String s= null;
//        try {
//            s = (OperationDto.dateFormat.parse("01.01.2017 00:00:00")).toString();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        operationDto.setDate("01.01.2017 00:00:00");
        return operationDto;
    }


    @After
    public void tearDown() {

    }

    @Configuration
    static class RateServiceTestContextConfiguration {
        @Bean
        public OperationService operationService() {
            return new OperationServiceImpl();
        }

        @Bean
        public SubcategoryRepository subcategoryRepository() {
            return Mockito.mock(SubcategoryRepository.class);
        }

        @Bean
        public CategoryRepository categoryRepository() {
            return Mockito.mock(CategoryRepository.class);
        }

        @Bean
        public UserRepository userRepository() {
            return Mockito.mock(UserRepository.class);
        }

        @Bean
        public AccountRepository accountRepository() {
            return Mockito.mock(AccountRepository.class);
        }

        @Bean
        public OperationRepository operationRepository() {
            return Mockito.mock(OperationRepository.class);
        }

        @Bean
        public ReportRepository reportRepository() {
            return Mockito.mock(ReportRepository.class);
        }

        @Bean
        public RateService rateService() {
            return Mockito.mock(RateServiceImpl.class);
        }
        @Bean
        public RateRepository rateRepository() {
            return Mockito.mock(RateRepository.class);
        }

    }


}
