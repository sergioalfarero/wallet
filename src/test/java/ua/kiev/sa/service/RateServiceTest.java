package ua.kiev.sa.service;


import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import org.powermock.core.classloader.annotations.PrepareForTest;

import org.powermock.modules.junit4.PowerMockRunner;

import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.kiev.sa.model.*;
import ua.kiev.sa.repository.RateRepository;
import ua.kiev.sa.repository.UserRepository;
import ua.kiev.sa.utils.CurrencyRatesDownloader;

import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;


@RunWith(PowerMockRunner.class)
@ContextConfiguration
@PrepareForTest({CurrencyRatesDownloader.class, RateServiceImpl.class})
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
public class RateServiceTest {
    final BankType BANK=BankType.NBU;

    @Autowired
    RateRepository rateRepository;
    @Autowired
    RateService rateService;
    @Autowired
    UserRepository userRepository;

    CurrencyRatesDownloader downloader;
//    @Mock
//    private CurrencyRatesDownloader downloader1;
    List<CurrencyRates> rates;
//@Rule
//public PowerMockRule rule=new PowerMockRule();
    @Before
    public void setup() throws Exception {
        rates= getCurrenciesRates();
        User user = getUser();
  //        CurrencyRatesDownloader downloader=Mockito.mock(CurrencyRatesDownloader.class);
//        when(downloader.getDaylyCurrencyRates()).thenReturn(null);
        Mockito.when(rateRepository.findTop1OrderByDate(new PageRequest(0, 1))).thenReturn(rates);
        Mockito.when(rateRepository.findRatesForUser(user)).thenReturn(rates.get(0));
        Mockito.when(rateRepository.findForUserLogin("222")).thenReturn(rates.get(0));



        downloader=PowerMockito.mock(CurrencyRatesDownloader.class);
        PowerMockito.whenNew(CurrencyRatesDownloader.class).withArguments(BANK).thenReturn(downloader);
        PowerMockito.when(downloader.getDaylyCurrencyRates()).thenReturn(rates.get(0).getRates());
        when(rateService.updateCurrenciesRates(any(Set.class))).thenReturn(rates.get(0));
    }

    @Test
    public void testSaveInvokesOnlyOnce() {
        rateService.save(any(CurrencyRates.class));
        verify(rateRepository, times(1)).save(any(CurrencyRates.class));
    }


    @Test
    public void getCustomRatesForLoginTest() {
        String login="222";
        CurrencyRates rates=rateService.getCustomRatesForLogin(login);
        assertThat(rates).isNotNull();
        assertThat(rates.getId()).isNotNull();
        assertThat(rates.getId()).isEqualTo(1);
        assertThat(rates.getBank()).isEqualTo(BankType.NBU);
        assertThat(rates.getRates().size()).isEqualTo(3);
        verify(rateRepository, times(1)).findForUserLogin(login);
    }

    @Test
    public void saveUserCurrenciesTest(){
        Set<Rate> rates=getCurrenciesRates().get(1).getRates();

        rateService.save(rates, "222");

        verify(rateRepository, times(1)).findForUserLogin(any(String.class));
        verify(rateRepository, times(1)).save(any(CurrencyRates.class), any(String.class));
    }
    @Test
    public void saveUserCurrenciesTestWhenUserRateNull(){
        Set<Rate> rates=getCurrenciesRates().get(1).getRates();
        Mockito.when(rateRepository.findForUserLogin("222")).thenReturn(null);
        rateService.save(rates, "222");

        verify(rateRepository, times(1)).findForUserLogin(any(String.class));
        verify(rateRepository, times(1)).save(any(CurrencyRates.class), any(String.class));
    }

    @Test
    public void getRatesForLoginTest() {
        CurrencyRates rates = rateRepository.findForUserLogin("222");
        assertThat(rates).isNotNull();
        assertThat(rates.getId()).isNotNull();
        assertThat(rates.getId()).isEqualTo(1);
        assertThat(rates.getBank()).isEqualTo(BankType.NBU);
        assertThat(rates.getRates().size()).isEqualTo(3);
        verify(rateRepository, times(1)).findForUserLogin(any(String.class));

    }


    @Test
    public void addRateTest(){
        Rate rate=new Rate();
        rate.setRate(new BigDecimal(10));
        rate.setCurrencyType(CurrencyType.AUD);

        rateService.addRate(rate, "222");

        verify(rateRepository, times(1)).findForUserLogin(any(String.class));
        verify(rateRepository, times(1)).save(any(CurrencyRates.class), any(String.class));
    }

    @Test
    public void addRateTestWhenUserRateIsNull(){
        when(rateRepository.findForUserLogin("222")).thenReturn(null);
        Rate rate=new Rate();
        rate.setRate(new BigDecimal(10));
        rate.setCurrencyType(CurrencyType.AUD);

        rateService.addRate(rate, "222");

        verify(rateRepository, times(1)).findForUserLogin(any(String.class));
        verify(rateRepository, times(1)).save(any(CurrencyRates.class), any(String.class));
    }

    @Test
    public void testDefaultRatesAreUpdate() {
        CurrencyRates rate = rateService.getDefaultRates();

        assertThat(rate.getId()).isEqualTo(null);
    }

    @Test
    public void testGetDefaultRates() throws Exception {
        List<CurrencyRates> currencies = getCurrenciesRates();
        Rate r=new Rate();r.setCurrencyType(CurrencyType.AUD);
        r.setRate(new BigDecimal(10));
        currencies.get(0).getRates().add(r);

        PowerMockito.when(downloader.getDaylyCurrencyRates()).thenReturn(currencies.get(0).getRates());

        CurrencyRates rates=rateService.getDefaultRates();

        assertThat(rates.getRates().size()).isEqualTo(4);
//        verify(rateService, times(3)).getDefaultRates();
    }



    private User getUser() {
        User user = new User();
        user.setId(1L);
        user.setLogin("222");
        return user;
    }
    @Test
    public void testGetDefaultRatesWhenNull() throws IOException {
        PowerMockito.when(downloader.getDaylyCurrencyRates()).thenReturn(null);
        CurrencyRates rates=rateService.getDefaultRates();
        assertThat(rates).isEqualTo(null);

    }


    private List<CurrencyRates> getCurrenciesRates() {

        List<CurrencyRates> rates = new ArrayList<CurrencyRates>();

        CurrencyRates currencyRates1 = new CurrencyRates();
        CurrencyRates currencyRates2 = new CurrencyRates();
        CurrencyRates currencyRates3 = new CurrencyRates();

        currencyRates1.setId(1L);
        currencyRates2.setId(2L);
        currencyRates3.setId(3L);

        currencyRates1.setBank(BankType.NBU);
        currencyRates2.setBank(BankType.NBU);
        currencyRates3.setBank(BankType.NBU);

        currencyRates1.setBaseRate(new BigDecimal(1));
        currencyRates2.setBaseRate(new BigDecimal(1));
        currencyRates3.setBaseRate(new BigDecimal(1));

        DateTime date = new DateTime();
        currencyRates1.setDate(date.minusDays(1).toDate());
        currencyRates2.setDate(date.minusDays(1).toDate());
        currencyRates3.setDate(date.minusDays(1).toDate());


        Rate rate1 = new Rate();
        rate1.setCurrencyType(CurrencyType.USD);
        rate1.setRate(new BigDecimal(2));
        Rate rate2 = new Rate();
        rate2.setCurrencyType(CurrencyType.EUR);
        rate2.setRate(new BigDecimal(3));
        Rate rate3 = new Rate();
        rate3.setCurrencyType(CurrencyType.RUB);
        rate3.setRate(new BigDecimal(4));
        Set<Rate> rate = new HashSet<Rate>();
        rate.add(rate1);
        rate.add(rate2);
        rate.add(rate3);

        currencyRates1.setRates(rate);


        Rate rate6 = new Rate();
        rate6.setCurrencyType(CurrencyType.USD);
        rate6.setRate(new BigDecimal(4));
        Rate rate4 = new Rate();
        rate4.setCurrencyType(CurrencyType.EUR);
        rate4.setRate(new BigDecimal(5));
        Rate rate5 = new Rate();
        rate5.setCurrencyType(CurrencyType.RUB);
        rate5.setRate(new BigDecimal(6));
        Set<Rate> rate0 =new HashSet<Rate>();
        rate0.add(rate6);
        rate0.add(rate4);
        rate0.add(rate5);

        currencyRates2.setRates(rate0);

        rates.add(currencyRates1);
        rates.add(currencyRates2);
        rates.add(currencyRates3);
        return rates;
    }

@After
public void tearDown(){
    reset(rateRepository);
//    reset(rateService);
    reset(userRepository);
}

    @Configuration
    static class RateServiceTestContextConfiguration {
        @Bean
        public RateService rateService() {
            return new RateServiceImpl();
        }

        @Bean
        public UserRepository userRepository() {
            return Mockito.mock(UserRepository.class);
        }

        @Bean
        public RateRepository rateRepository() {
            return Mockito.mock(RateRepository.class);
        }
    }


}
