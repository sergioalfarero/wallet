package ua.kiev.sa.validation;


import junit.framework.TestCase;
import org.junit.Test;


public class EmailValidatorTest extends TestCase{


@Test
    public void testincorrectEmailAddressReturnFalse(){
        new EmailValidator().isValid("12 3@10.22", null);
    }


    @Test(expected = NullPointerException.class)
    public void testIncorrectEmailAddressReturnFalse1(){
        assertTrue(new EmailValidator().isValid(null, null));

    }

    @Test
    public void testNullMail(){
        assertTrue(new EmailValidator().isValid("1241@er.re", null));

    }
}
