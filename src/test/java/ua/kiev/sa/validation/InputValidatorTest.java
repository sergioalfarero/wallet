package ua.kiev.sa.validation;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.context.annotation.Role;
import ua.kiev.sa.model.dto.AccountDto;

import java.math.BigDecimal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class InputValidatorTest {

    AccountDto dtoObject;

    @Rule
    public  ExpectedException expected= ExpectedException.none();

    @Before
    public void setup() {
    dtoObject=new AccountDto();
    dtoObject.setName("123");
    }

     @Test
    public void testCorrectObjectReturnsTrue(){
        assertTrue(new InputValidator().isValid(dtoObject,"Name"));

    }

    @Test
    public void isFieldValidReturnsTrueIfObjectContainsGetterWithGivenFieldName(){
        assertTrue(new InputValidator().isFieldValid(dtoObject,"Name"));
    }

//    @Test
    @Test(expected = NoSuchMethodException.class)
//    @Test(expected = Exception.class)
    public void isFieldValidReturnsFalseIfObjectDoesntContainGetterWithGivenFieldName(){
//        expected.expect(NoSuchMethodException.class);
//        expected.expectMessage("ua.kiev.sa.model.dto.AccountDto.getName1()");
       assertTrue( new InputValidator().isFieldValid(dtoObject,"Name1"));;
    }
    @Test
    public void isNullReturnsFalseIfObjectIsNotNull(){
        assertFalse(new InputValidator().isNull(new AccountDto()));
    }

    @Test
    public void isNullReturnsTrueIfObjectIsNull(){
        assertTrue(new InputValidator().isNull(null));
    }



//    @Test
//    public void testBalanceFormat1(){
//        assertTrue(new BalanceValidator().isValid(new BigDecimal("113.43"), null));
//
//    }
//    @Test
//    public void testBalanceFormat2(){
//        assertFalse(new BalanceValidator().isValid(new BigDecimal("113,414"), null));
//
//    }
//    @Test
//    public void testBalanceFormat3(){
//        assertFalse(new BalanceValidator().isValid(new BigDecimal("113,1222"), null));
//
//    }
//    @Test
//    public void testBalanceFormat4(){
//        assertTrue(new BalanceValidator().isValid(new BigDecimal("0113,00"), null));
//
//    }
//
//
//    @Test
//    public void testBalanceFormat6(){
//        assertTrue(new BalanceValidator().isValid(new BigDecimal("12,113.21"), null));
//
//    }
//    @Test
//    public void testBalanceFormat7(){
//        assertFalse(new BalanceValidator().isValid(new BigDecimal("0113.21"), null));
//
//    }
}