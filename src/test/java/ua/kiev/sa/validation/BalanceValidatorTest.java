package ua.kiev.sa.validation;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class BalanceValidatorTest {
     @Test
    public void testBalanceFormat(){
        assertTrue(new BalanceValidator().isValid(new BigDecimal("113"), null));

    }
    @Test
    public void testBalanceFormat1(){
        assertTrue(new BalanceValidator().isValid(new BigDecimal("113.43"), null));

    }
    @Test
    public void testBalanceFormat2(){
        assertFalse(new BalanceValidator().isValid(new BigDecimal("113,414"), null));

    }
    @Test
    public void testBalanceFormat3(){
        assertFalse(new BalanceValidator().isValid(new BigDecimal("113,1222"), null));

    }
    @Test
    public void testBalanceFormat4(){
        assertTrue(new BalanceValidator().isValid(new BigDecimal("0113,00"), null));

    }


    @Test
    public void testBalanceFormat6(){
        assertTrue(new BalanceValidator().isValid(new BigDecimal("12,113.21"), null));

    }
    @Test
    public void testBalanceFormat7(){
        assertFalse(new BalanceValidator().isValid(new BigDecimal("0113.21"), null));

    }
}