package ua.kiev.sa.it;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.stream.IDataSetProducer;
import org.dbunit.dataset.stream.StreamingDataSet;
import org.dbunit.dataset.xml.FlatXmlProducer;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.InputSource;
import ua.kiev.sa.config.ApplicationConfigTest;
import ua.kiev.sa.config.HibernateConfigTest;
import ua.kiev.sa.exceptions.AccountAlreadyExistsException;
import ua.kiev.sa.exceptions.UserNotFoundException;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.dto.AccountDto;
import ua.kiev.sa.repository.AccountRepository;
import ua.kiev.sa.service.AccountService;
import ua.kiev.sa.service.AccountServiceImpl;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration (locations = "/app_context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup ("/datasets/accounts.xml")
public class AccountServiceIT1 {

    //    Account findByName(String name, String login);
//    @Rule
//    ExpectedException expectedException=ExpectedException.none();

    @Autowired
    private AccountService accountService;
    //    protected static final String DATASET = "classpath:datasets/accounts.xml";
    private static final String ACCOUNT_DESC1 = "1232";
    private static final String ACCOUNT_DESC2= "1231";


    private static final String DESCRIPTION_FIELD = "description";


    @Test(expected = IllegalArgumentException.class)
    public void checkIfMethodThrowExceptionWhenNullNameParameterPassed() {
        String login="222";
        String name="12";
        accountService.findByName(null, login);


    }


    @Test(expected = IllegalArgumentException.class)
    public void checkIfMethodThrowExceptionWhenNullLoginParameterPassed() {
        String login="222";
        String name="12";
        accountService.findByName(name, null);


    }

    @Test(expected = IllegalArgumentException.class)
    public void checkIfMethodThrowExceptionWhenBothPassedParametersAreNull() {
        accountService.findByName(null, null);

    }

    @Test
    public void checkIfMethodReturnCorrectData() {
        String login="222";
        String name="12";
        Account expected=new Account();
        expected.setId(1L);
        expected.setBalance(new BigDecimal(200));
        expected.setCash(false);
        expected.setCredit(false);
        expected.setHidden(false);
        expected.setCurrencyType(CurrencyType.UAH);
        expected.setDescription("1232");
        expected.setIcon("12");
        expected.setName("12");
        expected.setStartBalance(new BigDecimal(200));
        User user=new User();
        user.setId(1L);
        expected.setUser(user);

        Account account=accountService.findByName(name, login);
        assertThat(account).isEqualTo(expected);
    }

    @Test
    public void checkThatMethodReturnNothingAfterPassingWrongNameParam() {
        String login="222";
        String name="123";
        assertThat(accountService.findByName(name, login)).isEqualTo(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void checkThatMethodReturnNothingAfterPassingWrongLoginParams() {
        String login="2222";
        String name="12";
        assertThat(accountService.findByName(name, login)).isEqualTo(null);
    }
//    List<Account> findAll();



//    List<Account> findAllForLogin(String login);
//
//    List<Account> findAllNotHiddenForLogin(String login);
//
//    Account createAccount(AccountDto accountDto) throws AccountAlreadyExistsException;
//
//    Account findById(long id);
//
//    void delete(Long id);
//
//    void delete(Account account);
//
//    Account merge(AccountDto accountDto, Long id);
//
//    List<AccountWithRatesDto> getAccountsWithRates(User user);
//
//    CurrencyRates getRateMap(User user);
//
//    BigDecimal getBalanceConverted(BigDecimal startBalance, CurrencyType currencyType);
//
//    AccountSummary getSummary();

    @Test
    public void findCheckedShouldReturnTwoItems() {

        assertThat(accountService.findAll())
                .hasSize(2)
                .extracting(DESCRIPTION_FIELD)
                .containsOnly(ACCOUNT_DESC1, ACCOUNT_DESC2);
    }

    @Test
    public void testFindUnhiddenAll()  {

        List<Account> result = accountService.findAllNotHiddenForLogin("222");
        assertThat(result.size()).isEqualTo(1);

    }


    @Test
    public void findCheckedShouldReturnThreeItems() throws AccountAlreadyExistsException{
        AccountDto accountDto=new AccountDto();
        accountDto.setName("213");
        accountDto.setCurrencyType(CurrencyType.AED);
        accountDto.setDescription("124");
        accountDto.setCash(false);
        accountDto.setHidden(false);
        accountDto.setCredit(false);
        accountDto.setIcon(null);
        accountDto.setUsername("222");
        accountDto.setStartBalance(new BigDecimal(1));


        try {
            accountService.createAccount(accountDto);
        } catch (AccountAlreadyExistsException e) {
            e.printStackTrace();
        }
        assertThat(accountService.findAll())
                .hasSize(2);
    }

    @Test
    public void findAccountForUser() {

        assertThat(accountService.findAllForLogin("222"))
                .hasSize(3);
    }

    @Test
    public void checkIfRepositoryReturnAccountForUsersLoginInList() {
        String login1="222";
//        String login2="333";
        ArrayList<String> users=new ArrayList<String>();
        users.add(login1);
//        users.add(login2);
        List<Account> allForSomeUsers = accountService.findAllForSomeUsers(users);
        for(Account a:allForSomeUsers){
            System.out.println(a);
        }
//        System.out.println(allForSomeUsers);
    }
}
