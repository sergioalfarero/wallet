package ua.kiev.sa.it;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ua.kiev.sa.model.Account;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.repository.AccountRepository;
import ua.kiev.sa.repository.UserRepository;
import ua.kiev.sa.service.AccountService;
import ua.kiev.sa.service.AccountServiceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static ua.kiev.sa.model.QAccount.account;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AccountServiceTest {
    @Autowired
    UserRepository userRepository;

    @Autowired
    private AccountService accountService;
    @Autowired
    private AccountRepository accountRepository;

    @Before
    public void setup() {
        List<Account> accounts = new ArrayList<Account>();
//        User u=new User();
//        u.setId(new Long(1));

        Account account1 = new Account();
        account1.setBalance(new BigDecimal(1));
        account1.setName("1");
        account1.setCurrencyType(CurrencyType.AED);
        account1.setId(1L);
        account1.setHidden(true);
        account1.setCash(true);
        account1.setDescription("1");
//        account1.setUser(u);
        Account account2 = new Account();
        account2.setBalance(new BigDecimal(1));
        account2.setName("1");
        account2.setCurrencyType(CurrencyType.AED);
        account2.setId(1L);
        account2.setHidden(false);
        account2.setCash(true);
        account2.setDescription("1");
//        account2.setUser(u);

        accounts.add(account1);
        accounts.add(account2);
        when(accountRepository.findAll()).thenReturn(accounts);
    }

//    @After
//    public void verify() {
//        Mockito.verify(accountRepository, VerificationModeFactory.times(1)).findAll();
//        // This is allowed here: using container injected mocks
//        Mockito.reset(accountRepository);
//    }

    @Test
    public void testFindAll()  {
        List<Account> result = accountService.findAll();
        assertEquals(2, result.size());


    }

//    @Test
//    public void testFindUnhiddenAll()  {
//        User user=new User();
//        user.setId(1L);
//        when(userRepository.getByUserName("222")).thenReturn(user);
//        List<Account> result = accountService.findAllNotHiddenForLogin("222");
//
//
//
//        assertEquals(1, result.size());
//
//
//    }


    @Configuration
    static class AccountServiceTestContextConfiguration {

        @Bean
        public UserRepository userRepository(){return  Mockito.mock(UserRepository.class);}
        @Bean
        public AccountService accountService() {
            return new AccountServiceImpl();
        }

        @Bean
        public AccountRepository accountRepository() {
            return Mockito.mock(AccountRepository.class);
        }
    }
}
