package ua.kiev.sa.it;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import ua.kiev.sa.exceptions.AccountAlreadyExistsException;
import ua.kiev.sa.exceptions.TokenAlreadyExistsException;
import ua.kiev.sa.exceptions.TokenNotFoundException;
import ua.kiev.sa.exceptions.UserNotFoundException;
import ua.kiev.sa.model.CurrencyType;
import ua.kiev.sa.model.User;
import ua.kiev.sa.model.VerificationToken;
import ua.kiev.sa.model.dto.AccountDto;
import ua.kiev.sa.service.AccountService;
import ua.kiev.sa.service.UserService;
import ua.kiev.sa.service.VerificationTokenService;

import java.math.BigDecimal;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration (locations = "/app_context.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup ("/datasets/verificationToken.xml")
public class VerificationTokenIT {

    @Autowired
    private VerificationTokenService verificationTokenService;

    @Autowired
    private UserService userServiceImpl;

//    VerificationToken getVerificationToken(String token);


    @Test
    public void verifyThatExpectedTokenExists() throws TokenNotFoundException {
        String expectedToken ="b7b7cceb-caac-4288-9c91-1284a889cdc9";
        assertThat(verificationTokenService.getVerificationToken(expectedToken).getToken()).isEqualTo(expectedToken);
    }

    @Test(expected = IllegalArgumentException.class)
    public void verifyThatThrowIllegalArgumentExceptionWhenPassingLargeToken() throws TokenNotFoundException {
        String expectedToken ="b7b7cceb-caac-4288-9c91-1284a889cdc91";
        assertThat(verificationTokenService.getVerificationToken(expectedToken));
    }

    @Test
    public void verifyThatMethodReturnsCorrectToken() throws TokenNotFoundException {
        VerificationToken verificationToken=verificationTokenService.getVerificationToken("b7b7cceb-caac-4288-9c91-1284a889cdc9");
        assertThat(verificationToken.getToken()).matches("^[a-f0-9-]{36}");
        assertThat(verificationToken.getExpiryDate()).hasYear(2017);
        assertThat(verificationToken.getExpiryDate()).hasMonth(1);
        assertThat(verificationToken.getExpiryDate()).hasDayOfMonth(23);
        assertThat(verificationToken.getExpiryDate()).hasHourOfDay(23);
        assertThat(verificationToken.getExpiryDate()).hasMinute(14);
        assertThat(verificationToken.getExpiryDate()).hasSecond(38);
        assertThat(verificationToken.getId()).isNotNegative();
        assertThat(verificationToken.getId()).isNotNull();

        assertThat(verificationToken.isVerified()).isEqualTo(false);
        assertThat(verificationToken.getUser().getId()).isEqualTo(1L);

    }

    @Test (expected = IllegalArgumentException.class)
    public void verifyThatMethodThrowIllegalArgumentExceptionWhenNullParameterPassed() throws TokenNotFoundException {
        assertThat(verificationTokenService.getVerificationToken("null"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void verifyThatMethodThrowIllegalArgumentExceptionWhenShortParameterPassed() throws TokenNotFoundException {

        assertThat(verificationTokenService.getVerificationToken("b7b7cceb-caac-"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void verifyThatMethodThrowIllegalArgumentExceptionWhenParameterWithIncorrectSymbolsPassed() throws TokenNotFoundException {

        assertThat(verificationTokenService.getVerificationToken("b7b7cceb-caac-4288-9c91-1284a889cфф9"));
    }

//    void createVerificationToken(User user, String token);

    @Test
    public void verifyThatTokenСreated() throws TokenNotFoundException, TokenAlreadyExistsException {
        User u=new User();
        u.setId(1L);
        u.setLogin("111");
        String token = UUID.randomUUID().toString();
        verificationTokenService.createVerificationToken(u, token);
        assertThat(verificationTokenService.getVerificationToken(token).getToken()).isEqualTo(token);
    }

    @Test(expected = UserNotFoundException.class)
    public void verifyThatMethodThrowsExceptionWhenUserWithoutLoginPassed() throws TokenNotFoundException, TokenAlreadyExistsException {
        User u=new User();
        u.setId(1L);
        u.setLogin(null);
        String token = UUID.randomUUID().toString();
        verificationTokenService.createVerificationToken(u, token);

    }


    @Test(expected = IllegalArgumentException.class)
    public void verifyThatMethodThrowsExceptionWhenNullUserParameterPassed() throws TokenNotFoundException, TokenAlreadyExistsException {
        String token = UUID.randomUUID().toString();
        verificationTokenService.createVerificationToken(null, token);

    }

    @Test(expected = IllegalArgumentException.class)
    public void verifyThatMethodThrowsExceptionWhenNullTokenParameterPassed() throws TokenNotFoundException, TokenAlreadyExistsException {
        User u=new User();
        u.setId(1L);
        u.setLogin("111");
        verificationTokenService.createVerificationToken(u, null);

    }

    @Test
    public void verifyThatTokenValid() throws TokenNotFoundException, TokenAlreadyExistsException {
        User u=new User();
        u.setId(1L);
        u.setLogin("111");
        String token = UUID.randomUUID().toString();
        verificationTokenService.createVerificationToken(u, token);
        VerificationToken newVerificationToken=verificationTokenService.getVerificationToken(token);
        assertThat(newVerificationToken.getToken()).isEqualTo(token);

        DateTime time=new DateTime();
        assertThat(newVerificationToken.getExpiryDate()).hasYear(time.toDateTime().getYear());
        assertThat(newVerificationToken.getExpiryDate()).hasMonth(time.toDateTime().getMonthOfYear());
        assertThat(newVerificationToken.getExpiryDate()).hasDayOfMonth(time.toDateTime().getDayOfMonth()+1);
        assertThat(newVerificationToken.getExpiryDate()).hasHourOfDay(time.toDateTime().getHourOfDay());
        assertThat(newVerificationToken.getExpiryDate()).hasMinute(time.toDateTime().getMinuteOfHour());
//        assertThat(newVerificationToken.getExpiryDate()).hasSecond(time.toDateTime().getSecondOfMinute());

        assertThat(newVerificationToken.getUser().getId()).isEqualTo(1L);
        assertThat(newVerificationToken.isVerified()).isEqualTo(false);

    }

    @Test(expected = TokenAlreadyExistsException.class)
    public void verifyThatThrowExceptionAfterTryingToСreateTokenThatAlreadyExists() throws TokenNotFoundException, TokenAlreadyExistsException {
        User u=new User();
        u.setId(1L);
        u.setLogin("111");
        String token = "b7b7cceb-caac-4288-9c91-1284a889cdc9";
        verificationTokenService.createVerificationToken(u, token);
 }


//    VerificationToken generateNewVerificationToken(String existingToken);

    @Test
    public void verifyThatMethodGeneratesNewTokenForUser() throws TokenNotFoundException {
        User u=new User();
        u.setId(1L);
        u.setLogin("111");
        String token = "b7b7cceb-caac-4288-9c91-1284a889cdc9";
        VerificationToken newVerificationToken=verificationTokenService.generateNewVerificationToken(token);
        assertThat(newVerificationToken.getToken()).isNotEqualTo(token);
        assertThat(newVerificationToken.getUser().getId()).isEqualTo(u.getId());

    }

    @Test(expected = TokenNotFoundException.class)
    public void verifyThatMethodThrowsExceptionIfOldTokenNotFound() throws TokenNotFoundException {
        User u=new User();
        u.setId(1L);
        u.setLogin("111");
        String token = "b7b7cceb-caac-4288-9c91-1284a889cd19";
        verificationTokenService.generateNewVerificationToken(token);
    }


    @Test(expected = IllegalArgumentException.class)
    public void verifyThatMethodThrowsExceptionWhenNullTokenPassed() throws TokenNotFoundException, TokenAlreadyExistsException {
        User u=new User();
        u.setId(1L);
        u.setLogin("111");
        verificationTokenService.generateNewVerificationToken(null);

    }





}
