package ua.kiev.sa;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ua.kiev.sa.it.AccountServiceIT1;
import ua.kiev.sa.it.VerificationTokenIT;

@RunWith(Suite.class)
@Suite.SuiteClasses( {
        AccountServiceIT1.class,
        VerificationTokenIT.class
})
public class TestSuit {

}
